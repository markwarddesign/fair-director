sub EventMain {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my $bgcolor = "#FFFFFF";
&DoFairHeader(7);
print << "END";
<table width='1200'align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td><div align='left'><a href='./fair.cgi?a=main' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('GOBACK',\'','../images/goback_down.gif',1)\"><img src='../images/goback_up.gif' alt='Click HERE to go back to the main menu' name='GOBACK' width='250' height='50' border='0'></a></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<table width='85%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='5' color='#ed5000'><b>Currently-Scheduled Events</b></div></font></td></tr>
<tr><td>&nbsp;</td></tr>
END
	my $ret = undef;
	my $today = &GetMunge();
  	my $sth = $dbh->prepare("SELECT * FROM FairMaster WHERE StartDate >= '$today' ORDER BY StartDate DESC"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING EventMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ret++;
	}
	$sth->finish();
	if ($ret) {
print << "END";
<tr><td>
	<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#ed5000'>
		<tr bgcolor='#FFFFFF'><td colspan='5'><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'><a href='./events.cgi?a=add'>Add Event</a></font></div></td></tr>
		<tr>
			<td width='25%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>Event</b></font></td>
			<td width='14%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>Type</b></font></td>
			<td width='20%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>Start Date</b></font></td>
			<td width='20%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>End Date</b></font></td>
			<td width='16%'></td>
		</tr>
END
	my $today = &GetMunge;
	my ($id,$nm,$start,$end,$n,$et,$type) = undef;
	my $sth = $dbh->prepare("SELECT * FROM FairMaster ORDER BY FairName"); 
	$sth->execute() or die &ErrorMsg("ERROR IN FairMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'FairID'};
		$nm = $ref->{'FairName'};
		$et = $ref->{'TypeID'};
		$type = &GetFairTypes($et,$dbh);
		$start = &GetSundayFormattedDate($ref->{'StartDate'});
		$end = &GetSundayFormattedDate($ref->{'EndDate'});
		if ($n) {
			print "<tr bgcolor='#ffffff'>";
			$n++;
		} else {
			print "<tr bgcolor='#fff2a7'>";
			$n = undef;
		}
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$nm</b></font></td>";
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$type</b></font></td>";
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$start</b></font></td>";
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$end</b></font></td>";
		print "<td><a href='./events.cgi?a=edit&id=$id'><img src='../images/edit.gif' width='25' height='25' border='N'></a>&nbsp;
				<a href='./events.cgi?a=del&id=$id'><img src='../images/delete.gif' width='25' height='25' border='N'></a>&nbsp;
				<a href='./events.cgi?a=info&id=$id'><img src='../images/info.gif' width='25' height='25' border='N'></a></td>";
		print "</tr>";
	}
	$sth->finish();
print << "END";
	</table>
</td></tr>
<tr><td>&nbsp;</td></tr>
END
	} else {
print << "END";
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='3' color='#CC00000'><b><i>(No Events Found)</i></b>
	<font color='#000000'><a href='./events.cgi?a=add'><b>Add Event</b></a></font></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
END
	}
print << "END";
</table>
</td></tr></table>
END
&DoFairFooter();
if ($dmo) {
	$dbh->disconnect();
}
}

sub AddEvent {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my ($name,$type,$start,$end,$dir,$url,$info,$phone,$notes) = undef;
if ($id) {
	($name,$type,$start,$end,$dir,$url,$info,$phone,$notes) = &GetEvent($id,$dbh);
}
my $bgcolor = "#FFFFFF";
&DoFairHeader(7);
print << "END";
<table width='1200'align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td><div align='left'><a href='./fair.cgi?a=main' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('GOBACK',\'','../images/goback_down.gif',1)\"><img src='../images/goback_up.gif' alt='Click HERE to go back to the main menu' name='GOBACK' width='250' height='50' border='0'></a></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<form name='AE' method='post' action='./events.cgi'>
<input type='hidden' name='a' value='se'>
<input type='hidden' name='id' value='$id'>
<table width='70%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='5' color='#ed5000'><b>Create An Event</b></div></font></td></tr>
<tr><td>
	<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
		<tr><td colspan='3'>&nbsp;</td></tr>
		<tr>
			<td width='28%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Event Name: </b></font></div></td>
			<td width='2%'>&nbsp;</td>
			<td width='70%'><div align='left'><input type='text' name='nm' id='nm' size='55' maxlength='255' value='$name'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Event Type: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><select name='et' id='et' size='1'><option value=''></option>
END
		my $sth = $dbh->prepare("SELECT * FROM FairTypes ORDER BY FairTypeDesc"); 
		$sth->execute() or die &ErrorMsg("ERROR IN FairType: $DBI::errstr");
		while (my $ref = $sth->fetchrow_hashref()) {
			my $ftid = $ref->{'FTID'};
			my $ft = $ref->{'FairTypeDesc'};
			if ($ftid eq $type) {
				print "<option value='$ftid' selected>$ft</option>";
			} else {
				print "<option value='$ftid'>$ft</option>";
			}
		}
		$sth->finish();
print << "END";			
			</select></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Start Date: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='StartDate' size='12' maxlength='12' value='$start'>&nbsp;
				<a href="#" border="0" id="atagclicked" onclick="calobj=this.parentNode.previousSibling;document.getElementById('calendarframe').style.display='inline';clicked='true';">
					<img id="btnimg" border="0" src="../images/calendaropen.gif" style="position:relative">
				</a>
				<iframe align="center" src="../ae_sd_calendar.html" id="calendarframe" align="left" marginheight="0" marginwidth="0" scrolling="no" style="position:absolute;z-index:100;display:none;top:400px;left:800px;width:160px;height:200px" frameborder="none"></iframe>
			</div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>End Date: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='EndDate' size='12' maxlength='12' value='$end'>&nbsp;
				<a href="#" border="0" id="atagclicked" onclick="calobj=this.parentNode.previousSibling;document.getElementById('calendarframe2').style.display='inline';clicked='true';">
					<img id="btnimg" border="0" src="../images/calendaropen.gif" style="position:relative">
				</a>
				<iframe align="center" src="../ae_ed_calendar.html" id="calendarframe2" align="left" marginheight="0" marginwidth="0" scrolling="no" style="position:absolute;z-index:100;display:none;top:400px;left:800px;width:160px;height:200px" frameborder="none"></iframe>
			</div></td></tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Event Director: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='dir' id='dir' size='55' maxlength='255' value='$dir'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Event URL: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='url' id='url' size='55' maxlength='255' value='$url'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Event Info Email: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='info' id='info' size='55' maxlength='255' value='$info'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Event Phone: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='phone' id='phone' size='55' maxlength='255' value='$phone'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Event Notes: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><textarea name='notes' cols='50' rows='10'>$notes</textarea></div></td>
		</tr>
		<tr><td colspan='3'>&nbsp;</td></tr>
		<tr><td colspan='3'><div align='center'><input type='submit' name='submit' value='Save Event Information'></div></td></tr>
		<tr><td colspan='3'>&nbsp;</td></tr>
	</table>
</td></tr>
</table></form>
</td></tr></table>
END
&DoFairFooter();
if ($dmo) {
	$dbh->disconnect();
}
}

sub SaveEvent {
my ($id,$name,$type,$start,$end,$dir,$url,$info,$phone,$notes,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my $Name = &WriteQuotes($name);
my $Dir = &WriteQuotes($dir);
my $Notes = &WriteQuotes($notes);
if (!$id) {				# if no ID is passed, 
# ==================================================================
# See if it already exists.
# ==================================================================
	my $sth = $dbh->prepare("SELECT * FROM FairMaster WHERE FairName='$Name' AND TypeID='$type' AND StartDate='$start'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN FairMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'FairID'};
	}
	$sth->finish();
	if (!$id) {				# Create a new one.
		$sth = $dbh->prepare("INSERT INTO FairMaster (FairMaster.FairName,
  									FairMaster.TypeID,
  									FairMaster.StartDate,
  									FairMaster.EndDate,
  									FairMaster.Director,
  									FairMaster.URL,
  									FairMaster.InfoEmail,
  									FairMaster.Phone,
  									FairMaster.Notes) 
  					VALUES ('$Name',
  							'$type',
  							'$start',
  							'$end',
  							'$Dir',
							'$url',
							'$info',
							'$phone',
							'$Notes')"); 
	    $sth->execute() or die &ErrorMsg("ERROR SAVING FairMaster: $DBI::errstr");
	    $sth->finish();
	    $sth = $dbh->prepare("SELECT * FROM FairMaster WHERE FairName='$Name' AND TypeID='$type' AND StartDate='$start'"); 
	    $sth->execute() or die &ErrorMsg("ERROR IN FairMaster: $DBI::errstr");
	    while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'FairID'};
	    }
	    $sth->finish();	
	} else {
		$sth = $dbh->prepare("Update FairMaster SET FairMaster.FairName='$Name',
													FairMaster.TypeID='$type',
													FairMaster.StartDate='$start',
													FairMaster.EndDate='$end',
													FairMaster.Director='$Dir',
													FairMaster.URL='$url',
													FairMaster.InfoEmail='$info',
													FairMaster.Phone='$phone',
													FairMaster.Notes='$Notes'
												WHERE FairID='$id'"); 
		$sth->execute() or die &ErrorMsg("ERROR IN FairMaster: $DBI::errstr");
		$sth->finish();
	}
} else {
	$sth = $dbh->prepare("UPDATE FairMaster SET FairMaster.FairName='$Name',
												FairMaster.TypeID='$type',
												FairMaster.StartDate='$start',
												FairMaster.EndDate='$end',
												FairMaster.Director='$Dir',
												FairMaster.URL='$url',
												FairMaster.InfoEmail='$info',
												FairMaster.Phone='$phone',
												FairMaster.Notes='$Notes'
											WHERE FairID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN FairMaster: $DBI::errstr");
	$sth->finish();	
}
if ($dmo) {
	$dbh->disconnect();
}
return $id;
}

sub GetFairTypes {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $ftdesc = undef;
	my $sth = $dbh->prepare("SELECT * FROM FairTypes WHERE FTID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN FairTypes: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ftdesc = $ref->{'FairTypeDesc'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $ftdesc;
}

sub DeleteFair {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $desc = undef;
	my $sth = $dbh->prepare("DELETE FROM FairMaster WHERE FairID='$id'"); 
	$sth->execute() or die &ErrorFairMsg("ERROR IN FairMaster: $DBI::errstr");
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
}

sub GetEvent {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($name,$type,$start,$end,$dir,$url,$info,$phone,$notes) = undef;
	my $sth = $dbh->prepare("SELECT * FROM FairMaster WHERE FairID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN FairMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$name = $ref->{'FairName'};
		$type = $ref->{'TypeID'};
		$start = $ref->{'StartDate'};
		$end = $ref->{'EndDate'};
		$dir = $ref->{'Director'};
		$url = $ref->{'URL'};
		$info = $ref->{'InfoEmail'};
		$phone = $ref->{'Phone'};
		$notes = $ref->{'Notes'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $name,$type,$start,$end,$dir,$url,$info,$phone,$notes;
}

sub FairSummary {
my ($id,$dbh) = @_;

my $bgcolor = "#FFFFFF";
&DoFairHeader(7);
my $name = &GetEventName($id,$dbh);
print << "END";
<table width='1200'align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td>
	<table width='90%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
		<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='5' color='#ee4e00'><b>Event Summary/Statistics</b></font></div></td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>
			<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
				<tr>
					<td width='18%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Event: </font></div></td>
					<td width='2%'>&nbsp;</td>
					<td width='28%'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$name</b></font></div></td>
					<td width='2%'>&nbsp;</td>
					<td width='22%'>&nbsp;</td>
					<td width='3%'>&nbsp;</td>
					<td width='25%'>&nbsp;</td>
				</tr>
				<tr><td colspan='7'>&nbsp;</td></tr>
				<tr>
					<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Average Ticket: </font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$4.72</b> <font color='#6d6d6d'>\$4.35</font></font></div></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>+\$ .36 (8.01%)</b></font></div></td>
				</tr>
				<tr>
					<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Total Attendance: </font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>98,486</b> <font color='#6d6d6d'></b>89,510</b></font></font></div></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>+8,976 (10.3%)</b></font></div></td>
				</tr>
				<tr>
					<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Total Ticket Revenue: </font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$464,853.92</b> <font color='#6d6d6d'></b>\$389,368.50</b></font></font></div></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>+\$75,485.42 (19.2%)</b></font></div></td>
				</tr>
				<tr><td colspan='7'><hr></td></tr>
				<tr>
					<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Total Presales: </b></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>39,426</b> <font color='#6d6d6d'>35,008</font></font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$186,090.72</b>&nbsp;<font color='#6d6d6d'>\$152,284.80</font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>+\$33,805.92 (22.21%)</b></font></div></td>
				</tr>
				<tr><td colspan='7'><hr></td></tr>
				<tr>
					<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Adult Presales: </font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>20,896</b> (53% of total presales) <font color='#6d6d6d'>18,099</font></font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$125,326</b>&nbsp;<font color='#6d6d6d'>\$108,594</font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>+\$16,732.00 (15.41%)</b></font></div></td>
				</tr>
				<tr>
					<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Child Presales: </font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>20,896</b> (53% of total) <font color='#6d6d6d'>18,099</font></font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$125,326</b>&nbsp;<font color='#6d6d6d'>\$108,594</font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>+\$16,732.00 (15.41%)</b></font></div></td>
				</tr>
				<tr>
					<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Student Presales: </font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>20,896</b> (53% of total) <font color='#6d6d6d'>18,099</font></font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$125,326</b>&nbsp;<font color='#6d6d6d'>\$108,594</font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>+\$16,732.00 (15.41%)</b></font></div></td>
				</tr>
				<tr>
					<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Senior Presales: </font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>20,896</b> (53% of total) <font color='#6d6d6d'>18,099</font></font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$125,326</b>&nbsp;<font color='#6d6d6d'>\$108,594</font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>+\$16,732.00 (15.41%)</b></font></div></td>
				</tr>
				<tr>
					<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Military Presales: </font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>20,896</b> (53% of total) <font color='#6d6d6d'>18,099</font></font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$125,326</b>&nbsp;<font color='#6d6d6d'>\$108,594</font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>+\$16,732.00 (15.41%)</b></font></div></td>
				</tr>
				<tr><td colspan='7'><hr></td></tr>
				<tr>
					<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Total Door Sales: </b></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>59,060</b> <font color='#6d6d6d'>57,942</font></font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$278,763.20</b>&nbsp;<font color='#6d6d6d'>\$252,047.70</font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>+\$26,715.50 (10.61%)</b></font></div></td>
				</tr>
				<tr><td colspan='7'><hr></td></tr>
				<tr>
					<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Adults: </font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>20,896</b> (53% of total presales) <font color='#6d6d6d'>18,099</font></font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$125,326</b>&nbsp;<font color='#6d6d6d'>\$108,594</font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>+\$16,732.00 (15.41%)</b></font></div></td>
				</tr>
				<tr>
					<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Children: </font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>20,896</b> (53% of total) <font color='#6d6d6d'>18,099</font></font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$125,326</b>&nbsp;<font color='#6d6d6d'>\$108,594</font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>+\$16,732.00 (15.41%)</b></font></div></td>
				</tr>
				<tr>
					<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Students: </font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>20,896</b> (53% of total) <font color='#6d6d6d'>18,099</font></font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$125,326</b>&nbsp;<font color='#6d6d6d'>\$108,594</font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>+\$16,732.00 (15.41%)</b></font></div></td>
				</tr>
				<tr>
					<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Seniors: </font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>20,896</b> (53% of total) <font color='#6d6d6d'>18,099</font></font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$125,326</b>&nbsp;<font color='#6d6d6d'>\$108,594</font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>+\$16,732.00 (15.41%)</b></font></div></td>
				</tr>
				<tr>
					<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Military: </font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>20,896</b> (53% of total) <font color='#6d6d6d'>18,099</font></font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$125,326</b>&nbsp;<font color='#6d6d6d'>\$108,594</font></font></div></td>
					<td>&nbsp;</td>
					<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>+\$16,732.00 (15.41%)</b></font></div></td>
				</tr>

				<tr><td colspan='3'>&nbsp;</td></tr>
			</table>
		</td></tr>
	</table>
</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
</table>
END
&DoFairFooter();
}

1;