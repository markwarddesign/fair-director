#!/usr/bin/perl

use strict;
use warnings;
use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use cPanelUserConfig;
use Getopt::Std;
use Imager::QRCode;

my %opts = (
    o => '../images/test/newtest.png',         # Output filename
    e => 'M',                   # Error correction level
    s => '5',                   # Pixel size
);
getopt('o:e:s:', \%opts);
my $qr = Imager::QRCode->new(
    size =>  $opts{s},
    level => $opts{e},
);
$qr->plot("http://www.fair-director.com")->write( file => $opts{o} );
print "Content-type:text/html\n\n";
print "<p><h2><div align='center'><img src='../images/test/newtest.png'></div></h2></p>";