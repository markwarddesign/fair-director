#!/usr/bin/perl

require "dateutil.pl";
require "dbutil.pl";
require "fairlayout.pl";
require "errormaster.pl";
require "FairReports.pl";
require "Fair.pl";
require "Staff.pl";

use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use DBI;

my ($Action,$id) = undef;
if (param()) {
	$Action = param('a');
	$t = param('t');
	$id = param('id');
	$sid = param('sid');
	$last = param('last');
	$first = param('first');
	$mi = param('mi');
	$addr = param('addr');
	$addr2 = param('addr2');
	$city = param('city');
	$state = param('state');
	$zip = param('zip');
	$home = param('home');
	$work = param('work');
	$cell = param('cell');
	$email = param('email');
	$ssn = param('ss');
	$cit = param('cit');
	$type = param('type');
	$ctype = param('ctype');
	$etype = param('etype');
	$rate = param('rate');
	$notes = param('notes');
	$jid = param('jobid');
}

my $dbh = &ConnectDB();
if ($Action eq "main") {
	&NewStaffMain(undef,$dbh);
} elsif ($Action eq "test") {
	&NewStaffMain(undef,$dbh);
} elsif ($Action eq "save_staff") {
	&Save_Staff(undef,$last,$first,$mi,$addr,$addr2,$city,$state,$zip,$home,$work,$cell,$email,$jid,
	$ssn,$cit,$type,$ctype,$etype,$rate,$notes,$dbh);
	&StaffMain(undef,$dbh);
} elsif ($Action eq "edits") {
	&StaffMain($sid,$dbh);
} elsif ($Action eq "dels") {
	&DeleteStaff($sid,$dbh);
	&StaffMain(undef,$dbh);
} else {
	&ErrorFairMsg("I don't know what you want me to do: [ $Action ]");
}
$dbh->disconnect();

exit;