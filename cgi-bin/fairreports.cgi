#!/usr/bin/perl

require "dateutil.pl";
require "dbutil.pl";
require "pagelayout.pl";
require "errormaster.pl";
require "Email.pl";
require "Main.pl";
require "Reports.pl";
require "Attend.pl";
require "Fair.pl";

use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use DBI;

my ($Action,$id) = undef;
if (param()) {
	$Action = param('a');
	$t = param('t');
	$id = param('id');
	$attend = param('attend');
	$exp = param('exp');
	$factor = param('factor');
	$margin = param('margin');
}

my $dbh = &ConnectDB();
if ($Action eq "main") {
	&FairMain($dbh);
} elsif ($Action eq "topdown") {
	&TopDown(undef,$dbh);
} elsif ($Action eq "dotopdown") {
	if (!$exp) {
		&ErrorMsg("Must have expenses to calculate");
		exit;
	}
	my $newfactor = ($factor/100)+1;
	my $newattend = $attend*$newfactor;
	my $newexp = $exp*$newfactor;
	my $newmargin = ($margin/100)+1;
	my $ticket = ($newexp/$newattend)*$newmargin;
#	&TopDownResults(undef,$attend,$exp,$newattend,$newexp,$factor,$margin,$ticket,$dbh);
	&SuccessFairMsg("Done.  [ Projected Attendance: $newattend | Projected Expenses: $newexp | Growth Factor: $factor% | Desired Margin: $margin% | Ticket Price: $ticket ]");
}  else {
	&ErrorFairMsg("I don't know what you want me to do: [ $Action ]");
}
$dbh->disconnect();

exit;