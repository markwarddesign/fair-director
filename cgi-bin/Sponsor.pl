sub SponsorMain {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$ebh=&ConnectDB;
	$dmo++;
}
$id = "17";
my ($enternum,$newtotal) = &GetSponsorNumbers($dbh);
if (!$newtotal) {
  $newtotal = "0";
}
&DoNewHeader(undef);
print << "END";
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="../demo/index.html">FairDirector® </a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-cog"></i> Account <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Settings</a></li>
              <li><a href="javascript:;">Help</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> fair-director.com <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Profile</a></li>
              <li><a href="javascript:;">Logout</a></li>
            </ul>
          </li>
        </ul>
        <form class="navbar-search pull-right">
          <input type="text" class="search-query" placeholder="Search">
        </form>
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <li><a href="./fair.cgi?a=main"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        <li><a href="./reports.cgi?a=main"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>
        <li><a href="./ticketing.cgi?a=main"><i class="icon-tags"></i><span>Ticketing</span> </a></li>
        <li><a href="./entertain.cgi?a=test"><i class="icon-star"></i><span>Entertainment</span> </a> </li>
        <li><a href="./vendors.cgi?a=main"><i class="icon-shopping-cart"></i><span>Vendors</span> </a> </li>
        <li><a href="./sponsors.cgi?a=main"><i class="icon-money"></i><span>Sponsors</span> </a> </li>
        <li><a href="./staff.cgi?a=main"><i class="icon-group"></i><span>Personnel</span> </a> </li>
       <!-- <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Drops</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="../demo/icons.html">Icons</a></li>
            <li><a href="../demo/faq.html">FAQ</a></li>
            <li><a href="../demo/pricing.html">Pricing Plans</a></li>
            <li><a href="../demo/login.html">Login</a></li>
            <li><a href="../demo/signup.html">Signup</a></li>
            <li><a href="../demo/error.html">404</a></li>
          </ul>
        </li> -->
      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->
<div class="main">
	<div class="main-inner">
	    <div class="container">
        <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3> Today's Sponsor Stats</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-user"></i> <span class="value">$enternum</span> <br />Sponsors</div>
                    <!-- .stat -->
                    
                    <div class="stat"> <i class="icon-signal"></i> <span class="value">\$$newtotal</span> <br>
Sponsor Contributions</div>
                    <!-- .stat -->
                  </div>
                </div>
                <!-- /widget-content --> 
               </div>
            </div>
          </div>
          <!-- /widget -->
     	 </div>
        <!-- /span6 -->
        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-bookmark"></i>
              <h3>Sponsor Actions</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts"> 
                  <a href="javascript:;" class="shortcut">
                        <i class="shortcut-icon icon-list-alt"></i>
                      <span class="shortcut-label">Reports</span> 
                  </a>
                  <a href="javascript:;" class="shortcut">
                        <i class="shortcut-icon icon-group"></i>
                      <span class="shortcut-label">View Sponsor</span> 
                  </a>
                  <a  data-toggle="modal" href="#responsive" class="shortcut popup">
                        <i class="shortcut-icon icon-plus"></i> 
                      <span class="shortcut-label">Add Sponsor</span> 
                  </a>
                  <a href="javascript:;" class="shortcut"> 
                        <i class="shortcut-icon icon-envelope-alt"></i>
                      <span class="shortcut-label">Send Message</span> 
                  </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->
</div>

</div>
	<div class="row">
	      	<div class="span12">
	      		<div class="widget">
                <div class="widget-header">
						<i class="icon-group"></i>
						<h3>Sponsor List</h3>
					</div> <!-- /widget-header -->
					
					<div class="widget-content">
                    <div style="float:left;"><a href="./sponsor.cgi?a=view">
            		<i class="icon-external-link"></i> 
                    	View Sponsor Application
              </a>
              </div>
    
           	<div style="float:right">
            		<i class="icon-plus"></i> 
                  <a  data-toggle="modal" href="#responsive" class="shortcut popup">Create Sponsor Record</a>
               </div>
               <div style="clear:both;"></div>
	      		<table style="width:100%;" cellpadding="0" cellspacing="0"  align="center">
<tbody><tr><td><div align="center"></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
	<table class="table">
		<tbody>
		<tr>
			<td width="20%"><b>Name</b></td>
			<td width="20%"><b>Level</b></td>			
			<td width="20%"><b>Phone</b></td>
			<td width="20%"><b>Cell</b></td>
			<td width="5%"><b>Email</b></td>
			<td width="15%">&nbsp;</td>
		</tr>
END
	my ($eid,$group,$contact,$type) = undef;
	my $sth = $dbh->prepare("SELECT * FROM SponsorMaster ORDER BY Org"); 
	$sth->execute() or die &ErrorMsg("ERROR IN SPONSORMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$sid = $ref->{'SponsorID'};
		$org = $ref->{'Org'};
		$type = &GetSponsorType($ref->{'TypeID'},$dbh);
		$last = $ref->{'ContactLast'};
		$first = $ref->{'ContactFirst'};
		$mi = $ref->{'ContactMI'};
		$addr = $ref->{'Addr'};
		$addr2 = $ref->{'Addr2'};
		$city = $ref->{'City'};
		$state = $ref->{'State'};
		$zip = $ref->{'Zip'};
		$phone = $ref->{'Phone'};
		$cell = $ref->{'Cell'};
		$email = $ref->{'Email'};
		$status = $ref->{'Status'};
		if ($mi) {
			$name = "$first $mi $last";
		} else {
			$name = "$first $last";
		}
		if ($n) { 
			print "<tr bgcolor='#f9f6f1'>";
			$n = undef;
		} else {
			print "<tr bgcolor='#ffffff'>";
			$n++;			
		}
		print "<td>$org</td>";
		print "<td>$type</td>";
		print "<td>$name</td>";
		print "<td>$phone</td>";
		print "<td>$cell</td>";
		print "<td><a href='mailto:$email'><img src='../images/emailicon.gif' width='30' height='25' border='no'><a/></td>";
		print "<td><a href='./sponsor.cgi?a=edit&id=$sid'><i class='icon-pencil'></i></a>&nbsp;&nbsp;<a href='./sponsor.cgi?a=del&id=$sid'><i class='icon-remove'></i></a></td>";
		print "</tr>";
	}
	$sth->finish();
print << "END";
</tbody>
</table>
</td></tr>
<tr><td>&nbsp;</td></tr>
</tbody></table>
</div>
</div>
  </div> <!-- /row -->
    </div> <!-- /container -->
	</div> <!-- /main-inner -->
	</div> <!-- /main -->
<div class="extra">
  <div class="extra-inner">
    <div class="container">
      <div class="row">
                    <div class="span3">
                        <h4>Footer Area 1</h4>
                        <ul>
                          Important Links HEre
                          <li><a href="javascript:;"></a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            Support</h4>
                        <ul>
                            <li><a href="javascript:;">Frequently Asked Questions</a></li>
                            <li><a href="javascript:;">Ask a Question</a></li>
                            <li><a href="javascript:;">Video Tutorial</a></li>
                            <li><a href="javascript:;">Feedback</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            Something Legal</h4>
                        <ul>
                            <li><a href="javascript:;">Read License</a></li>
                            <li><a href="javascript:;">Terms of Use</a></li>
                            <li><a href="javascript:;">Privacy Policy</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            Some More Info</h4>
                        <ul><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id elit non mi porta gravida at eget metus.</li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /extra-inner --> 
</div>
<!-- /extra -->
<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> &copy; 2015 <a href="http://www.flourish-solutions.com/">Flourish Solutions</a>. </div>
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 
<div id="responsive" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Add Sponsor</h4>
  </div>
  <div class="modal-body">
   <div class="the-alert"></div>
	<div class="row-fluid">
		<div class="span6">
			 <form name="AS" method="post" action="./saveSponsor.cgi" data-fv-addons="reCaptcha2">
			 <div class="form-group">
				 <label>Org/Company: </label>
				 <input type="text" name="org" id="org" size="25" maxlength="255" value="">
			 </div>
			 <div class="form-group">
				 <label>Level:</label>
				 <select name='level' id='level' size='1'><option value=''></option>
END
		&GetSponsorTypes_sel($type,$dbh);
print << "END";
			 </select></div>
			 <div class="form-group">
				 <label>Contact Last Name: </label>
				 <input type="text" name="last" id="last" size="25" maxlength="55" value="">&nbsp;
				 <input type="text" name="mi" id="mi" size="5" maxlength="5" value="">
			 </div>
			 <div class="form-group">
				 <label>Contact First Name/MI: </label>
				 <input type="text" name="first" id="first" size="25" maxlength="55" value="">&nbsp;
				 <input type="text" name="mi" id="mi" size="5" maxlength="5" value="">
			 </div>				
			 <div class="form-group">
				 <label>Address:</label>
				 <input type="text" name="addr" id="addr" size='25' maxlength='255' value="">
			 </div>
			 <div class="form-group">
				 <label></label>
				 <input type="text" name="addr2" id="addr2" size="25" maxlength="255" value="">
			 </div>
			 <div class="form-group">
				 <label>City/State/Zip: </label>
				 <input type="text" name="city" id="city" size="15" maxlength="55" value="">,&nbsp;
				 <select name='state' id='state' size='1'><option value=''></option>
END
	&GetStates_sel($state);
print << "END";
				</select>&nbsp;<input type="text" name="zip" id="zip" size="10" maxlength="10" value=""></div>
			 <div class="form-group">
				 <label>Phone: </label>
				 <input type="text" name="phone" id="phone" size="15" maxlength="15" value="">
			 </div>		
			 <div class="form-group">
				 <label>Cell: </label>
				 <input type="text" name="cell" id="cell" size="15" maxlength="15" value="">
			 </div>
			 <div class="form-group">
				 <label>Email: </label>
				 <input type="text" name="email" id="email" size="25" maxlength="55" value="">
			 </div>	
			 <div class="form-group">
				 <label>Status:</label>
				 <input type='radio' name='status' id='status' value='A' checked> Active<br>
				 <input type='radio' name='status' id='status' value='I'> Inactive<br>
				<input type='radio' name='status' id='status' value='E'> Expressed Interest
			 </div>
		<div class="span6">
				<div class="form-group">
					<label>Notes:</label>
					<textarea name="notes" cols="50" rows="6"></textarea>
				</div>
				<div class="form-group">
					<label>Captcha</label>
					<!-- The captcha container -->
					<div id="captchaContainer"></div>
				</div>
                </div>
	</div>
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
    <button type="submit" class="btn btn-primary">Save changes</button>
  </div>
  </form>
</div>

<!-- Required -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/base.js"></script>

<!-- Charts -->
<script src="../js/excanvas.min.js"></script>
<script src="../js/chart.min.js" type="text/javascript"></script>

<!-- FormValidation plugin and the class supports validating Bootstrap form -->
<script src="../js/formValidation/dist/js/formValidation.js"></script>
<script src="../js/formValidation/dist/js/framework/bootstrap.js"></script>
<script src="../js/formValidation/dist/addons/reCaptcha2.js"></script>

<!-- Modal -->
<script src="../js/bootstrap-modal.js"></script>
<script src="../js/bootstrap-modalmanager.js"></script>

<script id="ajax" type="text/javascript" src="../js/modals.js" ></script> 
END
&DoNewFooter();
if ($dmo) {
	$dbh->disconnect();
}
}

sub SaveSponsor {
my ($id,$type,$org,$contact,$addr,$addr2,$city,$state,$zip,$email,
		$phone,$cell,$goods,$tid,$blid,$status,$notes,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my $Org = &WriteQuotes($org);
my $Contact = &WriteQuotes($contact);
my $Addr = &WriteQuotes($addr);
my $Addr2 = &WriteQuotes($addr2);
my $City = &WriteQuotes($city);
my $Goods = &WriteQuotes($goods);
my $Notes = &WriteQuotes($notes);
if (!$id) {				# if no ID is passed, 
# ==================================================================
# See if it already exists.
# ==================================================================
	my $sth = $dbh->prepare("SELECT * FROM SponsorMaster WHERE Org='$Org' AND TypeID='$type'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN SponsorMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'SponsorID'};
	}
	$sth->finish();
	if (!$id) {				# Create a new one.
		$sth = $dbh->prepare("INSERT INTO SponsorMaster 
									(SponsorMaster.Org,
  									SponsorMaster.TypeID,
  									SponsorMaster.Contact,
  									SponsorMaster.Addr,
  									SponsorMaster.Addr2,
  									SponsorMaster.City,
  									SponsorMaster.State,
  									SponsorMaster.Zip,
  									SponsorMaster.Email,
  									SponsorMaster.Phone,
  									SponsorMaster.Cell,
  									SponsorMaster.Status,
									SponsorMaster.Notes) 
  					VALUES ('$Org',
							'$type',
  							'$Contact',
  							'$Addr',
  							'$Addr2',
  							'$City',
							'$state',
							'$zip',
							'$email',
							'$phone',
							'$cell',
							'$status',
							'$Notes')"); 
	    $sth->execute() or die &ErrorMsg("ERROR SAVING SponsorMaster: $DBI::errstr");
	    $sth->finish();
	    $sth = $dbh->prepare("SELECT * FROM SponsorMaster WHERE Org='$Org' AND TypeID='$type'"); 
	    $sth->execute() or die &ErrorMsg("ERROR IN SponsorMaster: $DBI::errstr");
	    while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'SponsorID'};
	    }
	    $sth->finish();	
	} else {
		$sth = $dbh->prepare("Update SponsorMaster SET SponsorMaster.Org='$Org',
													SponsorMaster.TypeID='$type',
													SponsorMaster.Contact='$Contact',
													SponsorMaster.Addr='$Addr',
													SponsorMaster.Addr2='$Addr2',
													SponsorMaster.City='$City',
													SponsorMaster.State='$state',
													SponsorMaster.Zip='$zip',
													SponsorMaster.Email='$email',
													SponsorMaster.Phone='$phone',
													SponsorMaster.Cell='$cell',
													SponsorMaster.Status='$status',
													SponsorMaster.Notes='$Notes'
												WHERE SponsorID='$id'"); 
		$sth->execute() or die &ErrorMsg("ERROR IN SponsorMaster: $DBI::errstr");
		$sth->finish();
	}
} else {
		$sth = $dbh->prepare("Update SponsorMaster SET SponsorMaster.Org='$Org',
													SponsorMaster.TypeID='$type',
													SponsorMaster.Contact='$Contact',
													SponsorMaster.Addr='$Addr',
													SponsorMaster.Addr2='$Addr2',
													SponsorMaster.City='$City',
													SponsorMaster.State='$state',
													SponsorMaster.Zip='$zip',
													SponsorMaster.Email='$email',
													SponsorMaster.Phone='$phone',
													SponsorMaster.Cell='$cell',
													SponsorMaster.Status='$status',
													SponsorMaster.Notes='$Notes'
												WHERE SponsorID='$id'"); 
		$sth->execute() or die &ErrorMsg("ERROR IN SponsorMaster: $DBI::errstr");
		$sth->finish();
}
if ($dmo) {
	$dbh->disconnect();
}
return $id;
}

sub GetSponsorType {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($temp,$amt,$stdesc) = undef;
	my $sth = $dbh->prepare("SELECT * FROM SponsorTypes WHERE STID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN SponsorTypes: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$temp = $ref->{'SponsorTypeDesc'};
		$amt = $ref->{'Amount'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
	$stdesc = "$temp - \$$amt";
return $stdesc;
}

sub GetSponsorTypes_sel {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($temp,$amt,$stid) = undef;
	my $sth = $dbh->prepare("SELECT * FROM SponsorTypes ORDER BY SponsorTypeDesc"); 
	$sth->execute() or die &ErrorMsg("ERROR IN SponsorTypes: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$stid = $ref->{'STID'};
		$temp = $ref->{'SponsorTypeDesc'};
		$amt = $ref->{'Amount'};
		if ($id eq $temp) {
			print "<option value='$stid' selected>$temp - \$$amt</option>";
		} else {
			print "<option value='$stid'>$temp - \$$amt</option>";		  
		}
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
}

sub DeleteSponsor {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $desc = undef;
	my $sth = $dbh->prepare("DELETE FROM SponsorMaster WHERE SponsorID='$id'"); 
	$sth->execute() or die &ErrorFairMsg("ERROR IN SponsorMaster: $DBI::errstr");
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
}

sub GetSponsor {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($org,$type,$contact,$addr,$addr2,$city,$state,$zip,$email,$phone,$cell,$tid,
		$blid,$goods,$status,$notes) = undef;
	my $sth = $dbh->prepare("SELECT * FROM SponsorMaster WHERE SponsorID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN SponsorMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$org = $ref->{'Org'};
		$type = $ref->{'TypeID'};
		$contact = $ref->{'Contact'};
		$addr = $ref->{'Addr'};
		$addr2 = $ref->{'Addr2'};
		$city = $ref->{'City'};
		$state = $ref->{'State'};
		$zip = $ref->{'Zip'};
		$email = $ref->{'Email'};
		$phone = $ref->{'Phone'};
		$cell = $ref->{'Cell'};
		$status = $ref->{'Status'};
		$notes = $ref->{'Notes'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $org,$type,$contact,$addr,$addr2,$city,$state,$zip,$email,$phone,
	$cell,$status,$notes;
}

sub GetSponsorName {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($org,$type,$contact,$addr,$addr2,$city,$state,$zip,$email,$phone,$cell,$tid,
		$blid,$goods,$status,$notes) = undef;
	my $sth = $dbh->prepare("SELECT * FROM SponsorMaster WHERE SponsorID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN SponsorMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$org = $ref->{'Org'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $org;
}

sub GetSponsors_sel {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($stid,$org) = undef;
	my $sth = $dbh->prepare("SELECT * FROM SponsorMaster ORDER BY Org"); 
	$sth->execute() or die &ErrorMsg("ERROR IN SponsorMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$stid = $ref->{'SponsorID'};
		$org = $ref->{'Org'};
		if ($id eq $stid) {
			print "<option value='$stid' selected>$org</option>";
		} else {
			print "<option value='$stid'>$org</option>";
		}
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
}

sub GetSponsorNumbers {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($sid,$stid,$ret,$total) = undef;
	my $sth = $dbh->prepare("SELECT * FROM SponsorMaster ORDER BY Org"); 
	$sth->execute() or die &ErrorMsg("ERROR IN SponsorMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
                $ret++;
		$sid = $ref->{'SponsorID'};
		$stid = $ref->{'TypeID'};
                my $sth2 = $dbh->prepare("SELECT * FROM SponsorTypes WHERE STID='$stid'"); 
                $sth2->execute() or die &ErrorMsg("ERROR IN SponsorMaster: $DBI::errstr");
                while (my $ref2 = $sth2->fetchrow_hashref()) {
                        $total+=$ref2->{'Amount'};
                }
                $sth2->finish();
              
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $ret,$total;
}


1;