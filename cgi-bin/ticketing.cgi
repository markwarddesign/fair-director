#!/usr/bin/perl

require "dateutil.pl";
require "dbutil.pl";
require "fairlayout.pl";
require "errormaster.pl";
require "FairReports.pl";
require "Fair.pl";
require "Ticketing.pl";

use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use DBI;

my ($Action,$id,$t,$name,$start,$end,$type) = undef;
if (param()) {
	$Action = param('a');
	$t = param('t');
	$id = param('id');
	$fid = param('fid');
	$ticket = param('ticket');
	$indiv = param('indiv');
	$group = param('grp');
	$special = param('spec');
	$status = param('status');
}

my $dbh = &ConnectDB();
if ($Action eq "main") {
	&TicketMain(undef,$dbh);
} elsif ($Action eq "add") {
	&AddTickets(undef,$dbh);
} elsif ($Action eq "edit") {
	&AddTickets($id,$dbh);
} elsif ($Action eq "view") {
	&TicketsApp($id,$dbh);
} elsif ($Action eq "st") {
	&SaveTicket($id,$fid,$ticket,$indiv,$group,$special,$status,$dbh);
	&TicketMain(undef,$dbh);
} elsif ($Action eq "del") {
	&DeleteTicket($id,$dbh);
	&TicketMain(undef,$dbh);
} else {
	&ErrorFairMsg("I don't know what you want me to do: [ $Action ]");
}
$dbh->disconnect();

exit;