sub EntertainMain {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my $bgcolor = "#FFFFFF";
&DoFairHeader(9);
print << "END";
<table width='1200'align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td><div align='left'><a href='./fair.cgi?a=main' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('GOBACK',\'','../images/goback_down.gif',1)\"><img src='../images/goback_up.gif' alt='Click HERE to go back to the main menu' name='GOBACK' width='250' height='50' border='0'></a></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<table width='85%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='5' color='#ed5000'><b>Entertainer Master List</b></div></font></td></tr>
<tr><td>&nbsp;</td></tr>
END
	my $ret = undef;
	my $today = &GetMunge();
  	my $sth = $dbh->prepare("SELECT * FROM EntertainMaster"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING EntertainMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ret++;
	}
	$sth->finish();
	if ($ret) {
print << "END";
<tr><td>
	<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#ed5000'>
		<tr bgcolor='#FFFFFF'><td colspan='5'><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'><a href='./entertain.cgi?a=add'>Add Entertainer</a></font></div></td></tr>
		<tr>
			<td width='25%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>Entertainer</b></font></td>
			<td width='14%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>Type</b></font></td>
			<td width='25%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>Contact</b></font></td>
			<td width='25%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>Phone</b></font></td>
			<td width='11%'></td>
		</tr>
END
	my $today = &GetMunge;
	my ($id,$org,$contact,$phone,$cell,$type,$etype) = undef;
	my $sth = $dbh->prepare("SELECT * FROM EntertainMaster ORDER BY GroupName"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EntertainMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'EntertainID'};
		$type = $ref->{'TypeID'};
		$org = $ref->{'GroupName'};
		$contact = $ref->{'Contact'};
		$phone = $ref->{'Phone'};
		$cell = $ref->{'Cell'};
		$etype = &GetEntertainTypes($type,$dbh);
		if ($n) {
			print "<tr bgcolor='#ffffff'>";
			$n++;
		} else {
			print "<tr bgcolor='#fff2a7'>";
			$n = undef;
		}
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$org</b></font></td>";
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$etype</b></font></td>";
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$contact</b></font></td>";
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$phone/$cell</b></font></td>";
		print "<td><a href='./entertain.cgi?a=edit&id=$id'><img src='../images/edit.gif' width='25' height='25' border='N'></a>&nbsp;&nbsp;
				<a href='./entertain.cgi?a=del&id=$id'><img src='../images/delete.gif' width='25' height='25' border='N'></a></td>";
		print "</tr>";
	}
	$sth->finish();
print << "END";
	</table>
</td></tr>
<tr><td>&nbsp;</td></tr>
END
	} else {
print << "END";
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='3' color='#CC00000'><b><i>(No Entertainers Found)</i></b>
	<font color='#000000'><a href='./entertain.cgi?a=add'><b>Add Entertainer</b></a></font></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
END
	}
print << "END";
</table>
</td></tr></table>
END
&DoFairFooter();
if ($dmo) {
	$dbh->disconnect();
}
}

sub AddEntertain {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my ($org,$type,$contact,$addr,$addr2,$city,$state,$zip,$email,$phone,$cell,$tid,$blid,$goods,$status,$notes) = undef;
if ($id) {
	($org,$type,$contact,$addr,$addr2,$city,$state,$zip,$email,$phone,$cell,$tid,$blid,$goods,$status,$notes) = &GetEntertain($id,$dbh);
}
my $bgcolor = "#FFFFFF";
&DoFairHeader(5);
print << "END";
<table width='1200'align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td><div align='left'><a href='./fair.cgi?a=main' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('GOBACK',\'','../images/goback_down.gif',1)\"><img src='../images/goback_up.gif' alt='Click HERE to go back to the main menu' name='GOBACK' width='250' height='50' border='0'></a></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<form name='AE' method='post' action='./entertain.cgi'>
<input type='hidden' name='a' value='se'>
<input type='hidden' name='id' value='$id'>
<table width='70%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='5' color='#ed5000'><b>Create an Entertainer Record</b></div></font></td></tr>
<tr><td>
	<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
		<tr><td colspan='3'>&nbsp;</td></tr>
		<tr>
			<td width='28%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Group Name: </b></font></div></td>
			<td width='2%'>&nbsp;</td>
			<td width='70%'><div align='left'><input type='text' name='GroupName' id='GroupName' size='55' maxlength='255' value='$org'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Entertainment Type: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><select name='et' id='et' size='1'><option value=''></option>
END
		my $sth = $dbh->prepare("SELECT * FROM EntertainTypes ORDER BY EnterType"); 
		$sth->execute() or die &ErrorMsg("ERROR IN EntertainTypes: $DBI::errstr");
		while (my $ref = $sth->fetchrow_hashref()) {
			my $etid = $ref->{'ETID'};
			my $et = $ref->{'EnterType'};
			if ($etid eq $type) {
				print "<option value='$etid' selected>$et</option>";
			} else {
				print "<option value='$etid'>$et</option>";
			}
		}
		$sth->finish();
print << "END";			
			</select></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Contact: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='Contact' id='Contact' size='55' maxlength='255' value='$contact'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Street Address: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='Addr' id='Addr' size='55' maxlength='255' value='$addr'></div></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='Addr2' id='Addr2' size='55' maxlength='255' value='$addr2'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>City/State/Zip: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='City' id='City' size='25' maxlength='255' value='$city'>, 
			<select name='State' size='1'><option value=''></option>
END
		&GetStates_sel($state);
print << "END";
			</select>&nbsp;<input type='text' name='Zip' id='Zip' size='12' maxlength='12' value='$zip'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Entertain Email: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='Email' id='Email' size='55' maxlength='255' value='$email'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Entertain Phone: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='Phone' id='Phone' size='55' maxlength='255' value='$phone'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Cell: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='Cell' id='Cell' size='55' maxlength='255' value='$cell'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Status: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>
				<input type='radio' name='Status' id='Status' value='N'> New Application<br>
				<input type='radio' name='Status' id='Status' value='A' checked> Approved<br>
				<input type='radio' name='Status' id='Status' value='P'> Probationary<br>
				<input type='radio' name='Status' id='Status' value='S'> Suspended<br>
				<input type='radio' name='Status' id='Status' value='I'> Inactive
				</div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Notes: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><textarea name='Notes' cols='50' rows='5'>$notes</textarea></div></td>
		</tr>
		<tr><td colspan='3'>&nbsp;</td></tr>
		<tr><td colspan='3'><div align='center'><input type='submit' name='submit' value='Save Entertain Information'></div></td></tr>
		<tr><td colspan='3'>&nbsp;</td></tr>
	</table>
</td></tr>
</table></form>
</td></tr></table>
END
&DoFairFooter();
if ($dmo) {
	$dbh->disconnect();
}
}

sub SaveEntertain {
my ($id,$type,$org,$contact,$addr,$addr2,$city,$state,$zip,$email,
		$phone,$cell,$status,$notes,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my $GroupName = &WriteQuotes($org);
my $Contact = &WriteQuotes($contact);
my $Addr = &WriteQuotes($addr);
my $Addr2 = &WriteQuotes($addr2);
my $City = &WriteQuotes($city);
my $Goods = &WriteQuotes($goods);
my $Notes = &WriteQuotes($notes);
if (!$id) {				# if no ID is passed, 
# ==================================================================
# See if it already exists.
# ==================================================================
	my $sth = $dbh->prepare("SELECT * FROM EntertainMaster WHERE GroupName='$GroupName' AND TypeID='$type'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EntertainMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'EntertainID'};
	}
	$sth->finish();
	if (!$id) {				# Create a new one.
		$sth = $dbh->prepare("INSERT INTO EntertainMaster 
									(EntertainMaster.GroupName,
  									EntertainMaster.TypeID,
  									EntertainMaster.Contact,
  									EntertainMaster.Addr,
  									EntertainMaster.Addr2,
  									EntertainMaster.City,
  									EntertainMaster.State,
  									EntertainMaster.Zip,
  									EntertainMaster.Email,
  									EntertainMaster.Phone,
  									EntertainMaster.Cell,
									EntertainMaster.Notes) 
  					VALUES ('$GroupName',
							'$type',
  							'$Contact',
  							'$Addr',
  							'$Addr2',
  							'$City',
							'$state',
							'$zip',
							'$email',
							'$phone',
							'$cell',
							'$Notes')"); 
	    $sth->execute() or die &ErrorMsg("ERROR SAVING EntertainMaster: $DBI::errstr");
	    $sth->finish();
	    $sth = $dbh->prepare("SELECT * FROM EntertainMaster WHERE GroupName='$GroupName' AND TypeID='$type'"); 
	    $sth->execute() or die &ErrorMsg("ERROR IN EntertainMaster: $DBI::errstr");
	    while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'EntertainID'};
	    }
	    $sth->finish();	
	} else {
		$sth = $dbh->prepare("Update EntertainMaster SET EntertainMaster.GroupName='$GroupName',
													EntertainMaster.TypeID='$type',
													EntertainMaster.Contact='$Contact',
													EntertainMaster.Addr='$Addr',
													EntertainMaster.Addr2='$Addr2',
													EntertainMaster.City='$City',
													EntertainMaster.State='$state',
													EntertainMaster.Zip='$zip',
													EntertainMaster.Email='$email',
													EntertainMaster.Phone='$phone',
													EntertainMaster.Cell='$cell',
													EntertainMaster.Notes='$Notes'
												WHERE EntertainID='$id'"); 
		$sth->execute() or die &ErrorMsg("ERROR IN EntertainMaster: $DBI::errstr");
		$sth->finish();
	}
} else {
		$sth = $dbh->prepare("Update EntertainMaster SET EntertainMaster.GroupName='$GroupName',
													EntertainMaster.TypeID='$type',
													EntertainMaster.Contact='$Contact',
													EntertainMaster.Addr='$Addr',
													EntertainMaster.Addr2='$Addr2',
													EntertainMaster.City='$City',
													EntertainMaster.State='$state',
													EntertainMaster.Zip='$zip',
													EntertainMaster.Email='$email',
													EntertainMaster.Phone='$phone',
													EntertainMaster.Cell='$cell',
													EntertainMaster.Notes='$Notes'
												WHERE EntertainID='$id'"); 
		$sth->execute() or die &ErrorMsg("ERROR IN EntertainMaster: $DBI::errstr");
		$sth->finish();
}
if ($dmo) {
	$dbh->disconnect();
}
return $id;
}

sub GetEntertainTypes {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($temp,$amt,$stdesc) = undef;
	my $sth = $dbh->prepare("SELECT * FROM EntertainTypes WHERE ETID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EntertainTypes: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$temp = $ref->{'EnterShort'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $temp;
}

sub GetEntertainTypes_sel {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($type,$etid) = undef;
	my $sth = $dbh->prepare("SELECT * FROM EntertainTypes ORDER BY EnterType"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EntertainTypes: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$etid = $ref->{'ETID'};
		$type = $ref->{'EnterType'};
		if ($id eq $etid) {
			print "<option value='$etid' selected>$type</option>";
		} else {
			print "<option value='$etid'>$type</option>";
		}
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $temp;
}

sub DeleteEntertain {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $desc = undef;
	my $sth = $dbh->prepare("DELETE FROM EntertainMaster WHERE EntertainID='$id'"); 
	$sth->execute() or die &ErrorFairMsg("ERROR IN EntertainMaster: $DBI::errstr");
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
}

sub GetEntertain {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($org,$type,$contact,$addr,$addr2,$city,$state,$zip,$email,$phone,$cell,$tid,
		$blid,$goods,$status,$notes) = undef;
	my $sth = $dbh->prepare("SELECT * FROM EntertainMaster WHERE EntertainID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EntertainMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$org = $ref->{'GroupName'};
		$type = $ref->{'TypeID'};
		$contact = $ref->{'Contact'};
		$addr = $ref->{'Addr'};
		$addr2 = $ref->{'Addr2'};
		$city = $ref->{'City'};
		$state = $ref->{'State'};
		$zip = $ref->{'Zip'};
		$email = $ref->{'Email'};
		$phone = $ref->{'Phone'};
		$cell = $ref->{'Cell'};
		$notes = $ref->{'Notes'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $org,$type,$contact,$addr,$addr2,$city,$state,$zip,$email,$phone,
	$cell,$notes;
}

sub NewEntertainMain {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$ebh=&ConnectDB;
	$dmo++;
}
$id = "17";
my ($enternum,$feetotal) = &GetEntertainNumbers($id,$dbh);
&DoNewHeader(undef);
print << "END";
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="../demo/index.html">FairDirector® </a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-cog"></i> Account <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Settings</a></li>
              <li><a href="javascript:;">Help</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> fair-director.com <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Profile</a></li>
              <li><a href="javascript:;">Logout</a></li>
            </ul>
          </li>
        </ul>
        <form class="navbar-search pull-right">
          <input type="text" class="search-query" placeholder="Search">
        </form>
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <li><a href="../demo/index.html"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        <li><a href="../demo/reports.html"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>
        <li><a href="../demo/tickets.html"><i class="icon-tags"></i><span>Ticketing</span> </a></li>
        <li class="active"><a href="../demo/entertainment.html"><i class="icon-star"></i><span>Entertainment</span> </a> </li>
        <li><a href="../demo/vendors.html"><i class="icon-shopping-cart"></i><span>Vendors</span> </a> </li>
        <li><a href="../demo/sponsors.html"><i class="icon-money"></i><span>Sponsors</span> </a> </li>
        <li><a href="../demo/personnel.html"><i class="icon-group"></i><span>Personnel</span> </a> </li>
       <!-- <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Drops</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="../demo/icons.html">Icons</a></li>
            <li><a href="../demo/faq.html">FAQ</a></li>
            <li><a href="../demo/pricing.html">Pricing Plans</a></li>
            <li><a href="../demo/login.html">Login</a></li>
            <li><a href="../demo/signup.html">Signup</a></li>
            <li><a href="../demo/error.html">404</a></li>
          </ul>
        </li> -->
      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->
<div class="main">
	<div class="main-inner">
	    <div class="container">
        <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3> Today's Entertainment Stats</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-user"></i> <span class="value">$enternum</span> <br />Entertainers</div>
                    <!-- .stat -->
                    
                    <div class="stat"> <i class="icon-signal"></i> <span class="value">\$$feetotal</span> <br>
Charges</div>
                    <!-- .stat -->
                  </div>
                </div>
                <!-- /widget-content --> 
               </div>
            </div>
          </div>
          <!-- /widget -->
     	 </div>
        <!-- /span6 -->
        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-bookmark"></i>
              <h3>Entertainment Actions</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts"> 
                  <a href="javascript:;" class="shortcut">
                        <i class="shortcut-icon icon-list-alt"></i>
                      <span class="shortcut-label">Reports</span> 
                  </a>
                  <a href="javascript:;" class="shortcut">
                        <i class="shortcut-icon icon-group"></i>
                      <span class="shortcut-label">View Entertainers</span> 
                  </a>
                  <a  data-toggle="modal" href="#responsive" class="shortcut popup">
                        <i class="shortcut-icon icon-plus"></i> 
                      <span class="shortcut-label">Add Entertainer</span> 
                  </a>
                  <a href="javascript:;" class="shortcut"> 
                        <i class="shortcut-icon icon-envelope-alt"></i>
                      <span class="shortcut-label">Send Message</span> 
                  </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->
</div>

</div>
	<div class="row">
	      	<div class="span12">
	      		<div class="widget">
                <div class="widget-header">
						<i class="icon-group"></i>
						<h3>Entertainer List</h3>
					</div> <!-- /widget-header -->
					
					<div class="widget-content">
                    <div style="float:left;"><a href="./entertain.cgi?a=view">
            		<i class="icon-external-link"></i> 
                    	View Entertainer Application
              </a>
              </div>
    
           	<div style="float:right">
            		<i class="icon-plus"></i> 
                  <a  data-toggle="modal" href="#responsive" class="shortcut popup">Add Entertainer</a>
               </div>
               <div style="clear:both;"></div>
	      		<table style="width:100%;" cellpadding="0" cellspacing="0"  align="center">
<tbody><tr><td><div align="center"></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
	<table class="table">
		<tbody>
		<tr>
			<td width="25%"><b>Entertainer</b></td>
			<td width="25%"><b>Contact Name</b></td>
			<td width="15%"><b>Act Type</b></td>
			<td width="10%"><b>Phone</b></td>
			<td width="10%"><b>Email</b></td>
			<td width="10%"><b>Fee</b></td>
			<td width="10%">&nbsp;</td>
		</tr>
END
	my ($eid,$group,$contact,$type) = undef;
	my $sth = $dbh->prepare("SELECT * FROM EntertainMaster WHERE FairID='$id' ORDER BY GroupName"); 
	$sth->execute() or die &ErrorMsg("ERROR IN ENTERTAINMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$eid = $ref->{'EnterID'};
		$group = $ref->{'GroupName'};
		$contact = $ref->{'Contact'};
		$phone = $ref->{'Phone'};
		$cell = $ref->{'Cell'};
		$email = $ref->{'Email'};
		$fee = &Commify($ref->{'Fee'});
		$status = $ref->{'Status'};
		$type = &GetEntertainTypes($ref->{'TypeID'},$dbh);
		if ($status eq "A") {
			print "<tr bgcolor='#94f69b'>";
		} else {
			if ($n) { 
				print "<tr bgcolor='#f9f6f1'>";
				$n = undef;
			} else {
				print "<tr bgcolor='#ffffff'>";
				$n++;			
			}
		}
		print "<td>$group</td>";
		print "<td>$contact</td>";
		print "<td>$type</td>";
		print "<td>$cell</td>";
		print "<td><a href='mailto:$email'>$email</a></td>";
		print "<td>\$$fee</td>";
		print "<td><a href='./entertain.cgi?a=edit&id=$eid'><i class='icon-pencil'></i></a>&nbsp;&nbsp;<a href='./entertain.cgi?a=del&id=$eid'><i class='icon-remove'></i></a></td>";
		print "</tr>";
	}
	$sth->finish();
print << "END";
</tbody>
</table>
</td></tr>
<tr><td>&nbsp;</td></tr>
</tbody></table>
</div>
</div>
  </div> <!-- /row -->
    </div> <!-- /container -->
	</div> <!-- /main-inner -->
	</div> <!-- /main -->
    


<div class="extra">
  <div class="extra-inner">
    <div class="container">
      <div class="row">
                    <div class="span3">
                        <h4>Footer Area 1</h4>
                        <ul>
                          Important Links HEre
                          <li><a href="javascript:;"></a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            Support</h4>
                        <ul>
                            <li><a href="javascript:;">Frequently Asked Questions</a></li>
                            <li><a href="javascript:;">Ask a Question</a></li>
                            <li><a href="javascript:;">Video Tutorial</a></li>
                            <li><a href="javascript:;">Feedback</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            Something Legal</h4>
                        <ul>
                            <li><a href="javascript:;">Read License</a></li>
                            <li><a href="javascript:;">Terms of Use</a></li>
                            <li><a href="javascript:;">Privacy Policy</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            Some More Info</h4>
                        <ul><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id elit non mi porta gravida at eget metus.</li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /extra-inner --> 
</div>
<!-- /extra -->
<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> &copy; 2015 <a href="http://www.flourish-solutions.com/">Flourish Solutions</a>. </div>
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 
<div id="responsive" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Add Entertainer</h4>
  </div>
  <div class="modal-body">
   <div class="the-alert"></div>
	<div class="row-fluid">
		<div class="span6">
			 <form name="AE" method="post" action="./saveEntertain.cgi" data-fv-addons="reCaptcha2">
			 <input type="hidden" name="fid" id="fid" value="17">
			 <div class="form-group">
				 <label>Group Name:</label>
				 <input type="text" name="group" id="group" size="55" maxlength="255" value="">
			 </div>
			 <div class="form-group">	
				 <label>Entertainment Type:</label>
				 <select name="type" id="type" size="1"><option value=""></option>
END
	&GetEntertainTypes_sel($type,$dbh);
print << "END";
			</select>
			 </div>
			 <div class="form-group">
				 <label>Contact:</label>
				 <input type="text" name="contact" id="contact" size="55" maxlength="255" value="">
			 </div>
			 <div class="form-group">
				 <label>Street Address:</label>
				 <input type="text" name="addr" id="addr" size="55" maxlength="255" value="">
			
				 <input type="text" name="addr2" id="addr2" size="55" maxlength="255" value="">
			 </div>
			<div class="form-group">
				 <label>City/State/Zip:</label>
				 <input type="text" name="city" id="city" size="25" maxlength="255" value="city">
			</div>
			<div class="form-group">
				 <select name="state" id="state" size="1"><option value="">select state</option>
END
	&GetStates_sel($state);
print << "END";
				 </select>
			</div>
			<div class="form-group">
				 <input type="text" name="zip" id="zip" size="12" maxlength="12" value="zip">
			</div>
			
		</div>
		<div class="span6">
			<div class="form-group">
				 <label>Entertain Email:</label>
				 <input type="text" name="email" id="email" size="55" maxlength="255" value="">
			 </div>
				<div class="form-group">
					<label>Entertain Phone:</label>
					<input type="text" name="phone" id="phone" size="55" maxlength="255" value="">
				</div>
				<div class="form-group">
					<label>Cell:</label>
					<input type="text" name="cell" id="cell" size="55" maxlength="255" value="">
				</div>
				<div class="form-group">
					<label>Status: </label>
					<div class="row-fluid">
						<div class="span6">
							<input type="radio" name="status" id="status" value="N" checked=""> New Application<br>
							<input type="radio" name="status" id="status" value="A"> First Appearance<br>
							<input type="radio" name="status" id="status" value="P"> Performed Previously<br>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Notes:</label>
					<textarea name="notes" cols="50" rows="5"></textarea>
				</div>

	
    	</div>
       
	</div>
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default dismiss">Close</button>
    <button type="submit" class="btn btn-primary">Save changes</button>
  </div>
  </form>
</div>

<!-- Required -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/base.js"></script>

<!-- Charts -->
<script src="../js/excanvas.min.js"></script>
<script src="../js/chart.min.js" type="text/javascript"></script>


<!-- FormValidation plugin and the class supports validating Bootstrap form -->
<script src="../js/formValidation/dist/js/formValidation.js"></script>
<script src="../js/formValidation/dist/js/framework/bootstrap.js"></script>
<!-- <script src="../js/formValidation/dist/addons/reCaptcha2.js"></script> -->

<!-- Modal -->
<script src="../js/bootstrap-modal.js"></script>
<script src="../js/bootstrap-modalmanager.js"></script>



<script id="ajax" type="text/javascript" src="../js/modals.js" ></script> 
END
&DoNewFooter();
if ($dmo) {
	$dbh->disconnect();
}
}

sub GetEntertainNumbers {
my ($fid,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($ret,$amt,$total) = undef;
	my $sth = $dbh->prepare("SELECT * FROM EntertainMaster WHERE FairID='$fid'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN ENTERTAINMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ret++;
		$total += $ref->{'Fee'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
$amt = &Commify($total);
return $ret,$amt;
}

sub Commify {
   local $_  = shift;
   s{(?<!\d|\.)(\d{4,})}
    {my $n = $1;
     $n=~s/(?<=.)(?=(?:.{3})+$)/,/g;
     $n;
    }eg;
   return $_;
}

1;