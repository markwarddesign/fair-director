#!/usr/bin/perl

require "dateutil.pl";
require "dbutil.pl";
require "fairlayout.pl";
require "errormaster.pl";
# require "Email.pl";
require "FairReports.pl";
require "Fair.pl";

use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use DBI;

my ($Action,$id) = undef;
if (param()) {
	$Action = param('a');
	$t = param('t');
	$id = param('id');
	$attend = param('attend');
	$exp = param('exp');
	$factor = param('factor');
	$margin = param('margin');
	$pwd = param('pwd');
	$ca = param('ca');
}

my $dbh = &ConnectDB();
if ($Action eq "main") {
	&FairLogin(undef,$dbh);
} elsif ($Action eq "test") {
	&FairBlank(undef,$dbh);
} elsif ($Action eq "inside") {
	&FairMain($dbh);
} elsif ($Action eq "check") {
	my $p = lc($pwd);
	if (($p eq "omgthisisawesome2015") && ($ca eq "3")) {
		&FairMain($dbh);
	} else {
		&ErrorFairMsg("Incorrect Password Entered: [ $p ]");
		exit;
	}
} elsif ($Action eq "dotopdown") {
	if (!$exp) {
		&ErrorMsg("Must have expenses to calculate");
		exit;
	}
	my $newfactor = ($factor/100)+1;
	my $newattend = $attend*$newfactor;
	my $newexp = $exp*$newfactor;
	my $newmargin = ($margin/100)+1;
	my $ticket = ($newexp/$newattend)*$newmargin;
#	&TopDownResults(undef,$attend,$exp,$newattend,$newexp,$factor,$margin,$ticket,$dbh);
	&SuccessFairMsg("Done.  [ Projected Attendance: $newattend | Projected Expenses: $newexp | Growth Factor: $factor% | Desired Margin: $margin% | Ticket Price: $ticket ]");
}  else {
	&ErrorFairMsg("I don't know what you want me to do: [ $Action ]");
}
$dbh->disconnect();

exit;