#!/usr/bin/perl

require "dateutil.pl";
require "dbutil.pl";
require "fairlayout.pl";
require "errormaster.pl";
require "FairReports.pl";
require "Fair.pl";
require "Sponsor.pl";

use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use DBI;

my ($Action,$id,$t,$name,$start,$end,$type) = undef;
if (param()) {
	$Action = param('a');
	$t = param('t');
	$id = param('id');
	$org = param('Org');
	$type = param('et');
	$contact = param('Contact');
	$addr = param('Addr');
	$addr2 = param('Addr2');
	$city = param('City');
	$state = param('State');
	$zip = param('Zip');
	$email = param('Email');
	$phone = param('Phone');
	$cell = param('Cell');
	$tid = param('TaxID');
	$blid = param('BusLicID');
	$goods = param('Goods');
	$status = param('Status');
	$notes = param('Notes');
}

my $dbh = &ConnectDB();
if ($Action eq "main") {
	&SponsorMain(undef,$dbh);
} elsif ($Action eq "add") {
	&AddSponsor(undef,$dbh);
} elsif ($Action eq "edit") {
	&AddSponsor($id,$dbh);
} elsif ($Action eq "ss") {
	&SaveSponsor($id,$type,$org,$contact,$addr,$addr2,$city,$state,$zip,$email,
		$phone,$cell,$goods,$tid,$blid,$status,$notes,$dbh);
	&SponsorMain(undef,$dbh);
} elsif ($Action eq "del") {
	&DeleteSponsor($id,$dbh);
	&SponsorMain(undef,$dbh);
} else {
	&ErrorFairMsg("I don't know what you want me to do: [ $Action ]");
}
$dbh->disconnect();

exit;