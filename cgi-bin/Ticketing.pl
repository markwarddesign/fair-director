sub TicketMain {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my $bgcolor = "#FFFFFF";
&DoFairHeader(9);
print << "END";
<table width='1200'align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td><div align='left'><a href='./fair.cgi?a=main' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('GOBACK',\'','../images/goback_down.gif',1)\"><img src='../images/goback_up.gif' alt='Click HERE to go back to the main menu' name='GOBACK' width='250' height='50' border='0'></a></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<table width='85%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='5' color='#ed5000'><b>Ticketing Master</b></div></font></td></tr>
<tr><td>&nbsp;</td></tr>
END
	my $ret = undef;
	my $today = &GetMunge();
  	my $sth = $dbh->prepare("SELECT * FROM TicketMaster"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING TicketMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ret++;
	}
	$sth->finish();
	if ($ret) {
print << "END";
<tr><td>
	<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#ed5000'>
		<tr bgcolor='#FFFFFF'><td><font face='Arial, Helvetica, sans-serif' size='2' color='#FFFFFF'><a href='./ticketing.cgi?a=view'>View Ticket Application</a></font></td><td colspan='4'>&nbsp;</td><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'><a href='./ticketing.cgi?a=add'>Add Ticket</a></font></div></td></tr>
		<tr>
			<td width='45%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>Event</b></font></td>
			<td width='15%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>Ticket Type</b></font></td>
			<td width='10%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>Individual</b></font></td>
			<td width='10%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>Group</b></font></td>
			<td width='10%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>Special</b></font></td>
			<td width='10%'></td>
		</tr>
END
	my $today = &GetMunge;
	my ($id,$org,$contact,$phone,$cell,$type,$etype) = undef;
	my $sth = $dbh->prepare("SELECT * FROM TicketMaster ORDER BY FairID,Ticket"); 
	$sth->execute() or die &ErrorMsg("ERROR IN TicketMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'TicketID'};
		$fair = &GetEventName($ref->{'FairID'},$dbh);
		$ticket = &GetTicketType($ref->{'Ticket'},$dbh);
		$indiv = $ref->{'Individual'};
		$group = $ref->{'Group'};
		$special = $ref->{'Special'};
		if ($n) {
			print "<tr bgcolor='#ffffff'>";
			$n++;
		} else {
			print "<tr bgcolor='#fff2a7'>";
			$n = undef;
		}
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$fair</b></font></td>";
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$ticket</b></font></td>";
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$ $indiv</b></font></td>";
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$ $group</b></font></td>";
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>\$ $special</b></font></td>";
		print "<td><a href='./ticketing.cgi?a=edit&id=$id'><img src='../images/edit.gif' width='25' height='25' border='N'></a>&nbsp;&nbsp;
				<a href='./ticketing.cgi?a=del&id=$id'><img src='../images/delete.gif' width='25' height='25' border='N'></a></td>";
		print "</tr>";
	}
	$sth->finish();
print << "END";
	</table>
</td></tr>
<tr><td>&nbsp;</td></tr>
END
	} else {
print << "END";
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='3' color='#CC00000'><b><i>(No Tickets Found)</i></b>
	<font color='#000000'><a href='./ticketing.cgi?a=add'><b>Add Ticket</b></a></font></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
END
	}
print << "END";
</table>
</td></tr></table>
END
&DoFairFooter();
if ($dmo) {
	$dbh->disconnect();
}
}

sub AddTickets {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my ($fid,$ticket,$indiv,$group,$special) = undef;
if ($id) {
	($fid,$ticket,$indiv,$group,$special) = &GetTicket($id,$dbh);
}
my $bgcolor = "#FFFFFF";
&DoFairHeader(6);
print << "END";
<table width='1200'align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td><div align='left'><a href='./fair.cgi?a=main' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('GOBACK',\'','../images/goback_down.gif',1)\"><img src='../images/goback_up.gif' alt='Click HERE to go back to the main menu' name='GOBACK' width='250' height='50' border='0'></a></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<form name='AE' method='post' action='./ticketing.cgi'>
<input type='hidden' name='a' value='st'>
<input type='hidden' name='id' value='$id'>
<table width='70%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='5' color='#ed5000'><b>Create A Ticket</b></div></font></td></tr>
<tr><td>
	<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
		<tr><td colspan='3'>&nbsp;</td></tr>
		<tr>
			<td width='28%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Event: </b></font></div></td>
			<td width='2%'>&nbsp;</td>
			<td width='70%'><div align='left'><select name='fid' id='fid' size='1'><option value=''></option>
END
	my $sth = $dbh->prepare("SELECT * FROM FairMaster ORDER BY StartDate,FairName"); 
	$sth->execute() or die &ErrorMsg("ERROR IN FairMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$nfid = $ref->{'FairID'};
		$name = $ref->{'FairName'};
		if ($fid eq $nfid) {
			print "<option value='$nfid' selected>$name</option>";
		} else {
			print "<option value='$nfid'>$name</option>";
		}
	}
	$sth->finish();
print << "END";					
			</div></td>
		</tr>

		<tr>
			<td width='28%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Ticket: </b></font></div></td>
			<td width='2%'>&nbsp;</td>
			<td width='70%'><div align='left'><select name='ticket' id='ticket' size='1'><option value=''></option>
END
	my $sth = $dbh->prepare("SELECT * FROM TicketTypes ORDER BY Ticket"); 
	$sth->execute() or die &ErrorMsg("ERROR IN TicketTypes: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ttid = $ref->{'TTID'};
		$ticketname = $ref->{'TicketShort'};
		if ($ticket eq $ttid) {
			print "<option value='$ttid' selected>$ticketname</option>";
		} else {
			print "<option value='$ttid'>$ticketname</option>";
		}
	}
	$sth->finish();
print << "END";					
			</div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Individual: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'>\$<input type='text' name='indiv' id='indiv' size='5' maxlength='5' value='$indiv'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Group: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'>\$<input type='text' name='grp' id='grp' size='5' maxlength='5' value='$group'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Special: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'>\$<input type='text' name='spec' id='spec' size='5' maxlength='5' value='$special'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Status: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>
END
	if ($status eq "A") {
		print "<input type='radio' name='status' id='status' value='A' checked> Active&nbsp;&nbsp;<input type='radio' name='status' id='status' value='I'> Inactive";
	} elsif ($status eq "I") {
		print "<input type='radio' name='status' id='status' value='A'> Active&nbsp;&nbsp;<input type='radio' name='status' id='status' value='I' checked> Inactive";
	} else {
		print "<input type='radio' name='status' id='status' value='A' checked> Active&nbsp;&nbsp;<input type='radio' name='status' id='status' value='I'> Inactive";
	}
print << "END";
				</div></td>
		</tr>
		<tr><td colspan='3'>&nbsp;</td></tr>
		<tr><td colspan='3'><div align='center'><input type='submit' name='submit' value='Save Ticket Information'></div></td></tr>
		<tr><td colspan='3'>&nbsp;</td></tr>
	</table>
</td></tr>
</table></form>
</td></tr></table>
END
&DoFairFooter();
if ($dmo) {
	$dbh->disconnect();
}
}

sub SaveTicket {
my ($id,$fid,$ticket,$indiv,$group,$spec,$status,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
if (!$id) {				# if no ID is passed, 
# ==================================================================
# See if it already exists.
# ==================================================================
	my $sth = $dbh->prepare("SELECT * FROM TicketMaster WHERE FairID='$fid' AND Ticket='$ticket'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN TicketMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'TicketID'};
	}
	$sth->finish();
	if (!$id) {				# Create a new one.
		$sth = $dbh->prepare("INSERT INTO TicketMaster 
									(TicketMaster.FairID,
  									TicketMaster.Ticket,
  									TicketMaster.Individual,
  									TicketMaster.Group,
  									TicketMaster.Special,
  									TicketMaster.Status) 
  					VALUES ('$fid',
							'$ticket',
  							'$indiv',
  							'$group',
  							'$spec',
							'$status')"); 
	    $sth->execute() or die &ErrorMsg("ERROR SAVING TicketMaster: $DBI::errstr");
	    $sth->finish();
	    $sth = $dbh->prepare("SELECT * FROM TicketMaster WHERE FairID='$fid' AND Ticket='$ticket'"); 
	    $sth->execute() or die &ErrorMsg("ERROR IN TicketMaster: $DBI::errstr");
	    while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'TicketID'};
	    }
	    $sth->finish();	
	} else {
		$sth = $dbh->prepare("Update TicketMaster SET TicketMaster.FairID='$fid',
													TicketMaster.Ticket='$Ticket',
													TicketMaster.Individual='$indiv',
													TicketMaster.Group='$group',
													TicketMaster.Special='$spec',
													TicketMaster.Status='$status'
												WHERE TicketID='$id'"); 
		$sth->execute() or die &ErrorMsg("ERROR IN TicketMaster: $DBI::errstr");
		$sth->finish();
	}
} else {
		$sth = $dbh->prepare("Update TicketMaster SET TicketMaster.FairID='$fid',
													TicketMaster.Ticket='$Ticket',
													TicketMaster.Individual='$indiv',
													TicketMaster.Group='$group',
													TicketMaster.Special='$spec',
													TicketMaster.Status='$status'
												WHERE TicketID='$id'"); 
		$sth->execute() or die &ErrorMsg("ERROR IN TicketMaster: $DBI::errstr");
		$sth->finish();
}
if ($dmo) {
	$dbh->disconnect();
}
return $id;
}

sub GetTicketType {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($temp,$amt,$stdesc) = undef;
	my $sth = $dbh->prepare("SELECT * FROM TicketTypes WHERE TTID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN TicketTypes: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$temp = $ref->{'TicketShort'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $temp;
}

sub DeleteTicket {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $desc = undef;
	my $sth = $dbh->prepare("DELETE FROM TicketMaster WHERE TicketID='$id'"); 
	$sth->execute() or die &ErrorFairMsg("ERROR IN TicketMaster: $DBI::errstr");
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
}

sub GetTicket {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($fid,$ticket,$indiv,$group,$spec) = undef;
	my $sth = $dbh->prepare("SELECT * FROM TicketMaster WHERE TicketID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN TicketMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$fid = $ref->{'FairID'};
		$ticket = $ref->{'Ticket'};
		$indiv = $ref->{'Individual'};
		$group = $ref->{'Group'};
		$special = $ref->{'Special'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $fid,$ticket,$indiv,$group,$special;
}

sub TicketsApp {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my ($name,$type,$start,$end,$dir,$url,$info,$phone,$notes) = undef;
if ($id) {
	($name,$type,$start,$end,$dir,$url,$info,$phone,$notes) = &GetEvent($id,$dbh);
}
my $bgcolor = "#FFFFFF";
&DoFairHeader2(16);
print << "END";
<table width='1200'align='center' cellpadding='0' cellspacing='0' border='0' bgcolor='#ee4e00'>
<tr><td><div align='left'><a href='./fair.cgi?a=main' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('GOBACK',\'','../images/goback_down.gif',1)\"><img src='../images/goback_up.gif' alt='Click HERE to go back to the main menu' name='GOBACK' width='250' height='50' border='0'></a></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<form name='BT' method='post' action='./ticketing.cgi'>
<input type='hidden' name='a' value='online'>
<input type='hidden' name='id' value='$id'>
<table width='70%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='5' color='#ed5000'><b>Buy Your Ticket HERE!!!</b></div></font></td></tr>
<tr><td>
	<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
		<tr><td colspan='3'>&nbsp;</td></tr>
		<tr><td colspan='3'><div align='center'><font face='Arial, Helvetica, sans-serif' size='5' color='#000000'>YOUR EVENT LOGO HERE</font></div></td></tr>
		<tr><td colspan='3'>&nbsp;</td></tr>
		<tr>
			<td width='28%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Is This Ticket For: </b></font></div></td>
			<td width='2%'>&nbsp;</td>
			<td width='70%'><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>
				<input type='radio' name='t' value='A' checked> <b>Adult</b> (12 and Older)<br>	
				<input type='radio' name='t' value='C'> <b>Child</b> (Under 12)<br>
				<input type='radio' name='t' value='M'> <b>Active-Duty Military</b> (Military ID required)<br>
				<input type='radio' name='t' value='S'> <b>Senior Citizen</b> (Valid ID required)<br>
				<input type='radio' name='t' value='X'> <b>Student</b> (Student ID required)<br>
				<input type='radio' name='t' value='N'> <b>Special Needs Adult/Child</b>
			</div></td>
		</tr>
		<tr><td colspan='3'><hr></td></tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Ticket-Holder Name: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>First: <input type='text' name='fn' id='fn' size='25' maxlength='55' value='$first'>&nbsp;Last: <input type='text' name='ln' id='on' size='35' maxlength='55' value='$last'></font></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Street Address: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='addr' id='addr' size='55' maxlength='55'></div></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='addr2' id='addr2' size='55' maxlength='55'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>City/State/Zip: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>
				<input type='text' name='city' id='city' size='35' maxlength='255'>, <select name='st' id='st' size='1'><option value=''></option>
END
	&GetStates_sel(undef);
print << "END";
				</select>&nbsp;<input type='text' name='zip' id='zip' size='12' maxlength='12'></font></div></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>Phone: <input type='text' name='phone' id='phone' size='15' maxlength='15'>&nbsp;Email: <input type='text' name='email' id='email' size='25' maxlength='55'></div></td>
		</tr>
		<tr><td colspan='3'>&nbsp;</td></tr>
		<tr><td colspan='3'><div align='center'><input type='submit' name='submit' value='Save Ticket Information'></div></td></tr>
		<tr><td colspan='3'>&nbsp;</td></tr>
	</table>
</td></tr>
</table></form>
</td></tr>
<tr><td>&nbsp;</td></tr>
</table>
END
&DoFairFooter2();
if ($dmo) {
	$dbh->disconnect();
}
}


1;