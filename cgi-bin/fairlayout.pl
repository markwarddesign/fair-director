sub DoHeader {
my $id = shift;

print "Content-type:text/html\n\n";
print << "END";
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="court reporting tools productivity legal transcription shorthand">
<meta name="description" content="Free productivity tools for freelance court reporting professionals and reporting firms.">
<title>County Fair Management Demo</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<link href="../table.css" rel="stylesheet" type="text/css">
<link href="../Layout.css" rel="stylesheet" type="text/css" />
<script src="../jquery.js"></script>
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function disablefield(myVal) {

       if (myVal != '*') {
             document.getElementById('FName').disabled = true;
             document.getElementById('FAddr').disabled = true;
             document.getElementById('FAddr2').disabled = true;
             document.getElementById('FCity').disabled = true;
             document.getElementById('FState').disabled = true;
             document.getElementById('FZip').disabled = true;
             document.getElementById('FCountry').disabled = true;
             document.getElementById('FLast').disabled = true;
             document.getElementById('FFirst').disabled = true;
             document.getElementById('FPhone').disabled = true;
             document.getElementById('FEmail').disabled = true;
             document.getElementById('FURL').disabled = true;
       } else {
             document.getElementById('FName').disabled = false;
             document.getElementById('FAddr').disabled = false;
             document.getElementById('FAddr2').disabled = false;
             document.getElementById('FCity').disabled = false;
             document.getElementById('FState').disabled = false;
             document.getElementById('FZip').disabled = false;
             document.getElementById('FCountry').disabled = false;
             document.getElementById('FLast').disabled = false;
             document.getElementById('FFirst').disabled = false;
             document.getElementById('FPhone').disabled = false;
             document.getElementById('FEmail').disabled = false;
             document.getElementById('FURL').disabled = false;
       }
       return;
}
//-->
</script>
</head>

<body bgcolor='#2a3753'>
<table width='1200' align='center' cellpadding='0' cellspacing='0' align='center' bgcolor='#2a3753'>
<tr><td width='1200'>
END
  if (!$id) {
	print "<img src='../images/MainBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq "1") {
	print "<img src='../images/main_banner.jpg' width='1200' height='75'>";
   } elsif ($id eq "2") {
	print "<img src='../images/TalkBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq "3") {
	print "<img src='../images/KeysBanner.jpg' width='1200' height='75'>";
    } elsif ($id eq "4") {
	print "<img src='../images/attend_banner.jpg' width='1200' height='75'>";
    } elsif ($id eq "5") {
	print "<img src='../images/ReportBanner.jpg' width='1200' height='75'>";
    } elsif ($id eq "6") {
	print "<img src='../images/ScheduleBanner.jpg' width='1200' height='75'>";    
    }
END
}

sub DoHeader2 {
my $id = shift;

print "Content-type:text/html\n\n";
print << "END";
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="court reporting tools productivity legal transcription shorthand">
<meta name="description" content="Free productivity tools for freelance court reporting professionals and reporting firms.">
<title>Ward Clerk Assistant</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<link href="../table.css" rel="stylesheet" type="text/css">
<link href="../Layout.css" rel="stylesheet" type="text/css" />
<script src="../jquery.js"></script>
<script type="text/javascript">

<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function disablefield(myVal) {

       if (myVal != '*') {
             document.getElementById('FName').disabled = true;
             document.getElementById('FAddr').disabled = true;
             document.getElementById('FAddr2').disabled = true;
             document.getElementById('FCity').disabled = true;
             document.getElementById('FState').disabled = true;
             document.getElementById('FZip').disabled = true;
             document.getElementById('FCountry').disabled = true;
             document.getElementById('FLast').disabled = true;
             document.getElementById('FFirst').disabled = true;
             document.getElementById('FPhone').disabled = true;
             document.getElementById('FEmail').disabled = true;
             document.getElementById('FURL').disabled = true;
       } else {
             document.getElementById('FName').disabled = false;
             document.getElementById('FAddr').disabled = false;
             document.getElementById('FAddr2').disabled = false;
             document.getElementById('FCity').disabled = false;
             document.getElementById('FState').disabled = false;
             document.getElementById('FZip').disabled = false;
             document.getElementById('FCountry').disabled = false;
             document.getElementById('FLast').disabled = false;
             document.getElementById('FFirst').disabled = false;
             document.getElementById('FPhone').disabled = false;
             document.getElementById('FEmail').disabled = false;
             document.getElementById('FURL').disabled = false;
       }
       return;
}
END
	print "articles = new Array();\n";
	my $x = 1;
	while ($x < 4) {
               print "articles[\"$x\"] = new Array();\n";
		$y = 2013;
		while ($y < 2015) {
                        print "articles[\"$x\"][\"$y\"] = new Array();\n";
			$z = 1;
			while ($z < 13) {
                                print "articles[\"$x\"][\"$y\"][\"$z\"] = new Array();\n";
				print "a = 0;\n";
				my $ret = undef;
				my $pdate = "$y-$z-01";
				my $sth = $dbh->prepare("SELECT a.AMID,a.Name FROM SourceMaster s,ArticleMaster a,PubDates p 
							WHERE a.SID='$x'
							AND a.PubDate='$pdate'
							ORDER BY a.Name");
				$sth->execute() or die &ErrorMsg("ERROR GETTING ARTICLES: $DBI::errstr");
				while (my $ref = $sth->fetchrow_hashref()) {
					$ret++;
					$aid = $ref->{'AMID'};
					$name = $ref->{'Name'};
                                        print "articles[\"$x\"][\"$y\"][\"$z\"][\"aid\"] = new Array();\n";
                                        print "articles[\"$x\"][\"$y\"][\"$z\"][\"name\"] = new Array();\n";
					print "articles[\"$x\"][\"$y\"][\"$z\"][\"aid\"][a] = \"$aid\";\n";
					print "articles[\"$x\"][\"$y\"][\"$z\"][\"name\"][a++] = \"$name\";\n";
				}
				$sth->finish();
				$z++;
			}
		$y++;
		}
	$x++;
	}
print << "END";
//-->
</script>
</head>

<body bgcolor='#2a3753'>
<table width='1200' align='center' cellpadding='0' cellspacing='0' align='center' bgcolor='#2a3753'>
<tr><td width='1200'>
END
    if (!$id) {
	print "<img src='../images/MainBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq "1") {
	print "<img src='../images/TalkBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq "2") {
	print "<img src='../images/TalkBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq "3") {
	print "<img src='../images/KeysBanner.jpg' width='1200' height='75'>";
   }
END
}

sub DoSpamHeader {
my $id = shift;

print "Content-type:text/html\n\n";
print << "END";
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="spam spammer spammers email">
<meta name="description" content="Spam Management Tools">
<title>Ward Clerk Tools</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<link href="../table.css" rel="stylesheet" type="text/css">
<link href="../Layout.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function disablefield(myVal) {

       if (myVal != '*') {
             document.getElementById('FName').disabled = true;
             document.getElementById('FAddr').disabled = true;
             document.getElementById('FAddr2').disabled = true;
             document.getElementById('FCity').disabled = true;
             document.getElementById('FState').disabled = true;
             document.getElementById('FZip').disabled = true;
             document.getElementById('FCountry').disabled = true;
             document.getElementById('FLast').disabled = true;
             document.getElementById('FFirst').disabled = true;
             document.getElementById('FPhone').disabled = true;
             document.getElementById('FEmail').disabled = true;
             document.getElementById('FURL').disabled = true;
       } else {
             document.getElementById('FName').disabled = false;
             document.getElementById('FAddr').disabled = false;
             document.getElementById('FAddr2').disabled = false;
             document.getElementById('FCity').disabled = false;
             document.getElementById('FState').disabled = false;
             document.getElementById('FZip').disabled = false;
             document.getElementById('FCountry').disabled = false;
             document.getElementById('FLast').disabled = false;
             document.getElementById('FFirst').disabled = false;
             document.getElementById('FPhone').disabled = false;
             document.getElementById('FEmail').disabled = false;
             document.getElementById('FURL').disabled = false;
       }
       return;
}
//-->
</script>
</head>

<body bgcolor='#000000'>
<table width='1200' align='center' cellpadding='0' cellspacing='0' align='center' bgcolor='#FDD675'>
<tr><td width='1200'>
	<img src='../images/Spam/spam_banner.jpg' width='1200' height='100'>
END
}


sub DoPrintHeader {
my $id = shift;

print "Content-type:text/html\n\n";
print << "END";
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="court reporting tools productivity legal transcription shorthand">
<meta name="description" content="Free productivity tools for freelance court reporting professionals and reporting firms.">
<title>Ward Clerk Tools</title>
</head>
<body>
END
}

sub DoPrintFooter {
print << "END";
<p><div align='center'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>=====***** CONFIDENTIAL *****=====</font></div></p>
</body>
</html>
END
}

sub DoLegalHeader {
	
print "Content-type:text/html\n\n";
print << "END";
<html xmlns:v="urn:schemas-microsoft-com:vml"
xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:w="urn:schemas-microsoft-com:office:word"
xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Word.Document>
<meta name=Generator content="Microsoft Word 12">
<meta name=Originator content="Microsoft Word 12">
<link rel=File-List
href="Ward Clerk Tools%20User%20Agreement_files/filelist.xml">
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:Author>Darryl</o:Author>
  <o:LastAuthor>Darryl</o:LastAuthor>
  <o:Revision>2</o:Revision>
  <o:TotalTime>769</o:TotalTime>
  <o:LastPrinted>2013-01-04T00:30:00Z</o:LastPrinted>
  <o:Created>2013-01-04T13:06:00Z</o:Created>
  <o:LastSaved>2013-01-04T13:06:00Z</o:LastSaved>
  <o:Pages>14</o:Pages>
  <o:Words>6622</o:Words>
  <o:Characters>37750</o:Characters>
  <o:Lines>314</o:Lines>
  <o:Paragraphs>88</o:Paragraphs>
  <o:CharactersWithSpaces>44284</o:CharactersWithSpaces>
  <o:Version>12.00</o:Version>
 </o:DocumentProperties>
</xml><![endif]-->
<link rel=themeData
href="Ward Clerk Tools%20User%20Agreement_files/themedata.thmx">
<link rel=colorSchemeMapping
href="Ward Clerk Tools%20User%20Agreement_files/colorschememapping.xml">
<!--[if gte mso 9]><xml>
 <w:WordDocument>
  <w:SpellingState>Clean</w:SpellingState>
  <w:GrammarState>Clean</w:GrammarState>
  <w:TrackMoves>false</w:TrackMoves>
  <w:TrackFormatting/>
  <w:DrawingGridHorizontalSpacing>6 pt</w:DrawingGridHorizontalSpacing>
  <w:DisplayHorizontalDrawingGridEvery>2</w:DisplayHorizontalDrawingGridEvery>
  <w:ValidateAgainstSchemas/>
  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
  <w:DoNotPromoteQF/>
  <w:LidThemeOther>EN-US</w:LidThemeOther>
  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>
  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
  <w:Compatibility>
   <w:BreakWrappedTables/>
   <w:SnapToGridInCell/>
   <w:WrapTextWithPunct/>
   <w:UseAsianBreakRules/>
   <w:DontGrowAutofit/>
   <w:SplitPgBreakAndParaMark/>
   <w:DontVertAlignCellWithSp/>
   <w:DontBreakConstrainedForcedTables/>
   <w:DontVertAlignInTxbx/>
   <w:Word11KerningPairs/>
   <w:CachedColBalance/>
  </w:Compatibility>
  <w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>
  <m:mathPr>
   <m:mathFont m:val="Cambria Math"/>
   <m:brkBin m:val="before"/>
   <m:brkBinSub m:val="&#45;-"/>
   <m:smallFrac m:val="off"/>
   <m:dispDef/>
   <m:lMargin m:val="0"/>
   <m:rMargin m:val="0"/>
   <m:defJc m:val="centerGroup"/>
   <m:wrapIndent m:val="1440"/>
   <m:intLim m:val="subSup"/>
   <m:naryLim m:val="undOvr"/>
  </m:mathPr></w:WordDocument>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="true"
  DefSemiHidden="true" DefQFormat="false" DefPriority="99"
  LatentStyleCount="267">
  <w:LsdException Locked="false" Priority="0" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Normal"/>
  <w:LsdException Locked="false" Priority="9" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="heading 1"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 2"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 3"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 4"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 5"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 6"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 7"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 8"/>
  <w:LsdException Locked="false" Priority="9" QFormat="true" Name="heading 9"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 1"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 2"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 3"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 4"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 5"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 6"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 7"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 8"/>
  <w:LsdException Locked="false" Priority="39" Name="toc 9"/>
  <w:LsdException Locked="false" Priority="35" QFormat="true" Name="caption"/>
  <w:LsdException Locked="false" Priority="10" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Title"/>
  <w:LsdException Locked="false" Priority="1" Name="Default Paragraph Font"/>
  <w:LsdException Locked="false" Priority="11" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtitle"/>
  <w:LsdException Locked="false" Priority="22" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Strong"/>
  <w:LsdException Locked="false" Priority="20" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Emphasis"/>
  <w:LsdException Locked="false" Priority="59" SemiHidden="false"
   UnhideWhenUsed="false" Name="Table Grid"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Placeholder Text"/>
  <w:LsdException Locked="false" Priority="1" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="No Spacing"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 1"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 1"/>
  <w:LsdException Locked="false" UnhideWhenUsed="false" Name="Revision"/>
  <w:LsdException Locked="false" Priority="34" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="List Paragraph"/>
  <w:LsdException Locked="false" Priority="29" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Quote"/>
  <w:LsdException Locked="false" Priority="30" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Quote"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 1"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 1"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 1"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 1"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 1"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 1"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 1"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 2"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 2"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 2"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 2"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 2"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 2"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 2"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 2"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 3"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 3"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 3"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 3"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 3"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 3"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 3"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 3"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 4"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 4"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 4"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 4"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 4"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 4"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 4"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 4"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 5"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 5"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 5"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 5"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 5"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 5"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 5"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 5"/>
  <w:LsdException Locked="false" Priority="60" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="61" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light List Accent 6"/>
  <w:LsdException Locked="false" Priority="62" SemiHidden="false"
   UnhideWhenUsed="false" Name="Light Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="63" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="64" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Shading 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="65" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="66" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium List 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="67" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 1 Accent 6"/>
  <w:LsdException Locked="false" Priority="68" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 2 Accent 6"/>
  <w:LsdException Locked="false" Priority="69" SemiHidden="false"
   UnhideWhenUsed="false" Name="Medium Grid 3 Accent 6"/>
  <w:LsdException Locked="false" Priority="70" SemiHidden="false"
   UnhideWhenUsed="false" Name="Dark List Accent 6"/>
  <w:LsdException Locked="false" Priority="71" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Shading Accent 6"/>
  <w:LsdException Locked="false" Priority="72" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful List Accent 6"/>
  <w:LsdException Locked="false" Priority="73" SemiHidden="false"
   UnhideWhenUsed="false" Name="Colorful Grid Accent 6"/>
  <w:LsdException Locked="false" Priority="19" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Emphasis"/>
  <w:LsdException Locked="false" Priority="21" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Emphasis"/>
  <w:LsdException Locked="false" Priority="31" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Subtle Reference"/>
  <w:LsdException Locked="false" Priority="32" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Intense Reference"/>
  <w:LsdException Locked="false" Priority="33" SemiHidden="false"
   UnhideWhenUsed="false" QFormat="true" Name="Book Title"/>
  <w:LsdException Locked="false" Priority="37" Name="Bibliography"/>
  <w:LsdException Locked="false" Priority="39" QFormat="true" Name="TOC Heading"/>
 </w:LatentStyles>
</xml><![endif]-->
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;
	mso-font-charset:0;
	mso-generic-font-family:roman;
	mso-font-pitch:variable;
	mso-font-signature:-536870145 1107305727 0 0 415 0;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-parent:"";
	margin:0in;
	margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";}
h1
	{mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Heading 1 Char";
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	mso-outline-level:1;
	font-size:24.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
h2
	{mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Heading 2 Char";
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	mso-outline-level:2;
	font-size:18.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
h3
	{mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Heading 3 Char";
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	mso-outline-level:3;
	font-size:13.5pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
h4
	{mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-qformat:yes;
	mso-style-link:"Heading 4 Char";
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	mso-outline-level:4;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	font-weight:bold;}
a:link, span.MsoHyperlink
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:blue;
	text-decoration:underline;
	text-underline:single;}
a:visited, span.MsoHyperlinkFollowed
	{mso-style-noshow:yes;
	mso-style-priority:99;
	color:purple;
	mso-themecolor:followedhyperlink;
	text-decoration:underline;
	text-underline:single;}
p
	{mso-style-noshow:yes;
	mso-style-priority:99;
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";}
span.Heading1Char
	{mso-style-name:"Heading 1 Char";
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 1";
	mso-ansi-font-size:24.0pt;
	mso-bidi-font-size:24.0pt;
	mso-font-kerning:18.0pt;
	font-weight:bold;}
span.Heading2Char
	{mso-style-name:"Heading 2 Char";
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 2";
	mso-ansi-font-size:18.0pt;
	mso-bidi-font-size:18.0pt;
	font-weight:bold;}
span.Heading3Char
	{mso-style-name:"Heading 3 Char";
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 3";
	mso-ansi-font-size:13.5pt;
	mso-bidi-font-size:13.5pt;
	font-weight:bold;}
span.Heading4Char
	{mso-style-name:"Heading 4 Char";
	mso-style-priority:9;
	mso-style-unhide:no;
	mso-style-locked:yes;
	mso-style-link:"Heading 4";
	mso-ansi-font-size:12.0pt;
	mso-bidi-font-size:12.0pt;
	font-weight:bold;}
p.last-changed, li.last-changed, div.last-changed
	{mso-style-name:last-changed;
	mso-style-unhide:no;
	mso-margin-top-alt:auto;
	margin-right:0in;
	mso-margin-bottom-alt:auto;
	margin-left:0in;
	mso-pagination:widow-orphan;
	font-size:12.0pt;
	font-family:"Times New Roman","serif";
	mso-fareast-font-family:"Times New Roman";}
span.locality
	{mso-style-name:locality;
	mso-style-unhide:no;}
span.region
	{mso-style-name:region;
	mso-style-unhide:no;}
span.postal-code
	{mso-style-name:postal-code;
	mso-style-unhide:no;}
span.SpellE
	{mso-style-name:"";
	mso-spl-e:yes;}
span.GramE
	{mso-style-name:"";
	mso-gram-e:yes;}
.MsoChpDefault
	{mso-style-type:export-only;
	mso-default-props:yes;
	font-size:10.0pt;
	mso-ansi-font-size:10.0pt;
	mso-bidi-font-size:10.0pt;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:.5in .5in .5in .5in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;
	mso-paper-source:0;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
@list l0
	{mso-list-id:60830048;
	mso-list-template-ids:-60384780;}
@list l0:level4
	{mso-level-number-format:alpha-lower;
	mso-level-tab-stop:2.0in;
	mso-level-number-position:left;
	text-indent:-.25in;}
ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>
<!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"Table Normal";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-qformat:yes;
	mso-style-parent:"";
	mso-padding-alt:0in 5.4pt 0in 5.4pt;
	mso-para-margin:0in;
	mso-para-margin-bottom:.0001pt;
	mso-pagination:widow-orphan;
	font-size:10.0pt;
	font-family:"Times New Roman","serif";}
</style>
<![endif]--><!--[if gte mso 9]><xml>
 <o:shapedefaults v:ext="edit" spidmax="2050"/>
</xml><![endif]--><!--[if gte mso 9]><xml>
 <o:shapelayout v:ext="edit">
  <o:idmap v:ext="edit" data="1"/>
 </o:shapelayout></xml><![endif]-->
</head>
<body lang=EN-US link=blue vlink=purple style='tab-interval:.5in' bgcolor='#000000'>
<table width='1200' align='center' cellpadding='0' cellspacing='0' align='center' bgcolor='#FDD675'>
<tr><td width='1200'>
	<img src='../images/main_banner.jpg' width='1200' height='100'>
END
}

sub DoHeader_notbl {
my $id = shift;

my $preload = undef;
if ($id eq "AppMain") {
	$preload = "onLoad=\"MM_preloadImages('../images/apply_btn_off.jpg')\"";
} elsif ($id eq "1") {
 	$preload = "onLoad=\"MM_preloadImages('../images/ErrorBanner.jpg')\"";
 }

print "Content-type:text/html\n\n";
print << "END";
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="court reporting tools productivity legal transcription shorthand">
<meta name="description" content="Free productivity tools for freelance court reporting professionals and reporting firms.">
<title>Ward Clerk Tools</title>
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>
<body $preload color='#000000'>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Ward Clerk Tools.com:  Welcome to our new site!</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="expires" CONTENT="Wed, 03 Nov 1999 12:21:14 GMT"> 
<META HTTP-EQUIV="Pragma" CONTENT="no-cache"> 
<meta http-equiv="Cache-Control" Content="no-cache"> 
</head>
<body bgcolor='#000000'>
<table width='1200' align='center' cellpadding='0' cellspacing='0' align='center' bgcolor='#FDD675'>
<tr><td width='1200'>
END
if (!$id) {
	print "<img src='../images/MainBanner.jpg' width='1200' height='75'>";
} elsif ($id eq "1") {
        print "<img src='../images/ErrorBanner.jpg' width='1200' height='75'>";
} elsif ($id eq "2") {
        print "<img src='../images/SuccessBanner.jpg' width='1200' height='75'>";
}
}

sub DoFooter {

print << "END";
	<table width='1200' align='center' cellpadding='0' cellspacing='0' bgcolor='#2a3753'>
		<tr>
			<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'>All content copyright &copy; 2014 Barksdale IT Solutions - All Rights Reserved</font></div></td>
		</tr>
		<tr>
			<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'>Technical Support: <a href='mailto:darryl\@barksdaleitsolutions.com' decor='N'>darryl\@barksdaleitsolutions.com</a></font></div></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
	</table>
</td></tr>
</table>
</body>
</html>	
END
}

sub DoSpamFooter {

print << "END";
	<table width='1200' align='center' cellpadding='0' cellspacing='0' bgcolor='#CCCCCC'>
		<tr>
			<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'>All content copyright &copy; 2013 Darryl L. Barksdale - All Rights Reserved</font></div></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
	</table>
</td></tr>
</table>
</body>
</html>	
END
}

sub DoFooter_admin {

print << "END";
	<table width='1200' align='center' cellpadding='0' cellspacing='0' bgcolor='#F7B000'>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'>All content copyright &copy; 2013 Barksdale Enterprises - All Rights Reserved</font></div></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
	</table>
</td></tr>
</table>
</body>
</html>	
END
}

sub DoHeader_admin {
my $id = shift;

my $preload = undef;

print "Content-type:text/html\n\n";
print << "END";
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="court reporting tools productivity legal transcription shorthand">
<meta name="description" content="Free productivity tools for freelance court reporting professionals and reporting firms.">
<title>Ward Clerk Tools</title>
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>
<body color='#000000'>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Ward Clerk Tools.com:  Welcome to our new site!</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="expires" CONTENT="Wed, 03 Nov 1999 12:21:14 GMT"> 
<META HTTP-EQUIV="Pragma" CONTENT="no-cache"> 
<meta http-equiv="Cache-Control" Content="no-cache"> 
</head>
<body bgcolor='#000000'>
<table width='1200' align='center' cellpadding='0' cellspacing='0' align='center' bgcolor='#FDD675'>
<tr><td width='1200'>
	<img src='../images/admin_banner.jpg' width='1200' height='100'>
END
END
}

sub ContentBox {
my ($title,$text) = @_;

print << "END";
<div class="contentSecondary">
    <h2 class="blue"><strong>$title</strong></h2>
    <p><strong>$text</strong></p>
</div>
END
}

sub DoFairHeader {
my $id = shift;

print "Content-type:text/html\n\n";
print << "END";
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="County State Fair Management Tools Software">
<meta name="description" content="Productivity tools for Fair and Event Management">
<title>Fair Management Software Demo</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<link href="../table.css" rel="stylesheet" type="text/css">
<link href="../Layout.css" rel="stylesheet" type="text/css" />
<script src="../jquery.js"></script>
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

var event,divname,display,notes;
function changeDiv(event,divname,display,notes) {

var en,dn,dd,dx;
     en = document.getElementById("en");
     en.innerHTML = "Event: " + event;
     en.style.color = "white";
     en.style.font= "bold 20px arial,serif";
     dn = document.getElementById("dn");
     dn.innerHTML = "Division: " + divname;
     dn.style.color = "white";
     dn.style.font= "bold 20px arial,serif";
     dd = document.getElementById("dd");
     dd.innerHTML = "Display ID: " + display;
     dd.style.color = "white";
     dd.style.font= "bold 20px arial,serif";
     dx = document.getElementById("dx");
     dx.innerHTML = "Notes: " + notes;
     dx.style.color = "white";
     dx.style.font= "bold 20px arial,serif";
}

function clearDiv() {
     document.getElementById("en").innerHTML = "";   
     document.getElementById("dn").innerHTML = "";  
     document.getElementById("dd").innerHTML = "";   
     document.getElementById("dx").innerHTML = "";  
}

var deptname,ddisplay,staff,dnotes;
function changeDept(deptname,ddisplay,staff,dnotes) {

var dd,di,st,dx;
     dd = document.getElementById("dd");
     dd.innerHTML = deptname;
     dd.style.color = "white";
     dd.style.font= "bold 20px arial,serif";
     di = document.getElementById("di");
     di.innerHTML = ddisplay;
     di.style.color = "white";
     di.style.font= "bold 20px arial,serif";
     st = document.getElementById("st");
     st.innerHTML = staff;
     st.style.color = "white";
     st.style.font= "bold 20px arial,serif";
     dx = document.getElementById("dx");
     dx.innerHTML = dnotes;
     dx.style.color = "white";
     dx.style.font= "bold 20px arial,serif";
}

function clearDept() {
     document.getElementById("dn").innerHTML = "";  
     document.getElementById("dd").innerHTML = "";   
     document.getElementById("di").innerHTML = "";  
     document.getElementById("st").innerHTML = "";  
     document.getElementById("dx").innerHTML = "";  
}

function disablefield(myVal) {

       if (myVal != '*') {
             document.getElementById('FName').disabled = true;
             document.getElementById('FAddr').disabled = true;
             document.getElementById('FAddr2').disabled = true;
             document.getElementById('FCity').disabled = true;
             document.getElementById('FState').disabled = true;
             document.getElementById('FZip').disabled = true;
             document.getElementById('FCountry').disabled = true;
             document.getElementById('FLast').disabled = true;
             document.getElementById('FFirst').disabled = true;
             document.getElementById('FPhone').disabled = true;
             document.getElementById('FEmail').disabled = true;
             document.getElementById('FURL').disabled = true;
       } else {
             document.getElementById('FName').disabled = false;
             document.getElementById('FAddr').disabled = false;
             document.getElementById('FAddr2').disabled = false;
             document.getElementById('FCity').disabled = false;
             document.getElementById('FState').disabled = false;
             document.getElementById('FZip').disabled = false;
             document.getElementById('FCountry').disabled = false;
             document.getElementById('FLast').disabled = false;
             document.getElementById('FFirst').disabled = false;
             document.getElementById('FPhone').disabled = false;
             document.getElementById('FEmail').disabled = false;
             document.getElementById('FURL').disabled = false;
       }
       return;
}
//-->
</script>
</head>

<body bgcolor='#3370b0'>
<table width='1200' align='center' cellpadding='0' cellspacing='0' align='center' bgcolor='#3370b0'>
<tr><td width='1200'>
END
  if ($id eq 1) {
	print "<img src='../images/MainFairBanner.jpg' width='1200' height='75'>";
  } elsif ($id eq 2) {
	print "<img src='../images/topdown_banner.jpg' width='1200' height='75'>";
  } elsif ($id eq 3) {
	print "<img src='../images/success_banner.jpg' width='1200' height='75'>";
  } elsif ($id eq 4) {
	print "<img src='../images/error_banner.jpg' width='1200' height='75'>";
   } elsif ($id eq 5) {
	print "<img src='../images/VendorBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 6) {
	print "<img src='../images/TicketBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 7) {
	print "<img src='../images/EventBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 8) {
	print "<img src='../images/SponsorBanner.jpg' width='1200' height='75'>";	
   } elsif ($id eq 9) {
	print "<img src='../images/EntertainBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 10) {
	print "<img src='../images/StaffBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 11) {
	print "<img src='../images/SettingsBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 12) {
	print "<img src='../images/ReportsBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 13) {
	print "<img src='../images/ConcessionBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 14) {
	print "<img src='../images/FinancialBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 15) {
	print "<img src='../images/LivestockBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 16) {
	print "<img src='../images/TicketSignUpBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 17) {
 	print "<img src='../images/ExhibitsBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 18) {
	print "<img src='../images/PropertyBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 19) {
 	print "<img src='../images/MaintenanceBanner.jpg' width='1200' height='75'>"; 
 }
END
}

sub DoFairHeader2 {
my $id = shift;

print "Content-type:text/html\n\n";
print << "END";
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="keywords" content="County State Fair Management Tools Software">
<meta name="description" content="Productivity tools for Fair and Event Management">
<title>Fair Management Software Demo</title>
<link href="../styles.css" rel="stylesheet" type="text/css">
<link href="../table.css" rel="stylesheet" type="text/css">
<link href="../Layout.css" rel="stylesheet" type="text/css" />
<script src="../jquery.js"></script>
<script type="text/javascript">
<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function disablefield(myVal) {

       if (myVal != '*') {
             document.getElementById('FName').disabled = true;
             document.getElementById('FAddr').disabled = true;
             document.getElementById('FAddr2').disabled = true;
             document.getElementById('FCity').disabled = true;
             document.getElementById('FState').disabled = true;
             document.getElementById('FZip').disabled = true;
             document.getElementById('FCountry').disabled = true;
             document.getElementById('FLast').disabled = true;
             document.getElementById('FFirst').disabled = true;
             document.getElementById('FPhone').disabled = true;
             document.getElementById('FEmail').disabled = true;
             document.getElementById('FURL').disabled = true;
       } else {
             document.getElementById('FName').disabled = false;
             document.getElementById('FAddr').disabled = false;
             document.getElementById('FAddr2').disabled = false;
             document.getElementById('FCity').disabled = false;
             document.getElementById('FState').disabled = false;
             document.getElementById('FZip').disabled = false;
             document.getElementById('FCountry').disabled = false;
             document.getElementById('FLast').disabled = false;
             document.getElementById('FFirst').disabled = false;
             document.getElementById('FPhone').disabled = false;
             document.getElementById('FEmail').disabled = false;
             document.getElementById('FURL').disabled = false;
       }
       return;
}
//-->
</script>
</head>

<body bgcolor='#ffaa01'>
<table width='1200' align='center' cellpadding='0' cellspacing='0' align='center' bgcolor='#ffaa01'>
<tr><td width='1200'>
END
  if ($id eq 1) {
	print "<img src='../images/MainFairBanner.jpg' width='1200' height='75'>";
  } elsif ($id eq 2) {
	print "<img src='../images/topdown_banner.jpg' width='1200' height='75'>";
  } elsif ($id eq 3) {
	print "<img src='../images/success_banner.jpg' width='1200' height='75'>";
  } elsif ($id eq 4) {
	print "<img src='../images/error_banner.jpg' width='1200' height='75'>";
   } elsif ($id eq 5) {
	print "<img src='../images/VendorBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 6) {
	print "<img src='../images/TicketBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 7) {
	print "<img src='../images/EventBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 8) {
	print "<img src='../images/SponsorBanner.jpg' width='1200' height='75'>";	
   } elsif ($id eq 9) {
	print "<img src='../images/EntertainBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 10) {
	print "<img src='../images/StaffBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 11) {
	print "<img src='../images/SettingsBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 12) {
	print "<img src='../images/ReportsBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 13) {
	print "<img src='../images/ConcessionBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 14) {
	print "<img src='../images/FinancialBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 15) {
	print "<img src='../images/LivestockBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 16) {
	print "<img src='../images/TicketSignUpBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 17) {
 	print "<img src='../images/ExhibitsBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 18) {
	print "<img src='../images/PropertyBanner.jpg' width='1200' height='75'>";
   } elsif ($id eq 19) {
 	print "<img src='../images/MaintenanceBanner.jpg' width='1200' height='75'>"; 
 }
END
}

sub DoFairFooter {

print << "END";
	<table width='1200' align='center' cellpadding='0' cellspacing='0' bgcolor='#3370b0'>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td valign='top'><div align='center'><img src='../images/company.jpg' height='50'></div></td>
		</tr>
		<tr>
			<td valign='top'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'>All content copyright &copy; 2015 - All Rights Reserved</font></div></td>
		</tr>
		<tr>
			<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'>Technical Support: <a href='mailto:darryl\@barksdaleitsolutions.com' decor='N'>darryl\@barksdaleitsolutions.com</a></font></div></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
	</table>
</td></tr>
</table>
</body>
</html>	
END
}

sub DoFairFooter2 {

print << "END";
	<table width='1200' align='center' cellpadding='0' cellspacing='0' bgcolor='#ffaa01'>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td valign='top'><div align='center'><img src='../images/company.jpg' height='50'></div></td>
		</tr>
		<tr>
			<td valign='top'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'>All content copyright &copy; 2015 - All Rights Reserved</font></div></td>
		</tr>
		<tr>
			<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'>Technical Support: <a href='mailto:darryl\@barksdaleitsolutions.com' decor='N'>darryl\@barksdaleitsolutions.com</a></font></div></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
	</table>
</td></tr>
</table>
</body>
</html>	
END
}

sub DoNewHeader {
my $banner = shift;

print "Content-type:text/html\n\n";
print << "END";
<!DOCTYPE html>
<html lang="en">
 <head>
    <meta charset="utf-8">
    <title>Entertainment | FairDirector</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/bootstrap-responsive.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
    <link href="../css/font-awesome.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/pages/reports.css" rel="stylesheet">
    <link href="../css/pages/dashboard.css" rel="stylesheet">
    
    <link href="../css/fair.css" rel="stylesheet">
    
    
    <!-- Modal -->
    <link href="../css/bootstrap-modal-bs3patch.css" rel="stylesheet">
    <link href="../css/bootstrap-modal.css" rel="stylesheet">

    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!-- Run code when the page loads. -->
<script language="Javascript">
function xmlhttpPost(strURL) {
    var xmlHttpReq = false;
    var self = this;
    // Mozilla/Safari
    if (window.XMLHttpRequest) {
        self.xmlHttpReq = new XMLHttpRequest();
    }
    // IE
    else if (window.ActiveXObject) {
        self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
    }
    self.xmlHttpReq.open('POST', strURL, true);
    self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    self.xmlHttpReq.onreadystatechange = function() {
        if (self.xmlHttpReq.readyState == 4) {
            updatepage(self.xmlHttpReq.responseText);
        }
    }
    self.xmlHttpReq.send(getquerystring());
}

function getquerystring() {
    var form     = document.forms['AE'];
    var id = form.id.value;
    var fid = form.fid.value;
    var group = form.group.value;
    var type = form.type.value;
    var contact = form.contact.value;
    var addr = form.addr.value; 
    var addr2 = form.addr2.value;
    var city = form.city.value;
    var state = form.state.value;
    var zip = form.zip.value;
    var email = form.email.value;
    var phone = form.phone.value;    
    var cell = form.cell.value;
    var status = form.status.value;
    var fee = form.fee.value;
      
    qstr = 'id=' + escape(id) + 
            '&group=' + escape(group) + 
            '&fid=' + escape(fid) + 
            '&type=' + escape(type) + 
            '&contact=' + escape(contact) + 
            '&addr=' + escape(addr) +
            '&addr2=' + escape(addr2) + 
            '&city=' + escape(city) +
            '&state=' + escape(state) +
            '&zip=' + escape(zip) +
            '&email=' + escape(email) +
            '&phone=' + escape(phone) +
            '&cell=' + escape(cell) +
            '&status=' + escape(status) +
            '&fee=' + escape(fee);  // NOTE: no '?' before querystring
    return qstr;
}

function updatepage(str){
    document.getElementById("result").innerHTML = str;
}

function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != 'function') {
    window.onload = func;
  } else {
    window.onload = function() {
      oldonload();
      func();
    }
  }
}
</script>
  </head>
<body>
END
}

sub DoNewFooter {

print << "END";
  </body>
</html>
END
}



1;