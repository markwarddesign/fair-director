#!/usr/bin/perl

require "dateutil.pl";
require "dbutil.pl";
require "fairlayout.pl";
require "errormaster.pl";
require "FairReports.pl";
require "Fair.pl";
require "Volunteer.pl";

use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use DBI;

my ($Action,$id) = undef;
if (param()) {
	$Action = param('a');
	$t = param('t');
	$id = param('id');
	$attend = param('attend');
	$exp = param('exp');
	$factor = param('factor');
	$margin = param('margin');
}

my $dbh = &ConnectDB();
if ($Action eq "main") {
	&VolunteerMain($dbh);
} else {
	&ErrorFairMsg("I don't know what you want me to do: [ $Action ]");
}
$dbh->disconnect();

exit;