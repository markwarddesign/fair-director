sub VendorMain {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my $bgcolor = "#FFFFFF";
&DoFairHeader(5);
print << "END";
<table width='1200'align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td><div align='left'><a href='./fair.cgi?a=main' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('GOBACK',\'','../images/goback_down.gif',1)\"><img src='../images/goback_up.gif' alt='Click HERE to go back to the main menu' name='GOBACK' width='250' height='50' border='0'></a></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<table width='85%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='5' color='#ed5000'><b>Vendor Master List</b></div></font></td></tr>
<tr><td>&nbsp;</td></tr>
END
	my $ret = undef;
	my $today = &GetMunge();
  	my $sth = $dbh->prepare("SELECT * FROM VendorMaster"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING VendorMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ret++;
	}
	$sth->finish();
	if ($ret) {
print << "END";
<tr><td>
	<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#ed5000'>
		<tr bgcolor='#FFFFFF'><td colspan='5'><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'><a href='./vendor.cgi?a=add'>Add Vendor</a></font></div></td></tr>
		<tr>
			<td width='25%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>Vendor</b></font></td>
			<td width='14%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>Type</b></font></td>
			<td width='24%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>Contact</b></font></td>
			<td width='24%'><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b>Phone</b></font></td>
			<td width='8%'></td>
		</tr>
END
	my $today = &GetMunge;
	my ($id,$org,$contact,$phone,$cell,$type,$vtype) = undef;
	my $sth = $dbh->prepare("SELECT * FROM VendorMaster ORDER BY Org"); 
	$sth->execute() or die &ErrorMsg("ERROR IN VendorMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'VendorID'};
		$type = $ref->{'TypeID'};
		$org = $ref->{'Org'};
		$contact = $ref->{'Contact'};
		$phone = $ref->{'Phone'};
		$cell = $ref->{'Cell'};
		$vtype = &GetVendorTypes($type,$dbh);
		if ($n) {
			print "<tr bgcolor='#ffffff'>";
			$n++;
		} else {
			print "<tr bgcolor='#fff2a7'>";
			$n = undef;
		}
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$org</b></font></td>";
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$vtype</b></font></td>";
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$contact</b></font></td>";
		print "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$phone/$cell</b></font></td>";
		print "<td><a href='./vendor.cgi?a=edit&id=$id'><img src='../images/edit.gif' width='25' height='25' border='N'></a>&nbsp;&nbsp;
				<a href='./vendor.cgi?a=del&id=$id'><img src='../images/delete.gif' width='25' height='25' border='N'></a></td>";
		print "</tr>";
	}
	$sth->finish();
print << "END";
	</table>
</td></tr>
<tr><td>&nbsp;</td></tr>
END
	} else {
print << "END";
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='3' color='#CC00000'><b><i>(No Vendors Found)</i></b>
	<font color='#000000'><a href='./vendor.cgi?a=add'><b>Add Vendor</b></a></font></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
END
	}
print << "END";
</table>
</td></tr></table>
END
&DoFairFooter();
if ($dmo) {
	$dbh->disconnect();
}
}

sub AddVendor {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my ($org,$type,$contact,$addr,$addr2,$city,$state,$zip,$email,$phone,$cell,$tid,$blid,$goods,$status,$notes) = undef;
if ($id) {
	($org,$type,$contact,$addr,$addr2,$city,$state,$zip,$email,$phone,$cell,$tid,$blid,$goods,$status,$notes) = &GetVendor($id,$dbh);
}
my $bgcolor = "#FFFFFF";
&DoFairHeader(5);
print << "END";
<table width='1200'align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td><div align='left'><a href='./fair.cgi?a=main' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('GOBACK',\'','../images/goback_down.gif',1)\"><img src='../images/goback_up.gif' alt='Click HERE to go back to the main menu' name='GOBACK' width='250' height='50' border='0'></a></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<form name='AE' method='post' action='./vendor.cgi'>
<input type='hidden' name='a' value='sv'>
<input type='hidden' name='id' value='$id'>
<table width='70%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='5' color='#ed5000'><b>Create A Vendor Record</b></div></font></td></tr>
<tr><td>
	<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
		<tr><td colspan='3'>&nbsp;</td></tr>
		<tr>
			<td width='28%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Organization: </b></font></div></td>
			<td width='2%'>&nbsp;</td>
			<td width='70%'><div align='left'><input type='text' name='Org' id='Org' size='55' maxlength='255' value='$org'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Vendor Type: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><select name='et' id='et' size='1'><option value=''></option>
END
		my $sth = $dbh->prepare("SELECT * FROM VendorTypes ORDER BY VendorTypeDesc"); 
		$sth->execute() or die &ErrorMsg("ERROR IN VendorTypes: $DBI::errstr");
		while (my $ref = $sth->fetchrow_hashref()) {
			my $vtid = $ref->{'VTID'};
			my $vt = $ref->{'VendorTypeDesc'};
			if ($vtid eq $type) {
				print "<option value='$vtid' selected>$vt</option>";
			} else {
				print "<option value='$vtid'>$vt</option>";
			}
		}
		$sth->finish();
print << "END";			
			</select></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Contact: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='Contact' id='Contact' size='55' maxlength='255' value='$contact'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Street Address: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='Addr' id='Addr' size='55' maxlength='255' value='$addr'></div></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='Addr2' id='Addr2' size='55' maxlength='255' value='$addr2'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>City/State/Zip: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='City' id='City' size='25' maxlength='255' value='$city'>, 
			<select name='State' size='1'><option value=''></option>
END
		&GetStates_sel($state);
print << "END";
			</select>&nbsp;<input type='text' name='Zip' id='Zip' size='12' maxlength='12' value='$zip'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Vendor Email: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='Email' id='Email' size='55' maxlength='255' value='$email'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Vendor Phone: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='Phone' id='Phone' size='55' maxlength='255' value='$phone'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Cell: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='Cell' id='Cell' size='55' maxlength='255' value='$cell'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Tax ID: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='TaxID' id='TaxID' size='25' maxlength='25' value='$tid'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Business License #: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><input type='text' name='BusLicID' id='BusLicID' size='55' maxlength='25' value='$blid'></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Goods/Services: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><textarea name='Goods' cols='50' rows='10'>$goods</textarea></div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Status: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>
				<input type='radio' name='Status' id='Status' value='N'> New Application<br>
				<input type='radio' name='Status' id='Status' value='A' checked> Approved<br>
				<input type='radio' name='Status' id='Status' value='P'> Probationary<br>
				<input type='radio' name='Status' id='Status' value='S'> Suspended<br>
				<input type='radio' name='Status' id='Status' value='I'> Inactive
				</div></td>
		</tr>
		<tr>
			<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Notes: </b></font></div></td>
			<td>&nbsp;</td>
			<td><div align='left'><textarea name='Notes' cols='50' rows='10'>$notes</textarea></div></td>
		</tr>
		<tr><td colspan='3'>&nbsp;</td></tr>
		<tr><td colspan='3'><div align='center'><input type='submit' name='submit' value='Save Vendor Information'></div></td></tr>
		<tr><td colspan='3'>&nbsp;</td></tr>
	</table>
</td></tr>
</table></form>
</td></tr></table>
END
&DoFairFooter();
if ($dmo) {
	$dbh->disconnect();
}
}

sub SaveVendor {
my ($id,$type,$org,$contact,$addr,$addr2,$city,$state,$zip,$email,
		$phone,$cell,$goods,$tid,$blid,$status,$notes,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my $Org = &WriteQuotes($org);
my $Contact = &WriteQuotes($contact);
my $Addr = &WriteQuotes($addr);
my $Addr2 = &WriteQuotes($addr2);
my $City = &WriteQuotes($city);
my $Goods = &WriteQuotes($goods);
my $Notes = &WriteQuotes($notes);
if (!$id) {				# if no ID is passed, 
# ==================================================================
# See if it already exists.
# ==================================================================
	my $sth = $dbh->prepare("SELECT * FROM VendorMaster WHERE Org='$Org' AND TypeID='$type'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN VendorMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'VendorID'};
	}
	$sth->finish();
	if (!$id) {				# Create a new one.
		$sth = $dbh->prepare("INSERT INTO VendorMaster 
									(VendorMaster.Org,
  									VendorMaster.TypeID,
  									VendorMaster.Contact,
  									VendorMaster.Addr,
  									VendorMaster.Addr2,
  									VendorMaster.City,
  									VendorMaster.State,
  									VendorMaster.Zip,
  									VendorMaster.Email,
  									VendorMaster.Phone,
  									VendorMaster.Cell,
  									VendorMaster.TaxID,
  									VendorMaster.BusLicID,
  									VendorMaster.Goods,
  									VendorMaster.Status,
									VendorMaster.Notes) 
  					VALUES ('$Org',
							'$type',
  							'$Contact',
  							'$Addr',
  							'$Addr2',
  							'$City',
							'$state',
							'$zip',
							'$email',
							'$phone',
							'$cell',
							'$tid',
							'$blid',
							'$Goods',
							'$status',
							'$Notes')"); 
	    $sth->execute() or die &ErrorMsg("ERROR SAVING VendorMaster: $DBI::errstr");
	    $sth->finish();
	    $sth = $dbh->prepare("SELECT * FROM VendorMaster WHERE Org='$Org' AND TypeID='$type'"); 
	    $sth->execute() or die &ErrorMsg("ERROR IN VendorMaster: $DBI::errstr");
	    while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'VendorID'};
	    }
	    $sth->finish();	
	} else {
		$sth = $dbh->prepare("Update VendorMaster SET VendorMaster.Org='$Org',
													VendorMaster.TypeID='$type',
													VendorMaster.Contact='$Contact',
													VendorMaster.Addr='$Addr',
													VendorMaster.Addr2='$Addr2',
													VendorMaster.City='$City',
													VendorMaster.State='$state',
													VendorMaster.Zip='$zip',
													VendorMaster.Email='$email',
													VendorMaster.Phone='$phone',
													VendorMaster.Cell='$cell',
													VendorMaster.TaxID='$tid',
													VendorMaster.BusLicID='$blid',
													VendorMaster.Goods='$Goods',
													VendorMaster.Status='$status',
													VendorMaster.Notes='$Notes'
												WHERE VendorID='$id'"); 
		$sth->execute() or die &ErrorMsg("ERROR IN VendorMaster: $DBI::errstr");
		$sth->finish();
	}
} else {
		$sth = $dbh->prepare("Update VendorMaster SET VendorMaster.Org='$Org',
													VendorMaster.TypeID='$type',
													VendorMaster.Contact='$Contact',
													VendorMaster.Addr='$Addr',
													VendorMaster.Addr2='$Addr2',
													VendorMaster.City='$City',
													VendorMaster.State='$state',
													VendorMaster.Zip='$zip',
													VendorMaster.Email='$email',
													VendorMaster.Phone='$phone',
													VendorMaster.Cell='$cell',
													VendorMaster.TaxID='$tid',
													VendorMaster.BusLicID='$blid',
													VendorMaster.Goods='$Goods',
													VendorMaster.Status='$status',
													VendorMaster.Notes='$Notes'
												WHERE VendorID='$id'"); 
		$sth->execute() or die &ErrorMsg("ERROR IN VendorMaster: $DBI::errstr");
		$sth->finish();
}
if ($dmo) {
	$dbh->disconnect();
}
return $id;
}

sub GetVendorTypes {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $vtdesc = undef;
	my $sth = $dbh->prepare("SELECT * FROM VendorTypes WHERE VTID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN VendorTypes: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$vtdesc = $ref->{'VendorTypeDesc'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $vtdesc;
}

sub DeleteVendor {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $desc = undef;
	my $sth = $dbh->prepare("DELETE FROM VendorMaster WHERE VendorID='$id'"); 
	$sth->execute() or die &ErrorFairMsg("ERROR IN VendorMaster: $DBI::errstr");
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
}

sub GetVendor {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($org,$type,$contact,$addr,$addr2,$city,$state,$zip,$email,$phone,$cell,$tid,
		$blid,$goods,$status,$notes) = undef;
	my $sth = $dbh->prepare("SELECT * FROM VendorMaster WHERE VendorID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN VendorMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$org = $ref->{'Org'};
		$type = $ref->{'TypeID'};
		$contact = $ref->{'Contact'};
		$addr = $ref->{'Addr'};
		$addr2 = $ref->{'Addr2'};
		$city = $ref->{'City'};
		$state = $ref->{'State'};
		$zip = $ref->{'Zip'};
		$email = $ref->{'Email'};
		$phone = $ref->{'Phone'};
		$cell = $ref->{'Cell'};
		$tid = $ref->{'TaxID'};
		$blid = $ref->{'BusLicID'};
		$goods = $ref->{'Goods'};
		$status = $ref->{'Status'};
		$notes = $ref->{'Notes'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $org,$type,$contact,$addr,$addr2,$city,$state,$zip,$email,$phone,
	$cell,$tid,$blid,$goods,$status,$notes;
}

1;