#!/usr/local/cpanel/3rdparty/perl/514/bin/perl

use cPanelUserConfig;
use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use warnings;
use Image::DecodeQR;

my $file = shift // '../images/test/newtest.png';
if ($file) {
	$string = Image::DecodeQR::decode($file);
}
print "Content-type:text/html\n\n";
print "<p><h2>The QRCode is: [ $string ]</h2></p>";
exit;