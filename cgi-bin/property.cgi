#!/usr/bin/perl

require "dateutil.pl";
require "dbutil.pl";
require "fairlayout.pl";
require "errormaster.pl";
require "FairReports.pl";
require "Fair.pl";
require "Staff.pl";
require "Property.pl";
require "Sponsor.pl";

use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use DBI;

my ($Action,$id,$fid,$name,$use,$sid,$cap,$booths,$exhibits,$loc,$lid,$hr,$dr,$wr,$mr,$power,$plumb,$kitchen,$air,$heat,$net,$wifi,$notes) = undef;
if (param()) {
	$Action = param('a');
	$id = param('id');
	$fid = param('fid');
	$name = param('name');
	$use = param('use');
	$sid = param('sid');
	$cap = param('capacity');
	$booths = param('booths');
	$exhibits = param('exhibits');
	$loc = param('sponsor');
	$lid = param('lid');
	$hr = param('hr');
	$dr = param('dr');
	$wr = param('wr');
	$mr = param('mr');
	$power = param('power');
	$plumb = param('plumb');
	$kitchen = param('kitchen');
	$air = param('air');
	$heat = param('heat');
	$rest = param('rest');
	$loc = param('loc');
	$sponsor = param('sponsor');
	$net = param('net');
	$wifi = param('wifi');
	$notes = param('notes');
	$status = param('status');
}

my $dbh = &ConnectDB();
if ($Action eq "main") {
	&NewPropertyMain(undef,$dbh);
} elsif ($Action eq "edit") {
	&PropertyMain($id,$dbh);
} elsif ($Action eq "del") {
	&DeleteProperty($id,$dbh);
	&PropertyMain(undef,$dbh);
} elsif ($Action eq "save") {
	&SaveProperty($id,$name,$use,$sid,$cap,$booths,$exhibits,$sponsor,$loc,$sponsor,$lid,$hr,$dr,$wr,$mr,$status,$power,$plumb,$rest,$air,$heat,$kitchen,$net,$wifi,$notes,$dbh);
	&PropertyMain(undef,$id,$dbh);
} else {
	&ErrorFairMsg("I don't know what you want me to do: [ $Action ]");
}
$dbh->disconnect();

exit;