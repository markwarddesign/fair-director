#!/usr/bin/perl

require "dateutil.pl";
require "dbutil.pl";
require "fairlayout.pl";
require "errormaster.pl";
require "FairReports.pl";
require "Fair.pl";
require "Entertain.pl";

use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use DBI;

my ($Action,$id,$t,$name,$start,$end,$type) = undef;
if (param()) {
	$Action = param('a');
	$t = param('t');
	$id = param('id');
	$org = param('GroupName');
	$type = param('et');
	$contact = param('Contact');
	$addr = param('Addr');
	$addr2 = param('Addr2');
	$city = param('City');
	$state = param('State');
	$zip = param('Zip');
	$email = param('Email');
	$phone = param('Phone');
	$cell = param('Cell');
	$tid = param('TaxID');
	$blid = param('BusLicID');
	$goods = param('Goods');
	$status = param('Status');
	$notes = param('Notes');
}

my $dbh = &ConnectDB();
if ($Action eq "main") {
	&EntertainMain(undef,$dbh);
} elsif ($Action eq "add") {
	&AddEntertain(undef,$dbh);
} elsif ($Action eq "edit") {
	&AddEntertain($id,$dbh);
} elsif ($Action eq "test") {
	&NewEntertainMain(undef,$dbh);
} elsif ($Action eq "se") {
	&SaveEntertain($id,$type,$org,$contact,$addr,$addr2,$city,$state,$zip,$email,
		$phone,$cell,$status,$notes,$dbh);
	&EntertainMain(undef,$dbh);
} elsif ($Action eq "del") {
	&DeleteEntertain($id,$dbh);
	&EntertainMain(undef,$dbh);
} elsif ($Action eq "mw") {
	$query = new CGI;
	$et = param('et');
	print $query->header;
	print "<p>theaction: <b>$Action</b><br />
	id: <b>$id</b><br />
	type: <b>$et</b><br />
	GroupName: <b>$org</b><br />
	";
	exit;
}
else {
	&ErrorFairMsg("I don't know what you want me to do: [ $Action ]");
}
$dbh->disconnect();

exit;