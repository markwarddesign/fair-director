# ============================================================
# Staff Management Routines
# ============================================================
sub NewStaffMain {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$ebh=&ConnectDB;
	$dmo++;
}
$id = "17";
my ($enternum,$newtotal) = &GetStaffNumbers($dbh);
if (!$newtotal) {
  $newtotal = "0";
}
&DoNewHeader(undef);
print << "END";
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="../demo/index.html">FairDirector® </a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-cog"></i> Account <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Settings</a></li>
              <li><a href="javascript:;">Help</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> fair-director.com <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Profile</a></li>
              <li><a href="javascript:;">Logout</a></li>
            </ul>
          </li>
        </ul>
        <form class="navbar-search pull-right">
          <input type="text" class="search-query" placeholder="Search">
        </form>
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <li><a href="./fair.cgi?a=main"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        <li><a href="./reports.cgi?a=main"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>
        <li><a href="./ticketing.cgi?a=main"><i class="icon-tags"></i><span>Ticketing</span> </a></li>
        <li><a href="./entertain.cgi?a=test"><i class="icon-star"></i><span>Entertainment</span> </a> </li>
        <li><a href="./vendors.cgi?a=main"><i class="icon-shopping-cart"></i><span>Vendors</span> </a> </li>
        <li><a href="./sponsors.cgi?a=main"><i class="icon-money"></i><span>Sponsors</span> </a> </li>
        <li><a href="./staff.cgi?a=main"><i class="icon-group"></i><span>Personnel</span> </a> </li>
       <!-- <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Drops</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="../demo/icons.html">Icons</a></li>
            <li><a href="../demo/faq.html">FAQ</a></li>
            <li><a href="../demo/pricing.html">Pricing Plans</a></li>
            <li><a href="../demo/login.html">Login</a></li>
            <li><a href="../demo/signup.html">Signup</a></li>
            <li><a href="../demo/error.html">404</a></li>
          </ul>
        </li> -->
      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->
<div class="main">
	<div class="main-inner">
	    <div class="container">
        <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3> Today's Staffing Stats</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-user"></i> <span class="value">$enternum</span> <br />Staff & Volunteers</div>
                    <!-- .stat -->
                    
                    <div class="stat"> <i class="icon-signal"></i> <span class="value">\$$newtotal</span> <br>
Annual Cost</div>
                    <!-- .stat -->
                  </div>
                </div>
                <!-- /widget-content --> 
               </div>
            </div>
          </div>
          <!-- /widget -->
     	 </div>
        <!-- /span6 -->
        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-bookmark"></i>
              <h3>Staff Actions</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts"> 
                  <a href="javascript:;" class="shortcut">
                        <i class="shortcut-icon icon-list-alt"></i>
                      <span class="shortcut-label">Reports</span> 
                  </a>
                  <a href="javascript:;" class="shortcut">
                        <i class="shortcut-icon icon-group"></i>
                      <span class="shortcut-label">View Staff</span> 
                  </a>
                  <a  data-toggle="modal" href="#responsive" class="shortcut popup">
                        <i class="shortcut-icon icon-plus"></i> 
                      <span class="shortcut-label">Add Staff</span> 
                  </a>
                  <a href="javascript:;" class="shortcut"> 
                        <i class="shortcut-icon icon-envelope-alt"></i>
                      <span class="shortcut-label">Send Message</span> 
                  </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->
</div>

</div>
	<div class="row">
	      	<div class="span12">
	      		<div class="widget">
                <div class="widget-header">
						<i class="icon-group"></i>
						<h3>Staff List</h3>
					</div> <!-- /widget-header -->
					
					<div class="widget-content">
                    <div style="float:left;"><a href="./staff.cgi?a=view">
            		<i class="icon-external-link"></i> 
                    	View Employment/Volunteer Application
              </a>
              </div>
    
           	<div style="float:right">
            		<i class="icon-plus"></i> 
                  <a  data-toggle="modal" href="#responsive" class="shortcut popup">Create Staff Record</a>
               </div>
               <div style="clear:both;"></div>
	      		<table style="width:100%;" cellpadding="0" cellspacing="0"  align="center">
<tbody><tr><td><div align="center"></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
	<table class="table">
		<tbody>
		<tr>
			<td width="20%"><b>Name</b></td>
			<td width="20%"><b>Job</b></td>			
			<td width="20%"><b>Phone</b></td>
			<td width="20%"><b>Cell</b></td>
			<td width="5%"><b>\$/V</b></td>
			<td width="5%"><b>Email</b></td>
			<td width="15%">&nbsp;</td>
		</tr>
END
	my ($eid,$group,$contact,$type) = undef;
	my $sth = $dbh->prepare("SELECT * FROM StaffMaster ORDER BY LastName,FirstName"); 
	$sth->execute() or die &ErrorMsg("ERROR IN PROPERTYMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$pmid = $ref->{'StaffID'};
		$sname = &GetStaffName($pmid,$dbh);
		$job = &GetJobInfo($ref->{'JobID'},$dbh);
		$phone = $ref->{'HomePhone'};
		$work = $ref->{'WorkPhone'};
		$cell = $ref->{'CellPhone'};
		$email = $ref->{'Email'};
		$ct = $ref->{'CompType'};
		if ($n) { 
			print "<tr bgcolor='#f9f6f1'>";
			$n = undef;
		} else {
			print "<tr bgcolor='#ffffff'>";
			$n++;			
		}
		print "<td>$sname</td>";
		print "<td>$job</td>";
		print "<td>$phone</td>";
		print "<td>$cell</td>";
		print "<td>$ct</td>";
		print "<td><a href='mailto:$email'><img src='../images/emailicon.gif' width='30' height='25' border='no'><a/></td>";
		print "<td><a href='./property.cgi?a=edit&id=$pmid'><i class='icon-pencil'></i></a>&nbsp;&nbsp;<a href='./property.cgi?a=del&id=$pmid'><i class='icon-remove'></i></a></td>";
		print "</tr>";
	}
	$sth->finish();
print << "END";
</tbody>
</table>
</td></tr>
<tr><td>&nbsp;</td></tr>
</tbody></table>
</div>
</div>
  </div> <!-- /row -->
    </div> <!-- /container -->
	</div> <!-- /main-inner -->
	</div> <!-- /main -->
    


<div class="extra">
  <div class="extra-inner">
    <div class="container">
      <div class="row">
                    <div class="span3">
                        <h4>Footer Area 1</h4>
                        <ul>
                          Important Links HEre
                          <li><a href="javascript:;"></a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            Support</h4>
                        <ul>
                            <li><a href="javascript:;">Frequently Asked Questions</a></li>
                            <li><a href="javascript:;">Ask a Question</a></li>
                            <li><a href="javascript:;">Video Tutorial</a></li>
                            <li><a href="javascript:;">Feedback</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            Something Legal</h4>
                        <ul>
                            <li><a href="javascript:;">Read License</a></li>
                            <li><a href="javascript:;">Terms of Use</a></li>
                            <li><a href="javascript:;">Privacy Policy</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            Some More Info</h4>
                        <ul><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id elit non mi porta gravida at eget metus.</li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /extra-inner --> 
</div>
<!-- /extra -->
<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> &copy; 2015 <a href="http://www.flourish-solutions.com/">Flourish Solutions</a>. </div>
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 
<div id="responsive" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Add Property</h4>
  </div>
  <div class="modal-body">
   <div class="the-alert"></div>
	<div class="row-fluid">
		<div class="span6">
			 <form name="AE" method="post" action="./saveStaff.cgi" data-fv-addons="reCaptcha2">
			 <div class="form-group">
				 <label>Last Name: </label>
				 <input type="text" name="last" id="last" size="15" maxlength="255" value="">
			 </div>
			 <div class="form-group">
				 <label>First Name/MI: </label>
				 <input type="text" name="first" id="first" size="15" maxlength="255" value="">&nbsp;
				 <input type="text" name="mi" id="mi" size="5" maxlength="5" value="">
			 </div>			
			 <div class="form-group">
				 <label>Address:</label>
				 <input type="text" name="addr" id="addr" size='25' maxlength='255' value="">
			 </div>
			 <div class="form-group">
				 <label></label>
				 <input type="text" name="addr2" id="addr2" size="25" maxlength="255" value="">
			 </div>
			 <div class="form-group">
				 <label>City/State/Zip: </label>
				 <input type="text" name="city" id="city" size="15" maxlength="55" value="">,&nbsp;
				 <select name='state' id='state' size='1'><option value=''></option>
END
	&GetStates_sel($state);
print << "END";
				</select>&nbsp;<input type="text" name="zip" id="zip" size="10" maxlength="10" value=""></div>
			 <div class="form-group">
				 <label>Phone: </label>
				 <input type="text" name="phone" id="phone" size="15" maxlength="15" value="">
			 </div>		
			 <div class="form-group">
				 <label>Cell: </label>
				 <input type="text" name="cell" id="cell" size="15" maxlength="15" value="">
			 </div>
			 <div class="form-group">
				 <label>Email: </label>
				 <input type="text" name="email" id="email" size="25" maxlength="55" value="">
			 </div>	
			 <div class="form-group">
				 <label>Supervisor:</label>
				 <select name='mid' id='mid' size='1'><option value=''></option>
END
		&GetStaff_sel($sid,$dbh);
print << "END";
			 </select></div>
			 <div class="form-group">
				 <label>Compensation Type:</label>
				 <input type='radio' name='ct' id='ct' value='H' checked> Hourly<br>
				 <input type='radio' name='ct' id='ct' value='S'> Salaried<br>
				<input type='radio' name='ct' id='ct' value='C'> Commissioned<br>
				<input type='radio' name='ct' id='ct' value='V'> Volunteer				 				 
			 </div>

				 <div class="form-group">
					 <label>Manager Email:</label>
					 <input type="text" name="email" id="email" size="55" maxlength="255" value="">
				 </div>
		<div class="span6">
				<div class="form-group">
					<label>Status: </label>
					<div class="row-fluid">
						<div class="span6">
							<input type="radio" name="status" id="status" value="A" checked=""> New Application<br>
							<input type="radio" name="status" id="status" value="H"> Hired<br>
							<input type="radio" name="status" id="status" value="R"> Rejected<br>
							<input type="radio" name="status" id="status" value="F"> Keep on File
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Notes:</label>
					<textarea name="notes" cols="50" rows="6"></textarea>
				</div>
				<div class="form-group">
					<label>Captcha</label>
					<!-- The captcha container -->
					<div id="captchaContainer"></div>
				</div>
                </div>
	</div>
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
    <button type="submit" class="btn btn-primary">Save changes</button>
  </div>
  </form>
</div>

<!-- Required -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/base.js"></script>

<!-- Charts -->
<script src="../js/excanvas.min.js"></script>
<script src="../js/chart.min.js" type="text/javascript"></script>

<!-- FormValidation plugin and the class supports validating Bootstrap form -->
<script src="../js/formValidation/dist/js/formValidation.js"></script>
<script src="../js/formValidation/dist/js/framework/bootstrap.js"></script>
<script src="../js/formValidation/dist/addons/reCaptcha2.js"></script>

<!-- Modal -->
<script src="../js/bootstrap-modal.js"></script>
<script src="../js/bootstrap-modalmanager.js"></script>

<script id="ajax" type="text/javascript" src="../js/modals.js" ></script> 
END
&DoNewFooter();
if ($dmo) {
	$dbh->disconnect();
}
}

sub GetStaffName {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
} 
my $name = undef;
my $sth = $dbh->prepare("SELECT * FROM StaffMaster WHERE StaffID='$id'"); 
$sth->execute() or die &ErrorMsg("ERROR IN STAFFMASTER: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
	$last = $ref->{'LastName'};
	$first = $ref->{'FirstName'};
	$mi = $ref->{'MI'};
	if ($mi) {
		$name = "$first $mi $last";
	} else {
		$name = "$first $last";
	}
}
$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $name;
}

sub GetStaffNumbers {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
} 
my ($ret,$last,$first,$mi,$name,$total,$sid) = undef;
my $sth = $dbh->prepare("SELECT * FROM StaffMaster WHERE Status <> 'A'"); 
$sth->execute() or die &ErrorMsg("ERROR IN STAFFMASTER: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
        $ret++;
        $sid = $ref->{'StaffID'};
        $total += &GetStaffAnnualCost($sid,$dbh);
}
$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $ret,$total;
}

sub GetStaffAnnualCost {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
} 
my ($ret,$last,$first,$mi,$name,$total,$sid) = undef;
my $sth = $dbh->prepare("SELECT * FROM StaffMaster WHERE StaffID='$id'"); 
$sth->execute() or die &ErrorMsg("ERROR IN STAFFMASTER: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
        $rid = $ref->{'RateID'};
        $type = $ref->{'Type'};
        $ct = $ref->{'CompType'};
        my $sth2 = $dbh->prepare("SELECT * FROM RateMaster WHERE RateID='$rid'"); 
        $sth2->execute() or die &ErrorMsg("ERROR IN RATEMASTER: $DBI::errstr");
        while (my $ref2 = $sth2->fetchrow_hashref()) {
               $amt = $ref2->{'Amount'};
        }
        $sth2->finish();
        if ($type eq "S") {                             # Calculate for Seasonal
                if ($ct eq "H") {
                     $total = 160*$amt;
                } elsif ($ct eq "S") {
                     $total = ($amt/30)*20;
                }
        } elsif ($type eq "P") {                    # Part-time
                if ($ct eq "H") {                       # Hourly
                      $total = $amt*1040;
                } elsif ($ct eq "S") {                  # Salaried
                      $total = $amt*6;
                }
        } elsif ($type eq "F") {                    # Full Time
                if ($ct eq "H") {
                      $total = $amt*2080;
                } elsif ($ct eq "S") {
                      $total = $amt*12;
                }
        }
}
$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $ret,$total;
}

sub GetStaff_sel {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
} 
my ($name,$sid,$last,$first,$mi) = undef;
my $sth = $dbh->prepare("SELECT * FROM StaffMaster ORDER BY LastName,FirstName"); 
$sth->execute() or die &ErrorMsg("ERROR IN STAFFMASTER: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
	$sid = $ref->{'StaffID'};
	$last = $ref->{'LastName'};
	$first = $ref->{'FirstName'};
	$mi = $ref->{'MI'};
	if ($mi) {
		$name = "$first $mi $last";
	} else {
		$name = "$first $last";
	}
	if ($id eq $sid) {
		print "<option value='$sid' selected>$name</option>";
	} else {
		print "<option value='$sid'>$name</option>";
	}
}
$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
}


sub GetJobInfo {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
} 
my $name = undef;
my $sth = $dbh->prepare("SELECT * FROM JobMaster WHERE JobID='$id'"); 
$sth->execute() or die &ErrorMsg("ERROR IN JOBMASTER: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
	$job = $ref->{'JobName'};
}
$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $job;
}

sub GetStaffName {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
} 
my $name = undef;
my $sth = $dbh->prepare("SELECT * FROM StaffMaster WHERE StaffID='$id'"); 
$sth->execute() or die &ErrorMsg("ERROR IN STAFFMASTER: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
	$last = $ref->{'LastName'};
	$first = $ref->{'FirstName'};
	$mi = $ref->{'MI'};
	if ($mi) {
		$name = "$first $mi $last";
	} else {
		$name = "$first $last";
	}
}
$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $name;
}

sub GetStaffInfoShort {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
} 
my $name = undef;
my $sth = $dbh->prepare("SELECT * FROM StaffMaster WHERE StaffID='$id'"); 
$sth->execute() or die &ErrorMsg("ERROR IN STAFFMASTER: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
	$last = $ref->{'LastName'};
	$first = $ref->{'FirstName'};
	$mi = $ref->{'MI'};
	$phone = $ref->{'CellPhone'};
	$email = $ref->{'Email'};
	if ($mi) {
		$name = "$first $mi $last";
	} else {
		$name = "$first $last";
	}
}
$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $name,$phone,$email;
}

sub GetStaffMember {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
} 
my $name = undef;
my $sth = $dbh->prepare("SELECT * FROM StaffMaster WHERE StaffID='$id'"); 
$sth->execute() or die &ErrorMsg("ERROR IN STAFFMASTER: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
	$last = $ref->{'LastName'};
	$first = $ref->{'FirstName'};
	$mi = $ref->{'MI'};
	$addr = $ref->{'Addr'};
	$addr2 = $ref->{'Addr2'};
	$city = $ref->{'City'};	
	$state = $ref->{'State'};
	$zip = $ref->{'Zip'};
	$home = $ref->{'HomePhone'};
	$work = $ref->{'WorkPhone'};
	$cell = $ref->{'CellPhone'};
	$email = $ref->{'Email'};
	$jobid = $ref->{'JobID'};
	$ssn = $ref->{'SSN'};
	$cit = $ref->{'Citizen'};
	$type = $ref->{'Type'};
	$hired = $ref->{'DateHired'};
	$term = $ref->{'DateTerminated'};
	$lastrev = $ref->{'LastReview'};	
	$ctype = $ref->{'CompType'};
	$etype = $ref->{'Designation'};
	$rateid = $ref->{'RateID'};	
	$notes = $ref->{'Notes'};
	if ($mi) {
		$name = "$first $mi $last";
	} else {
		$name = "$first $last";
	}
}
$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $last,$first,$mi,$name,$addr,$addr2,$city,$state,$zip,$home,$work,$cell,$email,$jobid,$ssn,$cit,$type,$hired,$term,
	$lastrev,$ctype,$etype,$rateid,$notes;
}

sub Save_Staff {
my ($id,$last,$first,$mi,$addr,$addr2,$city,$state,$zip,$home,$work,$cell,$email,$jid,
	$ssn,$cit,$type,$ctype,$etype,$rate,$notes,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my $Last = &WriteQuotes($last);
my $First = &WriteQuotes($first);
my $Addr = &WriteQuotes($addr);
my $Addr2 = &WriteQuotes($addr2);
my $City = &WriteQuotes($city);
my $Notes = &WriteQuotes($notes);
my $today = &GetMunge();
if (!$id) {
	my ($sid) = undef;
	my $sth = $dbh->prepare("SELECT * FROM StaffMaster WHERE LastName='$Last' AND FirstName='$First' AND CellPhone='$cell'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN STAFFMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$sid = $ref->{'StaffID'};
	}
	$sth->finish();
	if (!$sid) {
		my $sth2 = $dbh->prepare("INSERT INTO StaffMaster (StaffMaster.LastName,
													StaffMaster.FirstName,
													StaffMaster.MI,
													StaffMaster.Addr,
													StaffMaster.Addr2,
													StaffMaster.City,
													StaffMaster.State,
													StaffMaster.Zip,
													StaffMaster.HomePhone,
													StaffMaster.WorkPhone,
													StaffMaster.CellPhone,
													StaffMaster.Email,
													StaffMaster.JobID,
													StaffMaster.DateHired,
													StaffMaster.SSN,
													StaffMaster.Citizen,
													StaffMaster.Type,
													StaffMaster.CompType,
													StaffMaster.Designation,
													StaffMaster.RateID,
													StaffMaster.Notes)
										VALUES ('$Last',
												'$First',
												'$mi',
												'$Addr',
												'$Addr2',
												'$City',
												'$state',
												'$zip',
												'$home',
												'$work',
												'$cell',
												'$email',
												'$jid',
												'$today',
												'$ssn',
												'$cit',
												'$type',
												'$ctype',
												'$etype',
												'$rate',
												'$Notes')"); 
		$sth2->execute() or die &ErrorMsg("ERROR CREATING STAFF MEMBER RECORD: $DBI::errstr");
		$sth2->finish();
		$sth2 = $dbh->prepare("SELECT * FROM StaffMaster WHERE LastName='$Last' AND FirstName='$First' AND CellPhone='$cell'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN STAFFMASTER: $DBI::errstr");
		while (my $ref2 = $sth2->fetchrow_hashref()) {
			$id = $ref2->{'StaffID'};
		}
		$sth2->finish();
	} else {
		my $sth2 = $dbh->prepare("UPDATE StaffMaster SET StaffMaster.LastName='$Last',
													StaffMaster.FirstName='$First',
													StaffMaster.MI='$mi',
													StaffMaster.Addr='$Addr',
													StaffMaster.Addr2='$Addr2',
													StaffMaster.City='$City',
													StaffMaster.State='$state',
													StaffMaster.Zip='$zip',
													StaffMaster.HomePhone='$home',
													StaffMaster.WorkPhone='$work',
													StaffMaster.CellPhone='$cell',
													StaffMaster.Email='$email',
													StaffMaster.JobID='$jid',
													StaffMaster.SSN='$ssn',
													StaffMaster.Citizen='$cit',
													StaffMaster.Type='$type',
													StaffMaster.CompType='$ctype',
													StaffMaster.Designation='$etype',
													StaffMaster.RateID='$rate',
													StaffMaster.Notes='$Notes'
												WHERE StaffMaster.StaffID='$sid'"); 
		$sth2->execute() or die &ErrorMsg("ERROR UPDATING STAFFMASTER: $DBI::errstr");
		$sth2->finish();
		$id = $sid;
	}
} else {
		my $sth2 = $dbh->prepare("UPDATE StaffMaster SET StaffMaster.LastName='$Last',
													StaffMaster.FirstName='$First',
													StaffMaster.MI='$mi',
													StaffMaster.Addr='$Addr',
													StaffMaster.Addr2='$Addr2',
													StaffMaster.City='$City',
													StaffMaster.State='$state',
													StaffMaster.Zip='$zip',
													StaffMaster.HomePhone='$home',
													StaffMaster.WorkPhone='$work',
													StaffMaster.CellPhone='$cell',
													StaffMaster.Email='$email',
													StaffMaster.JobID='$jid',
													StaffMaster.SSN='$ssn',
													StaffMaster.Citizen='$cit',
													StaffMaster.Type='$type',
													StaffMaster.CompType='$ctype',
													StaffMaster.Designation='$etype',
													StaffMaster.RateID='$rate',
													StaffMaster.Notes='$Notes'
												WHERE StaffMaster.StaffID='$id'"); 
		$sth2->execute() or die &ErrorMsg("ERROR UPDATING STAFFMASTER: $DBI::errstr");
		$sth2->finish();
}
if ($dmo) {
	$dbh->disconnect();
}
return $id;
}

sub DeleteStaff {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
} 

my $sth = $dbh->prepare("DELETE FROM StaffMaster WHERE StaffID='$id'"); 
$sth->execute() or die &ErrorMsg("ERROR IN STAFFMASTER: $DBI::errstr");
$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
}

1;