#!/usr/bin/perl

require "dateutil.pl";
require "dbutil.pl";
require "fairlayout.pl";
require "errormaster.pl";
require "FairReports.pl";
require "Fair.pl";
require "Events.pl";

use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use DBI;

my ($Action,$id,$t,$name,$start,$end,$type) = undef;
if (param()) {
	$Action = param('a');
	$t = param('t');
	$id = param('id');
	$name = param('nm');
	$start = param('StartDate');
	$end = param('EndDate');
	$type = param('et');
	$dir = param('dir');
	$url = param('url');
	$info = param('info');
	$tel = param('phone');
	$notes = param('notes');
}

my $dbh = &ConnectDB();
if ($Action eq "main") {
	&EventMain(undef,$dbh);
} elsif ($Action eq "add") {
	&AddEvent(undef,$dbh);
} elsif ($Action eq "edit") {
	&AddEvent($id,$dbh);
} elsif ($Action eq "info") {
	&FairSummary($id,$dbh);
} elsif ($Action eq "se") {
	&SaveEvent($id,$name,$type,$start,$end,$dir,$url,$info,$tel,$notes,$dbh);
	&EventMain(undef,$dbh);
} elsif ($Action eq "del") {
	&DeleteFair($id,$dbh);
	&EventMain(undef,$dbh);
} else {
	&ErrorFairMsg("I don't know what you want me to do: [ $Action ]");
}
$dbh->disconnect();

exit;