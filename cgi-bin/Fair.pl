sub FairMain {
my ($id,$dbh) = @_;

my $bgcolor = "#FFFFFF";
&DoFairHeader(1);
print << "END";
<table width='1200'align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<table width='80%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
<tr><td>&nbsp;</td></tr>
<tr><td><table width='95%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
	<tr>
		<td width='33%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./events.cgi?a=main'>Event Management</a></b></font></div></td>
		<td width='33%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./entertain.cgi?a=test'>Entertainment Management</a></b></font></div></td>
		<td width='34%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./ticketing.cgi?a=main'>Ticketing Management</a></b></font></div></td>
	</tr>
	<tr>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./vendor.cgi?a=main'>Vendor Management</a></b></font></div></td>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./sponsor.cgi?a=main'>Sponsor Management</a></b></font></div></td>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./staff.cgi?a=main'>Personnel Management</a></b></font></div></td>
	</tr>
	<tr>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./marketing.cgi?a=main'>Marketing Management</a></b></font></div></td>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./exhibits.cgi?a=main'>Exhibits Management</a></b></font></div></td>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./setup.cgi?a=main'>System Setup/Options</a></b></font></div></td>
	</tr>
	<tr>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./reports.cgi?a=main'>Reports</a></b></font></div></td>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./tools.cgi?a=main'>Maintenance Tools</a></b></font></div></td>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./export.cgi?a=main'>Data Export/Import</a></b></font></div></td>
	</tr>
	<tr>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./property.cgi?a=main'>Property Management</a></b></font></div></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>

	</table>
</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
</table>
END
&DoFairFooter();
}

sub FairLogin {
my ($id,$dbh) = @_;

my $bgcolor = "#FFFFFF";
&DoFairHeader(1);
print << "END";
<table width='1200'align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td><form name='Login' method='post' action='./fair.cgi'>
<input type='hidden' name='a' id='a' value='check'>
<input type='hidden' name='id' id='id' value='$id'>
<table width='80%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
<tr><td>&nbsp;</td></tr>
<tr><td><table width='95%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'>Password: <input type='password' name='pwd' id='pwd' size='15' maxlength='25'>
			&nbsp;What is 9-6? <input type='text' name='ca' id='ca' size='2' maxlength='2'>
			&nbsp;<input type='submit' name='submit' value='Go'></font></div></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	</table></form>
</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
</table>
END
&DoFairFooter();
}

sub GetEventName {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($name) = undef;
	my $sth = $dbh->prepare("SELECT * FROM FairMaster WHERE FairID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN FairMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$name = $ref->{'FairName'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $name;
}

1;