sub TalkConfirm {
my ($sunday,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $today = &GetSundayFormattedDate(&GetMunge());
	my $longdate = &GetSundayFormattedDate($sunday);
 	my $sth = $dbh->prepare("SELECT * FROM TalkSchedule WHERE TalkDate='$sunday'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING TALKSCHEDULE: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$mid = $ref->{'MID'};
		$order = $ref->{'SpeakingOrder'};
		$dur = $ref->{'Duration'};
		$tid = $ref->{'TID'};
		$type = $ref->{'Type'};
		$notes = $ref->{'Notes'};
		my ($last,$first,$mi,$sex,$dob,$hphone,$iphone,$hemail,$iemail,$age,$youth) = &GetMember($mid,$dbh);
		my ($short,$long,$more) = &GetTopic($tid,$dbh);
		if ($type eq "Y") {
			$type = "youth";
		} elsif ($type eq "A") {
			$type = "adult";
		} elsif ($type eq "H") {
			$type = "High Council";
		} elsif ($type eq "B") {
			$type = "Bishopric";
		} else {
			$type = "adult";
		}
		if ($sex eq "M") {
			if ($type eq "youth") {
				$sal = "$first... ";
			} else {
				$sal = "Bro. $last;";
			}
		} else {
			if ($type eq "youth") {
				$sal = "$first... ";
			} else {
				$sal = "Sis. $last;";
			}
		}
		open (MAIL, "|/usr/sbin/sendmail -t") || die &BPErrorMsg("Error Occurred Sending Email.");
		print MAIL "to: darryl\@webspud.com\n";
		print MAIL "bcc: darryl\@hoteloptics.com\n";
		print MAIL "from: autoresponder\@donotrespond.com\n";
		print MAIL "Subject: You have a talk coming up!\n";
		print MAIL "Content-type:text/html\n\n";
		print MAIL "<html><head><title>Talk Reminder</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
		print MAIL "<body bgcolor='#2a3753' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'>";
		print MAIL "<table width='800' border='0' cellspacing='10' cellpadding='1' align='center' bgcolor='#FFFFFF'>";
		print MAIL "<tr><td><div align='center'><img src='http://barksdaleitsolutions.com/images/church_logo.gif' width='200'></div></td></tr>";
		print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>$today</font></div></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>$sal</font></td></tr>";
		print MAIL "<tr height='25'><td><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>As you now know, you have been assigned to speak in Sacrament Meeting on $longdate.  We wanted to you give you a bit more information on how best 
		to prepare for this assignment.</font></td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>You have been assigned to speak on <b>$short</b>. ";
		if ($long) {
			print MAIL "In the way of additional information, $long";
		}
		print MAIL "</font></b></td></tr>";
		if ($more) {
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>$more</font></td></tr>";
		}
		if ($notes) {
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>$notes</font></td></tr>";
		}
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>The assignment to speak in Sacrament Meeting is a sacred and profoundly important responsibility. 
					Therefore, we would ask that you approach this assignment with the humility and prayerfulness that it deserves.  Fasting before giving a talk is not improper or inappopriate, as we seek to 
					obtain guidance and direction from the Lord on this topic.</font></td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>We appreciate your willingness to accept this assignment, and look forward to hearing your message.</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>Sincerely,</font></td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>24th Ward Bishopric</font></td></tr>";
		print MAIL "</table></body></html>";
		close (MAIL);		
	}
	$sth->finish();
	&NoteConfirm($sunday,$dbh);
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub SendProgram {
my ($sunday,$dbh) = @_;
	
 	my ($i,$didmyown,$osong,$ssong,$rsong,$csong,$open,$close,$youth,$adult) = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $today = &GetSundayFormattedDate(&GetMunge());
	my $speaker = \@Speaker;
	my $date = &GetSundayFormattedDate($sunday);
	my ($ac,$ap,$pemail) = &GetSettings($dbh);
	my $sth = $dbh->prepare("SELECT * FROM Prayers WHERE PDate='$sunday'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING MemberMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		($olast,$ofirst,$omi,$osex,$odob,$ohphone,$oiphone,$ohemail,$oiemail,$oage,$oyouth) = &GetMember($ref->{'Opening'},$dbh);
		($clast,$cfirst,$cmi,$csex,$cdob,$chphone,$ciphone,$chemail,$ciemail,$cage,$cyouth) = &GetMember($ref->{'Closing'},$dbh);
	}
	$sth->finish();
	if ($olast) {
		if ($omi) {
			$open = "$ofirst $omi $olast";
		} else {
			$open = "$ofirst $olast";
		}
	} else {
		$open = "<i>(To Be Announced)</i>";
	}
	if ($clast) {
		if ($cmi) {
			$close = "$cfirst $cmi $clast";
		} else {
			$close = "$cfirst $clast";
		}
	} else {
		$close = "<i>(To Be Announced)</i>";
	}
	my $sth = $dbh->prepare("SELECT * FROM Music WHERE MusicType='O' AND MDate='$sunday'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING MUSIC: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$source = $ref->{'Source'};
		$otitle = $ref->{'Title'};
		$ogroup = $ref->{'Group'};
		if ($source eq "H") {
			$onum = $ref->{'Page'};
		} 
	}
	$sth->finish();
	if ($onum) {
		$osong = "Page $onum: $otitle";
	} elsif ($source eq "C") {
		$osong = "Ward Choir: $otitle";
	} elsif ($source eq "O") {
		$osong = "$otitle <i>($ogroup)</i>";
	}
	$sth = $dbh->prepare("SELECT * FROM Music WHERE MusicType='S' AND MDate='$sunday'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING MUSIC: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$source = $ref->{'Source'};
		$stitle = $ref->{'Title'};
		$sgroup = $ref->{'Group'};
		if ($source eq "H") {
			$snum = $ref->{'Page'};
		} 
	}
	$sth->finish();
	 $ssong = undef;
	if ($snum) {
		$ssong = "Page $snum: $stitle";
	} elsif ($source eq "C") {
		$ssong = "Ward Choir: $stitle";
	} elsif ($source eq "O") {
		$ssong = "$stitle <i>($sgroup)</i>";
	}
	$sth = $dbh->prepare("SELECT * FROM Music WHERE MusicType='R' AND MDate='$sunday'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING MUSIC: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$source = $ref->{'Source'};
		$rtitle = $ref->{'Title'};
		$rgroup = $ref->{'Group'};
		if ($source eq "H") {
			$rnum = $ref->{'Page'};
		} 
	}
	$sth->finish();
	$rsong = undef;
	if ($rnum) {
		$rsong = "Page $rnum: $rtitle";
	} elsif ($source eq "C") {
		$rsong = "Ward Choir: $rtitle";
	} elsif ($source eq "O") {
		$rsong = "$rtitle <i>($rgroup)</i>";
	}
	$sth = $dbh->prepare("SELECT * FROM Music WHERE MusicType='C' AND MDate='$sunday'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING MUSIC: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$source = $ref->{'Source'};
		$ctitle = $ref->{'Title'};
		$cgroup = $ref->{'Group'};
		if ($source eq "H") {
			$cnum = $ref->{'Page'};
		} 
	}
	$sth->finish();
	$csong = undef;
	if ($cnum) {
		$csong = "Page $cnum: $ctitle";
	} elsif ($source eq "C") {
		$csong = "Ward Choir: $ctitle";
	} elsif ($source eq "O") {
		$csong = "$ctitle <i>($cgroup)</i>";
	}
	open (MAIL, "|/usr/sbin/sendmail -t") || die &BPErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: $pemail\n";
	print MAIL "bcc: darryl\@hoteloptics.com\n";
	print MAIL "from: autoresponder\@your24thward.com\n";
	print MAIL "Subject: Sacrament Meeting Program for $date\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>Program Talks</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#2a3753' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'>";
	print MAIL "<table width='800' border='0' cellspacing='10' cellpadding='1' align='center' bgcolor='#FFFFFF'>";
	print MAIL "<tr><td><div align='center'><img src='http://barksdaleitsolutions.com/images/church_logo.gif' width='150'></div></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>$today</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>Greetings!</font></td></tr>";
	print MAIL "<tr height='25'><td><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>The Sacrament Meeting Program for this next Sunday are as follows: </font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><table width='800' border='0' cellspacing='10' cellpadding='1' align='center' bgcolor='#FFFFFF'>";
	print MAIL "<tr><td width='32%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>Opening Song: </font></div></td>";
	print MAIL "<td width='3%'>&nbsp;</td>";
	print MAIL "<td width='65%'><div align='left'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'><b>$osong</b></font></div></td></tr>";
	print MAIL "<tr><td width='32%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>Opening Prayer: </font></div></td>";
	print MAIL "<td width='3%'>&nbsp;</td>";
	print MAIL "<td width='65%'><div align='left'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'><b>$open</b></font></div></td></tr>";
	print MAIL "<tr><td width='32%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>Sacrament Song: </font></div></td>";
	print MAIL "<td width='3%'>&nbsp;</td>";
	print MAIL "<td width='65%'><div align='left'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'><b>$ssong</b></font></div></td></tr>";
	$sth = $dbh->prepare("SELECT * FROM TalkSchedule WHERE TalkDate='$sunday' AND Type<>'A' ORDER BY SpeakingOrder"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING TALKS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		($ylast,$yfirst,$ymi,$ysex,$ydob,$yhphone,$yiphone,$yhemail,$yiemail,$yage,$yyouth) = &GetMember($ref->{'MID'},$dbh);
		($short,$long,$comments) = &GetTopic($ref->{'TID'},$dbh);
		if ($ylast) {
			if ($ymi) {
				$youth = "$yfirst $ymi $ylast";
			} else {
				$youth = "$yfirst $ylast";
			}
		} else {
			$youth = "<i>(To Be Announced)</i>";
		}
		print MAIL "<tr><td width='32%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>Youth Speaker: </font></div></td>";
		print MAIL "<td width='3%'>&nbsp;</td>";
		print MAIL "<td width='65%'><div align='left'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'><b>$youth - \"$short\"</b></font></div></td></tr>";
	}
	$sth->finish();
	$sth = $dbh->prepare("SELECT * FROM TalkSchedule WHERE TalkDate='$sunday' AND Type='A' AND BeforeRest='Y' ORDER BY SpeakingOrder"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING TALKS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		($alast,$afirst,$ami,$asex,$adob,$ahphone,$aiphone,$ahemail,$aiemail,$aage,$ayouth) = &GetMember($ref->{'MID'},$dbh);
		($short,$long,$comments) = &GetTopic($ref->{'TID'},$dbh);
		if ($alast) {
			if ($ami) {
				$adult = "$afirst $ami $alast";
			} else {
				$adult = "$afirst $alast";
			}
		} else {
			$adult = "<i>(To Be Announced)</i>";
		}
		print MAIL "<tr><td width='32%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>Speaker: </font></div></td>";
		print MAIL "<td width='3%'>&nbsp;</td>";
		print MAIL "<td width='65%'><div align='left'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'><b>$adult - \"$short\"</b></font></div></td></tr>";
	}
	$sth->finish();
	if ($rsong) {
		print MAIL "<tr><td width='32%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>Rest Song: </font></div></td>";
		print MAIL "<td width='3%'>&nbsp;</td>";
		print MAIL "<td width='65%'><div align='left'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'><b>$rsong</b></font></div></td></tr>";
	}
	$sth = $dbh->prepare("SELECT * FROM TalkSchedule WHERE TalkDate='$sunday' AND Type='A' AND BeforeRest <> 'Y' ORDER BY SpeakingOrder"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING TALKS: $DBI::errstr");
	$adult = undef;
	while (my $ref = $sth->fetchrow_hashref()) {
		($alast,$afirst,$ami,$asex,$adob,$ahphone,$aiphone,$ahemail,$aiemail,$aage,$ayouth) = &GetMember($ref->{'MID'},$dbh);
		($short,$long,$comments) = &GetTopic($ref->{'TID'},$dbh);
		if ($alast) {
			if ($ami) {
				$adult = "$afirst $ami $alast";
			} else {
				$adult = "$afirst $alast";
			}
		} else {
			$adult = "<i>(To Be Announced)</i>";
		}
		print MAIL "<tr><td width='32%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>Speaker: </font></div></td>";
		print MAIL "<td width='3%'>&nbsp;</td>";
		print MAIL "<td width='65%'><div align='left'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'><b>$adult - \"$short\"</b></font></div></td></tr>";
	}
	$sth->finish();
	print MAIL "<tr><td width='32%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>Closing Song: </font></div></td>";
	print MAIL "<td width='3%'>&nbsp;</td>";
	print MAIL "<td width='65%'><div align='left'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'><b>$csong</b></font></div></td></tr>";
	print MAIL "<tr><td width='32%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>Closing Prayer: </font></div></td>";
	print MAIL "<td width='3%'>&nbsp;</td>";
	print MAIL "<td width='65%'><div align='left'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'><b>$close</b></font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>Sincerely,</font></td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'>24th Ward Bishopric</font></td></tr>";
	print MAIL "</table></body></html>";
	close (MAIL);		
	&NoteProgram($sunday,$dbh);
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub UserWelcome {
my ($id,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
 	my $sth = $dbh->prepare("SELECT * FROM UserMaster WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING UserMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$uid = $ref->{'UserID'};
		$first = $ref->{'FirstName'};
		$email = $ref->{'Email'};
		open (MAIL, "|/usr/sbin/sendmail -t") || die &BPErrorMsg("Error Occurred Sending Email.");
		print MAIL "to: $email\n";
		print MAIL "bcc: darryl\@edistrict.net\n";
		print MAIL "from: auto\@depotools.com\n";
		print MAIL "Subject: DepoTools hasn't forgotten about you!\n";
		print MAIL "Content-type:text/html\n\n";
		print MAIL "<html><head><title>Welcome to DepoTools!</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
		print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
		print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Greetings, $first!</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>I just wanted to reach out and welcome you to <font color='#FF6600'><b>Depo<font color='#999999'>Tools</b>.  <font color='#000000'> We are excited 
			about this system, and hope that it will be an invaluable tool in your business.</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Please don't hesitate to let us know if you ever encounter a problem, or have a suggestion for improvement.  And please... tell your other court reporting 
			friends about us!</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Sincerely,</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#FF6633'><b>Depo<font color='#999999'>Tools</font></b><font color='#000000'> Management Team</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2' color='#333333'>If you have received this email in error,or would like to opt out of future mailings, please click <a href='http://www.depotools.com/cgi-bin/users.cgi?a=remove&id=$uid'>HERE</a>.</font></div></td></tr>>";
		print MAIL "</table></body></html>";
		close (MAIL);		
	}
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub LawSendInvite {
my ($id,$fname,$cfirst,$clast,$ctype,$cemail,$dbh) = @_;	

 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
 	my $sth = $dbh->prepare("SELECT * FROM LawMaster WHERE LawID='$id'"); 
	$sth->execute() or die &ErrorMsg("LawSendInvite: ERROR GETTING LawMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$last = $ref->{'ContactLast'};
		$first = $ref->{'ContactFirst'};
		$email = $ref->{'Email'};
		$firm = $ref->{'FirmName'};
		open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
		print MAIL "to: $email\n";
		print MAIL "bcc: darryl\@edistrict.net\n";
		print MAIL "from: donotreply\@depotools.com\n";
		print MAIL "Subject: Hey, $cfirst, this is $first at $firm...\n";
		print MAIL "Content-type:text/html\n\n";
		print MAIL "<html><head><title>Welcome to DepoTools!</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
		print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
		print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Hey, $cfirst!</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>I just wanted to reach out and personally invite you to join me in using <font color='#FF6600'><b>Depo<font color='#999999'>Tools</b>.  <font color='#000000'> This system is 
			awesome.  :)   It's an online productivity tools to streamline and automate the workflow for depositions, hearings, etc. from our office to the court reporter or videographer, and back again.</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Try it out, and let me know what you think!    You can sign up <a href='http://www.depotools.com'>here</a>.</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Thanks!</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$first $last</font></td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$firm</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "</table></body></html>";
		close (MAIL);		
	}
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub UserSendInvite {
my ($id,$fname,$cfirst,$clast,$ctype,$cemail,$dbh) = @_;	

 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
 	my $sth = $dbh->prepare("SELECT * FROM UserMaster WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("UserSendInvite: ERROR GETTING UserMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$last = $ref->{'LastName'};
		$first = $ref->{'FirstName'};
		$email = $ref->{'Email'};
		open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
		print MAIL "to: $email\n";
		print MAIL "bcc: darryl\@edistrict.net\n";
		print MAIL "from: donotreply\@depotools.com\n";
		print MAIL "Subject: Hey, $cfirst, this is $first...\n";
		print MAIL "Content-type:text/html\n\n";
		print MAIL "<html><head><title>Welcome to DepoTools!</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
		print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
		print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Hey, $cfirst!</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>I just wanted to reach out and personally invite you to join me in using <font color='#FF6600'><b>Depo<font color='#999999'>Tools</b>.  <font color='#000000'> This system is 
			awesome.  :)   It's an online productivity tools to streamline and automate the workflow for depositions, hearings, etc. from our office to the court reporter or videographer, and back again.</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Try it out, and let me know what you think!    You can sign up <a href='http://www.depotools.com'>here</a>.</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Thanks!</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$first $last</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "</table></body></html>";
		close (MAIL);		
	}
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub FirmUserSendInvite {
my ($id,$fid,$dbh) = @_;	

 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
	my ($fname,$faddr,$faddr2,$fcity,$fstate,$fzip,$fcountry,$ffirst,$flast,$femail,$furl,$fphone,$fstatus,$fpay,$comments) = &GetFirmInfo($fid,$dbh);
 	my $sth = $dbh->prepare("SELECT * FROM UserMaster WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("FirmUserSendInvite: ERROR GETTING UserMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$last = $ref->{'LastName'};
		$first = $ref->{'FirstName'};
		$email = $ref->{'Email'};
		if ($email) {
			open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
			print MAIL "to: $email\n";
			print MAIL "bcc: darryl\@edistrict.net\n";
			print MAIL "from: donotreply\@depotools.com\n";
			print MAIL "Subject: Hey, $first, this is $ffirst...\n";
			print MAIL "Content-type:text/html\n\n";
			print MAIL "<html><head><title>Welcome to DepoTools!</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
			print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
			print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Hey, $first!</font></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>We just added you as one of our reporters, and just wanted to reach out and personally invite you to join us in using <font color='#FF6600'><b>Depo<font color='#999999'>Tools</b>.  <font color='#000000'> This system is 
				awesome.  :)   It's an online productivity tools to streamline and automate the workflow for depositions, hearings, etc. from our office to the court reporter or videographer, and back again.</font></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Try it out, and let me know what you think!    You can create an account <a href='http://www.mydepotools.com/cgi-bin/users.cgi?a=newfirmadd&u=$id&fid=$fid'>here</a>.</font></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Thanks!</font></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$ffirst $flast</font></td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$fname</font></td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$fphone</font></td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$furl</font></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "</table></body></html>";
			close (MAIL);	
		} else {
			&ErrorMsg("No Email Address Found... could not send invite.");
			exit;
		}	
	}
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub FirmSendInvite {
my ($id,$fname,$cfirst,$clast,$ctype,$cemail,$dbh) = @_;	

 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
 	my $sth = $dbh->prepare("SELECT * FROM Firms WHERE FirmID='$id'"); 
	$sth->execute() or die &ErrorMsg("FirmSendInvite: ERROR GETTING FirmMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$last = $ref->{'RepLast'};
		$first = $ref->{'RepFirst'};
		$email = $ref->{'RepEmail'};
		$firm = $ref->{'Name'};
		open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
		print MAIL "to: $email\n";
		print MAIL "bcc: darryl\@edistrict.net\n";
		print MAIL "from: donotreply\@depotools.com\n";
		print MAIL "Subject: Hey, $cfirst, this is $first at $firm...\n";
		print MAIL "Content-type:text/html\n\n";
		print MAIL "<html><head><title>Welcome to DepoTools!</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
		print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
		print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Hey, $cfirst!</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>I just wanted to reach out and personally invite you to join me in using <font color='#FF6600'><b>Depo<font color='#999999'>Tools</b>.  <font color='#000000'> This system is 
			awesome.  :)   It's an online productivity tools to streamline and automate the workflow for depositions, hearings, etc. from our office to the court reporter or videographer, and back again.</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Try it out, and let me know what you think!  You can sign up <a href='http://www.depotools.com'>here</a>.</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Thanks!</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$first $last</font></td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$firm</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "</table></body></html>";
		close (MAIL);		
	}
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub FirmInvite {
my ($id,$fid,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
 	my $sth = $dbh->prepare("SELECT * FROM UserMaster WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING UserMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$uid = $ref->{'UserID'};
		$first = $ref->{'FirstName'};
		$email = $ref->{'Email'};
		open (MAIL, "|/usr/sbin/sendmail -t") || die &BPErrorMsg("Error Occurred Sending Email.");
		print MAIL "to: $email\n";
		print MAIL "bcc: darryl\@edistrict.net\n";
		print MAIL "from: auto\@depotools.com\n";
		print MAIL "Subject: DepoTools hasn't forgotten about you!\n";
		print MAIL "Content-type:text/html\n\n";
		print MAIL "<html><head><title>Welcome to DepoTools!</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
		print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
		print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Greetings, $first!</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>I just wanted to personally reach out and welcome you to <font color='#FF6600'><b>Depo<font color='#999999'>Tools</b>.  <font color='#000000'> We are excited 
			about this system, and hope that it will be an invaluable tool in your business.</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Please don't hesitate to let us know if you ever encounter a problem, or have a suggestion for improvement.  And please... tell your other court reporting 
			friends about us!</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Sincerely,</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#FF6633'><b>Depo<font color='#999999'>Tools</font></b><font color='#000000'> Management Team</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2' color='#333333'>If you have received this email in error,or would like to opt out of future mailings, please click <a href='http://www.depotools.com/cgi-bin/users.cgi?a=remove&id=$uid'>HERE</a>.</font></div></td></tr>>";
		print MAIL "</table></body></html>";
		close (MAIL);		
	}
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub LawInvite {
my ($id,$fid,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
 	my $sth = $dbh->prepare("SELECT * FROM Firms WHERE FirmID='$fid'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING UserMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$fname = $ref->{'Name'};
		$femail = $ref->{'Email'};
	}
	$sth->finish();
 	$sth = $dbh->prepare("SELECT * FROM LawMaster WHERE LawID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING UserMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$name = $ref->{'FirmName'};
		$email = $ref->{'Email'};
		open (MAIL, "|/usr/sbin/sendmail -t") || die &BPErrorMsg("Error Occurred Sending Email.");
		print MAIL "to: darryl\@depotools.com\n";
#		print MAIL "to: $email\n";
#		print MAIL "bcc: darryl\@depotools.com\n";
		print MAIL "from: auto\@depotools.com\n";
		print MAIL "Subject: $fname\n";
		print MAIL "Content-type:text/html\n\n";
		print MAIL "<html><head><title>DepoTools</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
		print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
		print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Greetings!</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>I just wanted to personally reach out and invite you to explore <font color='#FF6600'><b>Depo<font color='#999999'>Tools</b>, since we are now using 
			it to manage the booking and scheduling process for the jobs we do for our clients.  <font color='#000000'> We are excited 
			about this system, and hope that it will be an invaluable tool in facilitating our working relationship.  And did I mention that it was FREE?</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>You can register and login at <a href='http://www.depotools.com'>http://www.depotools.com</a>.  
			Please don't hesitate to let us know if you ever encounter a problem, or have a suggestion for improvement.  And please... tell your other legal colleagues about this!</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Sincerely,</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3'>$fname</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2' color='#333333'>If you have received this email in error,or would like to opt out of future mailings, please click <a href='http://www.depotools.com/cgi-bin/users.cgi?a=remove&id=$uid'>HERE</a>.</font></div></td></tr>>";
		print MAIL "</table></body></html>";
		close (MAIL);		
	}
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
}
	
sub RateSheetRequest {
my $dbh = shift;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
 	my $sth = $dbh->prepare("SELECT * FROM UserMaster"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING UserMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$uid = $ref->{'UserID'};
		$first = $ref->{'FirstName'};
		$email = $ref->{'Email'};
		open (MAIL, "|/usr/sbin/sendmail -t") || die &BPErrorMsg("Error Occurred Sending Email.");
		print MAIL "to: $email\n";
		print MAIL "bcc: darryl\@edistrict.net\n";
		print MAIL "from: auto\@depotools.com\n";
		print MAIL "Subject: We Need Your Rate Sheets!\n";
		print MAIL "Content-type:text/html\n\n";
		print MAIL "<html><head><title>Welcome to DepoTools!</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
		print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
		print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Greetings, $first!</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>We're trying to complete our work on the Invoicing feature of <font color='#FF6600'><b>Depo<font color='#999999'>Tools</b>.  <font color='#000000'> In order to test it, we need 
		to find some live data that we can put in to test the Rates function... so we are asking for any rate sheets you have from existing firms or your own rate sheets as a reporter.  They will ONLY be used by the development team to average rates and identify commonly used 
		rate designations and breakdowns.  We will keep whatever you send us in the strictest of confidence... all rate sheets will be destroyed after use, and under no circumstances will they be used in the live product, nor will they be published.</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Thank you again for being willing to help us... we should be ready to begin testing on Monday, July 1.</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Sincerely,</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#FF6633'><b>Depo<font color='#999999'>Tools</font></b><font color='#000000'> Management Team</font></td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td>&nbsp;</td></tr>";
		print MAIL "<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2' color='#333333'>If you have received this email in error,or would like to opt out of future mailings, please click <a href='http://www.depotools.com/cgi-bin/users.cgi?a=remove&id=$uid'>HERE</a>.</font></div></td></tr>>";
		print MAIL "</table></body></html>";
		close (MAIL);		
	}
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub Darryl_Reporter {
my ($id,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
 	my $sth = $dbh->prepare("SELECT * FROM UserMaster WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING UserMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$last = $ref->{'LastName'};
		$first = $ref->{'FirstName'};
		$mi = $ref->{'MI'};
		$addr = $ref->{'Addr'};
		$addr2 = $ref->{'Addr2'};
		$city = $ref->{'City'};
		$state = $ref->{'State'};
		$zip = $ref->{'Zip'};
		$country = $ref->{'Country'};
		$email = $ref->{'Email'};
		$phone = $ref->{'Phone'};
		$rdate = &GetFormattedDate($ref->{'RegisterDate'});
		$time = $ref->{'RegisterTime'};
		$comments = $ref->{'Comments'};
		$prereg = $ref->{'PreReg'};
		$IP = $ref->{'IP'};
		$type = $ref->{'Type'};
		$rid = $ref->{'RoleID'};
		$status = $ref->{'Status'};
		$tester = $ref->{'Tester'};
		$agree = $ref->{'Agree'};
		$ws = $ref->{'WitnessSpecifics'};
		$ms = $ref->{'MediatorSpecifics'};
		$uid = $ref->{'UID'};
		$pwd = $ref->{'PWD'};
	}
	$sth->finish();
	open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: darryl\@depotools.com\n";
	print MAIL "from: auto\@depotools.com\n";
	print MAIL "Subject: New DepoTools Reporter Registration!\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>DepoTools</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>The following user just registered:</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><table width='90%' cellpadding='0' cellspacing='0' border='0' align='left'>";
	print MAIL "<tr><td width='20%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Name: </font></div></td>";
	print MAIL "<td width='2%'>&nbsp;</td>";
	print MAIL "<td width='78%'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$first $mi $last</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Addr: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$addr $addr2 $city, $state  $zip  ($country)</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Email: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$email</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Phone: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$phone</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Registered: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$rdate $time</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Pre-Reg: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$prereg</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>IP Address: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$IP</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Type: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$type</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Role: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$rid</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Status: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$status</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Tester: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$tester</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Agree: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$agree</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Witness Specifics: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$ws</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Mediator Specifics: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$ms</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Comments: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$comments</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Login: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$uid/$pwd</b></font></td></tr>";		
	print MAIL "</table></td></tr>";
	print MAIL "</table></body></html>";
	close (MAIL);		
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub Darryl_Firm {
my ($id,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
 	my $sth = $dbh->prepare("SELECT * FROM Firms WHERE FirmID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING Firms: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$name = $ref->{'Name'};
		$addr = $ref->{'Addr'};
		$addr2 = $ref->{'Addr2'};
		$city = $ref->{'City'};
		$state = $ref->{'State'};
		$zip = $ref->{'Zip'};
		$country = $ref->{'Country'};
		$first = $ref->{'RepFirst'};
		$last = $ref->{'RepLast'};
		$email = $ref->{'RepEmail'};
		$rdate = &GetFormattedDate($ref->{'RegisterDate'});
		$time = $ref->{'RegisterTime'};
		$url = $ref->{'URL'};
		$phone = $ref->{'RepPhone'};
		$status = $ref->{'Status'};
		$pay = $ref->{'Pay'};
		$comments = $ref->{'Comments'};
	}
	$sth->finish();
	open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: darryl\@depotools.com\n";
	print MAIL "from: auto\@depotools.com\n";
	print MAIL "Subject: New DepoTools FIRM Registration!\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>DepoTools</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>The following FIRM just registered:</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><table width='90%' cellpadding='0' cellspacing='0' border='0' align='left'>";
	print MAIL "<tr><td width='20%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Name: </font></div></td>";
	print MAIL "<td width='2%'>&nbsp;</td>";
	print MAIL "<td width='78%'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$name</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Addr: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$addr $addr2 $city, $state  $zip  ($country)</b></font></td></tr>";	
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Firm Rep: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$first $last</b></font></td></tr>";	
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Email: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$email</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Phone: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$phone</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Registered: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$rdate $time</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>URL: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$url</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Status: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$status</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Pay: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$pay</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Comments: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$comments</b></font></td></tr>";		
	print MAIL "</table></td></tr>";
	print MAIL "</table></body></html>";
	close (MAIL);		
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub Welcome_Law {
my ($id,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
 	my $sth = $dbh->prepare("SELECT * FROM LawMaster WHERE LawID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING LawMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$name = $ref->{'FirmName'};
		$addr = $ref->{'Addr'};
		$addr2 = $ref->{'Addr2'};
		$city = $ref->{'City'};
		$state = $ref->{'State'};
		$zip = $ref->{'Zip'};
		$country = $ref->{'Country'};
		$phone = $ref->{'Phone'};
		$last = $ref->{'ContactLast'};
		$first = $ref->{'ContactFirst'};
		$rdate = &GetFormattedDate($ref->{'RegisterDate'});
		$time = $ref->{'RegisterTime'};
		$email = $ref->{'Email'};
		$url = $ref->{'URL'};
		$status = $ref->{'Status'};
		$pr = $ref->{'PayRating'};
		$uid = $ref->{'UserID'};
		$pwd = $ref->{'Pwd'};
		$comments = $ref->{'Comments'};
	}
	$sth->finish();
	open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: darryl\@depotools.com\n";
	print MAIL "from: auto\@depotools.com\n";
	print MAIL "Subject: Welcome to DepoTools!\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>DepoTools</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Thank you for registering with us!</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>We are excited to show you how you can streamline your deposition organizing and 
	scheduling process!  Need a deposition reporter?  Coverage for a hearing?  Videographer?  Interpreter?  Expert Witnesses?  Arbitrators/Mediators?  Legal Photography/Courtroom Graphics?  We've got you covered.  Choose a deposition firm or independent court reporter, 
	and they will be notified by email automatically, and can accept or pass on your deposition right there in their email.  Once they accept, your job shows up automagically in their Job List and Master Schedule.  You can 
	upload case captions and word lists for them to download, and they will be able to invoice you through our system automatically! </font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Our unique <b><i>OneClick</i></b> feature allows you to book court reporting firms with a click of a button, eliminating time-consuming rounds of 'telephone tag.'  Our control panel for attorneys allows you 
	to track all of your depositions from one spot.</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>The best news of all is that our system is <b>FREE</b> for attorneys and law firms.  Check out what we can do for you!</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Sincerely,</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>The </font><font face='Arial, Helvetica, sans-serif' size='4' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Team</font></td></tr>";
	print MAIL "</table></body></html>";
	close (MAIL);		
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub Welcome_Reporter {
my ($id,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
 	my $sth = $dbh->prepare("SELECT * FROM UserMaster WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING LawMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$first = $ref->{'FirstName'};
		$email = $ref->{'Email'};
	}
	$sth->finish();
	open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: darryl\@depotools.com\n";
	print MAIL "from: auto\@depotools.com\n";
	print MAIL "Subject: Welcome to DepoTools!\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>DepoTools</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Dear $first;</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Thank you for registering with us!</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>We are excited to show you how you can streamline your workflow!  Keep your schedule, archive 
	an unlimited number of transcripts (and even audio files) with our <b><i>TranscriptCloud</i></b>, manage your jobs, and invoice automatically for the work you've done, product reports, and see how much you 
	have coming in from where at any given time!</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Our unique <b><i>OneClick</i></b> feature allows both court reporting firms and law firms to book you with a click of a button for those juicy all-day multiple copy sale expedites, eliminating time-consuming rounds of 'telephone tag.'  Our control panel for reporters allows you 
	to track all of your depositions from one spot.</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>The best news of all is that our system is <b>FREE</b> for attorneys and law firms.  Check out what we can do for you!</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Sincerely,</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>The </font><font face='Arial, Helvetica, sans-serif' size='4' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Team</font></td></tr>";
	print MAIL "</table></body></html>";
	close (MAIL);		
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub Darryl_Law {
my ($id,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
 	my $sth = $dbh->prepare("SELECT * FROM LawMaster WHERE LawID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING LawMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$name = $ref->{'FirmName'};
		$addr = $ref->{'Addr'};
		$addr2 = $ref->{'Addr2'};
		$city = $ref->{'City'};
		$state = $ref->{'State'};
		$zip = $ref->{'Zip'};
		$country = $ref->{'Country'};
		$phone = $ref->{'Phone'};
		$last = $ref->{'ContactLast'};
		$first = $ref->{'ContactFirst'};
		$rdate = &GetFormattedDate($ref->{'RegisterDate'});
		$time = $ref->{'RegisterTime'};
		$email = $ref->{'Email'};
		$url = $ref->{'URL'};
		$status = $ref->{'Status'};
		$pr = $ref->{'PayRating'};
		$uid = $ref->{'UserID'};
		$pwd = $ref->{'Pwd'};
		$comments = $ref->{'Comments'};
	}
	$sth->finish();
	open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: darryl\@depotools.com\n";
	print MAIL "from: auto\@depotools.com\n";
	print MAIL "Subject: New DepoTools LAWYER Registration!\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>DepoTools</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>The following LAWYER just registered:</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><table width='90%' cellpadding='0' cellspacing='0' border='0' align='left'>";
	print MAIL "<tr><td width='20%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Name: </font></div></td>";
	print MAIL "<td width='2%'>&nbsp;</td>";
	print MAIL "<td width='78%'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$name</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Addr: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$addr $addr2 $city, $state  $zip  ($country)</b></font></td></tr>";	
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Contact: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$first $last</b></font></td></tr>";	
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Email: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$email</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Phone: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$phone</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Registered: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$rdate $time</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>URL: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$url</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Status: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$status</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Pay Rating: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$pr</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Comments: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$comments</b></font></td></tr>";		
	print MAIL "</table></td></tr>";
	print MAIL "</table></body></html>";
	close (MAIL);		
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub NewJobNotification {
my ($firm,$jid,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
	my ($lid,$fid,$uid,$vid,$jn,$loc,$addr,$addr2,$city,$state,$zip,$cn,$cap,$dep,$de,$da,$jd,$jt,$dt,$dd,$dti,$dp,$pgs,$cop,$amt,$notes,$status,$type,$svcs,$start,$end,$cc,$wl,$contact,
		$email,$phone,$extra,$meal,$park,$tel,$rough,$expert,$after,$we,$exp,$which,$dur,$rt,$ln,$interp,$lang,$gi,$urge,$other) = &GetJobMaster($jid,$dbh);
	my $date = &GetFormattedDate($jd);
	my $time = &GetFormattedTime($jt);
	my $tdesc = &GetJobType($type,$dbh);
	my $sth = $dbh->prepare("SELECT * FROM LawMaster WHERE LawID='$lid'"); 
	$sth->execute() or die &ErrorMsg("NewJobNotification: ERROR GETTING LAWMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$lname = $ref->{'FirmName'};
		$clast = $ref->{'ContactLast'};
		$cfirst = $ref->{'ContactFirst'};
	}
	$sth->finish();
 	open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: darryl\@depotools.com\n";
	print MAIL "from: auto\@depotools.com\n";
	print MAIL "Subject: New Job Submission: $lname - $tdesc for $date at $time\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>DepoTools</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Job Details:</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><table width='90%' cellpadding='0' cellspacing='0' border='0' align='left'>";
	print MAIL "<tr><td width='20%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Law Firm: </font></div></td>";
	print MAIL "<td width='2%'>&nbsp;</td>";
	print MAIL "<td width='78%'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$lname</b> ($cfirst $clast)</font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Job: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$tdesc</b></font></td></tr>";	
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Date/Time: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$date at $time</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Services Needed: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	if ($svcs eq "R") {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Reporter Only</b></font></td></tr>";
	} elsif ($svcs eq "V") {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Videographer Only</b></font></td></tr>";
	} else {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Reporter and Videographer</b></font></td></tr>";
	}
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Location: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	if ($loc eq "R") {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Reporting Firm Offices</b></font></td></tr>";
	} elsif ($loc eq "L") {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Law Firm Offices</b></font></td></tr>";
	} elsif ($loc eq "O") {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$addr $addr2 $city, $state  $zip</b></font></td></tr>";
	} else {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>TBD</b></font></td></tr>";
	}
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Location Contact: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$contact</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Location Phone: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$phone</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Location Email: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$email</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Case #: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$cn</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Case Caption: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$cap</b></font></td></tr>";	
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Deponent/Witness: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$dep</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Job Details: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td>&nbsp;</td></tr>";
	if ($extra eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Allow extra time before job begins.</b></font></td></tr>";	
	}
	if ($park eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Parking will be validated.</b></font></td></tr>";	
	} else {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Parking may NOT be validated.</b></font></td></tr>";			
	}
	if ($meal eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>A meal will be provided.</b></font></td></tr>";	
	}
	if ($tel eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>At least one party will appear by telephone.</b></font></td></tr>";			
	}
	if ($rough eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>A rough will be required.</b></font></td></tr>";			
	}
	if ($expert eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Deponent/Witness is an Expert Witness.</b></font></td></tr>";			
	}
	if ($after eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#cc0000'><b>This job may go later than 6:00pm!</b></font></td></tr>";			
	}
	if ($we eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#CC0000'><b>This is a weekend/holiday job!</b></font></td></tr>";			
	}
	if ($exp eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This job is anticipated to be expedited.</b></font></td></tr>";			
	}
	if ($which eq "W") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is a Worker's Comp $tdesc.</b></font></td></tr>";			
	} elsif ($which eq "M") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is a Medical Malpractice $tdesc.</b></font></td></tr>";	
	} elsif ($which eq "P") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is a Personal Injury $tdesc.</b></font></td></tr>";		
	} elsif ($which eq "D") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is a <i>de bene esse</i> $tdesc.</b></font></td></tr>";	
	} elsif ($which eq "R") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is a PMK/PMQ Rule 30(b)(6) $tdesc.</b></font></td></tr>";		
	} elsif ($which eq "E") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is an Examination Under Oath (EUO) $tdesc.</b></font></td></tr>";		
	} elsif ($which eq "T") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is an <i>ore tenus</i> $tdesc.</b></font></td></tr>";	
	}
	if ($dur eq "1") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take less than 1 hour.</b></font></td></tr>";			
	} elsif ($dur eq "2") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take from 1-2 hours.</b></font></td></tr>";		
	} elsif ($dur eq "3") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take from 2-3 hours.</b></font></td></tr>";		
	} elsif ($dur eq "H") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take about a half day.</b></font></td></tr>";		
	} elsif ($dur eq "A") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take ALL DAY.</b></font></td></tr>";		
	} elsif ($dur eq "D") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take from 1-2 days.</b></font></td></tr>";		
	} elsif ($dur eq "T") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take from 2-3 days.</b></font></td></tr>";		
	} elsif ($dur eq "F") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take from 3-4 days.</b></font></td></tr>";		
	} elsif ($dur eq "W") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take 1 week.</b></font></td></tr>";		
	} elsif ($dur eq "M") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take more than 1 week.</b></font></td></tr>";		
	}
	if ($rt eq "R") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		if ($ln eq "Y") {
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Realtime is required for this $tdesc.  Attorney already has LiveNote.</b></font></td></tr>";	
		} else {
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Realtime is required for this $tdesc.  Attorney will need reporter to provide laptop.</b></font></td></tr>";			
		}
	} elsif ($rt eq "D") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		if ($ln eq "Y") {
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Realtime is highly desirable, but not required for this $tdesc.  Attorney already has LiveNote.</b></font></td></tr>";	
		} else {
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Realtime is highly desirable, but not required for this $tdesc.  Attorney will need reporter to provide laptop.</b></font></td></tr>";			
		}
	} elsif ($rt eq "N") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Realtime is NOT required for this $tdesc.</b></font></td></tr>";		
	}
	if ($interp eq "Y") {
		my $lan = &GetLanguage($lang,$dbh);
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		if ($gi eq "Y") {	
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc will be interpreted into the $lan language.<br>Reporting Firm will provide interpreter.</b></font></td></tr>";
		} else {
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc will be interpreted into the $lan language. An Interpreter will be provided.</b></font></td></tr>";
		}
	}
	if ($urge eq "D") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>SAME DAY EXPEDITE expected.</b></font></td></tr>";			
	} elsif ($urge eq "1") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>NEXT DAY EXPEDITE expected.</b></font></td></tr>";			
	} elsif ($urge eq "2") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>2-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "3") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>3-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "4") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>4-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "5") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>5-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "6") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>6-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "7") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>7-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "8") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>8-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "9") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>9-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "N") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Normal 10-Day expedite expected.</b></font></td></tr>";			
	}
	if ($other) {
		print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Addl Instructions: </font></div></td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$other</b></font></td></tr>";		
	}
	print MAIL "<tr><td colspan='3'>&nbsp;</td></tr>";
	print MAIL "<tr><td colspan='3'><hr></td></tr>";
	print MAIL "<tr><td colspan='3'><div align='center'><font face='Arial, Helvetica, sans-serif' size='3'><b><a href='http://www.mydepotools.com/cgi-bin/firms.cgi?a=accept&fid=$firm&id=$jid'>YES!  I want to accept this job on behalf of my firm.</a></b></font></div></td></tr>";
	print MAIL "<tr><td colspan='3'><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'><a href='http://www.mydepotools.com/cgi-bin/firms.cgi?a=reject&fid=$firm&id=$jid'>No thank you... We'll have to pass on this.</a></b></font></div></td></tr>";
	print MAIL "<tr><td colspan='3'><hr></td></tr>";
	print MAIL "<tr><td colspan='3'><div align='center'><font face='Arial, Helvetica, sans-serif' size='1'><b>=====-* CONFIDENTIAL *-=====</b></font></div></td></tr>";
	print MAIL "</table></td></tr>";
	print MAIL "</table></body></html>";
	close (MAIL);		
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub ReporterNotification {
my ($uid,$jid,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
	my ($lid,$fid,$uid,$vid,$jn,$loc,$addr,$addr2,$city,$state,$zip,$cn,$cap,$dep,$de,$da,$jd,$jt,$dt,$dd,$dti,$dp,$pgs,$cop,$amt,$notes,$status,$type,$svcs,$start,$end,$cc,$wl,$contact,
		$email,$phone,$extra,$meal,$park,$tel,$rough,$expert,$after,$we,$exp,$which,$dur,$rt,$ln,$interp,$lang,$gi,$urge,$other) = &GetJobMaster($jid,$dbh);
	my $date = &GetFormattedDate($jd);
	my $time = &GetFormattedTime($jt);
	my ($fname,$faddr,$faddr2,$fcity,$fstate,$fzip,$fcountry,$ffirst,$flast,$femail,$furl,$fphone,$fstatus,$fpay,$comments) = &GetFirmInfo($fid,$dbh);
	my ($lname,$laddr,$laddr2,$lcity,$lstate,$lzip,$lcountry,$lfirst,$llast,$lemail,$lurl,$lphone,$lstatus,$lpay,$lcomments) = &GetLawFirmInfo($lid,$dbh);
	my $tdesc = &GetJobType($type,$dbh);
	my ($rlast,$rfirst,$rmi) = &GetUserName($uid,$dbh);
 	open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: darryl\@depotools.com\n";
	print MAIL "from: auto\@depotools.com\n";
	print MAIL "Subject: Are You Available? $fname: $tdesc for $date at $time\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>DepoTools</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Hi, $rfirst... Hey, are you available on $date at $time for a $tdesc?  Click on the link at the bottom of the page if you are... if not, then click the other link.</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Thanks!</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><div align='left'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$ffirst</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><hr></td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Job Details:</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><table width='90%' cellpadding='0' cellspacing='0' border='0' align='left'>";
	print MAIL "<tr><td width='20%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Reporting Firm: </font></div></td>";
	print MAIL "<td width='2%'>&nbsp;</td>";
	print MAIL "<td width='78%'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$fname</b> ($ffirst $flast)</font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Job: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$tdesc</b></font></td></tr>";	
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Date/Time: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$date at $time</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Services Needed: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	if ($svcs eq "R") {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Reporter Only</b></font></td></tr>";
	} elsif ($svcs eq "V") {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Videographer Only</b></font></td></tr>";
	} else {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Reporter and Videographer</b></font></td></tr>";
	}
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Location: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	if ($loc eq "R") {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$faddr $faddr2, $fcity, $fstate, $fzip</b></font></td></tr>";
	} elsif ($loc eq "L") {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$laddr $laddr2, $lcity, $lstate $lzip</b></font></td></tr>";
	} elsif ($loc eq "O") {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$addr $addr2 $city, $state  $zip</b></font></td></tr>";
	} else {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>TBD</b></font></td></tr>";
	}
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Case Caption: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$cap</b></font></td></tr>";	
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Deponent/Witness: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$dep</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Job Details: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td>&nbsp;</td></tr>";
	if ($extra eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Allow extra time before job begins.</b></font></td></tr>";	
	}
	if ($park eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Parking will be validated.</b></font></td></tr>";	
	} else {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Parking may NOT be validated.</b></font></td></tr>";			
	}
	if ($meal eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>A meal will be provided.</b></font></td></tr>";	
	}
	if ($tel eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>At least one party will appear by telephone.</b></font></td></tr>";			
	}
	if ($rough eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>A rough will be required.</b></font></td></tr>";			
	}
	if ($expert eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Deponent/Witness is an Expert Witness.</b></font></td></tr>";			
	}
	if ($after eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#cc0000'><b>This job may go later than 6:00pm!</b></font></td></tr>";			
	}
	if ($we eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#CC0000'><b>This is a weekend/holiday job!</b></font></td></tr>";			
	}
	if ($exp eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This job is anticipated to be expedited.</b></font></td></tr>";			
	}
	if ($which eq "W") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is a Worker's Comp $tdesc.</b></font></td></tr>";			
	} elsif ($which eq "M") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is a Medical Malpractice $tdesc.</b></font></td></tr>";	
	} elsif ($which eq "P") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is a Personal Injury $tdesc.</b></font></td></tr>";		
	} elsif ($which eq "D") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is a <i>de bene esse</i> $tdesc.</b></font></td></tr>";	
	} elsif ($which eq "R") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is a PMK/PMQ Rule 30(b)(6) $tdesc.</b></font></td></tr>";		
	} elsif ($which eq "E") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is an Examination Under Oath (EUO) $tdesc.</b></font></td></tr>";		
	} elsif ($which eq "T") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is an <i>ore tenus</i> $tdesc.</b></font></td></tr>";	
	}
	if ($dur eq "1") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take less than 1 hour.</b></font></td></tr>";			
	} elsif ($dur eq "2") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take from 1-2 hours.</b></font></td></tr>";		
	} elsif ($dur eq "3") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take from 2-3 hours.</b></font></td></tr>";		
	} elsif ($dur eq "H") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take about a half day.</b></font></td></tr>";		
	} elsif ($dur eq "A") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take ALL DAY.</b></font></td></tr>";		
	} elsif ($dur eq "D") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take from 1-2 days.</b></font></td></tr>";		
	} elsif ($dur eq "T") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take from 2-3 days.</b></font></td></tr>";		
	} elsif ($dur eq "F") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take from 3-4 days.</b></font></td></tr>";		
	} elsif ($dur eq "W") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take 1 week.</b></font></td></tr>";		
	} elsif ($dur eq "M") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take more than 1 week.</b></font></td></tr>";		
	}
	if ($rt eq "R") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		if ($ln eq "Y") {
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Realtime is required for this $tdesc.  Attorney already has LiveNote.</b></font></td></tr>";	
		} else {
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Realtime is required for this $tdesc.  Attorney will need reporter to provide laptop.</b></font></td></tr>";			
		}
	} elsif ($rt eq "D") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		if ($ln eq "Y") {
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Realtime is highly desirable, but not required for this $tdesc.  Attorney already has LiveNote.</b></font></td></tr>";	
		} else {
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Realtime is highly desirable, but not required for this $tdesc.  Attorney will need reporter to provide laptop.</b></font></td></tr>";			
		}
	} elsif ($rt eq "N") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Realtime is NOT required for this $tdesc.</b></font></td></tr>";		
	}
	if ($interp eq "Y") {
		my $lan = &GetLanguage($lang,$dbh);
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		if ($gi eq "Y") {	
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc will be interpreted into the $lan language.<br>Reporting Firm will provide interpreter.</b></font></td></tr>";
		} else {
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc will be interpreted into the $lan language. An Interpreter will be provided.</b></font></td></tr>";
		}
	}
	if ($urge eq "D") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>SAME DAY EXPEDITE expected.</b></font></td></tr>";			
	} elsif ($urge eq "1") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>NEXT DAY EXPEDITE expected.</b></font></td></tr>";			
	} elsif ($urge eq "2") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>2-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "3") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>3-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "4") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>4-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "5") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>5-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "6") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>6-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "7") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>7-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "8") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>8-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "9") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>9-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "N") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Normal 10-Day expedite expected.</b></font></td></tr>";			
	}
	if ($other) {
		print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Addl Instructions: </font></div></td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$other</b></font></td></tr>";		
	}
	print MAIL "<tr><td colspan='3'>&nbsp;</td></tr>";
	print MAIL "<tr><td colspan='3'><hr></td></tr>";
	print MAIL "<tr><td colspan='3'><div align='center'><font face='Arial, Helvetica, sans-serif' size='3'><b><a href='http://www.mydepotools.com/cgi-bin/firms.cgi?a=raccept&id=$jid'>YES!  I want to accept this job!</a></b></font></div></td></tr>";
	print MAIL "<tr><td colspan='3'><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'><a href='http://www.mydepotools.com/cgi-bin/firms.cgi?a=rreject&id=$jid'>No thank you... I'll have to pass on this.</a></b></font></div></td></tr>";
	print MAIL "<tr><td colspan='3'><hr></td></tr>";
	print MAIL "<tr><td colspan='3'><div align='center'><font face='Arial, Helvetica, sans-serif' size='1'><b>=====-* CONFIDENTIAL *-=====</b></font></div></td></tr>";
	print MAIL "</table></td></tr>";
	print MAIL "</table></body></html>";
	close (MAIL);	
	&DidNotify($uid,$jid,$dbh);	
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub BookingNotify {
my ($jid,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
	my ($lid,$fid,$uid,$vid,$jn,$loc,$addr,$addr2,$city,$state,$zip,$cn,$cap,$dep,$de,$da,$jd,$jt,$dt,$dd,$dti,$dp,$pgs,$cop,$amt,$notes,$status,$type,$svcs,$start,$end,$cc,$wl,$contact,
		$email,$phone,$extra,$meal,$park,$tel,$rough,$expert,$after,$we,$exp,$which,$dur,$rt,$ln,$interp,$lang,$gi,$urge,$other) = &GetJobMaster($jid,$dbh);
	my $date = &GetFormattedDate($jd);
	my $time = &GetFormattedTime($jt);
	my ($fname,$faddr,$faddr2,$fcity,$fstate,$fzip,$fcountry,$ffirst,$flast,$femail,$furl,$fphone,$fstatus,$fpay,$comments) = &GetFirmInfo($fid,$dbh);
	my ($lname,$laddr,$laddr2,$lcity,$lstate,$lzip,$lcountry,$lfirst,$llast,$lemail,$lurl,$lphone,$lstatus,$lpay,$lcomments) = &GetLawFirmInfo($lid,$dbh);
	my $tdesc = &GetJobType($type,$dbh);
	my ($rlast,$rfirst,$rmi) = &GetUserName($uid,$dbh);
 	open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: darryl\@depotools.com\n";
	print MAIL "from: auto\@depotools.com\n";
	print MAIL "Subject: You've Been Booked!   $lname - $tdesc for $date at $time\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>DepoTools</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$rfirst;</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>We have assigned you to the following job.  If you have any questions, please give me a call at $fphone.  Thanks!</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";	
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Job Details:</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><table width='90%' cellpadding='0' cellspacing='0' border='0' align='left'>";
	print MAIL "<tr><td width='20%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Law Firm: </font></div></td>";
	print MAIL "<td width='2%'>&nbsp;</td>";
	print MAIL "<td width='78%'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$lname</b> ($cfirst $clast)</font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Job: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$tdesc</b></font></td></tr>";	
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Date/Time: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$date at $time</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Services Needed: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	if ($svcs eq "V") {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>There will be a videographer on this job as well.</b></font></td></tr>";
	} 
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Location: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	if ($loc eq "R") {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Reporting Firm Offices</b></font></td></tr>";
	} elsif ($loc eq "L") {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Law Firm Offices</b></font></td></tr>";
	} elsif ($loc eq "O") {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$addr $addr2 $city, $state  $zip</b></font></td></tr>";
	} else {
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>TBD</b></font></td></tr>";
	}
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Location Contact: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$contact</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Location Phone: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$phone</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Location Email: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$email</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Case #: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$cn</b></font></td></tr>";		
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Case Caption: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$cap</b></font></td></tr>";	
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Deponent/Witness: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$dep</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Job Details: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td>&nbsp;</td></tr>";
	if ($extra eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Allow extra time before job begins.</b></font></td></tr>";	
	}
	if ($park eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Parking will be validated.</b></font></td></tr>";	
	} else {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Parking may NOT be validated.</b></font></td></tr>";			
	}
	if ($meal eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>A meal will be provided.</b></font></td></tr>";	
	}
	if ($tel eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>At least one party will appear by telephone.</b></font></td></tr>";			
	}
	if ($rough eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>A rough will be required.</b></font></td></tr>";			
	}
	if ($expert eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Deponent/Witness is an Expert Witness.</b></font></td></tr>";			
	}
	if ($after eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#cc0000'><b>This job may go later than 6:00pm!</b></font></td></tr>";			
	}
	if ($we eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#CC0000'><b>This is a weekend/holiday job!</b></font></td></tr>";			
	}
	if ($exp eq "Y") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This job is anticipated to be expedited.</b></font></td></tr>";			
	}
	if ($which eq "W") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is a Worker's Comp $tdesc.</b></font></td></tr>";			
	} elsif ($which eq "M") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is a Medical Malpractice $tdesc.</b></font></td></tr>";	
	} elsif ($which eq "P") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is a Personal Injury $tdesc.</b></font></td></tr>";		
	} elsif ($which eq "D") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is a <i>de bene esse</i> $tdesc.</b></font></td></tr>";	
	} elsif ($which eq "R") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is a PMK/PMQ Rule 30(b)(6) $tdesc.</b></font></td></tr>";		
	} elsif ($which eq "E") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is an Examination Under Oath (EUO) $tdesc.</b></font></td></tr>";		
	} elsif ($which eq "T") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This is an <i>ore tenus</i> $tdesc.</b></font></td></tr>";	
	}
	if ($dur eq "1") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take less than 1 hour.</b></font></td></tr>";			
	} elsif ($dur eq "2") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take from 1-2 hours.</b></font></td></tr>";		
	} elsif ($dur eq "3") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take from 2-3 hours.</b></font></td></tr>";		
	} elsif ($dur eq "H") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take about a half day.</b></font></td></tr>";		
	} elsif ($dur eq "A") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take ALL DAY.</b></font></td></tr>";		
	} elsif ($dur eq "D") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take from 1-2 days.</b></font></td></tr>";		
	} elsif ($dur eq "T") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take from 2-3 days.</b></font></td></tr>";		
	} elsif ($dur eq "F") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take from 3-4 days.</b></font></td></tr>";		
	} elsif ($dur eq "W") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take 1 week.</b></font></td></tr>";		
	} elsif ($dur eq "M") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc should take more than 1 week.</b></font></td></tr>";		
	}
	if ($rt eq "R") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		if ($ln eq "Y") {
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Realtime is required for this $tdesc.  Attorney already has LiveNote.</b></font></td></tr>";	
		} else {
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Realtime is required for this $tdesc.  Attorney will need reporter to provide laptop.</b></font></td></tr>";			
		}
	} elsif ($rt eq "D") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		if ($ln eq "Y") {
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Realtime is highly desirable, but not required for this $tdesc.  Attorney already has LiveNote.</b></font></td></tr>";	
		} else {
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Realtime is highly desirable, but not required for this $tdesc.  Attorney will need reporter to provide laptop.</b></font></td></tr>";			
		}
	} elsif ($rt eq "N") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Realtime is NOT required for this $tdesc.</b></font></td></tr>";		
	}
	if ($interp eq "Y") {
		my $lan = &GetLanguage($lang,$dbh);
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		if ($gi eq "Y") {	
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc will be interpreted into the $lan language.<br>Reporting Firm will provide interpreter.</b></font></td></tr>";
		} else {
			print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>This $tdesc will be interpreted into the $lan language. An Interpreter will be provided.</b></font></td></tr>";
		}
	}
	if ($urge eq "D") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>SAME DAY EXPEDITE expected.</b></font></td></tr>";			
	} elsif ($urge eq "1") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>NEXT DAY EXPEDITE expected.</b></font></td></tr>";			
	} elsif ($urge eq "2") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>2-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "3") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>3-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "4") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>4-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "5") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>5-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "6") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>6-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "7") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>7-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "8") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>8-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "9") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>9-Day expedite expected.</b></font></td></tr>";			
	} elsif ($urge eq "N") {
		print MAIL "<tr><td>&nbsp;</td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>Normal 10-Day expedite expected.</b></font></td></tr>";			
	}
	if ($other) {
		print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Addl Instructions: </font></div></td>";
		print MAIL "<td>&nbsp;</td>";
		print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$other</b></font></td></tr>";		
	}
	print MAIL "<tr><td colspan='3'>&nbsp;</td></tr>";
	print MAIL "<tr><td colspan='3'><hr></td></tr>";
	print MAIL "<tr><td colspan='3'><div align='center'><font face='Arial, Helvetica, sans-serif' size='1'><b>=====-* CONFIDENTIAL *-=====</b></font></div></td></tr>";
	print MAIL "</table></td></tr>";
	print MAIL "</table></body></html>";
	close (MAIL);		
	&DidNotify($uid,$jid,$dbh);
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub RejectionEmail {
my ($fid,$id,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my ($fname,$faddr,$faddr2,$fcity,$fstate,$fzip,$fcountry,$ffirst,$flast,$femail,$furl,$fphone,$fstatus,$fpay,$fcomments) = &GetFirmInfo($fid,$dbh);
	my $date = &GetFormattedDate(&GetMunge());
	my ($lid,$fid,$uid,$vid,$jn,$loc,$addr,$addr2,$city,$state,$zip,$cn,$cap,$dep,$de,$da,$jd,$jt,$dt,$dd,$dti,$dp,$pgs,$cop,$amt,$notes,$status,$type,$svcs,$start,$end,$cc,$wl,$contact,
		$jemail,$jphone,$extra,$meal,$park,$tel,$rough,$expert,$after,$we,$exp,$which,$dur,$rt,$ln,$interp,$lang,$gi,$urge,$other,$rinv) = &GetJobMaster($id,$dbh);
	my $jdate = &GetFormattedDate($jd);
	my ($name,$addr,$addr2,$city,$state,$zip,$country,$first,$last,$email,$url,$phone,$status,$pay,$comments) = &GetLawFirmInfo($id,$dbh);
 	open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: darryl\@depotools.com\n";
#	print MAIL "to: $email\n";
	print MAIL "from: auto\@depotools.com\n";
	print MAIL "Subject: Job Refused by $fname...\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>DepoTools</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$fname just turned down your job submission:</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><table width='90%' cellpadding='0' cellspacing='0' border='0' align='left'>";
	print MAIL "<tr><td width='20%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Caption: </font></div></td>";
	print MAIL "<td width='2%'>&nbsp;</td>";
	print MAIL "<td width='78%'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$cap ($def)</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Date/Time: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$jdate at $dt</b></font></td></tr>";	
	print MAIL "<tr><td colspan='3'>&nbsp;</td></tr>";
	print MAIL "<tr><td colspan='3'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>If you have any questions, please feel free to contact $ffirst at $fname at $fphone.</font></td></tr>";	
	print MAIL "<tr><td colspan='3'>&nbsp;</td></tr>";
	print MAIL "<tr><td colspan='3'><hr></td></tr>";
	print MAIL "<tr><td colspan='3'><div align='center'><font face='Arial, Helvetica, sans-serif' size='1'><b>=====-* CONFIDENTIAL *-=====</b></font></div></td></tr>";
	print MAIL "</table></body></html>";
	close (MAIL);		
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub AcceptanceEmail {
my ($fid,$id,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my ($fname,$faddr,$faddr2,$fcity,$fstate,$fzip,$fcountry,$ffirst,$flast,$femail,$furl,$fphone,$fstatus,$fpay,$fcomments) = &GetFirmInfo($fid,$dbh);
	my $date = &GetFormattedDate(&GetMunge());
	my ($lid,$fid,$uid,$vid,$jn,$loc,$addr,$addr2,$city,$state,$zip,$cn,$cap,$dep,$de,$da,$jd,$jt,$dt,$dd,$dti,$dp,$pgs,$cop,$amt,$notes,$status,$type,$svcs,$start,$end,$cc,$wl,$contact,
		$jemail,$jphone,$extra,$meal,$park,$tel,$rough,$expert,$after,$we,$exp,$which,$dur,$rt,$ln,$interp,$lang,$gi,$urge,$other,$rinv) = &GetJobMaster($id,$dbh);
	my $jdate = &GetFormattedDate($jd);
	my ($name,$addr,$addr2,$city,$state,$zip,$country,$first,$last,$email,$url,$phone,$status,$pay,$comments) = &GetLawFirmInfo($id,$dbh);
 	open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: darryl\@depotools.com\n";
#	print MAIL "to: $email\n";
	print MAIL "from: auto\@depotools.com\n";
	print MAIL "Subject: Job Accepted by $fname...\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>DepoTools</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$fname just <b>ACCEPTED</b> your job submission:</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><table width='90%' cellpadding='0' cellspacing='0' border='0' align='left'>";
	print MAIL "<tr><td width='20%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Caption: </font></div></td>";
	print MAIL "<td width='2%'>&nbsp;</td>";
	print MAIL "<td width='78%'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$cap ($def)</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Date: </font></div></td>";
	print MAIL "<td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$jdate</b></font></td></tr>";	
	print MAIL "<tr><td colspan='3'>&nbsp;</td></tr>";
	print MAIL "<tr><td colspan='3'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>If you have any additional instructions regarding this job, please feel free to contact $ffirst at $fname at $fphone.</font></td></tr>";	
	print MAIL "<tr><td colspan='3'>&nbsp;</td></tr>";
	print MAIL "<tr><td colspan='3'><hr></td></tr>";
	print MAIL "<tr><td colspan='3'><div align='center'><font face='Arial, Helvetica, sans-serif' size='1'><b>=====-* CONFIDENTIAL *-=====</b></font></div></td></tr>";
	print MAIL "</table></body></html>";
	close (MAIL);		
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub SendBroadcast {
my ($mode,$subj,$msg,$dbh) = @_;
	
 	my ($didmyown,$flag) = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
 	my $sth = $dbh->prepare("SELECT * FROM UserMaster"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING UserMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$uid = $ref->{'UserID'};
		$first = $ref->{'FirstName'};
		$email = $ref->{'Email'};
		if (!$flag) {
			open (MAIL, "|/usr/sbin/sendmail -t") || die &BPErrorMsg("Error Occurred Sending Email.");
			if ($mode eq "P") {
				print MAIL "to: $email\n";
				print MAIL "bcc: darryl\@edistrict.net\n";
			} elsif ($mode eq "T") {
				print MAIL "to: darryl\@depotools.com\n";
				$flag++;
			}
			print MAIL "from: auto\@depotools.com\n";
			print MAIL "Subject: $subj\n";
			print MAIL "Content-type:text/html\n\n";
			print MAIL "<html><head><title>Welcome to DepoTools!</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
			print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
			print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Greetings, $first!</font></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$msg</font></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Sincerely,</font></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#FF6633'><b>Depo<font color='#999999'>Tools</font></b><font color='#000000'> Development Team</font></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2' color='#333333'>If you have received this email in error,or would like to opt out of future mailings, please click <a href='http://www.depotools.com/cgi-bin/admin.cgi?a=remove&id=$uid'>HERE</a>.</font></div></td></tr>>";
			print MAIL "</table></body></html>";
			close (MAIL);	
		}	
	}
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub SendCredentials {
my $dbh = shift;
	
 	my ($didmyown,$flag) = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
 	my $sth = $dbh->prepare("SELECT * FROM UserMaster"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING UserMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$uid = $ref->{'UserID'};
		$first = $ref->{'FirstName'};
		$email = $ref->{'Email'};
		$uid = $ref->{'UID'};
		$pwd = $ref->{'PWD'};
		if (!$flag) {
			open (MAIL, "|/usr/sbin/sendmail -t") || die &BPErrorMsg("Error Occurred Sending Email.");
			print MAIL "to: $email\n";
			print MAIL "bcc: darryl\@edistrict.net\n";
			print MAIL "from: auto\@depotools.com\n";
			print MAIL "Subject: Here are your login credentials for DepoTools!\n";
			print MAIL "Content-type:text/html\n\n";
			print MAIL "<html><head><title>Welcome to DepoTools!</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
			print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
			print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Greetings, $first!</font></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Your login credentials for DepoTools are as follows:</font></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Your UserID:&nbsp;&nbsp;&nbsp;$uid</font></td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Your Password:&nbsp;&nbsp;&nbsp;$pwd</font></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Please take a few minutes and poke around.</font></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Sincerely,</font></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#FF6633'><b>Depo<font color='#999999'>Tools</font></b><font color='#000000'> Development Team</font></td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td>&nbsp;</td></tr>";
			print MAIL "<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2' color='#333333'>If you have received this email in error,or would like to opt out of future mailings, please click <a href='http://www.depotools.com/cgi-bin/admin.cgi?a=remove&id=$uid'>HERE</a>.</font></div></td></tr>>";
			print MAIL "</table></body></html>";
			close (MAIL);	
		}	
	}
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub SendIndivCredentials {
my ($id,$first,$uid,$pwd,$email,$dbh) = @_;
	
 	my ($didmyown,$flag) = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $date = &GetFormattedDate(&GetMunge());
	open (MAIL, "|/usr/sbin/sendmail -t") || die &BPErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: $email\n";
	print MAIL "bcc: darryl\@edistrict.net\n";
	print MAIL "from: auto\@depotools.com\n";
	print MAIL "Subject: Here are your login credentials for DepoTools!\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>Welcome to DepoTools!</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Greetings, $first!</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Your login credentials for DepoTools are as follows:</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Your UserID:&nbsp;&nbsp;&nbsp;$uid</font></td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Your Password:&nbsp;&nbsp;&nbsp;$pwd</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Please take a few minutes and poke around.</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Sincerely,</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#FF6633'><b>Depo<font color='#999999'>Tools</font></b><font color='#000000'> Development Team</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2' color='#333333'>If you have received this email in error,or would like to opt out of future mailings, please click <a href='http://www.depotools.com/cgi-bin/admin.cgi?a=remove&id=$uid'>HERE</a>.</font></div></td></tr>>";
	print MAIL "</table></body></html>";
	close (MAIL);	
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub SendEmailInvoice {
my ($id,$dbh) = @_;

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $today_date = &GetMunge();
	my $today = &GetFormattedDate($date);
	my ($jid,$fid,$uid,$lid,$date,$num,$amt,$ptd,$status,$paid,$pd30,$pd60,$pd90,$pd120,$pd,$wo) = &GetInvoice($id,$dbh);
	my ($jlid,$jfid,$juid,$jvid,$jn,$loc,$jaddr,$jaddr2,$jcity,$jstate,$jzip,$cn,$cap,$dep,$de,$da,$jd,$jt,$dt,$dd,$dti,$dp,$pgs,$cop,$jamt,$notes,$jstatus,$type,$svcs,$start,$end,$cc,$wl,$contact,
		$email,$phone,$extra,$meal,$park,$tel,$rough,$expert,$after,$we,$exp,$which,$dur,$rt,$ln,$interp,$lang,$gi,$urge,$other,$rinv) = &GetJobMaster($jid,$dbh);
	my $lname = &GetLawFirmName($lid,$dbh);
	my ($fname,$addr,$addr2,$city,$state,$zip,$country,$first,$last,$email,$url,$phone,$status,$pay,$comments) = &GetFirmInfo($fid,$dbh);
	my ($lname,$laddr,$laddr2,$lcity,$lstate,$lzip,$lcountry,$lfirst,$llast,$lemail,$url,$lphone,$status,$pay,$comments) = &GetLawFirmInfo($lid,$dbh);
	my $name = &GetUserName($uid,$dbh);
	my ($ulast,$ufirst,$umi,$uaddr,$uaddr2,$ucity,$ustate,$uzip,$ucountry,$email,$uphone,$rdate,$rtime,$comments,$prereg,$ip,$utype,$rid,$status,$tester,$agree,$expert,$matters) = &GetUserInfo($uid,$dbh);
	my $date = &GetFormattedDate($jd);
	if ($fid) {
		$name = $fname;
	} elsif ($lid) {
		$email = $lemail;
		$name = $lname;
		$addr = $laddr;
		$addr2 = $laddr2;
		$city = $lcity;
		$state = $lstate;
		$zip = $lzip;
		$country = $lcountry;
		$phone = $lphone;
		$first = $lfirst;
		$last = $llast;
	}
	open (MAIL, "|/usr/sbin/sendmail -t") || die &BPErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: $email\n";
	print MAIL "bcc: darryl\@edistrict.net\n";
	print MAIL "from: auto\@depotools.com\n";
	print MAIL "Subject: Invoice for $cap\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>Welcome to DepoTools!</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'>";
	print MAIL "<table width='800' align='center' cellpadding='0' cellspacing='0' bgcolor='#FFFFFF'>";
	print MAIL "<tr><td><img src='http://www.mydepotools.com/images/Invoices/Invoice_Banner.jpg' width='800' height='50'></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='5' color='#c37401'><b>$ufirst $ulast</b></font></div></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$uaddr $uaddr2</b></font></div></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$ucity, $ustate  $uzip</b></font></div></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><b>$uphone</b></font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><table width='98%' align='center' cellpadding='0' cellspacing='0' border='0'><tr><td><table width='95%' align='center' cellpadding='0' cellspacing='0' border='0'>";
	print MAIL "<tr><td width='49%'><table width='95%' align='center' cellpadding='0' cellspacing='0' border='0'><tr><td colspan='3'>&nbsp;</td></tr>";
	print MAIL "<tr><td width='23%'><div align='right'><font face='Arial, Helvetica, sans-serif' size='2'>Firm:</font></div></td><td width='2%'>&nbsp;</td>";
	print MAIL "<td width='75%'><font face='Arial, Helvetica, sans-serif' size='2'><b>$name</b></font></td></tr><tr><td colspan='2'>&nbsp;</td><td>";
	print MAIL "<font face='Arial, Helvetica, sans-serif' size='2'><b>$addr $addr2</b></font></td></tr><tr><td colspan='2'>&nbsp;</td><td><font face='Arial, Helvetica, sans-serif' size='2'>";
	print MAIL "<b>$city, $state,  $zip</b></font></td></tr><tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='2'>Phone:</font></div></td><td>&nbsp;</td><td>";
	print MAIL "<font face='Arial, Helvetica, sans-serif' size='2'><b>$phone</b></font></td></tr><tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='2'>Attn:</font></div></td>";
	print MAIL "<td>&nbsp;</td><td><font face='Arial, Helvetica, sans-serif' size='2'><b>$first $last</b></font></td></tr><tr><td colspan='3'>&nbsp;</td></tr></table></td><td width='2%'>&nbsp;</td>";
	print MAIL "<td><table width='95%' align='center' cellpadding='0' cellspacing='0' border='0'><tr><td colspan='3'>&nbsp;</td></tr><tr><td width='23%'><div align='right'>";
	print MAIL "<font face='Arial, Helvetica, sans-serif' size='2'>Invoice #:</font></div></td><td width='2%'>&nbsp;</td><td width='75%'><font face='Arial, Helvetica, sans-serif' size='2'><b>";
	print MAIL "$num</b></font></td></tr><tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='2'>Invoice Date:</font></div></td><td>&nbsp;</td>";
	print MAIL "<td><font face='Arial, Helvetica, sans-serif' size='2'><b>$today</b></font></td></tr><tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='2'>Terms:</font>";
	print MAIL "</div></td><td>&nbsp;</td><td><font face='Arial, Helvetica, sans-serif' size='2'><b>Due On Receipt</b></font></td></tr><tr><td><div align='right'>";
	print MAIL "<font face='Arial, Helvetica, sans-serif' size='2'>Case #:</font></div></td><td>&nbsp;</td><td><font face='Arial, Helvetica, sans-serif' size='2'><b>$cn</b></font></td>";
	print MAIL "</tr><tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='2'>Caption:</font></div></td><td>&nbsp;</td><td><font face='Arial, Helvetica, sans-serif' size='2'>";
	print MAIL "<b>$cap</b></font></td></tr><tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='2'>Case Date:</font></div></td><td>&nbsp;</td><td>";
	print MAIL "<font face='Arial, Helvetica, sans-serif' size='2'><b>$cdate</b></font></td></tr><tr><td colspan='3'>&nbsp;</td></tr></table></td></tr></table>";
	print MAIL "</tr><tr><td><hr></td></tr><tr><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='4'>I N V O I C E&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D E T A I L</font></div>";
	print MAIL "</td></tr><tr><td>&nbsp;</td></tr>	<tr><td><table width='90%' align='center' cellpadding='0' cellspacing='0' border='0'><tr><td width='10%'><div align='center'>";
	print MAIL "<font face='Arial, Helvetica, sans-serif' size='2'><b>Qty</b></font></div></td><td width='60%'><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'><b>Item</b>";
	print MAIL "</font></div></td><td width='10%'><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'><b>Units</b></font></div></td><td width='10%'><div align='center'>";
	print MAIL "<font face='Arial, Helvetica, sans-serif' size='2'><b>Each</b></font></div></td><td width='10%'><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'><b>Total</b>";
	print MAIL "</font></div></td></tr><tr><td colspan='5'><hr></td></tr>";
	my $ttl = undef;
	my $sth = $dbh->prepare("SELECT * FROM JobBill WHERE InvoiceID='$id'"); 
	$sth->execute() or die &ErrorMsg("InvoiceDetail: ERROR GETTING JOBBILL: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$rt = $ref->{'RateType'};
		$rate = &GetRateType($rt,$dbh);
		$qty = $ref->{'Quantity'};
		$temp2 = $ref->{'Units'};
		$each = $ref->{'Each'};
		$amt = $ref->{'Amount'};
		$temp += $amt;
		if ($temp2 eq "1") {
			$units = "per page";
		} elsif ($temp2 eq "2") {
			$units = "per mile";
		} elsif ($temp2 eq "3") {
			$units = "per hour";
		} elsif ($temp2 eq "4") {
			$units = "each";
		}
		print MAIL "<tr>";
		print MAIL "<td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'>$qty</font></div></td>";
		print MAIL "<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='2'>$rate</font></div></td>";
		print MAIL "<td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'>$units</font></div></td>";
		print MAIL "<td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'>\$$each</font></div></td>";
		print MAIL "<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='2'>\$$amt</font></div></td>";
		print MAIL "</tr>";
	}
	$sth->finish();
	$ttl = sprintf "%0.2f",$temp;		
	print MAIL "<tr><td colspan='5'><hr></td></tr><tr><td colspan='4'><div align='right'><font face='Arial, Helvetica, sans-serif' size='2'><b>Total Due: </b></font></div></td>";
	print MAIL "<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='2'><b>\$$ttl</b></font></div></td></tr><tr><td colspan='5'>&nbsp;</td></tr><tr><td colspan='5'>&nbsp;</td></tr>";
	print MAIL "<tr><td colspan='5'>&nbsp;</td></tr>";
	if ($type eq "5") {
		print MAIL "<tr><td colspan='5'><hr></td></tr>";
		print MAIL "<tr><td colspan='5'><div align='center'><font face='Arial, Helvetica, sans-serif' size='3' color='#CC0000'><b>Transcript will NOT be released or delivered until payment is made in full.</b></font></div></td></tr>";
	} else {
		print MAIL "<tr><td colspan='5'><hr></td></tr>";
		print MAIL "<tr><td colspan='5'><div align='center'><font face='Arial, Helvetica, sans-serif' size='2' color='#000000'><b>All invoices over 30 days will be charged 1.8% interest per month until paid.<br>Should collection efforts become necessary, firm agrees to pay all costs of collection, including court costs and fees.</b></font></div></td></tr>";
	}
	print MAIL "<tr><td colspan='5'>&nbsp;</td></tr><tr><td colspan='5'><div align='center'><font face='Arial, Helvetica, sans-serif' size='5'><b>Thank you for your business!</b></font></div>";
	print MAIL "</td></tr></table></td></tr></table></td></tr><tr><td>&nbsp;</td></tr></table>";
	print MAIL "</table></body></html>";
	close (MAIL);	
	my $sth = $dbh->prepare("UPDATE InvoiceMaster SET SubmitDate='$today_date' WHERE JobID='$jid'"); 
	$sth->execute() or die &ErrorMsg("SendEmailInvoice: ERROR UPDATING INVOICEMASTER: $DBI::errstr");
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub NotifySP {
my ($id,$spid,$jid,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	my $sth = $dbh->prepare("SELECT * FROM UserMaster WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("InvoiceDetail: ERROR GETTING USERMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$first = $ref->{'FirstName'};
		$last = $ref->{'LastName'};
		$phone = $ref->{'Phone'};
		$email = $ref->{'Email'};
	}
	$sth->finish();
	my $titles = &GetTitles($uid,$dbh);
	$sth = $dbh->prepare("SELECT * FROM UserMaster WHERE UserID='$spid'"); 
	$sth->execute() or die &ErrorMsg("InvoiceDetail: ERROR GETTING USERMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$sp_first = $ref->{'FirstName'};
		$sp_last = $ref->{'LastName'};
		$sp_phone = $ref->{'Phone'};
		$sp_email = $ref->{'Email'};
	}
	$sth->finish();
	my $date = &GetFormattedDate(&GetMunge());
	my $due = &GetFormattedDate($dd);
	my ($lid,$fid,$uid,$vid,$jn,$loc,$addr,$addr2,$city,$state,$zip,$cn,$cap,$dep,$de,$da,$jd,$jt,$dt,$dd,$dti,$dp,$pgs,$cop,$amt,$notes,$status,$type,$svcs,$start,$end,$cc,$wl,$contact,
		$jemail,$jphone,$extra,$meal,$park,$tel,$rough,$expert,$after,$we,$exp,$which,$dur,$rt,$ln,$interp,$lang,$gi,$urge,$other,$rinv) = &GetJobMaster($jid,$dbh);
 	open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: darryl\@depotools.com\n";
#	print MAIL "to: $sp_email\n";
	print MAIL "from: auto\@depotools.com\n";
	print MAIL "Subject: Are you available?  I need a scopist/proofreader.\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>DepoTools</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Greetings!</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	if (($dd) && ($dd ne "0000-00-00")) {
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>My name is $first $last, and I'm looking for a scopist/proofreader to work with me on a job of about $pgs pages.  It's due on $due.  Are you available?</font></td></tr>";
	} else {
		print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>My name is $first $last, and I'm looking for a scopist/proofreader to work with me on a job of about $pgs pages.  I dont' have a due date yet.  Are you available?</font></td></tr>";
	}
	print MAIL "<tr><td></td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>If you could, please click here if you <b><a href='http://www.mydepotools.com/cgi-bin/jobs.cgi?a=sp_yes&jid=$jid&u=$id&sp=$spid'>CAN</a></b> do the job, or click here if you <b><a href='http://www.mydepotools.com/cgi-bin/jobs.cgi?a=sp_no&jid=$jid&u=$id&sp=$spid'>CANNOT</a></b> do the job.  In either case, please let me know ASAP.  You can reach me at $phone, or you can email me <a href='mailto:$email'>here</a>.  I look forward to hearing from you!</font></td></tr>";
	print MAIL "<tr><td></td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Thanks!</font></td></tr>";
	print MAIL "<tr><td></td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$first $last, $titles</font></td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$phone</font></td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$email</font></td></tr>";
	print MAIL "<tr><td></td></tr>";

	print MAIL "</body></html>";
	close (MAIL);		
	$sth = $dbh->prepare("UPDATE JobMaster SET SPNotifyFlag='Y' WHERE JobID='$jid'"); 
	$sth->execute() or die &ErrorMsg("NotifySP: ERROR UPDATING JOBMASTER: $DBI::errstr");
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub SP_Confirm {
my ($id,$spid,$jid,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	$sth = $dbh->prepare("SELECT * FROM UserMaster WHERE UserID='$spid'"); 
	$sth->execute() or die &ErrorMsg("InvoiceDetail: ERROR GETTING USERMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$sp_first = $ref->{'FirstName'};
		$sp_last = $ref->{'LastName'};
		$sp_phone = $ref->{'Phone'};
		$sp_email = $ref->{'Email'};
	}
	$sth->finish();
	my $date = &GetFormattedDate(&GetMunge());
	my $due = &GetFormattedDate($dd);
	my ($lid,$fid,$uid,$vid,$jn,$loc,$addr,$addr2,$city,$state,$zip,$cn,$cap,$dep,$de,$da,$jd,$jt,$dt,$dd,$dti,$dp,$pgs,$cop,$amt,$notes,$status,$type,$svcs,$start,$end,$cc,$wl,$contact,
		$jemail,$jphone,$extra,$meal,$park,$tel,$rough,$expert,$after,$we,$exp,$which,$dur,$rt,$ln,$interp,$lang,$gi,$urge,$other,$rinv) = &GetJobMaster($jid,$dbh);
 	open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: darryl\@depotools.com\n";
#	print MAIL "to: $email\n";
	print MAIL "from: auto\@depotools.com\n";
	print MAIL "Subject:  $sp_first $sp_last has AGREED to do this job $cap.\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>DepoTools</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Greetings!</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$sp_first $sp_last has agreed to scope/proofread your job ($cap).  If you need to contact them, you can reach them at $sp_phone or email them <a href='mailto:$sp_email'>HERE</a>.  They have alrady been given the link to the transcript.</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";

	print MAIL "</body></html>";
	close (MAIL);		
	$sth = $dbh->prepare("UPDATE JobMaster SET SPID='$spid' WHERE JobID='$jid'"); 
	$sth->execute() or die &ErrorMsg("NotifySP: ERROR UPDATING JOBMASTER: $DBI::errstr");
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub SP_Deny {
my ($id,$spid,$jid,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	$sth = $dbh->prepare("SELECT * FROM UserMaster WHERE UserID='$spid'"); 
	$sth->execute() or die &ErrorMsg("InvoiceDetail: ERROR GETTING USERMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$sp_first = $ref->{'FirstName'};
		$sp_last = $ref->{'LastName'};
		$sp_phone = $ref->{'Phone'};
		$sp_email = $ref->{'Email'};
	}
	$sth->finish();
	my $date = &GetFormattedDate(&GetMunge());
	my $due = &GetFormattedDate($dd);
	my ($lid,$fid,$uid,$vid,$jn,$loc,$addr,$addr2,$city,$state,$zip,$cn,$cap,$dep,$de,$da,$jd,$jt,$dt,$dd,$dti,$dp,$pgs,$cop,$amt,$notes,$status,$type,$svcs,$start,$end,$cc,$wl,$contact,
		$jemail,$jphone,$extra,$meal,$park,$tel,$rough,$expert,$after,$we,$exp,$which,$dur,$rt,$ln,$interp,$lang,$gi,$urge,$other,$rinv) = &GetJobMaster($jid,$dbh);
 	open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: darryl\@depotools.com\n";
#	print MAIL "to: $email\n";
	print MAIL "from: auto\@depotools.com\n";
	print MAIL "Subject:  $sp_first $sp_last has TURNED DOWN this job $cap.\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>DepoTools</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Greetings!</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$sp_first $sp_last has indicated they cannot do your job ($cap).  If you need to contact them, you can reach them at $sp_phone or email them <a href='mailto:$sp_email'>HERE</a>.  They have alrady been given </font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "</body></html>";
	close (MAIL);		
	$sth = $dbh->prepare("UPDATE JobMaster SET SPID='' WHERE JobID='$jid'"); 
	$sth->execute() or die &ErrorMsg("NotifySP: ERROR UPDATING JOBMASTER: $DBI::errstr");
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
}

sub SP_Tript {
my ($id,$spid,$jid,$dbh) = @_;
	
 	my $didmyown = undef;
 	if (!$dbh) {
		 $dbh = &ConnectDB();
		 $didmyown++;
	}
	$sth = $dbh->prepare("SELECT * FROM UserMaster WHERE UserID='$spid'"); 
	$sth->execute() or die &ErrorMsg("InvoiceDetail: ERROR GETTING USERMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$sp_first = $ref->{'FirstName'};
		$sp_last = $ref->{'LastName'};
		$sp_phone = $ref->{'Phone'};
		$sp_email = $ref->{'Email'};
	}
	$sth->finish();
	my $date = &GetFormattedDate(&GetMunge());
	my $due = &GetFormattedDate($dd);
	my ($lid,$fid,$uid,$vid,$jn,$loc,$addr,$addr2,$city,$state,$zip,$cn,$cap,$dep,$de,$da,$jd,$jt,$dt,$dd,$dti,$dp,$pgs,$cop,$amt,$notes,$status,$type,$svcs,$start,$end,$cc,$wl,$contact,
		$jemail,$jphone,$extra,$meal,$park,$tel,$rough,$expert,$after,$we,$exp,$which,$dur,$rt,$ln,$interp,$lang,$gi,$urge,$other,$rinv) = &GetJobMaster($jid,$dbh);
 	open (MAIL, "|/usr/sbin/sendmail -t") || die &ErrorMsg("Error Occurred Sending Email.");
	print MAIL "to: darryl\@depotools.com\n";
#	print MAIL "to: $email\n";
	print MAIL "from: auto\@depotools.com\n";
	print MAIL "Subject:  $sp_first $sp_last has AGREED to do this job $cap.\n";
	print MAIL "Content-type:text/html\n\n";
	print MAIL "<html><head><title>DepoTools</title><meta http-equiv='Content-Type' content='text/html; charset=iso-88592'></head>";
	print MAIL "<body bgcolor='#FFFFFF' marginwidth='0' marginheight='0' topmargin='0' leftmargin='0'><table width='800' border='0' cellspacing='5' cellpadding='1' align='center'>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='6' color='#FF6600'><b>Depo<font color='#999999'>Tools</b></font></td></tr>";
	print MAIL "<tr><td><div align='right'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$date</font></div></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>Greetings!</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>$sp_first $sp_last has agreed to scope/proofread your job ($cap).  If you need to contact them, you can reach them at $sp_phone or email them <a href='mailto:$sp_email'>HERE</a>.  They have alrady been given the link to the transcript.</font></td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "<tr><td>&nbsp;</td></tr>";
	print MAIL "</body></html>";
	close (MAIL);		
	$sth = $dbh->prepare("UPDATE JobMaster SET SPID='$spid' WHERE JobID='$jid'"); 
	$sth->execute() or die &ErrorMsg("NotifySP: ERROR UPDATING JOBMASTER: $DBI::errstr");
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
}

1;