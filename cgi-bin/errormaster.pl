sub ErrorMsg {
my $msg = shift;
	
# print "Content-type:text/html\n\n";
&DoHeader_notbl(1);
print << "END";
<table width="1200" border="0" cellspacing="0" cellpadding="0" bgcolor="#FDF5CE" align="center">
  <tr>
    <td><div align="center"><font size="5" face="Arial, Helvetica, sans-serif" color='$GBL_Body_Color'><i><b>We\'re Sorry...</b></i></font></div></td>
  </tr> 
  <tr>
    <td>&nbsp;</td>
  </tr> 
  <tr> 
    <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FDF5CE">
        <tr>
          <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><div align="center"><strong><font size="3" face="Arial, Helvetica, sans-serif">$msg</font></strong></div></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
END
&DoFooter();
}

sub SuccessMsg {
my $msg = shift;
	
&DoHeader_notbl(2);
print << "END";
<table width="800" border="0" cellspacing="0" cellpadding="0" bgcolor="#FDF5CE" align="center">
  <tr>
    <td><div align="center"><font size="5" face="Arial, Helvetica, sans-serif" color='$GBL_Body_Color'><i><b>SUCCESS!!!</b></i></font></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FDF5CE">
        <tr>
          <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><div align="center"><strong><font size="3" face="Arial, Helvetica, sans-serif">$msg</font></strong></div></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
END
&DoFooter();
}

sub DataCheck {
my ($table1,$field1,$id1,$table2,$field2,$id2,$table3,$field3,$id3,$dbh) = @_;

my $didmyown = undef;
if (!$dbh) {
  $dbh = &ConnectDB();
  $didmyown++;
}
my $query = "SELECT * FROM $table1 WHERE $field1=$id1";
open FILE, ">/home/mydepo/public_html/data/results.txt" or die &ErrorMsg("Can't open results.txt: $!");
my $sth = $dbh->prepare($query); 
$sth->execute() or die &ErrorMsg("ERROR OCCURRED OPENING $table1: $DBI::errstr");
my $rows = $sth->dump_results(80, '\n', ':', \*FILE );
$sth->finish();
close FILE or die &ErrorMsg("Error closing file: $!");  
&DoHeader_notbl();
print << "END";
<table width="1200" border="0" cellspacing="0" cellpadding="0" bgcolor="#FDF5CE" align="center">
  <tr>
    <td><div align="center"><font size="5" face="Arial, Helvetica, sans-serif" color='$GBL_Body_Color'><i><b>Data Check</b></i></font></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FDF5CE">
        <tr>
          <td>
END
            if ($table1) {
                    print "<fieldset><legend><font size='4' face='Arial, Helvetica, sans-serif'><b>$table1</b></font></legend>";
                    print "<table width='90%' border='0' align='center' cellpadding='0' cellspacing='0'>";
                    print "<tr><td>&nbsp;</td></tr>";
                    print "<tr><td><font size='3' face='Arial, Helvetica, sans-serif'><b>";
                    print "</b></font></strong></div></td></tr>";
                    print "<tr><td>&nbsp;</td></tr>";
                    print "</table></fieldset>";
            } else {
                    print "&nbsp;";
            }
print << "END";
                    </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
END
&DoFooter();
if ($didmyown) {
  $dbh->disconnect();
}
}

sub Congrats_Prelim {
my $msg = shift;
	
&DoHeader_notbl();
print << "END";
<table width="800" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td><div align="center"><font size="6" face="Arial, Helvetica, sans-serif" color='#000000'><i><b>Congratulations!!!</b></i></font></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FDF5CE">
        <tr>
          <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><div align="center"><strong><font size="3" face="Arial, Helvetica, sans-serif">You have passed the initial screening criteria, and are<br><br><font size='6'><i>1 STEP CLOSER</i></font><br><br>to obtaining your cash advance!</font></strong></div></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
END
&DoFooter();
}


sub GetTimeOut {
	
print "Content-type:text/html\n\n";
print << "END";
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="expires" CONTENT="Wed, 03 Nov 1999 12:21:14 GMT"> 
<META HTTP-EQUIV="Pragma" CONTENT="no-cache"> 
<meta http-equiv="Cache-Control" Content="no-cache"> 
<meta name="robots" content="noindex,nofollow">
<script language="javascript">
window.open('./security.cgi?a=to','TimeOut','width=600,height=400,scrollbars=no');
</script>
</head>
<body>
</body>
</html>
END
}

sub TimeOut {
	
print "Content-type:text/html\n\n";
print << "END";
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Session Has Timed Out</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<META HTTP-EQUIV="expires" CONTENT="Wed, 03 Nov 1999 12:21:14 GMT"> 
<META HTTP-EQUIV="Pragma" CONTENT="no-cache"> 
<meta http-equiv="Cache-Control" Content="no-cache"> 
<meta name="robots" content="noindex,nofollow">
</head>

<body bgcolor="#FBC2C2">
<table width="400" border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td><div align="center"><strong><font color="#990000" size="4" face="Arial, Helvetica, sans-serif">Your 
        Session Has Timed Out.</font></strong></div></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td><strong><font size="2" face="Arial, Helvetica, sans-serif">Please Log 
      In Again...</font></strong></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><form action="./security.cgi" method="post" name="TIMEOUT" target="_top" id="TIMEOUT">
    	<input type="hidden" name="a" value="ns">
        <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr> 
            <td width="33%"><div align="right"><strong><font size="2" face="Arial, Helvetica, sans-serif">Your 
                UserID: </font></strong></div></td>
            <td width="3%">&nbsp;</td>
            <td width="64%"><input name="UserID" type="text" id="UserID" size="25" maxlength="255"></td>
          </tr>
          <tr> 
            <td><div align="right"><strong><font size="2" face="Arial, Helvetica, sans-serif">Password: 
                </font></strong></div></td>
            <td>&nbsp;</td>
            <td><input name="Pwd" type="text" id="Pwd" size="25" maxlength="255"></td>
          </tr>
          <tr> 
            <td colspan="3"><hr></td>
          </tr>
          <tr> 
            <td colspan="3"><div align="center"> 
                <input type="submit" name="Submit" value="Log In Again">
              </div></td>
          </tr>
          <tr> 
            <td colspan="3">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="3"><div align="center"><em><strong><font size="1" face="Arial, Helvetica, sans-serif">Copyright 
                &copy; 2006 Barksdale Educational Technology Solutions</font></strong></em></div></td>
          </tr>
        </table>
      </form></td>
  </tr>
</table>
</body>
</html>
END
}

sub SpamErrorMsg {
my $msg = shift;
	
# print "Content-type:text/html\n\n";
&DoSpamHeader();
print << "END";
<table width="1200" border="0" cellspacing="0" cellpadding="0" bgcolor="#FDF5CE" align="center">
  <tr>
    <td><div align="center"><font size="5" face="Arial, Helvetica, sans-serif" color='$GBL_Body_Color'><i><b>We\'re Sorry...</b></i></font></div></td>
  </tr> 
  <tr>
    <td>&nbsp;</td>
  </tr> 
  <tr> 
    <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FDF5CE">
        <tr>
          <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><div align="center"><strong><font size="3" face="Arial, Helvetica, sans-serif">$msg</font></strong></div></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
END
&DoSpamFooter();
}

sub SpamSuccessMsg {
my $msg = shift;
	
&DoSpamHeader();
print << "END";
<table width="800" border="0" cellspacing="0" cellpadding="0" bgcolor="#FDF5CE" align="center">
  <tr>
    <td><div align="center"><font size="5" face="Arial, Helvetica, sans-serif" color='$GBL_Body_Color'><i><b>SUCCESS!!!</b></i></font></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FDF5CE">
        <tr>
          <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><div align="center"><strong><font size="3" face="Arial, Helvetica, sans-serif">$msg</font></strong></div></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
END
&DoSpamFooter();
}

sub SuccessFairMsg {
my $msg = shift;
	
&DoFairHeader(3);
print << "END";
<table width="800" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr>
          <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><div align="center"><strong><font size="3" face="Arial, Helvetica, sans-serif">$msg</font></strong></div></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
END
&DoFairFooter();
}

sub ErrorFairMsg {
my $msg = shift;
	
&DoFairHeader(4);
print << "END";
<table width="800" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr>
          <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr> 
                <td>&nbsp;</td>
              </tr>
              <tr> 
                <td><div align="center"><strong><font size="3" face="Arial, Helvetica, sans-serif">$msg</font></strong></div></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
  </tr>
</table>
END
&DoFairFooter();
}


1;