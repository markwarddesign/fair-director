sub NewPropertyMain {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$ebh=&ConnectDB;
	$dmo++;
}
$id = "17";
my ($enternum,$newtotal) = &GetProperties($dbh);
if (!$newtotal) {
  $newtotal = "0";
}
&DoNewHeader(undef);
print << "END";
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container"> <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span> </a><a class="brand" href="../demo/index.html">FairDirector® </a>
      <div class="nav-collapse">
        <ul class="nav pull-right">
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-cog"></i> Account <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Settings</a></li>
              <li><a href="javascript:;">Help</a></li>
            </ul>
          </li>
          <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                            class="icon-user"></i> fair-director.com <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="javascript:;">Profile</a></li>
              <li><a href="javascript:;">Logout</a></li>
            </ul>
          </li>
        </ul>
        <form class="navbar-search pull-right">
          <input type="text" class="search-query" placeholder="Search">
        </form>
      </div>
      <!--/.nav-collapse --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /navbar-inner --> 
</div>
<!-- /navbar -->
<div class="subnavbar">
  <div class="subnavbar-inner">
    <div class="container">
      <ul class="mainnav">
        <li><a href="./fair.cgi?a=main"><i class="icon-dashboard"></i><span>Dashboard</span> </a> </li>
        <li><a href="./reports.cgi?a=main"><i class="icon-list-alt"></i><span>Reports</span> </a> </li>
        <li><a href="./ticketing.cgi?a=main"><i class="icon-tags"></i><span>Ticketing</span> </a></li>
        <li><a href="./entertain.cgi?a=test"><i class="icon-star"></i><span>Entertainment</span> </a> </li>
        <li><a href="./vendors.cgi?a=main"><i class="icon-shopping-cart"></i><span>Vendors</span> </a> </li>
        <li><a href="./sponsors.cgi?a=main"><i class="icon-money"></i><span>Sponsors</span> </a> </li>
        <li><a href="./staff.cgi?a=main"><i class="icon-group"></i><span>Personnel</span> </a> </li>
       <!-- <li class="dropdown"><a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-long-arrow-down"></i><span>Drops</span> <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="../demo/icons.html">Icons</a></li>
            <li><a href="../demo/faq.html">FAQ</a></li>
            <li><a href="../demo/pricing.html">Pricing Plans</a></li>
            <li><a href="../demo/login.html">Login</a></li>
            <li><a href="../demo/signup.html">Signup</a></li>
            <li><a href="../demo/error.html">404</a></li>
          </ul>
        </li> -->
      </ul>
    </div>
    <!-- /container --> 
  </div>
  <!-- /subnavbar-inner --> 
</div>
<!-- /subnavbar -->
<div class="main">
	<div class="main-inner">
	    <div class="container">
        <div class="row">
        <div class="span6">
          <div class="widget widget-nopad">
            <div class="widget-header"> <i class="icon-list-alt"></i>
              <h3> Today's Property Stats</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="widget big-stats-container">
                <div class="widget-content">
                
                  <div id="big_stats" class="cf">
                    <div class="stat"> <i class="icon-user"></i> <span class="value">$enternum</span> <br />Properties</div>
                    <!-- .stat -->
                    
                    <div class="stat"> <i class="icon-signal"></i> <span class="value">$newtotal</span> <br>
Pending Rentals</div>
                    <!-- .stat -->
                  </div>
                </div>
                <!-- /widget-content --> 
               </div>
            </div>
          </div>
          <!-- /widget -->
     	 </div>
        <!-- /span6 -->
        <div class="span6">
          <div class="widget">
            <div class="widget-header"> <i class="icon-bookmark"></i>
              <h3>Entertainment Actions</h3>
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
              <div class="shortcuts"> 
                  <a href="javascript:;" class="shortcut">
                        <i class="shortcut-icon icon-list-alt"></i>
                      <span class="shortcut-label">Reports</span> 
                  </a>
                  <a href="javascript:;" class="shortcut">
                        <i class="shortcut-icon icon-group"></i>
                      <span class="shortcut-label">View Properties</span> 
                  </a>
                  <a  data-toggle="modal" href="#responsive" class="shortcut popup">
                        <i class="shortcut-icon icon-plus"></i> 
                      <span class="shortcut-label">Add Properties</span> 
                  </a>
                  <a href="javascript:;" class="shortcut"> 
                        <i class="shortcut-icon icon-envelope-alt"></i>
                      <span class="shortcut-label">Send Message</span> 
                  </a>
              </div>
              <!-- /shortcuts --> 
            </div>
            <!-- /widget-content --> 
          </div>
          <!-- /widget -->
</div>

</div>
	<div class="row">
	      	<div class="span12">
	      		<div class="widget">
                <div class="widget-header">
						<i class="icon-group"></i>
						<h3>Property List</h3>
					</div> <!-- /widget-header -->
					
					<div class="widget-content">
                    <div style="float:left;"><a href="./property.cgi?a=view">
            		<i class="icon-external-link"></i> 
                    	View Rental/Use Application
              </a>
              </div>
    
           	<div style="float:right">
            		<i class="icon-plus"></i> 
                  <a  data-toggle="modal" href="#responsive" class="shortcut popup">Add Property</a>
               </div>
               <div style="clear:both;"></div>
	      		<table style="width:100%;" cellpadding="0" cellspacing="0"  align="center">
<tbody><tr><td><div align="center"></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
	<table class="table">
		<tbody>
		<tr>
			<td width="20%"><b>Property</b></td>
			<td width="20%"><b>Sponsor</b></td>			
			<td width="20%"><b>Use Type</b></td>
			<td width="20%"><b>Manager</b></td>
			<td width="5%"><b>Capacity</b></td>
			<td width="5%"><b>Booths</b></td>
			<td width="5%"><b>Exhibits</b></td>
			<td width="15%">&nbsp;</td>
		</tr>
END
	my ($eid,$group,$contact,$type) = undef;
	my $sth = $dbh->prepare("SELECT * FROM PropertyMaster ORDER BY Facility"); 
	$sth->execute() or die &ErrorMsg("ERROR IN PROPERTYMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$pmid = $ref->{'PMID'};
		$name = $ref->{'Facility'};
		$sname = &GetStaffName($ref->{'ManagerID'},$dbh);
		$pname = &GetUseName($ref->{'PurposeID'},$dbh);
		$sponsor = &GetSponsorName($ref->{'SponsorID'},$dbh);
		$cap = $ref->{'Capacity'};
		$booths = $ref->{'BoothCapacity'};
		$exhibits = $ref->{'ExhibitCapacity'};
		$status = $ref->{'Status'};
		if ($status ne "A") {
			print "<tr bgcolor='#ffc9e7'>";
		} else {
			if ($n) { 
				print "<tr bgcolor='#f9f6f1'>";
				$n = undef;
			} else {
				print "<tr bgcolor='#ffffff'>";
				$n++;			
			}
		}
		print "<td>$name</td>";
		print "<td>$sponsor</td>";
		print "<td>$pname</td>";
		print "<td>$sname</td>";
		print "<td>$cap</td>";
		print "<td>$booths</td>";
		print "<td>$exhibits</td>";
		print "<td><a href='./property.cgi?a=edit&id=$pmid'><i class='icon-pencil'></i></a>&nbsp;&nbsp;<a href='./property.cgi?a=del&id=$pmid'><i class='icon-remove'></i></a></td>";
		print "</tr>";
	}
	$sth->finish();
print << "END";
</tbody>
</table>
</td></tr>
<tr><td>&nbsp;</td></tr>
</tbody></table>
</div>
</div>
  </div> <!-- /row -->
    </div> <!-- /container -->
	</div> <!-- /main-inner -->
	</div> <!-- /main -->
    


<div class="extra">
  <div class="extra-inner">
    <div class="container">
      <div class="row">
                    <div class="span3">
                        <h4>Footer Area 1</h4>
                        <ul>
                          Important Links HEre
                          <li><a href="javascript:;"></a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            Support</h4>
                        <ul>
                            <li><a href="javascript:;">Frequently Asked Questions</a></li>
                            <li><a href="javascript:;">Ask a Question</a></li>
                            <li><a href="javascript:;">Video Tutorial</a></li>
                            <li><a href="javascript:;">Feedback</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            Something Legal</h4>
                        <ul>
                            <li><a href="javascript:;">Read License</a></li>
                            <li><a href="javascript:;">Terms of Use</a></li>
                            <li><a href="javascript:;">Privacy Policy</a></li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                    <div class="span3">
                        <h4>
                            Some More Info</h4>
                        <ul><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec id elit non mi porta gravida at eget metus.</li>
                        </ul>
                    </div>
                    <!-- /span3 -->
                </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /extra-inner --> 
</div>
<!-- /extra -->
<div class="footer">
  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="span12"> &copy; 2015 <a href="http://www.flourish-solutions.com/">Flourish Solutions</a>. </div>
        <!-- /span12 --> 
      </div>
      <!-- /row --> 
    </div>
    <!-- /container --> 
  </div>
  <!-- /footer-inner --> 
</div>
<!-- /footer --> 
<div id="responsive" class="modal fade" tabindex="-1" data-width="760" style="display: none;">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">Add Property</h4>
  </div>
  <div class="modal-body">
   <div class="the-alert"></div>
	<div class="row-fluid">
		<div class="span6">
			 <form name="AE" method="post" action="./saveProperty.cgi" data-fv-addons="reCaptcha2">
			 <div class="form-group">
				 <label>Facility: </label>
				 <input type="text" name="fac" id="fac" size="55" maxlength="255" value="">
			 </div>
			 <div class="form-group">
				 <label>Location: </label>
				 <input type="text" name="loc" id="loc" size="55" maxlength="255" value="">
			 </div>			
			 <div class="form-group">
				 <label>map:</label>
				 <input type="file" name="map" id="map" value="">
			 </div>
			 <div class="form-group">
				 <label>Capacity: </label>
				 <input type="text" name="cap" id="cap" size="15" maxlength="15" value="capacity">
			 </div>
			 <div class="form-group">
				 <label>Max Booths: </label>
				 <input type="text" name="mb" id="mb" size="5" maxlength="5" value="">
			 </div>
			 <div class="form-group">
				 <label>Max Exhibits: </label>
				 <input type="text" name="me" id="me" size="5" maxlength="5" value="">
			 </div>		
		</div>
			 <div class="form-group">	
				 <label>Use Type:</label>
				 <select name="pid" id="pid" size="1"><option value=""></option>
END
		&GetUseTypes_sel($pid,$dbh);
print << "END";
				</select></div>
			 <div class="form-group">
				 <label>Manager:</label>
				 <select name='mid' id='mid' size='1'><option value=''></option>
END
		&GetStaff_sel($sid,$dbh);
print << "END";
			 </select></div>
			 <div class="form-group">
				 <label>Sponsor:</label>
				 <select name='sid' id='sid' size='1'><option value=''></option>
END
		&GetSponsors_sel($sid,$dbh);
print << "END";
			 </select></div>

				 <div class="form-group">
					 <label>Manager Email:</label>
					 <input type="text" name="email" id="email" size="55" maxlength="255" value="">
				 </div>
		<div class="span6">
				<div class="form-group">
					<label>Property Phone:</label>
					<input type="text" name="phone" id="phone" size="55" maxlength="255" value="">
				</div>
				<div class="form-group">
					<label>Manager Cell:</label>
					<input type="text" name="cell" id="cell" size="55" maxlength="255" value="">
				</div>
				<div class="form-group">
					<label>Last Inspected:</label>
					<input type="text" name="lid" id="lid" size="12" maxlength="12" value="">
				</div>
				<div class="form-group">
					<label>Status: </label>
					<div class="row-fluid">
						<div class="span6">
							<input type="radio" name="status" id="status" value="A" checked=""> Available<br>
							<input type="radio" name="status" id="status" value="N"> Not Available<br>
							<input type="radio" name="status" id="status" value="R"> Under Renovation<br>
							<input type="radio" name="status" id="status" value="C"> New Construction
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Power:</label>
					<input type="checkbox" name="power" id="power" value="Y" checked>
				</div>
				<div class="form-group">
					<label>Plumbing:</label>
					<input type="checkbox" name="plumb" id="plumb" value="Y" checked>
				</div>
				<div class="form-group">
					<label>Restrooms:</label>
					<input type="checkbox" name="rest" id="rest" value="Y" checked>
				</div>
				<div class="form-group">
					<label>Kitchen:</label>
					<input type="checkbox" name="kitchen" id="kitchen" value="Y" checked>
				</div>
				<div class="form-group">
					<label>A/C:</label>
					<input type="checkbox" name="air" id="air" value="Y" checked>
				</div>
				<div class="form-group">
					<label>Heat:</label>
					<input type="checkbox" name="heat" id="heat" value="Y" checked>
				</div>
				<div class="form-group">
					<label>Network Cabling:</label>
					<input type="checkbox" name="net" id="net" value="Y" checked>
				</div>
				<div class="form-group">
					<label>Wifi:</label>
					<input type="checkbox" name="wifi" id="wifi" value="Y" checked>
				</div>
				<div class="form-group">
					<label>Notes:</label>
					<textarea name="notes" cols="50" rows="6"></textarea>
				</div>

				<div class="form-group">
					<label>Captcha</label>
					<!-- The captcha container -->
					<div id="captchaContainer"></div>
				</div>
                </div>
	</div>
  </div>
  <div class="modal-footer">
    <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
    <button type="submit" class="btn btn-primary">Save changes</button>
  </div>
  </form>
</div>

<!-- Required -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/base.js"></script>

<!-- Charts -->
<script src="../js/excanvas.min.js"></script>
<script src="../js/chart.min.js" type="text/javascript"></script>

<!-- FormValidation plugin and the class supports validating Bootstrap form -->
<script src="../js/formValidation/dist/js/formValidation.js"></script>
<script src="../js/formValidation/dist/js/framework/bootstrap.js"></script>
<script src="../js/formValidation/dist/addons/reCaptcha2.js"></script>

<!-- Modal -->
<script src="../js/bootstrap-modal.js"></script>
<script src="../js/bootstrap-modalmanager.js"></script>

<script id="ajax" type="text/javascript" src="../js/modals.js" ></script> 
END
&DoNewFooter();
if ($dmo) {
	$dbh->disconnect();
}
}

sub GetProperties {
my $dbh = shift;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($ret,$ret2,$pmid) = undef;
	my $today = &GetMunge();
	my $sth = $dbh->prepare("SELECT * FROM PropertyMaster"); 
	$sth->execute() or die &ErrorMsg("ERROR IN PROPERTYMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ret++;
		$pmid = $ref->{'PMID'};
		my $sth2 = $dbh->prepare("SELECT * FROM PropertyRental WHERE PropertyID='$pmid' AND Status='N' AND StartDate >= '$todasy'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN PROPERTYMASTER: $DBI::errstr");
		while (my $ref2 = $sth2->fetchrow_hashref()) {
			$ret2++;
		}
		$sth2->finish();
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $ret,$ret2;
}


sub GetUses_sel {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $ret = undef;
	my $sth = $dbh->prepare("SELECT * FROM PropertyPurpose ORDER BY Purpose"); 
	$sth->execute() or die &ErrorMsg("ERROR IN PROPERTYPURPOSE: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ret++;
		$ppid = $ref->{'PPID'};
		$purpose = $ref->{'Purpose'};
		if ($id eq $ppid) {
			print "<option value='$ppid' selected>$purpose</option>";
		} else {
			print "<option value='$ppid'>$purpose</option>";
		}
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
}

sub GetUseName {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($ret,$purpose) = undef;
	my $sth = $dbh->prepare("SELECT * FROM PropertyPurpose WHERE PPID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN PROPERTYPURPOSE: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ret++;
		$purpose = $ref->{'Purpose'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $purpose;
}

sub SaveProperty {
my ($id,$name,$use,$sid,$cap,$booths,$exhibits,$sponsor,$loc,$sponsor,$lid,$hr,$dr,$wr,$mr,$status,$power,$plumb,$rest,$air,$heat,$kitchen,$net,$wifi,$notes,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my $Name = &WriteQuotes($name);
my $Loc = &WriteQuotes($loc);
my $Notes = &WriteQuotes($notes);
my $today = &GetMunge();
my $now = &GetRawTime();

if (!$id) {
	my ($pmid) = undef;
	my $sth = $dbh->prepare("SELECT * FROM PropertyMaster WHERE Facility='$Name' AND PurposeID='$use'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN PROPERTYMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$pmid = $ref->{'PMID'};
	}
	$sth->finish();
	if (!$pmid) {
		my $sth2 = $dbh->prepare("INSERT INTO PropertyMaster (PropertyMaster.Facility,
													PropertyMaster.Location,
													PropertyMaster.Map,
													PropertyMaster.Capacity,
													PropertyMaster.MaxBooths,
													PropertyMaster.MaxExhibits,
													PropertyMaster.PurposeID,
													PropertyMaster.ManagerID,
													PropertyMaster.SponsorID,
													PropertyMaster.LastInspected,
													PropertyMaster.Status,
													PropertyMaster.Power,
													PropertyMaster.Plumbing,
													PropertyMaster.Restrooms,
													PropertyMaster.Kitchen,
													PropertyMaster.Air,
													PropertyMaster.Heat,
													PropertyMaster.Network,
													PropertyMaster.WiFi,
													PropertyMaster.Notes)
										VALUES ('$Name',
												'$Loc',
												'$map',
												'$cap',
												'$booths',
												'$exhibits',
												'$use',
												'$sid',
												'$sponsor',
												'$lid',
												'$status',
												'$power',
												'$plumb',
												'$rest',
												'$kitchen',
												'$air',
												'$heat',
												'$net',
												'$wifi',
												'$Notes')"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN PROPERTYMASTER: $DBI::errstr");
		$sth2->finish();
		$sth2 = $dbh->prepare("SELECT * FROM PropertyMaster WHERE Facility='$Name' AND PurposeID='$use'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN PROPERTYMASTER: $DBI::errstr");
		while (my $ref2 = $sth2->fetchrow_hashref()) {
			$id = $ref2->{'PMID'};
		}
		$sth2->finish();
	} else {
		my $sth2 = $dbh->prepare("UPDATE PropertyMaster SET PropertyMaster.Facility='$Name',
													PropertyMaster.Location='$Loc',
													PropertyMaster.Map='$map',
													PropertyMaster.Capacity='$cap',
													PropertyMaster.MaxBooths='$booths',
													PropertyMaster.MaxExhibits='$exhibits',
													PropertyMaster.PurposeID='$use',
													PropertyMaster.ManagerID='$sid',
													PropertyMaster.SponsorID='$sponsor',
													PropertyMaster.LastInspected='$lid',
													PropertyMaster.Status='$status',
													PropertyMaster.Power='$power',
													PropertyMaster.Plumbing='$plumb',
													PropertyMaster.Restrooms='$rest',
													PropertyMaster.Kitchen='$kitchen',
													PropertyMaster.Air='$air',
													PropertyMaster.Heat='$heat',
													PropertyMaster.Network='$network',
													PropertyMaster.Wifi='$wifi',
													PropertyMaster.Notes='$Notes'
												WHERE PropertyMaster.PMID='$pmid'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN PropertyMaster: $DBI::errstr");
		$sth2->finish();
		$id = $pmid;
	}
} else {
		my $sth2 = $dbh->prepare("UPDATE PropertyMaster SET PropertyMaster.Facility='$Name',
													PropertyMaster.Location='$Loc',
													PropertyMaster.Map='$map',
													PropertyMaster.Capacity='$cap',
													PropertyMaster.MaxBooths='$booths',
													PropertyMaster.MaxExhibits='$exhibits',
													PropertyMaster.PurposeID='$use',
													PropertyMaster.ManagerID='$sid',
													PropertyMaster.SponsorID='$sponsor',
													PropertyMaster.LastInspected='$lid',
													PropertyMaster.Status='$status',
													PropertyMaster.Power='$power',
													PropertyMaster.Plumbing='$plumb',
													PropertyMaster.Restrooms='$rest',
													PropertyMaster.Kitchen='$kitchen',
													PropertyMaster.Air='$air',
													PropertyMaster.Heat='$heat',
													PropertyMaster.Network='$network',
													PropertyMaster.Wifi='$wifi',
													PropertyMaster.Notes='$Notes'
												WHERE PropertyMaster.PMID='$id'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN PropertyMaster: $DBI::errstr");
		$sth2->finish();
}
if ($dmo) {
	$dbh->disconnect();
}
return $id;
}

1;