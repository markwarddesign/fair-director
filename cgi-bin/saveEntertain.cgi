#!/usr/bin/perl

require "dbutil.pl";
require "errormaster.pl";
require "fairlayout.pl";

use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use DBI;

$cgi = new CGI;
print $cgi->header;


my ($id,$fid,$group,$type,$contact,$addr,$addr2,$city,$state,$zip,$email,$phone,$cell,$status,$fee,$notes) = undef;
if (param()) {
	$id = param('id');
	$fid = param('fid');
	$group = param('group');
	$type = param('type');
	$contact = param('contact');
	$addr = param('addr');
	$addr2 = param('addr2');
	$city = param('city');
	$state = param('state');
	$zip = param('zip');
	$email = param('email');
	$phone = param('phone');
	$cell = param('cell');
	$status = param('status');
	$fee = param('fee');
	$notes = param('notes');
}

if($group && $type && $contact && $addr && $email && $phone){
my $dbh = &ConnectDB();
my $Group = &WriteQuotes($group);
my $Contact = &WriteQuotes($contact);
my $Addr = &WriteQuotes($addr);
my $Addr2 = &WriteQuotes($addr2);
my $City = &WriteQuotes($city);
my $Notes = &WriteQuotes($notes);
if (!$id) {				# if no ID is passed, 
# ==================================================================
# See if it already exists.
# ==================================================================
	my $sth = $dbh->prepare("SELECT * FROM EntertainMaster WHERE GroupName='$Group' AND TypeID='$type'"); 
	$sth->execute() or return NULL;
	while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'EnterID'};
	}
	$sth->finish();
	if (!$id) {				# Create a new one.
		$sth = $dbh->prepare("INSERT INTO EntertainMaster 
									(EntertainMaster.FairID,
									EntertainMaster.GroupName,
  									EntertainMaster.TypeID,
  									EntertainMaster.Contact,
  									EntertainMaster.Addr,
  									EntertainMaster.Addr2,
  									EntertainMaster.City,
  									EntertainMaster.State,
  									EntertainMaster.Zip,
  									EntertainMaster.Email,
  									EntertainMaster.Phone,
  									EntertainMaster.Cell,
									EntertainMaster.Notes) 
  					VALUES ('$fid',
							'$Group',
							'$type',
  							'$Contact',
  							'$Addr',
  							'$Addr2',
  							'$City',
							'$state',
							'$zip',
							'$email',
							'$phone',
							'$cell',
							'$Notes')"); 
	    $sth->execute() or return NULL;
	    $sth->finish();
	    $sth = $dbh->prepare("SELECT * FROM EntertainMaster WHERE GroupName='$Group' AND TypeID='$type'"); 
	    $sth->execute() or return NULL;
	    while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'EnterID'};
	    }
	    $sth->finish();	
	} else {
		$sth = $dbh->prepare("Update EntertainMaster SET EntertainMaster.FairID='$fid',
													EntertainMaster.GroupName='$Group',
													EntertainMaster.TypeID='$type',
													EntertainMaster.Contact='$Contact',
													EntertainMaster.Addr='$Addr',
													EntertainMaster.Addr2='$Addr2',
													EntertainMaster.City='$City',
													EntertainMaster.State='$state',
													EntertainMaster.Zip='$zip',
													EntertainMaster.Email='$email',
													EntertainMaster.Phone='$phone',
													EntertainMaster.Cell='$cell',
													EntertainMaster.Notes='$Notes'
												WHERE EnterID='$id'"); 
		$sth->execute() or $id = "";
		$sth->finish();
	}
} else {
	$sth = $dbh->prepare("Update EntertainMaster SET EntertainMaster.FairID='$fid',
												EntertainMaster.GroupName='$Group',
												EntertainMaster.TypeID='$type',
												EntertainMaster.Contact='$Contact',
												EntertainMaster.Addr='$Addr',
												EntertainMaster.Addr2='$Addr2',
												EntertainMaster.City='$City',
												EntertainMaster.State='$state',
												EntertainMaster.Zip='$zip',
												EntertainMaster.Email='$email',
												EntertainMaster.Phone='$phone',
												EntertainMaster.Cell='$cell',
												EntertainMaster.Notes='$Notes'
											WHERE EnterID='$id'"); 
	$sth->execute() or $id = undef;
	$sth->finish();
}
$dbh->disconnect();
$status = 1;
$msg = "$group has been added!";
print "$status|$msg";
} else{
	$status = 0;
	$msg = "Something went wrong and no data was saved, please double check your form entry and submit.  If problem persists please contact us at <a href='mailto:webmaster\@fair-director.com'>webmaster\@fair-director.com</a>";

	print "$status|$msg";
}
exit;