#!/usr/bin/perl

require "dateutil.pl";
require "dbutil.pl";
require "fairlayout.pl";
require "errormaster.pl";
require "FairReports.pl";
require "Fair.pl";
require "Vendor.pl";

use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use DBI;

my ($Action,$id,$t,$name,$start,$end,$type) = undef;
if (param()) {
	$Action = param('a');
	$t = param('t');
	$id = param('id');
	$org = param('Org');
	$type = param('et');
	$contact = param('Contact');
	$addr = param('Addr');
	$addr2 = param('Addr2');
	$city = param('City');
	$state = param('State');
	$zip = param('Zip');
	$email = param('Email');
	$phone = param('Phone');
	$cell = param('Cell');
	$tid = param('TaxID');
	$blid = param('BusLicID');
	$goods = param('Goods');
	$status = param('Status');
	$notes = param('Notes');
}

my $dbh = &ConnectDB();
if ($Action eq "main") {
	&VendorMain(undef,$dbh);
} elsif ($Action eq "add") {
	&AddVendor(undef,$dbh);
} elsif ($Action eq "edit") {
	&AddVendor($id,$dbh);
} elsif ($Action eq "sv") {
	&SaveVendor($id,$type,$org,$contact,$addr,$addr2,$city,$state,$zip,$email,
		$phone,$cell,$goods,$tid,$blid,$status,$notes,$dbh);
	&VendorMain(undef,$dbh);
} elsif ($Action eq "del") {
	&DeleteVendor($id,$dbh);
	&VendorMain(undef,$dbh);
} else {
	&ErrorFairMsg("I don't know what you want me to do: [ $Action ]");
}
$dbh->disconnect();

exit;