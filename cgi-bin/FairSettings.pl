sub FairMain {
my ($id,$dbh) = @_;

my $bgcolor = "#FFFFFF";
&DoFairHeader(1);
print "<p><h3>&nbsp;</h3></p>";
print "<table width='600' align='center' cellpadding='0' cellspacing='0' border='0'>";
print "<tr bgcolor='$bgcolor'><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='5' color='#2a3753'><b>Fair Management Tools</b></font></div></td></tr>";
print "<tr bgcolor='$bgcolor'><td>&nbsp;</td></tr>";
print "<tr bgcolor='$bgcolor'><td>";
print "<table width='95%' cellpadding='0' cellspacing='0' border='0' align='center'>";
# =======================================================================
# Row 1: Vendor/Sponsor Management
# =======================================================================
print "<tr bgcolor='$bgcolor'><td width='49%'><div align='center'><a href='./fair.cgi?a=vendor' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('VEND',\'','../images/vendor_down.gif',1)\"><img src='../images/vendor_up.gif' alt='Click HERE to manage vendors' name='VEND' width='200' height='50' border='0'></a></div></td>";
print "<td width='2%'>&nbsp;</td>";
print "<td width='49%'><div align='center'><a href='./fair.cgi?a=sponsor' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('SPONSOR',\'','../images/sponsor_down.gif',1)\"><img src='../images/sponsor_up.gif' alt='Click HERE to manage sponsors' name='SPONSOR' width='200' height='50' border='0'></a></div></td></tr>";
# =======================================================================
# Row 2: Exhibit/Event Scheduling
# =======================================================================
print "<tr bgcolor='$bgcolor'><td><div align='center'><a href='./fair.cgi?a=exh' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('EXHIBIT',\'','../images/exhibit_down.gif',1)\"><img src='../images/exhibit_up.gif' alt='Click HERE to manage exhibits' name='EXHIBIT' width='200' height='50' border='0'></a></div></td>";
print "<td>&nbsp;</td>";
print "<td><div align='center'><a href='./fair.cgi?a=events' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('EVENTS',\'','../images/event_down.gif',1)\"><img src='../images/event_up.gif' alt='Click HERE to manage event schedules' name='EVENTS' width='200' height='50' border='0'></a></div></td></tr>";
# =======================================================================
# Row 3: Entertainment/Concession Mgt
# =======================================================================
print "<tr bgcolor='$bgcolor'><td><div align='center'><a href='./fair.cgi?a=show' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('ENT',\'','../images/show_down.gif',1)\"><img src='../images/show_up.gif' alt='Click HERE to manage entertainment' name='ENT' width='200' height='50' border='0'></a></div></td>";
print "<td>&nbsp;</td>";
print "<td><div align='center'><a href='./fair.cgi?a=eats' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('EATS',\'','../images/eats_down.gif',1)\"><img src='../images/eats_up.gif' alt='Click HERE to manage concessions' name='EATS' width='200' height='50' border='0'></a></div></td></tr>";
# =======================================================================
# Row 4:  Financial/Volunteer Mgt.
# =======================================================================
print "<tr bgcolor='$bgcolor'><td><div align='center'><a href='./fair.cgi?a=fin' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('FIN',\'','../images/money_down.gif',1)\"><img src='../images/money_up.gif' alt='Click HERE to manage finances' name='FIN' width='200' height='50' border='0'></a></div></td>";
print "<td>&nbsp;</td>";
print "<td><div align='center'><a href='./fair.cgi?a=vol' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('VOL',\'','../images/vols_down.gif',1)\"><img src='../images/vols_up.gif' alt='Click HERE to manage volunteers' name='VOL' width='200' height='50' border='0'></a></div></td></tr>";
# =======================================================================
# Row 5:  Ticketing/Volunteer Mgt.
# =======================================================================
print "<tr bgcolor='$bgcolor'><td><div align='center'><a href='./fair.cgi?a=ticket' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('TICKET',\'','../images/ticket_down.gif',1)\"><img src='../images/ticket_up.gif' alt='Click HERE to manage ticketing' name='TICKET' width='200' height='50' border='0'></a></div></td>";
print "<td>&nbsp;</td>";
print "<td><div align='center'><a href='./fair.cgi?a=ads' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('ADS',\'','../images/market_down.gif',1)\"><img src='../images/market_up.gif' alt='Click HERE to manage marketing' name='ADS' width='200' height='50' border='0'></a></div></td></tr>";

# print "<tr bgcolor='$bgcolor'><td><div align='center'><a href='./fair.cgi?a=ticket' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('TICKET',\'','../images/ticket_down.gif',1)\"><img src='../images/ticket_up.gif' alt='Click HERE to manage ticketing' name='TICKET' width='200' height='50' border='0'></a></div></td>";
# print "<td>&nbsp;</td>";
# print "<td>This works.</td></tr>";
# print "<td><div align='center'><img src='../images/ads_up.gif' width='200' height='50'></div></td></tr>";
# print "<td><div align='center'><a href='./fair.cgi?a=ads' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('MARKETING',\'','../images/ads_down.gif',1)\"><img src='../images/ads_up.gif' alt='Click HERE to manage marketing' name='MARKETING' width='200' height='50' border='0'></a></div></td></tr>";
# =======================================================================
# Row 6: Reports/Settings
# =======================================================================
print "<tr bgcolor='$bgcolor'><td><div align='center'><a href='./fair.cgi?a=rep' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('REPORTS',\'','../images/reports_down.gif',1)\"><img src='../images/reports_up.gif' alt='Click HERE to print reports' name='REPORTS' width='200' height='50' border='0'></a></div></td>";
print "<td>&nbsp;</td>";
print "<td><div align='center'><a href='./fair.cgi?a=setting' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('SETTINGS',\'','../images/settings_down.gif',1)\"><img src='../images/settings_up.gif' alt='Click HERE to review and change settings' name='SETTINGS' width='200' height='50' border='0'></a></div></td></tr>";

print "<tr bgcolor='$bgcolor'><td colspan='3'>&nbsp;</td></tr>";
print "<tr bgcolor='$bgcolor'><td colspan='3'>&nbsp;</td></tr>";
print "<tr bgcolor='$bgcolor'><td colspan='3'>&nbsp;</td></tr>";
print "</table>";
print "</td></tr></table>";
print "<p><h3>&nbsp;</h3></p>";
print "<p><h3>&nbsp;</h3></p>";
&DoFairFooter();
}

sub TopDown {
my ($id,$dbh) = @_;

my $bgcolor = "#FFFFFF";
&DoFairHeader(2);
print << "END";
<form name='talkform' method='post' action='./fair.cgi'>
<input type='hidden' name='id' value='$id'>
<input type='hidden' name='a' value='dotopdown'>

<table width='800' align='center' cellpadding='0' cellspacing='0' border='0'>
	<tr><td><a href='./fair.cgi?a=main' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('BACK',\'','../images/menu_down.gif',1)\"><img src='../images/menu_up.gif' alt='Click HERE to go back to Talk Management' name='BACK' width='200' height='50' border='0'></a></td></tr>
	<tr bgcolor='$bgcolor'><td><div align='center'><font face='Arial, Helvetica, sans-serif' size='5' color='#2a3753'><b>Pricing/Profitability Analysis</b></font></div></td></tr>
	<tr bgcolor='$bgcolor'><td>&nbsp;</td></tr>
	<tr bgcolor='$bgcolor'><td>
		<fieldset><legend><font face='Arial, Helvetica, sans-serif' size='3' color='#2a3753'><b><i>Enter Analysis Metrics</i></b></font></legend>
		<table width='95%' cellpadding='0' cellspacing='0' border='0' align='center'>
			<tr><td colspan='3'>&nbsp;</td></tr>
			<tr>	<td width='35%'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><div align='right'><b>Last Year Attendance: </b></div></font></td>
				<td width='5%'>&nbsp;</td>
				<td width='60%'><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><input type='text' name='attend' size='5' maxlength='10'></font></td>
			</tr>
			<tr>	<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><div align='right'><b>Total Expenses (last year): </b></div></font></td>
				<td>&nbsp;</td>
				<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'>\$ <input type='text' name='exp' id='exp' size='10' maxlength='15'></font></td>
			</tr>	
			<tr>	<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><div align='right'><b>Growth Factor (%): </b></div></font></td>
				<td>&nbsp;</td>
				<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><input type='text' name='factor' id='factor' size='5' maxlength='10'>%</font></td>
			</tr>
			<tr>	<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><div align='right'><b>Desired Profit Margin (%): </b></div></font></td>
				<td>&nbsp;</td>
				<td><font face='Arial, Helvetica, sans-serif' size='3' color='#000000'><input type='text' name='margin' id='margin' size='5' maxlength='10'>%</font></td>
			</tr>
			<tr><td colspan='3'>&nbsp;</td></tr>
			<tr><td colspan='3'><div align='center'><input type='submit' name='submit' value='Get Results'></div></td></tr>
			<tr><td colspan='3'>&nbsp;</td></tr>
		</table>
		</fieldset>
		</td></tr>	
<tr bgcolor='$bgcolor'><td>&nbsp;</td></tr>
</table>
</form>
<p><h3>&nbsp;</h3></p>
<p><h3>&nbsp;</h3></p>
END
&DoFairFooter();
}

1;