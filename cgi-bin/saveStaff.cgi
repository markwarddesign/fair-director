#!/usr/bin/perl

require "dbutil.pl";
require "errormaster.pl";
require "fairlayout.pl";

use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use DBI;

$cgi = new CGI;
print $cgi->header;


my ($id,$last,$first,$mi,$addr,$addr2,$city,$state,$zip,$phone,$cell,$email,$jid,$status,$ct,$notes) = undef;
if (param()) {
	$id = param('id');
	$last = param('last');
	$first = param('first');
	$mi = param('mi');
	$addr = param('addr');
	$addr2 = param('addr2');
	$city = param('city');
	$state = param('state');
	$zip = param('zip');
	$phone = param('phone');
	$cell = param('cell');
	$email = param('email');
	$jid = param('jid');
	$ct = param('ct');
	$notes = param('notes');
}

if($last && $first && $addr && $city && $state && $zip && $cell){
my $dbh = &ConnectDB();
my $Last = &WriteQuotes($last);
my $First = &WriteQuotes($first);
my $Addr = &WriteQuotes($addr);
my $Addr2 = &WriteQuotes($addr2);
my $City = &WriteQuotes($city);
my $Notes = &WriteQuotes($notes);
if (!$id) {				# if no ID is passed, 
# ==================================================================
# See if it already exists.
# ==================================================================
	my $sth = $dbh->prepare("SELECT * FROM StaffMaster WHERE LastName='$Last' AND FirstName='$First && Cell='$cell''"); 
	$sth->execute() or return NULL;
	while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'StaffID'};
	}
	$sth->finish();
	if (!$id) {				# Create a new one.
		$sth = $dbh->prepare("INSERT INTO StaffMaster 
									(StaffMaster.LastName,
									StaffMaster.FirstName,
  									StaffMaster.MI,
  									StaffMaster.Addr,
  									StaffMaster.Addr2,
  									StaffMaster.City,
  									StaffMaster.State,
  									StaffMaster.Zip,
  									StaffMaster.HomePhone,
  									StaffMaster.WorkPhone,
  									StaffMaster.CellPhone,
  									StaffMaster.Email,
									StaffMaster.JobID,
									StaffMaster.SSN,
									StaffMaster.Citizen,
									StaffMaster.Type,
									StaffMaster.DateHired,
									StaffMaster.DateTerminated,
									StaffMaster.LastReview,
									StaffMaster.CompType,
									StaffMaster.Designation,
									StaffMaster.RateID,
									StaffMaster.Notes) 
  					VALUES ('$Last',
							'$First',
							'$mi',
  							'$Addr',
  							'$Addr2',
  							'$City',
  							'$state',
							'$zip',
							'$phone',
							'$work',
							'$cell',
							'$email',
							'$jid',
							'$ssn',
							'$citizen',
							'$type',
							'$today',
							'0000-00-00',
							'0000-00-00',
							'$ct',
							'$desig',
							'$rid',
							'$Notes')"); 
	    $sth->execute() or return NULL;
	    $sth->finish();
	    $sth = $dbh->prepare("SELECT * FROM StaffMaster WHERE LastName='$Last' AND FirstName='$First' AND CellPhone='$cell'"); 
	    $sth->execute() or return NULL;
	    while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'StaffID'};
	    }
	    $sth->finish();	
	} else {
		$sth = $dbh->prepare("Update StaffMaster SET StaffMaster.LastName='$Last',
													StaffMaster.Location='$First',
													StaffMaster.MI='$mi',
													StaffMaster.Addr='$Addr',
													StaffMaster.Addr2='$Addr2',
													StaffMaster.City='$City',
													StaffMaster.State='$state',
													StaffMaster.Zip='$zip',
													StaffMaster.HomePhone='$home',
													StaffMaster.WorkPhone='$work',
													StaffMaster.CellPhone='$cell',
													StaffMaster.Email='$email',
													StaffMaster.JobID='$jid',
													StaffMaster.SSN='$ssn',
													StaffMaster.Citizen='$cit',
													StaffMaster.Type='$type',
													StaffMaster.Status='$status',
													StaffMaster.CompType='$ct',
													StaffMaster.Designation='$desig',
													StaffMaster.RateID='$rid',
													StaffMaster.Supervisor='$mid',
													StaffMaster.Notes='$Notes'
												WHERE StaffID='$id'"); 
		$sth->execute() or $id = undef;
		$sth->finish();
	}
} else {
		$sth = $dbh->prepare("Update StaffMaster SET StaffMaster.LastName='$Last',
													StaffMaster.Location='$First',
													StaffMaster.MI='$mi',
													StaffMaster.Addr='$Addr',
													StaffMaster.Addr2='$Addr2',
													StaffMaster.City='$City',
													StaffMaster.State='$state',
													StaffMaster.Zip='$zip',
													StaffMaster.HomePhone='$home',
													StaffMaster.WorkPhone='$work',
													StaffMaster.CellPhone='$cell',
													StaffMaster.Email='$email',
													StaffMaster.JobID='$jid',
													StaffMaster.SSN='$ssn',
													StaffMaster.Citizen='$cit',
													StaffMaster.Type='$type',
													StaffMaster.Status='$status',
													StaffMaster.CompType='$ct',
													StaffMaster.Designation='$desig',
													StaffMaster.RateID='$rid',
													StaffMaster.Supervisor='$mid',
													StaffMaster.Notes='$Notes'
												WHERE StaffID='$id'"); 
		$sth->execute() or $id = undef;
		$sth->finish();
}
$dbh->disconnect();
print "$id";
} else{
	print "nada";
}
exit;