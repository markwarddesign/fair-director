#!/usr/bin/perl

require "dbutil.pl";
require "errormaster.pl";
require "fairlayout.pl";

use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use DBI;

$cgi = new CGI;
print $cgi->header;

my ($id,$type,$org,$last,$first,$mi,$addr,$addr2,$city,$state,$zip,$email,
		$phone,$cell,$status,$notes,$dbh) = undef;
		
if (param()) {
	$id = param('id');
	$type = param('type');
	$org = param('org');
	$last = param('last');
	$first = param('first');
	$mi = param('mi');
	$addr = param('addr');
	$addr2 = param('addr2');
	$city = param('city');
	$state = param('state');
	$zip = param('zip');
	$phone = param('phone');
	$cell = param('cell');
	$email = param('email');
	$status = param('status');
	$notes = param('notes');
}

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my $Org = &WriteQuotes($org);
my $Last = &WriteQuotes($last);
my $First = &WriteQuotes($first);
my $Addr = &WriteQuotes($addr);
my $Addr2 = &WriteQuotes($addr2);
my $City = &WriteQuotes($city);
my $Notes = &WriteQuotes($notes);
if (!$id) {				# if no ID is passed, 
# ==================================================================
# See if it already exists.
# ==================================================================
	my $sth = $dbh->prepare("SELECT * FROM SponsorMaster WHERE Org='$Org' AND TypeID='$type'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN SponsorMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'SponsorID'};
	}
	$sth->finish();
	if (!$id) {				# Create a new one.
		$sth = $dbh->prepare("INSERT INTO SponsorMaster 
									(SponsorMaster.Org,
  									SponsorMaster.TypeID,
  									SponsorMaster.ContactLast,
  									SponsorMaster.ContactFirst,
  									SponsorMaster.ContactMI,
  									SponsorMaster.Addr,
  									SponsorMaster.Addr2,
  									SponsorMaster.City,
  									SponsorMaster.State,
  									SponsorMaster.Zip,
  									SponsorMaster.Email,
  									SponsorMaster.Phone,
  									SponsorMaster.Cell,
  									SponsorMaster.Status,
									SponsorMaster.Notes) 
  					VALUES ('$Org',
							'$type',
  							'$Last',
  							'$First',
  							'$mi',
  							'$Addr',
  							'$Addr2',
  							'$City',
							'$state',
							'$zip',
							'$email',
							'$phone',
							'$cell',
							'$status',
							'$Notes')"); 
	    $sth->execute() or die &ErrorMsg("ERROR SAVING SponsorMaster: $DBI::errstr");
	    $sth->finish();
	    $sth = $dbh->prepare("SELECT * FROM SponsorMaster WHERE Org='$Org' AND TypeID='$type'"); 
	    $sth->execute() or die &ErrorMsg("ERROR IN SponsorMaster: $DBI::errstr");
	    while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'SponsorID'};
	    }
	    $sth->finish();	
	} else {
		$sth = $dbh->prepare("Update SponsorMaster SET SponsorMaster.Org='$Org',
													SponsorMaster.TypeID='$type',
													SponsorMaster.ContactLast='$Last',
													SponsorMaster.ContactFirst='$First',
													SponsorMaster.ContactMI='$mi',
													SponsorMaster.Addr='$Addr',
													SponsorMaster.Addr2='$Addr2',
													SponsorMaster.City='$City',
													SponsorMaster.State='$state',
													SponsorMaster.Zip='$zip',
													SponsorMaster.Email='$email',
													SponsorMaster.Phone='$phone',
													SponsorMaster.Cell='$cell',
													SponsorMaster.Status='$status',
													SponsorMaster.Notes='$Notes'
												WHERE SponsorID='$id'"); 
		$sth->execute() or die &ErrorMsg("ERROR IN SponsorMaster: $DBI::errstr");
		$sth->finish();
	}
} else {
		$sth = $dbh->prepare("Update SponsorMaster SET SponsorMaster.Org='$Org',
													SponsorMaster.TypeID='$type',
													SponsorMaster.ContactLast='$Last',
													SponsorMaster.ContactFirst='$First',
													SponsorMaster.ContactMI='$mi',
													SponsorMaster.Addr='$Addr',
													SponsorMaster.Addr2='$Addr2',
													SponsorMaster.City='$City',
													SponsorMaster.State='$state',
													SponsorMaster.Zip='$zip',
													SponsorMaster.Email='$email',
													SponsorMaster.Phone='$phone',
													SponsorMaster.Cell='$cell',
													SponsorMaster.Status='$status',
													SponsorMaster.Notes='$Notes'
												WHERE SponsorID='$id'"); 
		$sth->execute() or die &ErrorMsg("ERROR IN SponsorMaster: $DBI::errstr");
		$sth->finish();
}
if ($dmo) {
	$dbh->disconnect();
}
return $id;
print "$id";
} else{
	print "nada";
}
exit;