sub ExhibitsMain {
my ($id,$dbh) = @_;

my $bgcolor = "#FFFFFF";
&DoFairHeader(17);
print << "END";
<table width='1200'align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td><div align='left'><a href='./fair.cgi?a=inside' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('GOBACK',\'','../images/goback_down.gif',1)\"><img src='../images/goback_up.gif' alt='Click HERE to go back to the main menu' name='GOBACK' width='250' height='50' border='0'></a></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<table width='80%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
<tr><td>&nbsp;</td></tr>
<tr><td><table width='95%'cellpadding='0' cellspacing='0' border='0' align='center' bgcolor='#FFFFFF'>
	<tr>
		<td width='33%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./exhibits.cgi?a=ediv'>Exhibit Division Management</a></b></font></div></td>
		<td width='33%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./exhibits.cgi?a=edpt'>Exhibit Department Management</a></b></font></div></td>
		<td width='34%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./exhibits.cgi?a=list'>Exhibits Management</a></b></font></div></td>
	</tr>
	<tr>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./exhibits.cgi?a=pr'>Prize Management</a></b></font></div></td>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./exhibits.cgi?a=jdg'>Judge Management</a></b></font></div></td>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./exhibits.cgi?a=entry'>Judging Entries</a></b></font></div></td>
	</tr>
	<tr>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./exhibits.cgi?a=ann'>Announcement Management</a></b></font></div></td>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./exhibits.cgi?a=pub'>Publish Results</a></b></font></div></td>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./exhibits.cgi?a=auc'>Auction Management</a></b></font></div></td>
	</tr>
	<tr>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./reports.cgi?a=main'>Reports</a></b></font></div></td>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./tools.cgi?a=main'>Maintenance Tools</a></b></font></div></td>
		<td><div align='center'><font face="Arial, Helvetica, sans-serif" size='3'><b>
			<a href='./export.cgi?a=main'>Data Export/Import</a></b></font></div></td>
	</tr>
	</table>
</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
</table>
END
&DoFairFooter();
}

sub ExhibitDivisions {
my ($id,$dbh) = @_;

my $bgcolor = "#FFFFFF";
&DoFairHeader(17);
my ($d_fid,$d_did,$d_name,$d_desc) = undef;
if ($id) {
	($d_fid,$d_did,$d_name,$d_desc) = &GetExhDiv($id,$dbh);
}
print << "END";
<table width='1200' align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td><div align='left'><a href='./exhibits.cgi?a=main' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('GOBACK',\'','../images/exh_man_back_down.gif',1)\"><img src='../images/exh_man_back_up.gif' alt='Click HERE to go back to the main menu' name='GOBACK' width='250' height='50' border='0'></a></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<table width='90%'cellpadding='0' cellspacing='0' border='0' align='center'>
<tr><td>&nbsp;</td></tr>
<tr><td valign="top">
	<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center'>
		<tr><td width="65%" valign="top">
				<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center'>
					<tr bgcolor='#3370af'><td colspan='3'><div align='center'><font face="Arial, Helvetica, sans-serif" size='5' color='#ffffff'><b>Exhibit Divisions</b></font></div></td></tr>
					<tr bgcolor='#0c3f74'>
						<td width='40%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Event</b></font></div></td>
						<td width='40%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Division</b></font></div></td>
						<td width='20%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Actions</b></font></div></td>
					</tr>
END
my ($did,$name,$desc,$n,$ret,$edid,$fid,$ename) = undef;
my $sth = $dbh->prepare("SELECT * FROM ExhDiv ORDER BY EventID,Name"); 
$sth->execute() or die &ErrorMsg("ERROR IN ExhDiv: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
	$ret++;
  	$edid = $ref->{'EDID'};
  	$fid = $ref->{'EventID'};
    	$did = $ref->{'DivID'};
  	$name = $ref->{'Name'};
  	$desc = $ref->{'Desc'};
  	$ename = &GetEventName($fid,$dbh);
  	if ($n) {
  		print "<tr bgcolor='#FFFFFF'>";
  		$n = undef;
  	} else {
  		print "<tr bgcolor='#efe5c0'>";
  		$n++;
  	}
    	print "<td><div align='center'><font face='Arial, Helvetica, sans-serif' size='3'><b>$ename</b></font></div></td>";
     	print "<td><div align='center'><font face='Arial, Helvetica, sans-serif' size='3'><b>$name</b></font></div></td>";
 	print "<td><div align='left'><a href='./exhibits.cgi?a=edit&edid=$edid'><img src='../images/editicon.gif' width='20' height='25' border='no'></a>&nbsp;
		<a href='./exhibits.cgi?a=del&edid=$edid'><img src='../images/garbageicon.gif' width='20' height='25' border='no'></a>&nbsp;
		<a href='./exhibits.cgi?a=edept&edid=$edid'><img src='../images/depticon.gif' width='25' height='25' border='no'></a></td>";
	print "</tr>";
}
$sth->finish();
if (!$ret) {
	print "<tr><td colspan='2'><div align='center'><font face='Arial, Helvetica, sans-serif' size='3' color='#CC0000'><b><i>(No Exhibit Divisions Found)</i></b></font></div></td></tr>";
}
print << "END";
		</table>	
	</td>
	<td width='35%'>
		<form name='ADDDIV' method='post' action='./exhibits.cgi'>
		<input type='hidden' name='a' value='save_ed'>
		<input type='hidden' name='edid' id='edid' value='$id'>
		<fieldset><legend><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b><i>Add/Edit Division</i></b></font></legend>
		<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center'>
			<tr>
				<td width='27%'><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Event: </b></font></div></td>
				<td width='3%'>&nbsp;</td>
				<td width='70%'><div align='left'>
					<select name='fid' id='fid' size='1'>
						<option value=''></option>
END
my ($event_eid,$event_name,$n,$ret) = undef;
my $sth = $dbh->prepare("SELECT * FROM FairMaster ORDER BY FairName"); 
$sth->execute() or die &ErrorMsg("ERROR IN FAIRMASTER: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
	$ret++;
  	$event_eid = $ref->{'FairID'};
    	$event_name = $ref->{'FairName'};
    	if ($fid eq $event_eid) {
		print "<option value='$event_eid' selected>$event_name</option>";
	} else {
		print "<option value='$event_eid'>$event_name</option>";
	}
}
$sth->finish();
print << "END";
				</select></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Division Name: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='name' id='name' size='25' maxlength='255' value='$d_name'></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2'color='#ffffff'><b>Display ID: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='did' id='did' size='5' maxlength='15' value='$d_did'></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2'color='#ffffff'><b>Information: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><textarea name='desc' id='desc' cols='25' rows='10'>$d_desc</textarea></div></td>
			</tr>
			<tr><td colspan='3'>&nbsp;</td></tr>
			<tr><td colspan='3'><div align='center'><input type='submit' name='submit' value='Save Exhibit Division'></div></td></tr>
		</table></fieldset></form>
	</td></tr>
</td></tr>
<tr><td>&nbsp;</td></tr>
</table>
END
&DoFairFooter();
}

sub SaveExhDiv {
my ($id,$fid,$name,$did,$desc,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my $Desc = &WriteQuotes($desc);
my $Name = &WriteQuotes($name);
if (!$id) {
	my ($edid) = undef;
	my $sth = $dbh->prepare("SELECT * FROM ExhDiv WHERE EventID='$fid' AND Name='$Name'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHDIV: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$edid = $ref->{'EDID'};
	}
	$sth->finish();
	if (!$edid) {
		my $sth2 = $dbh->prepare("INSERT INTO ExhDiv (ExhDiv.EventID,ExhDiv.DivID,ExhDiv.Name,ExhDiv.Desc)
										VALUES ('$fid','$did','$Name','$Desc')"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN EXHDIV: $DBI::errstr");
		$sth2->finish();
		$sth2 = $dbh->prepare("SELECT * FROM ExhDiv WHERE EventID='$fid' AND Name='$Name'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN EXHDIV: $DBI::errstr");
		while (my $ref2 = $sth2->fetchrow_hashref()) {
			$id = $ref2->{'EDID'};
		}
		$sth2->finish();
	} else {
		my $sth2 = $dbh->prepare("UPDATE ExhDiv SET ExhDiv.EventID='$fid',
													ExhDiv.DivID='$did',
													ExhDiv.Name='$Name',
													ExhDiv.Desc='$Desc'
												WHERE ExhDiv.EDID='$edid'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN EXHDIV: $DBI::errstr");
		$sth2->finish();
		$id = $edid;
	}
} else {
	my $sth2 = $dbh->prepare("UPDATE ExhDiv SET ExhDiv.EventID='$fid',
												ExhDiv.DivID='$did',
												ExhDiv.Name='$Name',
												ExhDiv.Desc='$Desc'
											WHERE ExhDiv.EDID='$id'"); 
	$sth2->execute() or die &ErrorMsg("ERROR IN EXHDIV: $DBI::errstr");
	$sth2->finish();	
}
if ($dmo) {
	$dbh->disconnect();
}
}

sub GetExhDiv {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $sth = $dbh->prepare("SELECT * FROM ExhDiv WHERE EDID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHDIV: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$fid = $ref->{'EventID'};
		$did = $ref->{'DivID'};
		$name = $ref->{'Name'};
		$desc = $ref->{'Desc'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $fid,$did,$name,$desc;
}

sub GetExhDivName {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $name = undef;
	my $sth = $dbh->prepare("SELECT * FROM ExhDiv WHERE EDID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHDIV: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$name = $ref->{'Name'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $name;
}

sub DelExhDiv {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $sth = $dbh->prepare("DELETE FROM ExhDiv WHERE EDID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHDIV: $DBI::errstr");
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
}

# ============================================================
# Exhibit Department Routines
#
# parm1:    Dept ID for edits
# parm2:    Division ID
# parm3:    Database handle
# ============================================================
sub ExhibitDepartments {
my ($id,$divid,$dbh) = @_;

if (!$dbh) {
	$dbh = &ConnectDB();
}
my $bgcolor = "#FFFFFF";
&DoFairHeader(17);
my ($edid,$dispid,$deptname,$sid,$desc) = undef;
if ($divid) {
	$divname = &GetExhDivName($divid,$dbh);
} else {
	&ErrorFairMsg("No Division Passed");
	exit;
}
if ($id) {
	($edid,$dispid,$deptname,$sid,$desc) = &GetExhDept($id,$dbh);
} 
print << "END";
<table width='1200' align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td><div align='left'><a href='./exhibits.cgi?a=main' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('GOBACK',\'','../images/goback_down.gif',1)\"><img src='../images/goback_up.gif' alt='Click HERE to go back to the main menu' name='GOBACK' width='250' height='50' border='0'></a></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<table width='90%'cellpadding='0' cellspacing='0' border='0' align='center'>
<tr><td>&nbsp;</td></tr>
<tr><td valign="top">
	<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center'>
		<tr><td width="65%" valign="top">
				<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center'>
					<tr bgcolor='#3370af'><td colspan='5'><div align='center'><font face="Arial, Helvetica, sans-serif" size='5' color='#ffffff'><b>$divname Division</b></font></div></td></tr>
					<tr bgcolor='#0c3f74'>
						<td width='25%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Department</b></font></div></td>
						<td width='25%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Manager</b></font></div></td>
						<td width='20%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Phone</b></font></div></td>
						<td width='10%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Email</b></font></div></td>
						<td width='20%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Actions</b></font></div></td>
					</tr>
END
my ($edtid,$dispid,$name,$desc,$ret) = undef;
my $sth = $dbh->prepare("SELECT * FROM ExhDept WHERE EDID='$divid' ORDER BY Name"); 
$sth->execute() or die &ErrorMsg("ERROR IN ExhDept: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
	$ret++;
  	$edtid = $ref->{'EDTID'};
    	$dispid = $ref->{'DisplayID'};
  	$name = $ref->{'Name'};
  	($staff,$phone,$email) = &GetStaffInfoShort($ref->{'StaffID'},$dbh);
  	$desc = $ref->{'Desc'};
  	if ($n) {
  		print "<tr bgcolor='#FFFFFF'>";
  		$n = undef;
  	} else {
  		print "<tr bgcolor='#efe5c0'>";
  		$n++;
  	}
  	if (!$staff) {
  		$staff = "unassigned";
  	}
     	print "<td><div align='left'><font face='Arial, Helvetica, sans-serif' size='2'>&nbsp;&nbsp;<b>$name</b></font></div></td>";
     	print "<td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'>$staff</font></div></td>";
     	print "<td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'>$phone</font></div></td>";
    	print "<td><div align='center'><a href='mailto:$email'><img src='../images/emailicon.gif' width='30' height='25' border='n'></div></td>";
	print "<td><div align='left'><a href='./exhibits.cgi?a=editd&id=$edtid&edid=$divid'><img src='../images/editicon.gif' width='20' height='25' border='no'></a>&nbsp;
		<a href='./exhibits.cgi?a=deld&id=$edtid&edid=$divid'><img src='../images/garbageicon.gif' width='20' height='25' border='no'></a>&nbsp;
		<a href='./exhibits.cgi?a=eclass&id=$edtid&edid=$divid'><img src='../images/depticon.gif' width='25' height='25' border='no'></a></td>";
	print "</tr>";
}
$sth->finish();
if (!$ret) {
	print "<tr><td colspan='5'><div align='center'><font face='Arial, Helvetica, sans-serif' size='3' color='#ffffff'><b><i>(No $divname Departments Found)</i></b></font></div></td></tr>";
}
print << "END";
		</table>	
	</td>
	<td width='35%'>
		<form name='ADDDEPT' method='post' action='./exhibits.cgi'>
		<input type='hidden' name='a' value='save_edpt'>
		<input type='hidden' name='edid' value='$divid'>
		<input type='hidden' name='id' value='$id'>
		<fieldset><legend><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b><i>Add/Edit Department</i></b></font></legend>
		<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center'>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Dept Name: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='name' id='name' size='25' maxlength='255' value='$name'></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Manager: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><select name='sid' id='sid' size='1'><option value=''></option>
END
	my ($ret) = undef;
	my $sth = $dbh->prepare("SELECT * FROM StaffMaster ORDER BY LastName,FirstName"); 
	$sth->execute() or die &ErrorMsg("ERROR IN STAFFMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ret++;
		$sid = $ref->{'StaffID'};
		$last = $ref->{'LastName'};
		$first = $ref->{'FirstName'};
		$mi = $ref->{'MI'};
		if ($dsid eq $sid) {
			if ($mi) {
				print "<option value='$sid' selected>$first $mi $last</option>";
			} else {
				print "<option value='$sid'>$first $mi $last</option>";
			}
		} else {
			if ($mi) {
				print "<option value='$sid' selected>$first $last</option>";
			} else {
				print "<option value='$sid'>$first $last</option>";
			}			
		}
	}
	$sth->finish();
print << "END";				
				</select></div></td>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2'color='#ffffff'><b>Display ID: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='did' id='did' size='5' maxlength='15' value='$dispid'></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2'color='#ffffff'><b>Information: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><textarea name='desc' id='desc' cols='25' rows='10'>$desc</textarea></div></td>
			</tr>
			<tr><td colspan='3'>&nbsp;</td></tr>
			<tr><td colspan='3'><div align='center'><input type='submit' name='submit' value='Save $divname Department'></div></td></tr>
		</table></fieldset></form>
	</td></tr>
</td></tr>
<tr><td>&nbsp;</td></tr>
</table>
END
&DoFairFooter();
}

sub SaveExhDept {
my ($id,$edid,$did,$sid,$name,$desc,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my $Desc = &WriteQuotes($desc);
my $Name = &WriteQuotes($name);
if (!$id) {
	my ($edtid) = undef;
	my $sth = $dbh->prepare("SELECT * FROM ExhDept WHERE EDID='$edid' AND Name='$Name'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHDEPT: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$edtid = $ref->{'EDTID'};
	}
	$sth->finish();
	if (!$edtid) {
		my $sth2 = $dbh->prepare("INSERT INTO ExhDept (ExhDept.EDID,
													ExhDept.DisplayID,
													ExhDept.Name,
													ExhDept.StaffID,
													ExhDept.Desc)
										VALUES ('$edid',
												'$did',
												'$Name',
												'$sid',
												'$Desc')"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN EXHDEPT: $DBI::errstr");
		$sth2->finish();
		$sth2 = $dbh->prepare("SELECT * FROM ExhDept WHERE EDID='$edid' AND Name='$Name'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN EXHDEPT: $DBI::errstr");
		while (my $ref2 = $sth2->fetchrow_hashref()) {
			$id = $ref2->{'EDTID'};
		}
		$sth2->finish();
	} else {
		my $sth2 = $dbh->prepare("UPDATE ExhDept SET ExhDept.EDID='$edid',
													ExhDept.DisplayID='$did',
													ExhDept.Name='$Name',
													ExhDept.StaffID='$sid',
													ExhDept.Desc='$Desc'
												WHERE ExhDept.EDTID='$edid'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN EXHDEPT: $DBI::errstr");
		$sth2->finish();
		$id = $edid;
	}
} else {
	my $sth2 = $dbh->prepare("UPDATE ExhDept SET ExhDept.EDID='$edid',
												ExhDept.DisplayID='$did',
												ExhDept.Name='$Name',
												ExhDept.StaffID='$sid',
												ExhDept.Desc='$Desc'
											WHERE ExhDept.EDTID='$edid'"); 
	$sth2->execute() or die &ErrorMsg("ERROR IN EXHDEPT: $DBI::errstr");
	$sth2->finish();
}
if ($dmo) {
	$dbh->disconnect();
}
}

sub GetExhDept {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $sth = $dbh->prepare("SELECT * FROM ExhDept WHERE EDTID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHDEPT: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$edid = $ref->{'EDID'};
		$did = $ref->{'DisplayID'};
		$name = $ref->{'Name'};
		$sid = $ref->{'StaffID'};
		$desc = $ref->{'Desc'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $edid,$did,$name,$sid,$desc;
}

sub GetExhDeptName {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $sth = $dbh->prepare("SELECT * FROM ExhDept WHERE EDTID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHDEPT: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$name = $ref->{'Name'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $name;
}

sub DelExhDept {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $sth = $dbh->prepare("DELETE FROM ExhDept WHERE EDTID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHDept: $DBI::errstr");
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
}

# ============================================================
# Exhibit Class Routines
# ============================================================
sub ExhibitClasses {
my ($id,$deptid,$dbh) = @_;

if (!$dbh) {
	$dbh = &ConnectDB();
}
my $bgcolor = "#FFFFFF";
&DoFairHeader(17);
my ($dept_edid,$dept_did,$dept_name,$dept_sid,$dept_desc,$divname,$class_deptid,$class_edid,$class_dispid,$class_name,$class_desc) = undef;
if ($deptid) {
	($dept_edid,$dept_did,$dept_name,$dept_sid,$dept_desc) = &GetExhDept($deptid,$dbh);
	$divname = &GetExhDivName($dept_edid,$dbh);
} else {
	&ErrorFairMsg("No Department Passed");
	exit;
}
if ($id) {
	($class_deptid,$class_edid,$class_dispid,$class_name,$class_desc) = &GetExhClass($id,$dbh);		
}
print << "END";
<table width='1200' align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td><div align='left'><a href='./exhibits.cgi?a=ediv' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('GOBACK',\'','../images/exh_div_back_down.gif',1)\"><img src='../images/exh_div_back_up.gif' alt='Click HERE to go back to the main menu' name='GOBACK' width='250' height='50' border='0'></a></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<table width='90%'cellpadding='0' cellspacing='0' border='0' align='center'>
<tr><td>&nbsp;</td></tr>
<tr><td valign="top">
	<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center'>
		<tr><td width="65%" valign="top">
				<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center'>
					<tr bgcolor='#3370af'><td colspan='2'><div align='center'><font face="Arial, Helvetica, sans-serif" size='5' color='#ffffff'><b>$divname Division : $dept_name Department</b></font></div></td></tr>
					<tr bgcolor='#0c3f74'>
						<td width='80%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Class</b></font></div></td>
						<td width='20%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Actions</b></font></div></td>
					</tr>
END
my ($edtid,$dispid,$name,$desc,$ret) = undef;
my $sth = $dbh->prepare("SELECT * FROM ExhClasses WHERE EDTID='$deptid' ORDER BY ClassName"); 
$sth->execute() or die &ErrorMsg("ERROR IN ExhClasses: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
	$ret++;
	$ecid = $ref->{'ECID'};
	$edid = $ref->{'EDID'};
  	$edtid = $ref->{'EDTID'};
    	$dispid = $ref->{'DisplayID'};
  	$name = $ref->{'ClassName'};
  	$desc = $ref->{'ClassDesc'};
  	if (!$edid) {
		$edid = &GetEDIDFromDept($edtid,$dbh);
		my $sth2 = $dbh->prepare("UPDATE ExhClasses SET EDID='$edid' WHERE ECID='$ecid'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN ExhClasses: $DBI::errstr");	
		$sth2->finish();
  	}
  	if ($n) {
  		print "<tr bgcolor='#FFFFFF'>";
  		$n = undef;
  	} else {
  		print "<tr bgcolor='#efe5c0'>";
  		$n++;
  	}
     	print "<td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'>&nbsp;&nbsp;<b>$name</b></font></div></td>";
	print "<td><div align='left'><a href='./exhibits.cgi?a=editc&id=$ecid&deptid=$edtid'><img src='../images/editicon.gif' width='20' height='25' border='no'></a>&nbsp;
		<a href='./exhibits.cgi?a=delc&id=$ecid&deptid=$edtid'><img src='../images/garbageicon.gif' width='20' height='25' border='no'></a>&nbsp;
		<a href='./exhibits.cgi?a=eexh&cid=$ecid'><img src='../images/depticon.gif' width='25' height='25' border='no'></a></td>";
	print "</tr>";
}
$sth->finish();
if (!$ret) {
	print "<tr><td colspan='2'><div align='center'><font face='Arial, Helvetica, sans-serif' size='3' color='#ffffff'><b><i>(No Classes Found)</i></b></font></div></td></tr>";
}
print << "END";
		</table>	
	</td>
	<td width='35%'>
		<form name='ADDCLASS' method='post' action='./exhibits.cgi'>
		<input type='hidden' name='a' value='save_ec'>
		<input type='hidden' name='id' id='id' value='$id'>
		<input type='hidden' name='deptid' value='$deptid'>
		<fieldset><legend><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b><i>Add/Edit Class</i></b></font></legend>
		<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center'>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Class Name: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='name' id='name' size='25' maxlength='255' value='$name'></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Manager: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><select name='sid' id='sid' size='1'><option value=''></option>
END
	my ($ret) = undef;
	my $sth = $dbh->prepare("SELECT * FROM StaffMaster ORDER BY LastName,FirstName"); 
	$sth->execute() or die &ErrorMsg("ERROR IN STAFFMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ret++;
		$sid = $ref->{'StaffID'};
		$last = $ref->{'LastName'};
		$first = $ref->{'FirstName'};
		$mi = $ref->{'MI'};
		if ($dsid eq $sid) {
			if ($mi) {
				print "<option value='$sid' selected>$first $mi $last</option>";
			} else {
				print "<option value='$sid'>$first $mi $last</option>";
			}
		} else {
			if ($mi) {
				print "<option value='$sid' selected>$first $last</option>";
			} else {
				print "<option value='$sid'>$first $last</option>";
			}			
		}
	}
	$sth->finish();
print << "END";				
				</select></div></td>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2'color='#ffffff'><b>Display ID: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='did' id='did' size='5' maxlength='15' value='$class_dispid'></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2'color='#ffffff'><b>Information: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><textarea name='desc' id='desc' cols='25' rows='10'>$class_desc</textarea></div></td>
			</tr>
			<tr><td colspan='3'>&nbsp;</td></tr>
			<tr><td colspan='3'><div align='center'><input type='submit' name='submit' value='Save Class'></div></td></tr>
		</table></fieldset></form>
	</td></tr>
</td></tr>
<tr><td>&nbsp;</td></tr>
</table>
END
&DoFairFooter();
}

sub SaveExhClass {
my ($id,$edtid,$edid,$did,$name,$desc,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my $Desc = &WriteQuotes($desc);
my $Name = &WriteQuotes($name);
if (!$id) {
	my ($ecid) = undef;
	my $sth = $dbh->prepare("SELECT * FROM ExhClasses WHERE EDTID='$edtid' AND ClassName='$Name'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHCLASSES: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ecid = $ref->{'ECID'};
	}
	$sth->finish();
	if (!$ecid) {
		my $sth2 = $dbh->prepare("INSERT INTO ExhClasses (ExhClasses.EDTID,
													ExhClasses.EDID,
													ExhClasses.DisplayID,
													ExhClasses.ClassName,
													ExhClasses.ClassDesc)
										VALUES ('$edtid',
												'$edid',
												'$did',
												'$Name',
												'$Desc')"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN EXHCLASSES: $DBI::errstr");
		$sth2->finish();
		$sth2 = $dbh->prepare("SELECT * FROM ExhClasses WHERE EDTID='$edtid' AND ClassName='$Name'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN EXHCLASSES: $DBI::errstr");
		while (my $ref2 = $sth2->fetchrow_hashref()) {
			$id = $ref2->{'ECID'};
		}
		$sth2->finish();
	} else {
		my $sth2 = $dbh->prepare("UPDATE ExhClasses SET ExhClasses.EDTID='$edtid',
													ExhClasses.EDID='$edid',
													ExhClasses.DisplayID='$did',
													ExhClasses.ClassName='$Name',
													ExhClasses.ClassDesc='$Desc'
												WHERE ExhClasses.ECID='$ecid'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN EXHCLASSES: $DBI::errstr");
		$sth2->finish();
		$id = $ecid;
	}
} else {
	my $sth2 = $dbh->prepare("UPDATE ExhClasses SET ExhClasses.EDTID='$edtid',
												ExhClasses.EDID='$edid',
												ExhClasses.DisplayID='$did',
												ExhClasses.ClassName='$Name',
												ExhClasses.ClassDesc='$Desc'
											WHERE ExhClasses.ECID='$ecid'"); 
	$sth2->execute() or die &ErrorMsg("ERROR IN EXHCLASSES: $DBI::errstr");
	$sth2->finish();
}
if ($dmo) {
	$dbh->disconnect();
}
}

sub GetExhClass {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($edtid,$edid,$did,$name,$desc) = undef;
	my $sth = $dbh->prepare("SELECT * FROM ExhClasses WHERE ECID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHCLASSES: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$edtid = $ref->{'EDTID'};
		$edid = $ref->{'EDID'};
		$did = $ref->{'DisplayID'};
		$name = $ref->{'ClassName'};
		$desc = $ref->{'ClassDesc'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $edtid,$edid,$did,$name,$desc;
}


sub DelExhClass {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $sth = $dbh->prepare("DELETE FROM ExhClasses WHERE ECID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHCLASSES: $DBI::errstr");
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
}

# ==================================================================
# Individual Exhibitor Master
# ==================================================================
sub EntryMaster {
my ($id,$cid,$dbh) = @_;

if (!$dbh) {
	$dbh = &ConnectDB();
}
my $bgcolor = "#FFFFFF";
&DoFairHeader(17);
my ($divname,$desc,$fid,$edid,$did,$deptname,$dispid) = undef;
if ($cid) {
	($deptid,$edid,$dispid,$cname,$desc) = &GetExhClass($cid,$dbh);		
	($fid,$did,$divname,$divdesc) = &GetExhDiv($edid,$dbh);
	$dname = &GetExhDeptName($deptid,$dbh);
} else {
	&ErrorFairMsg("No Class Specified");
	exit;
}
print << "END";
<table width='1200' align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td><div align='left'><a href='./exhibits.cgi?a=eclass' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('GOBACK',\'','../images/exh_class_back_down.gif',1)\"><img src='../images/exh_class_back_up.gif' alt='Click HERE to go back to the main menu' name='GOBACK' width='250' height='50' border='0'></a></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<table width='90%'cellpadding='0' cellspacing='0' border='0' align='center'>
<tr><td>&nbsp;</td></tr>
<tr><td valign="top">
	<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center'>
		<tr><td width="65%" valign="top">
				<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center'>
					<tr bgcolor='#3370af'><td colspan='5'><div align='center'><font face="Arial, Helvetica, sans-serif" size='5' color='#ffffff'><b>$divname : $dname : $cname </b></font></div></td></tr>
					<tr bgcolor='#0c3f74'>
						<td width='30%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Entry</b></font></div></td>
						<td width='30%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Exhibitor</b></font></div></td>
						<td width='15%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Place</b></font></div></td>
						<td width='5%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Email</b></font></div></td>
						<td width='20%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Actions</b></font></div></td>
					</tr>
END
my ($eeid,$title,$emid,$name,$cell,$email,$n,$ret) = undef;
my $sth = $dbh->prepare("SELECT * FROM ExhibitEntry WHERE FairID='$fid' AND ECID='$cid' ORDER BY Title"); 
$sth->execute() or die &ErrorMsg("ERROR IN ExhibitEntry: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
	$ret++;
  	$eeid = $ref->{'EEID'};
    	$title = $ref->{'Title'};
  	$emid = $ref->{'EMID'};
  	($name,$cell,$email) = &GetExhibitorInfoShort($emid,$dbh);
	if ($n) {
		print "<tr bgcolor='#FFFFFF'>";
		$n = undef;
	} else {
		print "<tr bgcolor='#efe5c0'>";
		$n++;
	}		
	print "<td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'>&nbsp;&nbsp;<b>$title</b></font></div></td>";
	print "<td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'><i>$name</i></font></div></td>";
	print "<td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'>$cell</font></div></td>";
	print "<td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'>
		<a href='mailto:$email'><img src='../images/emailicon.gif' width='25' height='25' border='no'></a></font></div></td>";
	print "<td><div align='right'><a href='./exhibits.cgi?a=editx&id=$eeid&cid=$cid'><img src='../images/editicon.gif' width='20' height='25' border='no'></a>&nbsp;
		<a href='./exhibits.cgi?a=delx&id=$eeid&cid=$cid'><img src='../images/garbageicon.gif' width='20' height='25' border='no'></a></td>";
	print "</tr>";
}
$sth->finish();
if (!$ret) {
	print "<tr><td colspan='5'><div align='center'><font face='Arial, Helvetica, sans-serif' size='3' color='#ffffff'><b><i>(No Entries Found)</i></b></font></div></td></tr>";
}
print << "END";
		</table>	
	</td>
	<td width='35%'>
		<form name='ADDENTRY' method='post' action='./exhibits.cgi'>
		<input type='hidden' name='a' value='save_en'>
		<input type='hidden' name='id' value='$id'>
		<input type='hidden' name='cid' value='$cid'>
		<input type='hidden' name='fid' value='$fid'>
		<input type='hidden' name='deptid' value='$deptid'>
		<input type='hidden' name='edid' value='$edid'>
		<fieldset><legend><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b><i>Add/Edit Entry</i></b></font></legend>
		<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center'>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Exhibitor: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><select name='emid' id='emid' size='1'><option value=''></option>
END
					&GetExhibitors_sel($emid,$dbh);
print << "END";
				</select></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Title: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='title' id='title' size='30' maxlength='255' value='$title'></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Short Name: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='short' id='short' size='30' maxlength='255' value='$short'></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Long Desc: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='long' id='long' size='30' maxlength='255' value='$long'></div></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td><div align='left'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><input type='checkbox' name='paid' id='paid' value='Y'> Entry Fee Paid</font></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Entry Fee: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'>\$<input type='text' name='fee' id='fee' size='5' maxlength='5' value='$fee'></font></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Upload Image: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><input type='file' name='image' id='image' size='25' maxlength='255' value='$image'></font></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>4H: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'>
						<input type='radio' name='4H' id='4H' value='Y'> Yes 
						<input type='radio' name='4H' id='4H' value='N' checked> No</font></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Status: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'>
						<input type='radio' name='status' id='status' value='A' checked> Active 
						<input type='radio' name='status' id='status' value='I'> Inactive</font></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2'color='#ffffff'><b>Notes: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><textarea name='desc' id='desc' cols='25' rows='10'>$notes</textarea></div></td>
			</tr>
			<tr><td colspan='3'>&nbsp;</td></tr>
			<tr><td colspan='3'><div align='center'><input type='submit' name='submit' value='Save Entry'></div></td></tr>
		</table></fieldset></form>
	</td></tr>
</td></tr>
<tr><td>&nbsp;</td></tr>
</table>
END
&DoFairFooter();
}

sub SaveEntry {
my ($id,$fid,$emid,$ecid,$title,$short,$long,$paid,$fee,$image,$h4,$status,$notes,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my $Title = &WriteQuotes($title);
my $Short = &WriteQuotes($short);
my $Long = &WriteQuotes($long);
my $Notes = &WriteQuotes($notes);
my $today = &GetMunge();
my $now = &GetRawTime();

if (!$id) {
	my ($eid) = undef;
	my $sth = $dbh->prepare("SELECT * FROM ExhibitEntry WHERE Title='$Title' AND EMID='$emid'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN ENTRYMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$eid = $ref->{'EID'};
	}
	$sth->finish();
	if (!$eid) {
		if ($paid eq "Y") {
			$feedate = $today;
			$feetime = $now;
		} else {
			$feedate = "0000-00-00";
			$feetime = "00:00:00";
		}
		my $sth2 = $dbh->prepare("INSERT INTO ExhibitEntry (ExhibitEntry.EMID,
													ExhibitEntry.FairID,
													ExhibitEntry.ECID,
													ExhibitEntry.Title,
													ExhibitEntry.ShortName,
													ExhibitEntry.LongDesc,
													ExhibitEntry.Image,
													ExhibitEntry.Paid,
													ExhibitEntry.Fee,
													ExhibitEntry.DatePaid,
													ExhibitEntry.TimePaid,
													ExhibitEntry.H4,
													ExhibitEntry.Status,
													ExhibitEntry.DateEntered,
													ExhibitEntry.TimeEntered,
													ExhibitEntry.Notes)
										VALUES ('$emid',
												'$fid',
												'$ecid',
												'$Title',
												'$Short',
												'$Long',
												'$mage',
												'$paid',
												'$fee',
												'$feedate',
												'$feetime',
												'$h4',
												'$status',
												'$today',
												'$now',
												'$Notes')"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN EXHIBITENTRY: $DBI::errstr");
		$sth2->finish();
		$sth2 = $dbh->prepare("SELECT * FROM ExhibitEntry WHERE Title='$Title' AND EMID='$emid'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN EXHIBITENTRY: $DBI::errstr");
		while (my $ref2 = $sth2->fetchrow_hashref()) {
			$id = $ref2->{'EID'};
		}
		$sth2->finish();
	} else {
		my $sth2 = $dbh->prepare("UPDATE ExhibitEntry SET ExhibitEntry.EMID='$emid',
													ExhibitEntry.FairID='$fid',
													ExhibitEntry.ECID='$ecid',
													ExhibitEntry.Title='$Title',
													ExhibitEntry.ShortName='$Short',
													ExhibitEntry.LongDesc='$Long',
													ExhibitEntry.Image='$image',
													ExhibitEntry.Paid='$paid',
													ExhibitEntry.Fee='$fee',
													ExhibitEntry.DatePaid='$feedate',
													ExhibitEntry.TimePaid='$feetime',
													ExhibitEntry.H4='$h4',
													ExhibitEntry.Status='$status',
													ExhibitEntry.Notes='$Notes'
												WHERE ExhibitEntry.EEID='$eid'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN EXHIBITENTRY: $DBI::errstr");
		$sth2->finish();
		$id = $eid;
	}
} else {
		my $sth2 = $dbh->prepare("UPDATE ExhibitEntry SET ExhibitEntry.EMID='$emid',
													ExhibitEntry.FairID='$fid',
													ExhibitEntry.ECID='$ecid',
													ExhibitEntry.Title='$Title',
													ExhibitEntry.ShortName='$Short',
													ExhibitEntry.LongDesc='$Long',
													ExhibitEntry.Image='$image',
													ExhibitEntry.Paid='$paid',
													ExhibitEntry.Fee='$fee',
													ExhibitEntry.DatePaid='$feedate',
													ExhibitEntry.TimePaid='$feetime',
													ExhibitEntry.H4='$h4',
													ExhibitEntry.Status='$status',
													ExhibitEntry.Notes='$Notes'
												WHERE ExhibitEntry.EEID='$id'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN EXHIBITENTRY: $DBI::errstr");
		$sth2->finish();
}
if ($dmo) {
	$dbh->disconnect();
}
}

sub SaveExhibitor {
my ($id,$edtid,$edid,$last,$first,$mi,$addr,$addr2,$city,$state,$zip,$phone,$cell,$email,$H4,$status,$edate,$etime,$notes,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
my $Last = &WriteQuotes($last);
my $First = &WriteQuotes($first);
my $Mi = &WriteQuotes($mi);
my $Addr = &WriteQuotes($addr);
my $Addr2 = &WriteQuotes($addr2);
my $City = &WriteQuotes($city);
if (!$id) {
	my ($emid) = undef;
	my $sth = $dbh->prepare("SELECT * FROM ExhibitMaster WHERE LastName='$Last' AND FirstName='$First' AND Email='$email'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHIBITMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$emid = $ref->{'EMID'};
	}
	$sth->finish();
	if (!$emid) {
		my $sth2 = $dbh->prepare("INSERT INTO ExhibitMaster (ExhibitMaster.LastName,
													ExhibitMaster.FirstName,
													ExhibitMaster.MI,
													ExhibitMaster.Addr,
													ExhibitMaster.Addr2,
													ExhibitMaster.City,
													ExhibitMaster.State,
													ExhibitMaster.Zip,
													ExhibitMaster.Phone,
													ExhibitMaster.Cell,
													ExhibitMaster.Email,
													ExhibitMaster.H4,
													ExhibitMaster.Status,
													ExhibitMaster.DateEntered,
													ExhibitMaster.TimeEntered,
													ExhibitMaster.Notes)
										VALUES ('$Last',
												'$First',
												'$Mi',
												'$Addr',
												'$Addr2',
												'$City',
												'$state',
												'$zip',
												'$phone',
												'$cell',
												'$email',
												'$h4',
												'$status',
												'$edate',
												'$etime',
												'$Notes')"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN EXHIBITMASTER: $DBI::errstr");
		$sth2->finish();
		$sth2 = $dbh->prepare("SELECT * FROM ExhibitMaster WHERE LastName='$Last' AND FirstName='$First' AND Email='$email'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN EXHIBITMASTER: $DBI::errstr");
		while (my $ref2 = $sth2->fetchrow_hashref()) {
			$id = $ref2->{'ECID'};
		}
		$sth2->finish();
	} else {
		my $sth2 = $dbh->prepare("UPDATE ExhibitMaster SET ExhibitMaster.LastName='$Last',
													ExhibitMaster.FirstName='$First',
													ExhibitMaster.MI='$Mi',
													ExhibitMaster.Addr='$Addr',
													ExhibitMaster.Addr2='$Addr2',
													ExhibitMaster.City='$City',
													ExhibitMaster.State='$state',
													ExhibitMaster.Zip='$zip',
													ExhibitMaster.Phone='$phone',
													ExhibitMaster.Cell='$cell',
													ExhibitMaster.Email='$email',
													ExhibitMaster.H4='$h4',
													ExhibitMaster.Status='$status',
													ExhibitMaster.DateEntered='$edate',
													ExhibitMaster.TimeEntered='$etime',
													ExhibitMaster.Notes='$Notes'
												WHERE ExhibitMaster.EMID='$emid'"); 
		$sth2->execute() or die &ErrorMsg("ERROR IN EXHIBITMASTER: $DBI::errstr");
		$sth2->finish();
		$id = $emid;
	}
} else {
	my $sth2 = $dbh->prepare("UPDATE ExhibitMaster SET ExhibitMaster.LastName='$Last',
												ExhibitMaster.FirstName='$First',
												ExhibitMaster.MI='$Mi',
												ExhibitMaster.Addr='$Addr',
												ExhibitMaster.Addr2='$Addr2',
												ExhibitMaster.City='$City',
												ExhibitMaster.State='$state',
												ExhibitMaster.Zip='$zip',
												ExhibitMaster.Phone='$phone',
												ExhibitMaster.Cell='$cell',
												ExhibitMaster.Email='$email',
												ExhibitMaster.H4='$h4',
												ExhibitMaster.Status='$status',
												ExhibitMaster.DateEntered='$edate',
												ExhibitMaster.TimeEntered='$etime',
												ExhibitMaster.Notes='$Notes'
											WHERE ExhibitMaster.EMID='$emid'"); 
	$sth2->execute() or die &ErrorMsg("ERROR IN EXHIBITMASTER: $DBI::errstr");
	$sth2->finish();
}
if ($dmo) {
	$dbh->disconnect();
}
}

sub GetEntry {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($emid,$fid,$ecid,$title,$short,$long,$image,$paid,$fee,$datepaid,$timepaid,$h4,$status,$edate,$etime,$notes) = undef;
	my $sth = $dbh->prepare("SELECT * FROM ExhibitEntry WHERE EEID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHIBITENTRY: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$emid = $ref->{'EMID'};
		$fid = $ref->{'FairID'};
		$ecid = $ref->{'ECID'};
		$title = $ref->{'Title'};
		$short = $ref->{'ShortName'};
		$long = $ref->{'LongDesc'};
		$image = $ref->{'Image'};
		$paid = $ref->{'Paid'};
		$fee = $ref->{'Fee'};
		$dpaid = $ref->{'DatePaid'};
		$tpaid = $ref->{'TimePaid'};
		$h4 = $ref->{'H4'};
		$status = $ref->{'Status'};
		$edate = $ref->{'DateEntered'};
		$etime = $ref->{'TimeEntered'};
		$notes = $ref->{'Notes'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $emid,$fid,$ecid,$title,$short,$long,$image,$paid,$fee,$dpaid,$tpaid,$h4,$status,$edate,$etime,$notes;
}

sub DelEntry {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $sth = $dbh->prepare("DELETE FROM ExhibitEntry WHERE EEID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHIBITENTRY: $DBI::errstr");
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
}

# ==================================================================
# Exhibit Entry Master
# ==================================================================
sub ExhibitEntries {
my ($cid,$dbh) = @_;

if (!$dbh) {
	$dbh = &ConnectDB();
}
my $bgcolor = "#FFFFFF";
&DoFairHeader(17);
my ($deptid,$edid,$displayid,$cname,$cdesc,$divname,$deptname) = undef;
if ($cid) {
	($deptid,$edid,$displayid,$cname,$cdesc) = &GetExhClass($cid,$dbh);		
	$divname = &GetExhDiv($edid,$dbh);
	$deptname = &GetExhDeptName($deptid,$dbh);
} else {
	&ErrorFairMsg("No Class Specified");
	exit;
}
print << "END";
<table width='1200' align='center' cellpadding='0' cellspacing='0' border='0'>
<tr><td><div align='left'><a href='./exhibits.cgi?a=eclass' onMouseOut='MM_swapImgRestore()' onMouseOver=\"MM_swapImage('GOBACK',\'','../images/exh_class_back_down.gif',1)\"><img src='../images/exh_class_back_up.gif' alt='Click HERE to go back to the main menu' name='GOBACK' width='250' height='50' border='0'></a></div></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td>
<table width='90%'cellpadding='0' cellspacing='0' border='0' align='center'>
<tr><td>&nbsp;</td></tr>
<tr><td valign="top">
	<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center'>
		<tr><td width="65%" valign="top">
				<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center'>
					<tr bgcolor='#3370af'><td colspan='5'><div align='center'><font face="Arial, Helvetica, sans-serif" size='5' color='#ffffff'><b>$divname Division : $deptname Department: $cname Class</b></font></div></td></tr>
					<tr bgcolor='#0c3f74'>
						<td width='40%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Exhibitor</b></font></div></td>
						<td width='40%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Email</b></font></div></td>
						<td width='20%'><div align='center'><font face="Arial, Helvetica, sans-serif" size='3' color='#FFFFFF'><b>Actions</b></font></div></td>
					</tr>
END
my ($edtid,$dispid,$name,$desc,$ret) = undef;
my $sth = $dbh->prepare("SELECT * FROM ExhibitEntry WHERE ECID='$cid' ORDER BY Title"); 
$sth->execute() or die &ErrorMsg("ERROR IN ExhibitEntry: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
	$ret++;
  	$eeid = $ref->{'EEID'};
    	$emid = $ref->{'EMID'};
  	$title = $ref->{'Title'};
  	$ename = &GetExhibitorName($emid,$dbh);
	if ($n) {
		print "<tr bgcolor='#FFFFFF'>";
		$n = undef;
	} else {
		print "<tr bgcolor='#efe5c0'>";
		$n++;
	}
     	print "<td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'>&nbsp;&nbsp;<b>$title</b></font></div></td>";
     	print "<td><div align='center'><font face='Arial, Helvetica, sans-serif' size='2'><b><i>$ename</i></b></font></div></td>";
	print "<td><div align='left'><a href='./exhibits.cgi?a=edite&id=$ecid'><img src='../images/editicon.gif' width='20' height='25' border='no'></a>&nbsp;
		<a href='./exhibits.cgi?a=dele&id=$ecid'><img src='../images/garbageicon.gif' width='20' height='25' border='no'></a>&nbsp;
		<a href='./exhibits.cgi?a=entries&id=$ecid'><img src='../images/depticon.gif' width='25' height='25' border='no'></a>		
		<a href='./exhibits.cgi?a=infoe&id=$ecid'><img src='../images/info2icon.gif' width='25' height='25' border='no'></a>
		</td>";
	print "</tr>";
}
$sth->finish();
if (!$ret) {
	print "<tr><td colspan='5'><div align='center'><font face='Arial, Helvetica, sans-serif' size='3' color='#ffffff'><b><i>(No Exhibitors Found)</i></b></font></div></td></tr>";
}
print << "END";
		</table>	
	</td>
	<td width='35%'>
		<form name='ADDEXH' method='post' action='./exhibits.cgi'>
		<input type='hidden' name='a' value='save_ex'>
		<input type='hidden' name='deptid' value='$deptid'>
		<input type='hidden' name='cid' value='$cid'>
		<fieldset><legend><font face='Arial, Helvetica, sans-serif' size='3' color='#FFFFFF'><b><i>Add/Edit Exhibitor</i></b></font></legend>
		<table width='95%'cellpadding='0' cellspacing='0' border='0' align='center'>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Last Name: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='last' id='last' size='25' maxlength='255' value='$last'></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>First Name/MI: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='first' id='first' size='25' maxlength='255' value='$first'>&nbsp;<input type='text' name='mi' id='mi' size='5' maxlength='5' value='$mi'></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Addr: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='addr' id='addr' size='25' maxlength='255' value='$addr'></div></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='addr2' id='addr2' size='25' maxlength='255' value='$addr2'></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>City: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='city' id='city' size='25' maxlength='255' value='$city'></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>State/Zip: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><select name='state' size='1'><option value=''></option>
END
	&GetStates_sel();
print << "END";
				</select>&nbsp;<input type='text' name='zip' id='zip' size='12' maxlength='255' value='$zip'></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#ffffff'><b>Phone: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='phone' id='phone' size='15' maxlength='5' value='$phone'></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face='Arial, Helvetica, sans-serif' size='2' color='#ffffff'><b>Cell: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='cell' id='cell' size='15' maxlength='15' value='$cell'></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Email: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><input type='text' name='email' id='email' size='25' maxlength='255' value='$email'></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>4H: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'>
						<input type='radio' name='4H' id='4H' value='Y'> Yes 
						<input type='radio' name='4H' id='4H' value='N' checked> No</font></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'><b>Status: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><font face="Arial, Helvetica, sans-serif" size='2' color='#ffffff'>
						<input type='radio' name='status' id='status' value='A' checked> Active 
						<input type='radio' name='status' id='status' value='I'> Inactive</font></div></td>
			</tr>
			<tr>
				<td><div align='right'><font face="Arial, Helvetica, sans-serif" size='2'color='#ffffff'><b>Notes: </b></font></div></td>
				<td>&nbsp;</td>
				<td><div align='left'><textarea name='desc' id='desc' cols='25' rows='10'>$notes</textarea></div></td>
			</tr>
			<tr><td colspan='3'>&nbsp;</td></tr>
			<tr><td colspan='3'><div align='center'><input type='submit' name='submit' value='Save Exhibitor'></div></td></tr>
		</table></fieldset></form>
	</td></tr>
</td></tr>
<tr><td>&nbsp;</td></tr>
</table>
END
&DoFairFooter();
}

sub GetExhibitor {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my ($edtid,$edid,$did,$name,$desc) = undef;
	my $sth = $dbh->prepare("SELECT * FROM ExhibitMaster WHERE EMID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$last = $ref->{'LastName'};
		$first = $ref->{'FirstName'};
		$mi = $ref->{'MI'};
		$addr = $ref->{'Addr'};
		$addr2 = $ref->{'Addr2'};
		$city = $ref->{'City'};
		$state = $ref->{'State'};
		$zip = $ref->{'Zip'};
		$phone = $ref->{'Phone'};
		$cell = $ref->{'Cell'};
		$email = $ref->{'Email'};
		$H4 = $ref->{'H4'};
		$status = $ref->{'Status'};
		$edate = $ref->{'DateEntered'};
		$etime = $ref->{'TimeEntered'};
		$notes = $ref->{'Notes'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $last,$first,$mi,$addr,$addr2,$city,$state,$zip,$phone,$cell,$email,$H4,$status,$edate,$etime,$notes;
}


sub DelExhibitor {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $sth = $dbh->prepare("DELETE FROM ExhibitMaster WHERE EMID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHIBITMASTER: $DBI::errstr");
	$sth->finish();
	$sth = $dbh->prepare("DELETE FROM ExhibitEntry WHERE EMID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHIBITENTRY: $DBI::errstr");
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
}

sub GetEDIDFromDept {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $sth = $dbh->prepare("SELECT * FROM ExhDept WHERE EDTID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHDEPT: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$edid = $ref->{'EDID'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $edid;
}

sub CheckEntries {
my ($id,$fid,$cid,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $ret = undef;
	my $sth = $dbh->prepare("SELECT * FROM ExhibitEntry WHERE EMID='$id' AND FairID='$fid' AND ECID='$cid'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHIBITENTRY: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ret++;
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $ret;
}

sub GetExhibitorInfoShort {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $ret = undef;
	my $sth = $dbh->prepare("SELECT * FROM ExhibitMaster WHERE EMID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHIBITMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ret++;
		$last = $ref->{'LastName'};
		$first = $ref->{'FirstName'};
		$mi = $ref->{'MI'};
		if ($mi) {
			$name = "$first $mi $last";
		} else {
			$name = "$first $last";
		}
		$cell = $ref->{'Cell'};
		$email = $ref->{'Email'};
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
return $name,$cell,$email;
}

sub GetExhibitors_sel {
my ($id,$dbh) = @_;

my $dmo = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$dmo++;
}
	my $ret = undef;
	my $sth = $dbh->prepare("SELECT * FROM ExhibitMaster ORDER BY LastName,FirstName"); 
	$sth->execute() or die &ErrorMsg("ERROR IN EXHIBITMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ret++;
		$emid = $ref->{'EMID'};
		$last = $ref->{'LastName'};
		$first = $ref->{'FirstName'};
		$mi = $ref->{'MI'};
		if ($mi) {
			$name = "$last, $first $mi";
		} else {
			$name = "$last, $first";
		}
		if ($id eq $emid) {
			print "<option value='$emid' selected>$name</option>";
		} else {
			print "<option value='$emid'>$name</option>";
		}
	}
	$sth->finish();
if ($dmo) {
	$dbh->disconnect();
}
}

1;