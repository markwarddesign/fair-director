#!/usr/bin/perl

require "dateutil.pl";
require "dbutil.pl";
require "fairlayout.pl";
require "errormaster.pl";
require "FairReports.pl";
require "Fair.pl";
require "Exhibits.pl";
require "Staff.pl";

use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use DBI;

my ($Action,$id,$t,$fid,$did,$name,$attend,$exp,$factor,$margin) = undef;
if (param()) {
	$Action = param('a');
	$t = param('t');
	$id = param('id');
	$fid = param('fid');
	$did = param('did');
	$cdid = param('cdid');
	$cid = param('cid');
	$sid = param('sid');
	$edid = param('edid');
	$emid = param('emid');
	$eeid = param('eeid');
	$edtid = param('edtid');
	$deptid = param('deptid');
	$name = param('name');
	$desc = param('desc');
	$title = param('title');
	$short = param('short');
	$long = param('long');
	$image = param('image');
	$h4 = param('h4');
	$status = param('status');
	$fee = param('fee');
	$paid = param('paid');
}

my $dbh = &ConnectDB();
if ($Action eq "main") {
	&ExhibitsMain(undef,$dbh);
} elsif ($Action eq "ediv") {
	&ExhibitDivisions($edid,$dbh);
#	&NewTemplate($id,$dbh);
} elsif ($Action eq "edept") {
	&ExhibitDepartments(undef,$edid,$dbh);
} elsif ($Action eq "eclass") {
	&ExhibitClasses(undef,$id,$dbh);
} elsif ($Action eq "eexh") {
	&EntryMaster(undef,$cid,$dbh);
} elsif ($Action eq "edit") {
	&ExhibitDivisions($edid,$dbh);
#	&NewTemplate($id,$dbh);
} elsif ($Action eq "editc") {
	&ExhibitClasses($id,$deptid,$dbh);
} elsif ($Action eq "editd") {
	&ExhibitDepartments($id,$edid,$dbh);
} elsif ($Action eq "editx") {
	&EntryMaster($id,$cid,$dbh);
} elsif ($Action eq "del") {
	&DelExhDiv($edid,$dbh);
	&ExhibitDivisions(undef,$dbh);
} elsif ($Action eq "deld") {
	&DelExhDept($id,$dbh);
	&ExhibitDepartments(undef,$edid,$dbh);
} elsif ($Action eq "delc") {
	&DelExhClass($id,$dbh);
	&ExhibitClasses(undef,$deptid,$dbh);
} elsif ($Action eq "delx") {
	&DelEntry($id,$dbh);
	&EntryMaster(undef,$cid,$dbh);
} elsif ($Action eq "save_ed") {
	&SaveExhDiv($id,$fid,$name,$did,$desc,$dbh);
	&ExhibitDivisions(undef,$dbh);
} elsif ($Action eq "save_edpt") {
	&SaveExhDept($id,$edid,$did,$sid,$name,$desc,$dbh);
	&ExhibitDepartments(undef,$edid,$dbh);
} elsif ($Action eq "save_ec") {
	&SaveExhClass($id,$deptid,$edid,$did,$name,$desc,$dbh);
	&ExhibitClasses(undef,$deptid,$dbh);
} elsif ($Action eq "save_en") {
	&SaveEntry($id,$fid,$emid,$cid,$title,$short,$long,$paid,$fee,$image,$h4,$status,$notes,$dbh);
	&EntryMaster(undef,$cid,$dbh);
} else {
	&ErrorFairMsg("I don't know what you want me to do: [ $Action ]");
}
$dbh->disconnect();

exit;