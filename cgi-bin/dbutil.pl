#  print "<script>window.setInterval('history.back(1)', 1000);</script>";
#  print "<script>window.parent.newChange();</script>";
require "dateutil.pl";

sub ConnectDB {
    $dbh = DBI->connect("DBI:mysql:database=fairdire_Master;
    host=localhost",
    "fairdire_DBA",
    "Fender1957",
    {PrintError => 0,RaiseError => 0,}) or die &ErrorMsg("Error Connecting: $DBI::errstr");
    return $dbh;
}

sub EncodeHtml 
{
	my $TextIn = shift;
	
	$TextIn =~ s/'/&#39;/g;
	$TextIn =~ s/"/&quot;/g;
	$TextIn =~ s/</&lt;/g;
	$TextIn =~ s/>/&gt;/g;
	
	return $TextIn;
}

sub DecodeHTML 
{
	my $TextIn = shift;
	
	$TextIn =~ s/&#39;/'/g;
	$TextIn =~ s/&#8217;/'/g;
	$TextIn =~ s/&#8220;/"/g;
	$TextIn =~ s/&#8221;/"/g;
	$TextIn =~ s/&quot;/"/g;
	$TextIn =~ s/&lt;/</g;
	$TextIn =~ s/&gt;/>/g;
	$TextIn =~ s/<br>/\n/g;
	
	return $TextIn;
}

sub WriteQuotes {
my $TextIn = shift;
	$TextIn =~ s/\\/\\\\/g; # handle single backslashes first before encoding below - cjc 2002.07.07
	$TextIn =~ s/'/''/g;
	$TextIn =~ s/"/\"/g;
	$TextIn =~ s/\n/<br>/g;
	$TextIn =~ s/@/\@/g;
#	$TextIn =~ s/!//g;
	
return $TextIn;
}

sub NoPerc {
my $TextIn = shift;

	$TextIn =~ s/%//g;

return $TextIn;
}

sub RestoreQuotes {
my $TextIn = shift;

	$TextIn =~ s/''/'/g;
	$TextIn =~ s/<br>/\n/g;
	$TextIn =~ s/<li>/\* /g;
return $TextIn;
}

sub LineBreaks {
my $TextIn = shift;

	$TextIn =~ s/\n/<br>/g;
	
return $TextIn;
}

sub GetStates {
my $id = shift;

    print "<select name='State' size='1'>";
    if ($id eq "AL") {
		print "<option value='AL' selected>AL</option>";
    } else {
		print "<option value='AL'>AL</option>";
    }
    if ($id eq "AK") {
		print "<option value='AK' selected>AK</option>";
    } else {
		print "<option value='AK'>AK</option>";
    }
    if ($id eq "AR") {
		print "<option value='AR' selected>AR</option>";
    } else {
		print "<option value='AR'>AR</option>";
    }
    if ($id eq "AZ") {
		print "<option value='AZ' selected>AZ</option>";
    } else {
		print "<option value='AZ'>AZ</option>";
    }
    if ($id eq "CA") {
		print "<option value='CA' selected>CA</option>";
    } else {
		print "<option value='CA'>CA</option>";
    }
    if ($id eq "CO") {
		print "<option value='CO' selected>CO</option>";
    } else {
		print "<option value='CO'>CO</option>";
    }
    if ($id eq "CT") {
		print "<option value='CT' selected>CT</option>";
    } else {
		print "<option value='CT'>CT</option>";
    }
    if ($id eq "DE") {
		print "<option value='DE' selected>DE</option>";
    } else {
		print "<option value='DE'>DE</option>";
    }
    if ($id eq "DC") {
		print "<option value='DC' selected>DC</option>";
    } else {
		print "<option value='DC'>DC</option>";
    }
    if ($id eq "FL") {
		print "<option vFLue='FL' selected>FL</option>";
    } else {
		print "<option vFLue='FL'>FL</option>";
    }
    if ($id eq "GA") {
		print "<option value='GA' selected>GA</option>";
    } else {
		print "<option value='GA'>GA</option>";
    }
    if ($id eq "HI") {
		print "<option value='HI' selected>HI</option>";
    } else {
		print "<option value='HI'>HI</option>";
    }
    if ($id eq "ID") {
		print "<option value='ID' selected>ID</option>";
    } else {
		print "<option value='ID'>ID</option>";
    }
    if ($id eq "IL") {
		print "<option value='IL' selected>IL</option>";
    } else {
		print "<option value='IL'>IL</option>";
    }
    if ($id eq "IA") {
		print "<option value='IA' selected>IA</option>";
    } else {
		print "<option value='IA'>IA</option>";
    }
    if ($id eq "KS") {
		print "<option value='KS' selected>KS</option>";
    } else {
		print "<option value='KS'>KS</option>";
    }
    if ($id eq "KY") {
		print "<option value='KY' selected>KY</option>";
    } else {
		print "<option value='KY'>KY</option>";
    }
    if ($id eq "LA") {
		print "<option value='LA' selected>LA</option>";
    } else {
		print "<option value='LA'>LA</option>";
    }
    if ($id eq "ME") {
		print "<option value='ME' seleMEed>ME</option>";
    } else {
		print "<option value='ME'>ME</option>";
    }
    if ($id eq "MD") {
		print "<option value='MD' selected>MD</option>";
    } else {
		print "<option value='MD'>MD</option>";
    }
    if ($id eq "MA") {
		print "<option value='MA' selected>MA</option>";
    } else {
		print "<option value='MA'>MA</option>";
    }
    if ($id eq "MI") {
		print "<option value='MI' selected>MI</option>";
    } else {
		print "<option value='MI'>MI</option>";
    }
    if ($id eq "MN") {
		print "<option value='MN' selected>MN</option>";
    } else {
		print "<option value='MN'>MN</option>";
    }
    if ($id eq "MS") {
		print "<option value='MS' selected>MS</option>";
    } else {
		print "<option value='MS'>MS</option>";
    }
    if ($id eq "MO") {
		print "<option value='MO' selected>MO</option>";
    } else {
		print "<option value='MO'>MO</option>";
    }
    if ($id eq "MT") {
		print "<option value='MT' selected>MT</option>";
    } else {
		print "<option value='MT'>MT</option>";
    }
    if ($id eq "NE") {
		print "<option value='NE' selected>NE</option>";
    } else {
		print "<option value='NE'>NE</option>";
    }
    if ($id eq "NV") {
		print "<option value='NV' selected>NV</option>";
    } else {
		print "<option value='NV'>NV</option>";
    }
    if ($id eq "NH") {
		print "<option value='NH' selected>NH</option>";
    } else {
		print "<option value='NH'>NH</option>";
    }
    if ($id eq "NM") {
		print "<option value='NM' selected>NM</option>";
    } else {
		print "<option value='NM'>NM</option>";
    }
    if ($id eq "NJ") {
		print "<option value='NJ' selected>NJ</option>";
    } else {
		print "<option value='NJ'>NJ</option>";
    }
    if ($id eq "NC") {
		print "<option value='NC' selected>NC</option>";
    } else {
		print "<option value='NC'>NC</option>";
    }
    if ($id eq "ND") {
		print "<option value='ND' selected>ND</option>";
    } else {
		print "<option value='ND'>ND</option>";
    }
    if ($id eq "NY") {
		print "<option value='NY' selected>NY</option>";
    } else {
		print "<option value='NY'>NY</option>";
    }
    if ($id eq "OH") {
		print "<option value='OH' selected>OH</option>";
    } else {
		print "<option value='OH'>OH</option>";
    }
    if ($id eq "OK") {
		print "<option value='OK' selected>OK</option>";
    } else {
		print "<option value='OK'>OK</option>";
    }
    if ($id eq "OR") {
		print "<option value='OR' selected>OR</option>";
    } else {
		print "<option value='OR'>OR</option>";
    }
    if ($id eq "PA") {
		print "<option value='PA' selected>PA</option>";
    } else {
		print "<option value='PA'>PA</option>";
    }
    if ($id eq "PR") {
		print "<option value='PR' selected>PR</option>";
    } else {
		print "<option value='PR'>PR</option>";
    }
    if ($id eq "RI") {
		print "<option value='RI' selected>RI</option>";
    } else {
		print "<option value='RI'>RI</option>";
    }
    if ($id eq "SC") {
		print "<option value='SC' selected>SC</option>";
    } else {
		print "<option value='SC'>SC</option>";
    }
    if ($id eq "SD") {
		print "<option value='SD' selected>SD</option>";
    } else {
		print "<option value='SD'>SD</option>";
    }
    if ($id eq "TN") {
		print "<option value='TN' selected>TN</option>";
    } else {
		print "<option value='TN'>TN</option>";
    }
    if ($id eq "TX") {
		print "<option value='TX' selected>TX</option>";
    } else {
		print "<option value='TX'>TX</option>";
    }
    if ($id eq "UT") {
		print "<option value='UT' selected>UT</option>";
    } else {
		print "<option value='UT'>UT</option>";
    }
    if ($id eq "VT") {
		print "<option value='VT' selected>VT</option>";
    } else {
		print "<option value='VT'>VT</option>";
    }
    if ($id eq "VI") {
		print "<option value='VI' selected>VI</option>";
    } else {
		print "<option value='VI'>VI</option>";
    }
    if ($id eq "VA") {
		print "<option value='VA' selected>VA</option>";
    } else {
		print "<option value='VA'>VA</option>";
    }
    if ($id eq "WA") {
		print "<option value='WA' selected>WA</option>";
    } else {
		print "<option value='WA'>WA</option>";
    }
    if ($id eq "WV") {
		print "<option value='WV' selected>WV</option>";
    } else {
		print "<option value='WV'>WV</option>";
    }
    if ($id eq "WI") {
		print "<option value='WI' selected>WI</option>";
    } else {
		print "<option value='WI'>WI</option>";
    }
    if ($id eq "WY") {
		print "<option WYlue='WY' selected>WY</option>";
    } else {
		print "<option WYlue='WY'>WY</option>";
    }
	print "</select>";
}

sub GetStates_sel {
my $id = shift;

    if ($id eq "AL") {
		print "<option value='AL' selected>AL</option>";
    } else {
		print "<option value='AL'>AL</option>";
    }
    if ($id eq "AR") {
		print "<option value='AR' selected>AR</option>";
    } else {
		print "<option value='AR'>AR</option>";
    }
    if ($id eq "AZ") {
		print "<option value='AZ' selected>AZ</option>";
    } else {
		print "<option value='AZ'>AZ</option>";
    }
    if ($id eq "CA") {
		print "<option value='CA' selected>CA</option>";
    } else {
		print "<option value='CA'>CA</option>";
    }
    if ($id eq "CO") {
		print "<option value='CO' selected>CO</option>";
    } else {
		print "<option value='CO'>CO</option>";
    }
    if ($id eq "CT") {
		print "<option value='CT' selected>CT</option>";
    } else {
		print "<option value='CT'>CT</option>";
    }
    if ($id eq "DE") {
		print "<option value='DE' selected>DE</option>";
    } else {
		print "<option value='DE'>DE</option>";
    }
    if ($id eq "DC") {
		print "<option value='DC' selected>DC</option>";
    } else {
		print "<option value='DC'>DC</option>";
    }
    if ($id eq "FL") {
		print "<option vFLue='FL' selected>FL</option>";
    } else {
		print "<option vFLue='FL'>FL</option>";
    }
    if ($id eq "GA") {
		print "<option value='GA' selected>GA</option>";
    } else {
		print "<option value='GA'>GA</option>";
    }
    if ($id eq "HI") {
		print "<option value='HI' selected>HI</option>";
    } else {
		print "<option value='HI'>HI</option>";
    }
    if ($id eq "ID") {
		print "<option value='ID' selected>ID</option>";
    } else {
		print "<option value='ID'>ID</option>";
    }
    if ($id eq "IL") {
		print "<option value='IL' selected>IL</option>";
    } else {
		print "<option value='IL'>IL</option>";
    }
    if ($id eq "IA") {
		print "<option value='IA' seleIAed>IA</option>";
    } else {
		print "<option value='IA'>IA</option>";
    }
    if ($id eq "KS") {
		print "<option value='KS' selected>KS</option>";
    } else {
		print "<option value='KS'>KS</option>";
    }
    if ($id eq "KY") {
		print "<option value='KY' selected>KY</option>";
    } else {
		print "<option value='KY'>KY</option>";
    }
    if ($id eq "LA") {
		print "<option value='LA' selected>LA</option>";
    } else {
		print "<option value='LA'>LA</option>";
    }
    if ($id eq "ME") {
		print "<option value='ME' seleMEed>ME</option>";
    } else {
		print "<option value='ME'>ME</option>";
    }
    if ($id eq "MD") {
		print "<option value='MD' selected>MD</option>";
    } else {
		print "<option value='MD'>MD</option>";
    }
    if ($id eq "MA") {
		print "<option value='MA' selected>MA</option>";
    } else {
		print "<option value='MA'>MA</option>";
    }
    if ($id eq "MI") {
		print "<option value='MI' selected>MI</option>";
    } else {
		print "<option value='MI'>MI</option>";
    }
    if ($id eq "MN") {
		print "<option value='MN' selected>MN</option>";
    } else {
		print "<option value='MN'>MN</option>";
    }
    if ($id eq "MS") {
		print "<option value='MS' selected>MS</option>";
    } else {
		print "<option value='MS'>MS</option>";
    }
    if ($id eq "MO") {
		print "<option value='MO' selected>MO</option>";
    } else {
		print "<option value='MO'>MO</option>";
    }
    if ($id eq "MT") {
		print "<option value='MT' selected>MT</option>";
    } else {
		print "<option value='MT'>MT</option>";
    }
    if ($id eq "NE") {
		print "<option value='NE' selected>NE</option>";
    } else {
		print "<option value='NE'>NE</option>";
    }
    if ($id eq "NV") {
		print "<option value='NV' selected>NV</option>";
    } else {
		print "<option value='NV'>NV</option>";
    }
    if ($id eq "NH") {
		print "<option value='NH' selected>NH</option>";
    } else {
		print "<option value='NH'>NH</option>";
    }
    if ($id eq "NM") {
		print "<option value='NM' selected>NM</option>";
    } else {
		print "<option value='NM'>NM</option>";
    }
    if ($id eq "NJ") {
		print "<option value='NJ' selected>NJ</option>";
    } else {
		print "<option value='NJ'>NJ</option>";
    }
    if ($id eq "NC") {
		print "<option value='NC' selected>NC</option>";
    } else {
		print "<option value='NC'>NC</option>";
    }
    if ($id eq "ND") {
		print "<option value='ND' selected>ND</option>";
    } else {
		print "<option value='ND'>ND</option>";
    }
    if ($id eq "NY") {
		print "<option value='NY' selected>NY</option>";
    } else {
		print "<option value='NY'>NY</option>";
    }
    if ($id eq "OH") {
		print "<option value='OH' selected>OH</option>";
    } else {
		print "<option value='OH'>OH</option>";
    }
    if ($id eq "OK") {
		print "<option value='OK' selected>OK</option>";
    } else {
		print "<option value='OK'>OK</option>";
    }
    if ($id eq "OR") {
		print "<option value='OR' selected>OR</option>";
    } else {
		print "<option value='OR'>OR</option>";
    }
    if ($id eq "PA") {
		print "<option value='PA' selected>PA</option>";
    } else {
		print "<option value='PA'>PA</option>";
    }
    if ($id eq "PR") {
		print "<option value='PR' selected>PR</option>";
    } else {
		print "<option value='PR'>PR</option>";
    }
    if ($id eq "RI") {
		print "<option value='RI' selected>RI</option>";
    } else {
		print "<option value='RI'>RI</option>";
    }
    if ($id eq "SC") {
		print "<option value='SC' selected>SC</option>";
    } else {
		print "<option value='SC'>SC</option>";
    }
    if ($id eq "SD") {
		print "<option value='SD' selected>SD</option>";
    } else {
		print "<option value='SD'>SD</option>";
    }
    if ($id eq "TN") {
		print "<option value='TN' selected>TN</option>";
    } else {
		print "<option value='TN'>TN</option>";
    }
    if ($id eq "TX") {
		print "<option value='TX' selected>TX</option>";
    } else {
		print "<option value='TX'>TX</option>";
    }
    if ($id eq "UT") {
		print "<option value='UT' selected>UT</option>";
    } else {
		print "<option value='UT'>UT</option>";
    }
    if ($id eq "VT") {
		print "<option value='VT' selected>VT</option>";
    } else {
		print "<option value='VT'>VT</option>";
    }
    if ($id eq "VI") {
		print "<option value='VI' selected>VI</option>";
    } else {
		print "<option value='VI'>VI</option>";
    }
    if ($id eq "VA") {
		print "<option value='VA' selected>VA</option>";
    } else {
		print "<option value='VA'>VA</option>";
    }
    if ($id eq "WA") {
		print "<option value='WA' selected>WA</option>";
    } else {
		print "<option value='WA'>WA</option>";
    }
    if ($id eq "WV") {
		print "<option value='WV' selected>WV</option>";
    } else {
		print "<option value='WV'>WV</option>";
    }
    if ($id eq "WI") {
		print "<option value='WI' selected>WI</option>";
    } else {
		print "<option value='WI'>WI</option>";
    }
    if ($id eq "WY") {
		print "<option WYlue='WY' selected>WY</option>";
    } else {
		print "<option WYlue='WY'>WY</option>";
    }
}

sub GetCountry {
my $country = shift;

print "<option value=''></option>";
if (($country eq "United States") || (!$country)) {
	print "<option value='United States' selected>United States</option>";
} else {
	print "<option value='United States'>United States</option>";
}
if ($country eq "United Kingdom") {
	print "<option value='United Kingdom' selected>United Kingdom</option>";
} else {
	print "<option value='United Kingdom'>United Kingdom</option>";
}
if ($country eq "Albania") {
	print "<option value='Albania' selected>Albania</option>";
} else {
	print "<option value='Albania'>Albania</option>";
}
if ($country eq "Algeria") {
	print "<option value='Algeria' selected>Algeria</option>";
} else {
	print "<option value='Algeria'>Algeria</option>";
}
if ($country eq "American Samoa") {
	print "<option value='American Samoa' selected>American Samoa</option>";
} else {
	print "<option value='American Samoa'>American Samoa</option>";
}
if ($country eq "Andorra") {
	print "<option value='Andorra' selected>Andorra</option>";
} else {
	print "<option value='Andorra'>Andorra</option>";
}
if ($country eq "Angola") {
	print "<option value='Angola' selected>Angola</option>";
} else {
	print "<option value='Angola'>Angola</option>";
}
if ($country eq "Anguilla") {
	print "<option value='Anguilla' selected>Anguilla</option>";
} else {
	print "<option value='Anguilla'>Anguilla</option>";
}
if ($country eq "Antigua") {
	print "<option value='Antigua' selected>Antigua</option>";
} else {
	print "<option value='Antigua'>Antigua</option>";
}
if ($country eq "Argentina") {
	print "<option value='Argentina' selected>Argentina</option>";
} else {
	print "<option value='Argentina'>Argentina</option>";
}
if ($country eq "Armenia") {
	print "<option value='Armenia' selected>Armenia</option>";
} else {
	print "<option value='Armenia'>Armenia</option>";
}
if ($country eq "Aruba") {
	print "<option value='Aruba' selected>Aruba</option>";
} else {
	print "<option value='Aruba'>Aruba</option>";
}
if ($country eq "Australia") {
	print "<option value='Australia' selected>Australia</option>";
} else {
	print "<option value='Australia'>Australia</option>";
}
if ($country eq "Austria") {
	print "<option value='Austria' selected>Austria</option>";
} else {
	print "<option value='Austria'>Austria</option>";
}
if ($country eq "Azerbaijan") {
	print "<option value='Azerbaijan' selected>Azerbaijan</option>";
} else {
	print "<option value='Azerbaijan'>Azerbaijan</option>";
}
if ($country eq "Bahamas") {
	print "<option value='Bahamas' selected>Bahamas</option>";
} else {
	print "<option value='Bahamas'>Bahamas</option>";
}
if ($country eq "Bahrain") {
	print "<option value='Bahrain' selected>Bahrain</option>";
} else {
	print "<option value='Bahrain'>Bahrain</option>";
}
if ($country eq "Bangladesh") {
	print "<option value='Bangladesh' selected>Bangladesh</option>";
} else {
	print "<option value='Bangladesh'>Bangladesh</option>";
}
if ($country eq "Barbados") {
	print "<option value='Barbados' selected>Barbados</option>";
} else {
	print "<option value='Barbados'>Barbados</option>";
}
if ($country eq "Barbuda") {
	print "<option value='Barbuda' selected>Barbuda</option>";
} else {
	print "<option value='Barbuda'>Barbuda</option>";
}
if ($country eq "Belgium") {
	print "<option value='Belgium' selected>Belgium</option>";
} else {
	print "<option value='Belgium'>Belgium</option>";
}
if ($country eq "Belize") {
	print "<option value='Belize' selected>Belize</option>";
} else {
	print "<option value='Belize'>Belize</option>";
}
if ($country eq "Benin") {
	print "<option value='Benin' selected>Benin</option>";
} else {
	print "<option value='Benin'>Benin</option>";
}
if ($country eq "Bermuda") {
	print "<option value='Bermuda' selected>Bermuda</option>";
} else {
	print "<option value='Bermuda'>Bermuda</option>";
}
if ($country eq "Bhutan") {
	print "<option value='Bhutan' selected>Bhutan</option>";
} else {
	print "<option value='Bhutan'>Bhutan</option>";
}
if ($country eq "Bolivia") {
	print "<option value='Bolivia' selected>Bolivia</option>";
} else {
	print "<option value='Bolivia'>Bolivia</option>";
}
if ($country eq "Bonaire") {
	print "<option value='Bonaire' selected>Bonaire</option>";
} else {
	print "<option value='Bonaire'>Bonaire</option>";
}
if ($country eq "Botswana") {
	print "<option value='Botswana' selected>Botswana</option>";
} else {
	print "<option value='Botswana'>Botswana</option>";
}
if ($country eq "Brazil") {
	print "<option value='Brazil' selected>Brazil</option>";
} else {
	print "<option value='Brazil'>Brazil</option>";
}
if ($country eq "British Virgin Islands") {
	print "<option value='British Virgin Islands' selected>British Virgin Islands</option>";
} else {
	print "<option value='British Virgin Islands'>British Virgin Islands</option>";
}
if ($country eq "Brunei") {
	print "<option value='Brunei' selected>Brunei</option>";
} else {
	print "<option value='Brunei'>Brunei</option>";
}
if ($country eq "Bulgaria") {
	print "<option value='Bulgaria' selected>Bulgaria</option>";
} else {
	print "<option value='Bulgaria'>Bulgaria</option>";
}
if ($country eq "Burundi") {
	print "<option value='Burundi' selected>Burundi</option>";
} else {
	print "<option value='Burundi'>Burundi</option>";
}
if ($country eq "Cambodia") {
	print "<option value='Cambodia' selected>Cambodia</option>";
} else {
	print "<option value='Cambodia'>Cambodia</option>";
}
if ($country eq "Cameroon") {
	print "<option value='Cameroon' selected>Cameroon</option>";
} else {
	print "<option value='Cameroon'>Cameroon</option>";
}
if ($country eq "Canada") {
	print "<option value='Canada' selected>Canada</option>";
} else {
	print "<option value='Canada'>Canada</option>";
}
if ($country eq "Cape Verde") {
	print "<option value='Cape Verde' selected>Cape Verde</option>";
} else {
	print "<option value='Cape Verde'>Cape Verde</option>";
}
if ($country eq "Cayman Islands") {
	print "<option value='Cayman Islands' selected>Cayman Islands</option>";
} else {
	print "<option value='Cayman Islands'>Cayman Islands</option>";
}
if ($country eq "Central African Republic") {
	print "<option value='Central African Republic' selected>Central African Republic</option>";
} else {
	print "<option value='Central African Republic'>Central African Republic</option>";
}
if ($country eq "Chad") {
	print "<option value='Chad' selected>Chad</option>";
} else {
	print "<option value='Chad'>Chad</option>";
}
if ($country eq "Channel Islands") {
	print "<option value='Channel Islands' selected>Channel Islands</option>";
} else {
	print "<option value='Channel Islands'>Channel Islands</option>";
}
if ($country eq "Chile") {
	print "<option value='Chile' selected>Chile</option>";
} else {
	print "<option value='Chile'>Chile</option>";
}
if ($country eq "China") {
	print "<option value='China' selected>China</option>";
} else {
	print "<option value='China'>China</option>";
}
if ($country eq "Colombia") {
	print "<option value='Colombia' selected>Colombia</option>";
} else {
	print "<option value='Colombia'>Colombia</option>";
}
if ($country eq "Congo") {
	print "<option value='Congo' selected>Congo</option>";
} else {
	print "<option value='Congo'>Congo</option>";
}
if ($country eq "Cook Islands") {
	print "<option value='Cook Islands' selected>Cook Islands</option>";
} else {
	print "<option value='Cook Islands'>Cook Islands</option>";
}
if ($country eq "Costa Rica") {
	print "<option value='Costa Rica' selected>Costa Rica</option>";
} else {
	print "<option value='Costa Rica'>Costa Rica</option>";
}
if ($country eq "Croatia") {
	print "<option value='Croatia' selected>Croatia</option>";
} else {
	print "<option value='Croatia'>Croatia</option>";
}
if ($country eq "Curacao") {
	print "<option value='Curacao' selected>Curacao</option>";
} else {
	print "<option value='Curacao'>Curacao</option>";
}
if ($country eq "Cyprus") {
	print "<option value='Cyprus' selected>Cyprus</option>";
} else {
	print "<option value='Cyprus'>Cyprus</option>";
}
if ($country eq "Czech Republic") {
	print "<option value='Czech Republic' selected>Czech Republic</option>";
} else {
	print "<option value='Czech Republic'>Czech Republic</option>";
}
if ($country eq "Denmark") {
	print "<option value='Denmark' selected>Denmark</option>";
} else {
	print "<option value='Denmark'>Denmark</option>";
}
if ($country eq "Djibouti") {
	print "<option value='Djibouti' selected>Djibouti</option>";
} else {
	print "<option value='Djibouti'>Djibouti</option>";
}
if ($country eq "Dominica") {
	print "<option value='Dominica' selected>Dominica</option>";
} else {
	print "<option value='Dominica'>Dominica</option>";
}
if ($country eq "Dominican Republic") {
	print "<option value='Dominican Republic' selected>Dominican Republic</option>";
} else {
	print "<option value='Dominican Republic'>Dominican Republic</option>";
}
if ($country eq "Ecuador") {
	print "<option value='Ecuador' selected>Ecuador</option>";
} else {
	print "<option value='Ecuador'>Ecuador</option>";
}
if ($country eq "Egypt") {
	print "<option value='Egypt' selected>Egypt</option>";
} else {
	print "<option value='Egypt'>Egypt</option>";
}
if ($country eq "El Salvador") {
	print "<option value='El Salvador' selected>El Salvador</option>";
} else {
	print "<option value='El Salvador'>El Salvador</option>";
}
if ($country eq "Equatorial Guinea") {
	print "<option value='Equatorial Guinea' selected>Equatorial Guinea</option>";
} else {
	print "<option value='Equatorial Guinea'>Equatorial Guinea</option>";
}
if ($country eq "Eritrea") {
	print "<option value='Eritrea' selected>Eritrea</option>";
} else {
	print "<option value='Eritrea'>Eritrea</option>";
}
if ($country eq "Estonia") {
	print "<option value='Estonia' selected>Estonia</option>";
} else {
	print "<option value='Estonia'>Estonia</option>";
}
if ($country eq "Ethiopia") {
	print "<option value='Ethiopia' selected>Ethiopia</option>";
} else {
	print "<option value='Ethiopia'>Ethiopia</option>";
}
if ($country eq "Faeroe Islands") {
	print "<option value='Faeroe Islands' selected>Faeroe Islands</option>";
} else {
	print "<option value='Faeroe Islands'>Faeroe Islands</option>";
}
if ($country eq "Fiji") {
	print "<option value='Fiji' selected>Fiji</option>";
} else {
	print "<option value='Fiji'>Fiji</option>";
}
if ($country eq "Finland") {
	print "<option value='Finland' selected>Finland</option>";
} else {
	print "<option value='Finland'>Finland</option>";
}
if ($country eq "France") {
	print "<option value='France' selected>France</option>";
} else {
	print "<option value='France'>France</option>";
}
if ($country eq "French Guiana") {
	print "<option value='French Guiana' selected>French Guiana</option>";
} else {
	print "<option value='French Guiana'>French Guiana</option>";
}
if ($country eq "French Polynesia") {
	print "<option value='French Polynesia' selected>French Polynesia</option>";
} else {
	print "<option value='French Polynesia'>French Polynesia</option>";
}
if ($country eq "Gabon") {
	print "<option value='Gabon' selected>Gabon</option>";
} else {
	print "<option value='Gabon'>Gabon</option>";
}
if ($country eq "Gambia") {
	print "<option value='Gambia' selected>Gambia</option>";
} else {
	print "<option value='Gambia'>Gambia</option>";
}
if ($country eq "Georgia") {
	print "<option value='Georgia' selected>Georgia</option>";
} else {
	print "<option value='Georgia'>Georgia</option>";
}
if ($country eq "Germany") {
	print "<option value='Germany' selected>Germany</option>";
} else {
	print "<option value='Germany'>Germany</option>";
}
if ($country eq "Ghana") {
	print "<option value='Ghana' selected>Ghana</option>";
} else {
	print "<option value='Ghana'>Ghana</option>";
}
if ($country eq "Gibraltar") {
	print "<option value='Gibraltar' selected>Gibraltar</option>";
} else {
	print "<option value='Gibraltar'>Gibraltar</option>";
}
if ($country eq "Great Britain") {
	print "<option value='Great Britain' selected>Great Britain</option>";
} else {
	print "<option value='Great Britain'>Great Britain</option>";
}
if ($country eq "Greece") {
	print "<option value='Greece' selected>Greece</option>";
} else {
	print "<option value='Greece'>Greece</option>";
}
if ($country eq "Greenland") {
	print "<option value='Greenland' selected>Greenland</option>";
} else {
	print "<option value='Greenland'>Greenland</option>";
}
if ($country eq "Grenada") {
	print "<option value='Grenada' selected>Grenada</option>";
} else {
	print "<option value='Grenada'>Grenada</option>";
}
if ($country eq "Guadeloupe") {
	print "<option value='Guadeloupe' selected>Guadeloupe</option>";
} else {
	print "<option value='Guadeloupe'>Guadeloupe</option>";
}
if ($country eq "Guam") {
	print "<option value='Guam' selected>Guam</option>";
} else {
	print "<option value='Guam'>Guam</option>";
}
if ($country eq "Guatemala") {
	print "<option value='Guatemala' selected>Guatemala</option>";
} else {
	print "<option value='Guatemala'>Guatemala</option>";
}
if ($country eq "Guinea") {
	print "<option value='Guinea' selected>Guinea</option>";
} else {
	print "<option value='Guinea'>Guinea</option>";
}
if ($country eq "Guinea Bissau") {
	print "<option value='Guinea Bissau' selected>Guinea Bissau</option>";
} else {
	print "<option value='Guinea Bissau'>Guinea Bissau</option>";
}
if ($country eq "Guyana") {
	print "<option value='Guyana' selected>Guyana</option>";
} else {
	print "<option value='Guyana'>Guyana</option>";
}
if ($country eq "Haiti") {
	print "<option value='Haiti' selected>Haiti</option>";
} else {
	print "<option value='Haiti'>Haiti</option>";
}
if ($country eq "Honduras") {
	print "<option value='Honduras' selected>Honduras</option>";
} else {
	print "<option value='Honduras'>Honduras</option>";
}
if ($country eq "Hong Kong") {
	print "<option value='Hong Kong' selected>Hong Kong</option>";
} else {
	print "<option value='Hong Kong'>Hong Kong</option>";
}
if ($country eq "Hungary") {
	print "<option value='Hungary' selected>Hungary</option>";
} else {
	print "<option value='Hungary'>Hungary</option>";
}
if ($country eq "Iceland") {
	print "<option value='Iceland' selected>Iceland</option>";
} else {
	print "<option value='Iceland'>Iceland</option>";
}
if ($country eq "India") {
	print "<option value='India' selected>India</option>";
} else {
	print "<option value='India'>India</option>";
}
if ($country eq "Indonesia") {
	print "<option value='Indonesia' selected>Indonesia</option>";
} else {
	print "<option value='Indonesia'>Indonesia</option>";
}
if ($country eq "Iraq") {
	print "<option value='Iraq' selected>Iraq</option>";
} else {
	print "<option value='Iraq'>Iraq</option>";
}
if ($country eq "Iran") {
	print "<option value='Iran' selected>Iran</option>";
} else {
	print "<option value='Iran'>Iran</option>";
}
if ($country eq "Ireland") {
	print "<option value='Ireland' selected>Ireland</option>";
} else {
	print "<option value='Ireland'>Ireland</option>";
}
if ($country eq "Northern Ireland") {
	print "<option value='Northern Ireland' selected>Northern Ireland</option>";
} else {
	print "<option value='Northern Ireland'>Northern Ireland</option>";
}
if ($country eq "Israel") {
	print "<option value='Israel' selected>Israel</option>";
} else {
	print "<option value='Israel'>Israel</option>";
}
if ($country eq "Italy") {
	print "<option value='Italy' selected>Italy</option>";
} else {
	print "<option value='Italy'>Italy</option>";
}
if ($country eq "Ivory Coast") {
	print "<option value='Ivory Coast' selected>Ivory Coast</option>";
} else {
	print "<option value='Ivory Coast'>Ivory Coast</option>";
}
if ($country eq "Jamaica") {
	print "<option value='Jamaica' selected>Jamaica</option>";
} else {
	print "<option value='Jamaica'>Jamaica</option>";
}
if ($country eq "Japan") {
	print "<option value='Japan' selected>Japan</option>";
} else {
	print "<option value='Japan'>Japan</option>";
}
if ($country eq "Jordan") {
	print "<option value='Jordan' selected>Jordan</option>";
} else {
	print "<option value='Jordan'>Jordan</option>";
}
if ($country eq "Kazakhstan") {
	print "<option value='Kazakhstan' selected>Kazakhstan</option>";
} else {
	print "<option value='Kazakhstan'>Kazakhstan</option>";
}
if ($country eq "Kenya") {
	print "<option value='Kenya' selected>Kenya</option>";
} else {
	print "<option value='Kenya'>Kenya</option>";
}
if ($country eq "Kuwait") {
	print "<option value='Kuwait' selected>Kuwait</option>";
} else {
	print "<option value='Kuwait'>Kuwait</option>";
}
if ($country eq "Kyrqyzstan") {
	print "<option value='Kyrqyzstan' selected>Kyrqyzstan</option>";
} else {
	print "<option value='Kyrqyzstan'>Kyrqyzstan</option>";
}
if ($country eq "Latvia") {
	print "<option value='Latvia' selected>Latvia</option>";
} else {
	print "<option value='Latvia'>Latvia</option>";
}
if ($country eq "Lebanon") {
	print "<option value='Lebanon' selected>Lebanon</option>";
} else {
	print "<option value='Lebanon'>Lebanon</option>";
}
if ($country eq "Liberia") {
	print "<option value='Liberia' selected>Liberia</option>";
} else {
	print "<option value='Liberia'>Liberia</option>";
}
if ($country eq "Leichtenstein") {
	print "<option value='Leichtenstein' selected>Leichtenstein</option>";
} else {
	print "<option value='Leichtenstein'>Leichtenstein</option>";
}
if ($country eq "Lithuania") {
	print "<option value='Lithuania' selected>Lithuania</option>";
} else {
	print "<option value='Lithuania'>Lithuania</option>";
}
if ($country eq "Luxembourg") {
	print "<option value='Luxembourg' selected>Luxembourg</option>";
} else {
	print "<option value='Luxembourg'>Luxembourg</option>";
}
if ($country eq "Macau") {
	print "<option value='Macau' selected>Macau</option>";
} else {
	print "<option value='Macau'>Macau</option>";
}
if ($country eq "Macedonia") {
	print "<option value='Macedonia' selected>Macedonia</option>";
} else {
	print "<option value='Macedonia'>Macedonia</option>";
}
if ($country eq "Madagascar") {
	print "<option value='Madagascar' selected>Madagascar</option>";
} else {
	print "<option value='Madagascar'>Madagascar</option>";
}
if ($country eq "Malawi") {
	print "<option value='Malawi' selected>Malawi</option>";
} else {
	print "<option value='Malawi'>Malawi</option>";
}
if ($country eq "Malaysia") {
	print "<option value='Malaysia' selected>Malaysia</option>";
} else {
	print "<option value='Malaysia'>Malaysia</option>";
}
if ($country eq "Maldives") {
	print "<option value='Maldives' selected>Maldives</option>";
} else {
	print "<option value='Maldives'>Maldives</option>";
}
if ($country eq "Mali") {
	print "<option value='Mali' selected>Mali</option>";
} else {
	print "<option value='Mali'>Mali</option>";
}
if ($country eq "Malta") {
	print "<option value='Malta' selected>Malta</option>";
} else {
	print "<option value='Malta'>Malta</option>";
}
if ($country eq "Marshall Islands") {
	print "<option value='Marshall Islands' selected>Marshall Islands</option>";
} else {
	print "<option value='Marshall Islands'>Marshall Islands</option>";
}
if ($country eq "Martinique") {
	print "<option value='Martinique' selected>Martinique</option>";
} else {
	print "<option value='Martinique'>Martinique</option>";
}
if ($country eq "Mauritania") {
	print "<option value='Mauritania' selected>Mauritania</option>";
} else {
	print "<option value='Mauritania'>Mauritania</option>";
}
if ($country eq "Mauritius") {
	print "<option value='Mauritius' selected>Mauritius</option>";
} else {
	print "<option value='Mauritius'>Mauritius</option>";
}
if ($country eq "Mexico") {
	print "<option value='Mexico' selected>Mexico</option>";
} else {
	print "<option value='Mexico'>Mexico</option>";
}
if ($country eq "Micronesia") {
	print "<option value='Micronesia' selected>Micronesia</option>";
} else {
	print "<option value='Micronesia'>Micronesia</option>";
}
if ($country eq "Moldova") {
	print "<option value='Moldova' selected>Moldova</option>";
} else {
	print "<option value='Moldova'>Moldova</option>";
}
if ($country eq "Monaco") {
	print "<option value='Monaco' selected>Monaco</option>";
} else {
	print "<option value='Monaco'>Monaco</option>";
}
if ($country eq "Mongolia") {
	print "<option value='Mongolia' selected>Mongolia</option>";
} else {
	print "<option value='Mongolia'>Mongolia</option>";
}
if ($country eq "Montserrat") {
	print "<option value='Montserrat' selected>Montserrat</option>";
} else {
	print "<option value='Montserrat'>Montserrat</option>";
}
if ($country eq "Morocco") {
	print "<option value='Morocco' selected>Morocco</option>";
} else {
	print "<option value='Morocco'>Morocco</option>";
}
if ($country eq "Mozambique") {
	print "<option value='Mozambique' selected>Mozambique</option>";
} else {
	print "<option value='Mozambique'>Mozambique</option>";
}
if ($country eq "Myanmar/Burma") {
	print "<option value='Myanmar/Burma' selected>Myanmar/Burma</option>";
} else {
	print "<option value='Myanmar/Burma'>Myanmar/Burma</option>";
}
if ($country eq "Namibia") {
	print "<option value='Namibia' selected>Namibia</option>";
} else {
	print "<option value='Namibia'>Namibia</option>";
}
if ($country eq "Nepal") {
	print "<option value='Nepal' selected>Nepal</option>";
} else {
	print "<option value='Nepal'>Nepal</option>";
}
if ($country eq "Netherlands") {
	print "<option value='Netherlands' selected>Netherlands</option>";
} else {
	print "<option value='Netherlands'>Netherlands</option>";
}
if ($country eq "Netherlands Antilles") {
	print "<option value='Netherlands Antilles' selected>Netherlands Antilles</option>";
} else {
	print "<option value='Netherlands Antilles'>Netherlands Antilles</option>";
}
if ($country eq "New Caledonia") {
	print "<option value='New Caledonia' selected>New Caledonia</option>";
} else {
	print "<option value='New Caledonia'>New Caledonia</option>";
}
if ($country eq "New Zealand") {
	print "<option value='New Zealand' selected>New Zealand</option>";
} else {
	print "<option value='New Zealand'>New Zealand</option>";
}
if ($country eq "Nicaragua") {
	print "<option value='Nicaragua' selected>Nicaragua</option>";
} else {
	print "<option value='Nicaragua'>Nicaragua</option>";
}
if ($country eq "Niger") {
	print "<option value='Niger' selected>Niger</option>";
} else {
	print "<option value='Niger'>Niger</option>";
}
if ($country eq "Nigeria") {
	print "<option value='Nigeria' selected>Nigeria</option>";
} else {
	print "<option value='Nigeria'>Nigeria</option>";
}
if ($country eq "Norway") {
	print "<option value='Norway' selected>Norway</option>";
} else {
	print "<option value='Norway'>Norway</option>";
}
if ($country eq "Oman") {
	print "<option value='Oman' selected>Oman</option>";
} else {
	print "<option value='Oman'>Oman</option>";
}
if ($country eq "Palau") {
	print "<option value='Palau' selected>Palau</option>";
} else {
	print "<option value='Palau'>Palau</option>";
}
if ($country eq "Panama") {
	print "<option value='Panama' selected>Panama</option>";
} else {
	print "<option value='Panama'>Panama</option>";
}
if ($country eq "Papua New Guinea") {
	print "<option value='Papua New Guinea' selected>Papua New Guinea</option>";
} else {
	print "<option value='Papua New Guinea'>Papua New Guinea</option>";
}
if ($country eq "Paraguay") {
	print "<option value='Paraguay' selected>Paraguay</option>";
} else {
	print "<option value='Paraguay'>Paraguay</option>";
}
if ($country eq "Peru") {
	print "<option value='Peru' selected>Peru</option>";
} else {
	print "<option value='Peru'>Peru</option>";
}
if ($country eq "Philippines") {
	print "<option value='Philippines' selected>Philippines</option>";
} else {
	print "<option value='Philippines'>Philippines</option>";
}
if ($country eq "Poland") {
	print "<option value='Poland' selected>Poland</option>";
} else {
	print "<option value='Poland'>Poland</option>";
}
if ($country eq "Portugal") {
	print "<option value='Portugal' selected>Portugal</option>";
} else {
	print "<option value='Portugal'>Portugal</option>";
}
if ($country eq "Puerto Rico") {
	print "<option value='Puerto Rico' selected>Puerto Rico</option>";
} else {
	print "<option value='Puerto Rico'>Puerto Rico</option>";
}
if ($country eq "Qatar") {
	print "<option value='Qatar' selected>Qatar</option>";
} else {
	print "<option value='Qatar'>Qatar</option>";
}
if ($country eq "Reunion") {
	print "<option value='Reunion' selected>Reunion</option>";
} else {
	print "<option value='Reunion'>Reunion</option>";
}
if ($country eq "Rwanda") {
	print "<option value='Rwanda' selected>Rwanda</option>";
} else {
	print "<option value='Rwanda'>Rwanda</option>";
}
if ($country eq "Saba") {
	print "<option value='Saba' selected>Saba</option>";
} else {
	print "<option value='Saba'>Saba</option>";
}
if ($country eq "Saipan") {
	print "<option value='Saipan' selected>Saipan</option>";
} else {
	print "<option value='Saipan'>Saipan</option>";
}
if ($country eq "Saudi Arabia") {
	print "<option value='Saudi Arabia' selected>Saudi Arabia</option>";
} else {
	print "<option value='Saudi Arabia'>Saudi Arabia</option>";
}
if ($country eq "Scotland") {
	print "<option value='Scotland' selected>Scotland</option>";
} else {
	print "<option value='Scotland'>Scotland</option>";
}
if ($country eq "Senegal") {
	print "<option value='Senegal' selected>Senegal</option>";
} else {
	print "<option value='Senegal'>Senegal</option>";
}
if ($country eq "Seychelles") {
	print "<option value='Seychelles' selected>Seychelles</option>";
} else {
	print "<option value='Seychelles'>Seychelles</option>";
}
if ($country eq "Sierra Leone") {
	print "<option value='Sierra Leone' selected>Sierra Leone</option>";
} else {
	print "<option value='Sierra Leone'>Sierra Leone</option>";
}
if ($country eq "Singapore") {
	print "<option value='Singapore' selected>Singapore</option>";
} else {
	print "<option value='Singapore'>Singapore</option>";
}
if ($country eq "Slovac Republic") {
	print "<option value='Slovac Republic' selected>Slovac Republic</option>";
} else {
	print "<option value='Slovac Republic'>Slovac Republic</option>";
}
if ($country eq "Slovenia") {
	print "<option value='Slovenia' selected>Slovenia</option>";
} else {
	print "<option value='Slovenia'>Slovenia</option>";
}
if ($country eq "South Africa") {
	print "<option value='South Africa' selected>South Africa</option>";
} else {
	print "<option value='South Africa'>South Africa</option>";
}
if ($country eq "South Korea") {
	print "<option value='South Korea' selected>South Korea</option>";
} else {
	print "<option value='South Korea'>South Korea</option>";
}
if ($country eq "Spain") {
	print "<option value='Spain' selected>Spain</option>";
} else {
	print "<option value='Spain'>Spain</option>";
}
if ($country eq "Sri Lanka") {
	print "<option value='Sri Lanka' selected>Sri Lanka</option>";
} else {
	print "<option value='Sri Lanka'>Sri Lanka</option>";
}
if ($country eq "Sudan") {
	print "<option value='Sudan' selected>Sudan</option>";
} else {
	print "<option value='Sudan'>Sudan</option>";
}
if ($country eq "Suriname") {
	print "<option value='Suriname' selected>Suriname</option>";
} else {
	print "<option value='Suriname'>Suriname</option>";
}
if ($country eq "Swaziland") {
	print "<option value='Swaziland' selected>Swaziland</option>";
} else {
	print "<option value='Swaziland'>Swaziland</option>";
}
if ($country eq "Sweden") {
	print "<option value='Sweden' selected>Sweden</option>";
} else {
	print "<option value='Sweden'>Sweden</option>";
}
if ($country eq "Switzerland") {
	print "<option value='Switzerland' selected>Switzerland</option>";
} else {
	print "<option value='Switzerland'>Switzerland</option>";
}
if ($country eq "Syria") {
	print "<option value='Syria' selected>Syria</option>";
} else {
	print "<option value='Syria'>Syria</option>";
}
if ($country eq "Taiwan") {
	print "<option value='Taiwan' selected>Taiwan</option>";
} else {
	print "<option value='Taiwan'>Taiwan</option>";
}
if ($country eq "Tanzania") {
	print "<option value='Tanzania' selected>Tanzania</option>";
} else {
	print "<option value='Tanzania'>Tanzania</option>";
}
if ($country eq "Thailand") {
	print "<option value='Thailand' selected>Thailand</option>";
} else {
	print "<option value='Thailand'>Thailand</option>";
}
if ($country eq "Togo") {
	print "<option value='Togo' selected>Togo</option>";
} else {
	print "<option value='Togo'>Togo</option>";
}
if ($country eq "Trinidad-Tobago") {
	print "<option value='Trinidad-Tobago' selected>Trinidad-Tobago</option>";
} else {
	print "<option value='Trinidad-Tobago'>Trinidad-Tobago</option>";
}
if ($country eq "Tunesia") {
	print "<option value='Tunesia' selected>Tunesia</option>";
} else {
	print "<option value='Tunesia'>Tunesia</option>";
}
if ($country eq "Turkey") {
	print "<option value='Turkey' selected>Turkey</option>";
} else {
	print "<option value='Turkey'>Turkey</option>";
}
if ($country eq "Turkmenistan") {
	print "<option value='Turkmenistan' selected>Turkmenistan</option>";
} else {
	print "<option value='Turkmenistan'>Turkmenistan</option>";
}
if ($country eq "United Arab EmiPreferences") {
	print "<option value='United Arab EmiPreferences' selected>United Arab EmiPreferences</option>";
} else {
	print "<option value='United Arab EmiPreferences'>United Arab EmiPreferences</option>";
}
if ($country eq "U.S. Virgin Islands") {
	print "<option value='U.S. Virgin Islands' selected>U.S. Virgin Islands</option>";
} else {
	print "<option value='U.S. Virgin Islands'>U.S. Virgin Islands</option>";
}
if ($country eq "Uganda") {
	print "<option value='Uganda' selected>Uganda</option>";
} else {
	print "<option value='Uganda'>Uganda</option>";
}
if ($country eq "Uruguay") {
	print "<option value='Uruguay' selected>Uruguay</option>";
} else {
	print "<option value='Uruguay'>Uruguay</option>";
}
if ($country eq "Uzbekistan") {
	print "<option value='Uzbekistan' selected>Uzbekistan</option>";
} else {
	print "<option value='Uzbekistan'>Uzbekistan</option>";
}
if ($country eq "Vanuatu") {
	print "<option value='Vanuatu' selected>Vanuatu</option>";
} else {
	print "<option value='Vanuatu'>Vanuatu</option>";
}
if ($country eq "Vatican City") {
	print "<option value='Vatican City' selected>Vatican City</option>";
} else {
	print "<option value='Vatican City'>Vatican City</option>";
}
if ($country eq "Venezuela") {
	print "<option value='Venezuela' selected>Venezuela</option>";
} else {
	print "<option value='Venezuela'>Venezuela</option>";
}
if ($country eq "Vietnam") {
	print "<option value='Vietnam' selected>Vietnam</option>";
} else {
	print "<option value='Vietnam'>Vietnam</option>";
}
if ($country eq "Wales") {
	print "<option value='Wales' selected>Wales</option>";
} else {
	print "<option value='Wales'>Wales</option>";
}
if ($country eq "Yemen") {
	print "<option value='Yemen' selected>Yemen</option>";
} else {
	print "<option value='Yemen'>Yemen</option>";
}
if ($country eq "Zaire") {
	print "<option value='Zaire' selected>Zaire</option>";
} else {
	print "<option value='Zaire'>Zaire</option>";
}
if ($country eq "Zambia") {
	print "<option value='Zambia' selected>Zambia</option>";
} else {
	print "<option value='Zambia'>Zambia</option>";
}
if ($country eq "Zimbabwe") {
	print "<option value='Zimbabwe' selected>Zimbabwe</option>";
} else {
	print "<option value='Zimbabwe'>Zimbabwe</option>";
}
}

sub Write_DayMaster {
my ($day,$sched,$type,$holiday,$insession,$earlyout,$dayofweek,$special,$daysleft,$term,$year) = @_;

	my $dbh = &ConnectDB();
	my $dayflag = &Check_DayMaster($day);
	if (!$dayflag) {
		my $sth = $dbh->prepare("INSERT INTO DayMaster (DayMaster.Day,
													 DayMaster.Sched,
													 DayMaster.Type,
													 DayMaster.Holiday,
													 DayMaster.InSession,
													 DayMaster.EarlyOut,
													 DayMaster.DayOfWeek,
													 DayMaster.Special,
													 DayMaster.DaysLeft) 
													VALUES ('$day',
															'$sched',
															'$type',
															'$holiday',
															'$insession',
															'$earlyout',
															'$dayofweek',
															'$special',
															'$daysleft')");
		$sth->execute() or die print "<p><h3>ERROR IN DAYMASTER: $DBI::errstr</h3></p>";
		print "<p><h3>Day Master has been Added.</h3></p>";
		$sth->finish();
		$dbh->disconnect();
	} else {
		my $sth = $dbh->prepare("UPDATE DayMaster SET DayMaster.Sched='$sched',
													 DayMaster.Type='$type',
													 DayMaster.Holiday='$holiday',
													 DayMaster.InSession='$insession',
													 DayMaster.EarlyOut='$earlyout',
													 DayMaster.DayOfWeek='$dayofweek',
													 DayMaster.Special='$special',
													 DayMaster.DaysLeft='$daysleft' WHERE DayMaster.Day='$day'");
		$sth->execute() or die print "<p><h3>ERROR IN DAYMASTER: $DBI::errstr</h3></p>";
		print "<p><h3>Day Master has been Updated.</h3></p>";
		$sth->finish();
		$dbh->disconnect();		
	}
}

sub Day_Master {
	my $error = &Write_Days();
	if ($error ne "99") {
		&Write_Holidays();
	}
}

sub Write_Days {						# Write Day Records for the next 10 years

my ($year,$month,$day) = undef;
$year = "2006";
$month = "03";
$day = "18";
$dayweek = "Saturday";
$newmonth = sprintf "%0.2u",$month;
$newday = sprintf "%0.2u",$day;
while ($year <= "2016") {
	$numdays = &GetDaysInMonth($newmonth);
	if ($newday > $numdays) {
		if (($year eq "2008") && ($newmonth eq "02")) {
			if ($newday > 29) {
				$newmonth++;
				$newday = 1;
			}
		} elsif ($newmonth < 12) {
			$newmonth++;
			$newday = 1;
		} else {
			$newmonth = 1;
			$newday = 1;
			$year++;
		}
	}
	$month = sprintf "%0.2u",$newmonth;
	$day = sprintf "%0.2u",$newday;
	$date = "$year-$month-$day";
	if ($insession eq "N") {
		$insess = "Weekend";
	} else {
		$insess = "School Day";
	}
	($sched,$type,$holiday,$earlyout,$daysleft,$special) = "";
	my $error = &Write_DayMaster($date,$sched,$type,$holiday,$insession,$earlyout,$dayweek,$special,$daysleft,$term,$year);
	if ($error eq "99") {
		return "99";
	}
	if ($dayweek eq "Monday") {
		$dayweek = "Tuesday";
		$insession = "Y";
	} elsif ($dayweek eq "Tuesday") {
		$dayweek = "Wednesday";
		$insession = "Y";
	} elsif ($dayweek eq "Wednesday") {
		$dayweek = "Thursday";
		$insession = "Y";
	} elsif ($dayweek eq "Thursday") {
		$dayweek = "Friday";
		$insession = "Y";
	} elsif ($dayweek eq "Friday") {
		$dayweek = "Saturday";
		$insession = "N";
	} elsif ($dayweek eq "Saturday") {
		$dayweek = "Sunday";
		$insession = "N";
	} elsif ($dayweek eq "Sunday") {
		$dayweek = "Monday";
		$insession = "Y";
	}
	$newday++;
	}
}

sub Tester {
my ($id,$dbh) = @_;

# &Tester($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE UserMaster SET UserMaster.Tester='Y' WHERE UserID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING UserMaster: $DBI::errstr");
  	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# UserMaster Database Routines
# ==========================================================================================================================================================
sub SaveUser {
my ($ln,$fn,$mi,$ad,$ad2,$ci,$state,$zip,$co,$email,$phone,$com,$type,$ip,$agree,$uid,$pwd,$expert,$matters,$dbh) = @_;

# my $id = &SaveUser($ln,$fn,$mi,$ad,$ad2,$ci,$state,$zip,$co,$email,$phone,$com,$type,$rip,$ip,$agree,$uid,$pwd,$dbh);

my $id = undef;
my $first = &WriteQuotes($fn);
my $last = &WriteQuotes($ln);
my $middle = &WriteQuotes($mi);
my $addr = &WriteQuotes($ad);
my $addr2 = &WriteQuotes($ad2);
my $city = &WriteQuotes($ci);
my $country = &WriteQuotes($co);
my $comments = &WriteQuotes($com);
my $Expert = &WriteQuotes($expert);
my $Matters = &WriteQuotes($matters);

my $today = &GetMunge();
my $time = &GetRawTime();

my $temp_fn = lc($first);
my $temp_fn2 = substr($temp_fn,0,1);
my $temp_fn3 = uc($temp_fn2);
my $temp_fn4 = substr($temp_fn,1);
my $first = $temp_fn3;
$first .= $temp_fn4;
my $temp_ln = lc($last);
my $temp_ln2 = substr($temp_ln,0,1);
my $temp_ln3 = uc($temp_ln2);
my $temp_ln4 = substr($temp_ln,1);
my $last = $temp_ln3;
$last .= $temp_ln4;

my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $id = undef;
my $sth = $dbh->prepare("SELECT * FROM UserMaster WHERE FirstName='$first' AND LastName='$last' AND Addr='$addr'"); 
$sth->execute() or die &ErrorMsg("ERROR IN UserMaster: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$id = $ref->{'UserID'};
}
$sth->finish();
if (!$id) {
	$sth = $dbh->prepare("INSERT INTO UserMaster (UserMaster.LastName,
  									UserMaster.FirstName,
  									UserMaster.MI,
  									UserMaster.Addr,
  									UserMaster.Addr2,
  									UserMaster.City,
  									UserMaster.State,
  									UserMaster.Zip,
  									UserMaster.Country,
  									UserMaster.Email,
  									UserMaster.Phone,
  									UserMaster.RegisterDate,
  									UserMaster.RegisterTime,
  									UserMaster.Comments,
  									UserMaster.Prereg,
  									UserMaster.IP,
  									UserMaster.Type,
  									UserMaster.RoleID,
  									UserMaster.Agree,
									UserMaster.UID,
									UserMaster.PWD,
									UserMaster.Expert,
									UserMaster.Matters) 
  					VALUES ('$last',
  							'$first',
  							'$middle',
  							'$addr',
  							'$addr2',
							'$city',
							'$state',
							'$zip',
							'$country',
							'$email',
							'$phone',
							'$today',
							'$time',
							'$comments',
							'Y',
							'$ip',
							'$type',
							'$rid',
							'$agree',
							'$uid',
							'$pwd',
							'$Expert',
							'$Matters')"); 
	    $sth->execute() or die &ErrorMsg("ERROR SAVING UserMaster: $DBI::errstr");
	    $sth->finish();
	    $sth = $dbh->prepare("SELECT * FROM UserMaster WHERE Email='$email' AND RegisterDate='$today' AND RegisterTime='$time'"); 
	    $sth->execute() or die &ErrorMsg("ERROR IN UserMaster: $DBI::errstr");
	    while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'UserID'};
	    }
	    $sth->finish();
#  &LogActivity($id,"11",$dbh);
} else {
#	&DeleteUser($id,$dbh);
	&ErrorMsg("A user with this name already exists.");
#	&ErrorMsg("An account for this email address already exists.<br><br>Please login using your userid and password.");
	exit;
}
if ($didmyown) {
	$dbh->disconnect();
}
return $id;
}

# ====================================================================================================================
# Simple User Check:	TRUE/POS = Good match.  FALSE=no match
# ====================================================================================================================
sub CheckUser {
my ($uid,$pwd,$dbh) = @_;

	    my $didmyown = undef;
	    if (!$dbh) {
			$didmyown++;
			$dbh = &ConnectDB();
	    }
	    my $id = undef;
	    my $sth = $dbh->prepare("SELECT * FROM UserMaster WHERE UID='$uid' AND PWD='$pwd'"); 
	    $sth->execute() or die &ErrorMsg("ERROR IN UserMaster: $DBI::errstr");
	    while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'UserID'};
	    }
	    $sth->finish();

	    if ($didmyown) {
			$dbh->disconnect();
	    }
	    return $id;
}

# ====================================================================================================================
# Simple User Subscription Level:	
#
#	undef 	= Free Subscription
#	I	= Individual Premium Subscription
#	F	= Firm Premium Subscription
# ====================================================================================================================
sub CheckSubscription {
my ($id,$dbh) = @_;

	    my $didmyown = undef;
	    if (!$dbh) {
			$didmyown++;
			$dbh = &ConnectDB();
	    }
	    my ($id,$st) = undef;
	    my $sth = $dbh->prepare("SELECT * FROM Subscriptions WHERE UserID='$id' AND Status='A' AND Agree='Y'"); 
	    $sth->execute() or die &ErrorMsg("ERROR IN SUBSCRIPTIONS: $DBI::errstr");
	    while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'SubID'};
		$st = $ref->{'SubTypeID'};
	    }
	    $sth->finish();

	    if ($didmyown) {
			$dbh->disconnect();
	    }
	    if ($st eq "1") {
		    $st = undef;
	    } elsif (($st >= 2) && ($st <= 6)) {
		    $st = "I";
	    } elsif ($st >= 7) {
		    $st = "F";
	    }
	    $st = "I";
	    return $st;
}

sub SaveUserCC {
my ($id,$ctype,$cnm,$cnum,$cmonth,$cyear,$csec,$dbh) = @_;

# my $id = &SaveUserCC($id,$ctype,$cnm,$cnum,$cmonth,$cyear,$csec,$dbh);

my $cname = &WriteQuotes($cnm);
my $today = &GetMunge();
my $now = &GetRawTime();
my $didmyown = undef;
if (!$dbh) {	
	$dbh = &ConnectDB();
	$didmyown++;
}
my $ucid = undef;
my $sth = $dbh->prepare("SELECT * FROM UserCard WHERE UserID='$id'"); 
$sth->execute() or die &ErrorMsg("ERROR IN UserCard: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$ucid = $ref->{'UserCardID'};
}
$sth->finish();
if (!$ucid) {
	$sth = $dbh->prepare("INSERT INTO UserCard (UserCard.UserID,
						UserCard.CardType,
  						UserCard.CardName,
  						UserCard.CardNum,
  						UserCard.CardMonth,
  						UserCard.CardYear,
  						UserCard.CardSec,
						UserCard.CreateDate,
						UserCard.CreateTime) 
  					VALUES ('$id',
  						'$ctype',
  						'$cname',
  						'$cnum',
  						'$cmonth',
						'$cyear',
						'$csec',
						'$today',
						'$now')"); 
  $sth->execute() or die &ErrorMsg("ERROR SAVING UserCard: $DBI::errstr");
  $sth->finish();
}
if ($didmyown) {
	$dbh->disconnect();
}
return $ucid;
}

sub UpdateUser {
my ($id,$ln,$fn,$mi,$ad,$ad2,$ci,$state,$zip,$co,$email,$phone,$com,$type,$rid,$ip,$dbh) = @_;

# my $id = &SaveUser($id,$ln,$fn,$mi,$ad,$ad2,$ci,$state,$zip,$co,$email,$phone,$com,$type,$ip,$dbh);

	my $first = &WriteQuotes($fn);
	my $last = &WriteQuotes($ln);
	my $middle = &WriteQuotes($mi);
	my $addr = &WriteQuotes($ad);
	my $addr2 = &WriteQuotes($ad2);
	my $city = &WriteQuotes($ci);
	my $country = &WriteQuotes($co);
	my $comments = &WriteQuotes($com);
	my $temp_fn = lc($first);
	my $temp_fn2 = substr($temp_fn,0,1);
	my $temp_fn3 = uc($temp_fn2);
	my $temp_fn4 = substr($temp_fn,1);
	my $first = $temp_fn3;
	$first .= $temp_fn4;
	my $temp_ln = lc($last);
	my $temp_ln2 = substr($temp_ln,0,1);
	my $temp_ln3 = uc($temp_ln2);
	my $temp_ln4 = substr($temp_ln,1);
	my $last = $temp_ln3;
	$last .= $temp_ln4;

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE UserMaster SET UserMaster.LastName='$last',
  									UserMaster.FirstName='$first',
  									UserMaster.MI='$mi',
  									UserMaster.Addr='$addr',
  									UserMaster.Addr2='$addr2',
  									UserMaster.City='$city',
  									UserMaster.State='$state',
  									UserMaster.Zip='$zip',
  									UserMaster.Country='$country',
  									UserMaster.Email='$email',
  									UserMaster.Comments='$comments',
  									UserMaster.Type='$type',
									UserMaster.RoleID='$rid' WHERE UserID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR SAVING UserMaster: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"12",$dbh);
	if ($didmyown) { 		
		$dbh->disconnect();
	}
	return;
}

sub GetUserInfo {
my ($id,$dbh) = @_;

# my ($last,$first,$mi,$addr,$addr2,$city,$state,$zip,$country,$email,$phone,$rdate,$rtime,$comments,$prereg,$ip,$type,$rid,$status,$tester,$agree) = &GetUserInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($last,$first,$mi,$addr,$addr2,$city,$state,$zip,$country,$email,$phone,$rdate,$rtime,$comments,$prereg,$ip,$type,$rid,$status,$agree) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM UserMaster WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("DATABASE ERROR ENCOUNTERED: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$last = $ref->{'LastName'};
  		$first = $ref->{'FirstName'};
	  	$mi = $ref->{'MI'};
	  	$addr = $ref->{'Addr'};
	  	$addr2 = $ref->{'Addr2'};
  		$city = $ref->{'City'};
	  	$state = $ref->{'State'};
	  	$zip = $ref->{'Zip'};
	  	$country = $ref->{'Country'};
  		$email = $ref->{'Email'};
	  	$phone = $ref->{'Phone'};
	  	$rdate = $ref->{'RegisterDate'};
		$rtime = $ref->{'RegisterTime'};
		$comments = $ref->{'Comments'};
	  	$prereg = $ref->{'Prereg'};
  		$ip = $ref->{'IP'};
	  	$type = $ref->{'Type'};
	  	$rid = $ref->{'RoleID'};
	  	$status = $ref->{'Status'};
	  	$tester = $ref->{'Tester'};
	  	$agree = $ref->{'Agree'};
	  	$expert = $ref->{'Expert'};
	  	$matters = $ref->{'Matters'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $last,$first,$mi,$addr,$addr2,$city,$state,$zip,$country,$email,$phone,$rdate,$rtime,$comments,$prereg,$ip,$type,$rid,$status,$tester,$agree,$expert,$matters;
}

# ==========================================================================================================================================================
# LogActivity Database Routines
# ==========================================================================================================================================================
sub SaveLogActivity {
my ($activity,$dbh) = @_;

# my $id = &SaveLogActivity($activity,$dbh);

my $id = undef;

my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT * FROM LogActivity WHERE Activity='$activity'"); 
$sth->execute() or die &ErrorMsg("DATABASE ERROR ENCOUNTERED (SaveLogActivity): $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$id = $ref->{'LAID'};
}
$sth->finish();
if (!$id) {
  $sth = $dbh->prepare("INSERT INTO LogActivity (LogActivity.Activity) VALUES ('$activity')"); 
  $sth->execute() or die &ErrorMsg("ERROR SAVING LOGACTIVITY: $DBI::errstr");
  $sth->finish();
  $sth = $dbh->prepare("SELECT * FROM LogActivity WHERE Activity='$activity'"); 
  $sth->execute() or die &ErrorMsg("ERROR IN LOGACTIVITY: $DBI::errstr");
  while (my $ref = $sth->fetchrow_hashref()) {
  	$aid = $ref->{'LAID'};
  }
  $sth->finish();
  &LogActivity($id,"16",$dbh);
} 
if ($didmyown) {
	$dbh->disconnect();
}
return $aid;
}

sub UpdateLogActivity {
my ($id,$activity,$dbh) = @_;

# &UpdateLogActivity($id,$activity,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE LogActivity SET LogActivity.UserID='$activity',LogActivity.FirmID='$fid' WHERE LogActivityID='$aid'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING AFFILIATIONS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"14",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetLogActivityInfo {
my ($id,$dbh) = @_;

# my ($activity,$fid) = &GetLogActivityInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($activity) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM LogActivity WHERE LAID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING LOGACTIVITY: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$activity = $ref->{'Activity'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $activity;
}

sub GetLogActivity_sel {
my ($id,$dbh) = @_;

# &GetLogActivity_sel($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($aid,$activity) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM LogActivity ORDER BY Activity"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING LOGACTIVITY: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$aid = $ref->{'LAID'};
	  	$activity = $ref->{'Activity'};
		if ($aid eq $id) {
			print "<option value='$aid' selected>$activity</option>";
		} else {
			print "<option value='$aid'>$activity</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteLogActivity {
my ($id,$dbh) = @_;

# &DeleteLogActivity($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM LogActivity WHERE LAID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING LOGACTIVITY: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"15",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# Rate Type Database Routines
# ==========================================================================================================================================================
sub SaveRateType {
my ($ratetype,$dbh) = @_;

# my $id = &SaveRateType($ratetype,$dbh);

my $id = undef;
my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT * FROM RateTypes WHERE RateType='$ratetype'"); 
$sth->execute() or die &ErrorMsg("DATABASE ERROR ENCOUNTERED (SaveRateType): $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$id = $ref->{'RTID'};
}
$sth->finish();
if (!$id) {
  $sth = $dbh->prepare("INSERT INTO RateTypes (RateTypes.RateType) VALUES ('$ratetype')"); 
  $sth->execute() or die &ErrorMsg("ERROR SAVING RATETYPES: $DBI::errstr");
  $sth->finish();
  $sth = $dbh->prepare("SELECT * FROM RateTypes WHERE RateType='$ratetype'"); 
  $sth->execute() or die &ErrorMsg("ERROR IN RATETYPES: $DBI::errstr");
  while (my $ref = $sth->fetchrow_hashref()) {
  	$aid = $ref->{'RTID'};
  }
  $sth->finish();
  &LogActivity($id,"19",$dbh);
} 
if ($didmyown) {
	$dbh->disconnect();
}
return $aid;
}

sub UpdateRateTypes {
my ($id,$ratetype,$dbh) = @_;

# &UpdateRateTypes($id,$ratetype,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE RateTypes SET RateTypes.UserID='$ratetype',RateTypes.FirmID='$fid' WHERE RateTypesID='$aid'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING AFFILIATIONS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"20",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetRateTypeInfo {
my ($id,$dbh) = @_;

# my $ratetype = &GetRateTypeInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($ratetype) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM RateTypes WHERE RTID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING RATETYPES: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$ratetype = $ref->{'RateType'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $ratetype;
}

sub GetRateTypes_sel {
my ($id,$dbh) = @_;

# &GetRateTypes_sel($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($aid,$ratetype) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM RateTypes ORDER BY RateType"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING RATETYPES: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$aid = $ref->{'RTID'};
	  	$ratetype = $ref->{'RateType'};
		if ($aid eq $id) {
			print "<option value='$aid' selected>$ratetype</option>";
		} else {
			print "<option value='$aid'>$ratetype</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteRateType {
my ($id,$dbh) = @_;

# &DeleteRateType($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM RateTypes WHERE RTID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING RATETYPES: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"21",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# Subscription Type Database Routines
# ==========================================================================================================================================================
sub SaveSubType {
my ($subtype,$amount,$term,$type,$dbh) = @_;

# my $id = &SaveSubType($subtype,$amount,$term,$type,$dbh);

my $id = undef;
my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT * FROM SubTypes WHERE Description='$subtype'"); 
$sth->execute() or die &ErrorMsg("DATABASE ERROR ENCOUNTERED (SaveSubType): $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$id = $ref->{'SubTypeID'};
}
$sth->finish();
if (!$id) {
  $sth = $dbh->prepare("INSERT INTO SubTypes (SubTypes.Description,SubTypes.Amount,SubTypes.Term,SubTypes.Type) VALUES ('$subtype','$amount','$term','$type')"); 
  $sth->execute() or die &ErrorMsg("ERROR SAVING SUBTYPES: $DBI::errstr");
  $sth->finish();
  $sth = $dbh->prepare("SELECT * FROM SubTypes WHERE Description='$subtype'"); 
  $sth->execute() or die &ErrorMsg("ERROR IN SUBTYPES: $DBI::errstr");
  while (my $ref = $sth->fetchrow_hashref()) {
  	$stid = $ref->{'SubTypeID'};
  }
  $sth->finish();
  &LogActivity($stid,"22",$dbh);
} 
if ($didmyown) {
	$dbh->disconnect();
}
return $aid;
}

sub UpdateSubTypes {
my ($id,$desc,$amt,$term,$dbh) = @_;

# &UpdateSubTypes($id,$desc,$amt,$term,$type,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE SubTypes SET SubTypes.Description='$desc',SubTypes.Amount='$amt',SubTypes.Term='$term',SubTypes.Type='$type' WHERE SubTypesID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING SUBTYPES: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"23",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetSubTypeInfo {
my ($id,$dbh) = @_;

# my ($desc,$amt,$term,$type) = &GetSubTypeInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($desc,$amt,$term) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM SubTypes WHERE SubTypeID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING SUBTYPES: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$desc = $ref->{'Description'};
	  	$amt = $ref->{'Amount'};
	  	$term = $ref->{'Term'};
	  	$type = $ref->{'Type'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $desc,$amt,$term,$type;
}

sub GetSubTypes_sel {
my ($id,$type,$dbh) = @_;

# &GetSubTypes_sel($id,$type,$dbh);

	my ($sth,$didmyown) = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($aid,$subtype) = undef;
	if ($type) {
	  	$sth = $dbh->prepare("SELECT * FROM SubTypes WHERE Type='$type'"); 
	} else {
	  	$sth = $dbh->prepare("SELECT * FROM SubTypes"); 
	}
	$sth->execute() or die &ErrorMsg("ERROR GETTING SUBTYPES: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$stid = $ref->{'SubTypeID'};
	  	$desc = $ref->{'Description'};
	  	$amt = $ref->{'Amount'};
	  	$type = $ref->{'Type'};
		if ($stid eq $id) {
			print "<option value='$stid' selected>$desc - \$$amt</option>";
		} else {
			print "<option value='$stid'>$desc - \$$amt</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteSubType {
my ($id,$dbh) = @_;

# &DeleteSubType($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM SubTypes WHERE SubTypeID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING SUBTYPES: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"24",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# Firms Database Routines
# ==========================================================================================================================================================
sub SaveFirm {
my ($uid,$nm,$ad,$ad2,$ci,$state,$zip,$ctry,$repl,$repf,$repe,$url,$ph,$com,$current,$fav,$oneclick,$pay,$restrict,$restnum,$exc1,$exc2,$dbh) = @_;

# my $id = &SaveFirm($uid,$nm,$ad,$ad2,$ci,$state,$zip,$ctry,$repl,$repf,$repe,$url,$ph,$com,$current,$fav,$oneclick,$pay,$restrict,$restnum,$exc1,$exc2,$dbh);

my ($fid,$id) = undef;
my $name = &WriteQuotes($nm);
my $addr = &WriteQuotes($ad);
my $addr2 = &WriteQuotes($ad2);
my $city = &WriteQuotes($ci);
my $country = &WriteQuotes($ctry);
my $replast = &WriteQuotes($repl);
my $repfirst = &WriteQuotes($repf);
my $comments = &WriteQuotes($com);

my $today = &GetMunge();
my $now = &GetRawTime();

my $temp_fn = lc($repfirst);
my $temp_fn2 = substr($temp_fn,0,1);
my $temp_fn3 = uc($temp_fn2);
my $temp_fn4 = substr($temp_fn,1);
my $repfirst = $temp_fn3;
$repfirst .= $temp_fn4;
my $temp_ln = lc($replast);
my $temp_ln2 = substr($temp_ln,0,1);
my $temp_ln3 = uc($temp_ln2);
my $temp_ln4 = substr($temp_ln,1);
my $replast = $temp_ln3;
$replast .= $temp_ln4;

my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT * FROM Firms WHERE Name='$name' AND City='$city' AND State='$state' AND RepEmail='$repe'"); 
$sth->execute() or die &ErrorMsg("ERROR IN FIRMS: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$id = $ref->{'FirmID'};
}
$sth->finish();
if (!$id) {
	$sth = $dbh->prepare("INSERT INTO Firms (Firms.Name,
  						Firms.Addr,
  						Firms.Addr2,
  						Firms.City,
  						Firms.State,
  						Firms.Zip,
  						Firms.Country,
  						Firms.RepFirst,
  						Firms.RepLast,
  						Firms.RepEmail,
  						Firms.URL,
  						Firms.RepPhone,
  						Firms.RegisterDate,
  						Firms.RegisterTime,
  						Firms.Comments) 
  					VALUES ('$name',
  						'$addr',
  						'$addr2',
  						'$city',
  						'$state',
						'$zip',
						'$country',
						'$repfirst',
						'$replast',
						'$repe',
						'$url',
						'$ph',
						'$today',
						'$now',
						'$comments')"); 
  $sth->execute() or die &ErrorMsg("ERROR SAVING FIRM: $DBI::errstr");
  $sth->finish();
  $sth = $dbh->prepare("SELECT * FROM Firms WHERE Name='$name' AND RegisterDate='$today' AND RegisterTime='$now'"); 
  $sth->execute() or die &ErrorMsg("ERROR IN Firms: $DBI::errstr");
  while (my $ref = $sth->fetchrow_hashref()) {
  	$fid = $ref->{'FirmID'};
  }
  $sth->finish();
} 
if (($fid) && ($uid)) {
	my $foid = undef;
	my $sth = $dbh->prepare("SELECT * FROM FirmOpts WHERE UserID='$uid' AND FirmID='$fid'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN FirmOpts: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	    $foid = $ref->{'FOID'};
	}
	$sth->finish();
	if (!$foid) {
	    $sth = $dbh->prepare("INSERT INTO FirmOpts (FirmOpts.UserID,
  						FirmOpts.FirmID,
  						FirmOpts.Current,
  						FirmOpts.Favorite,
  						FirmOpts.OneClick,
  						FirmOpts.Restrictions,
  						FirmOpts.RestrictNum,
						FirmOpts.AllDay,
						FirmOpts.MultipleCopies) 
  					VALUES ('$uid',
  						'$fid',
  						'$current',
  						'$fav',
  						'$oneclick',
						'$restrict',
						'$restnum',
						'$exc1',
						'$exc2')"); 
	    $sth->execute() or die &ErrorMsg("ERROR SAVING FirmOpts: $DBI::errstr");
	    $sth->finish();
	    $sth = $dbh->prepare("SELECT * FROM FirmOpts WHERE UserID='$uid' AND FirmID='$fid'"); 
	    $sth->execute() or die &ErrorMsg("ERROR IN FirmOpts: $DBI::errstr");
	    while (my $ref = $sth->fetchrow_hashref()) {
		$foid = $ref->{'FOID'};
	    }
	    $sth->finish();
	    $sth = $dbh->prepare("SELECT * FROM Affiliations WHERE UserID='$uid' AND FirmID='$fid'"); 
	    $sth->execute() or die &ErrorMsg("ERROR IN FirmOpts: $DBI::errstr");
	    while (my $ref = $sth->fetchrow_hashref()) {
		$aid = $ref->{'AID'};
	    }
	    $sth->finish();
	    if (!$aid) {
		$sth = $dbh->prepare("INSERT INTO Affiliations (Affiliations.UserID,
  						Affiliations.FirmID,
  						Affiliations.CreateDate,
  						Affiliations.CreateTime) 
  					VALUES ('$uid',
  						'$fid',
  						'$today',
  						'$now')"); 
		$sth->execute() or die &ErrorMsg("ERROR SAVING AFFILIATIONS: $DBI::errstr");
		$sth->finish();
		$sth = $dbh->prepare("SELECT * FROM Affiliations WHERE UserID='$uid' AND FirmID='$fid'"); 
		$sth->execute() or die &ErrorMsg("ERROR IN AFFILIATIONS: $DBI::errstr");
		while (my $ref = $sth->fetchrow_hashref()) {
			$aid = $ref->{'AID'};
		}
		$sth->finish();
            }

        }
} else {
	&ErrorMsg("Missing Identifier: [ $fid | $uid ]");
}
if ($didmyown) {
	$dbh->disconnect();
}
return $fid;
}

sub UpdateFirm {
my ($id,$nm,$ad,$ad2,$ci,$state,$zip,$ctry,$repl,$repf,$repe,$url,$ph,$status,$com,$dbh) = @_;

# &UpdateFirm($id,$nm,$ad,$ad2,$ci,$state,$zip,$ctry,$repl,$repf,$repe,$url,$ph,$status,$com,$dbh);

	my $name = &WriteQuotes($nm);
	my $addr = &WriteQuotes($ad);
	my $addr2 = &WriteQuotes($ad2);
	my $city = &WriteQuotes($ci);
	my $country = &WriteQuotes($ctry);
	my $replast = &WriteQuotes($repl);
	my $repfirst = &WriteQuotes($repf);
	my $comments = &WriteQuotes($com);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE Firms SET Firms.Name='$name',
												Firms.Addr='$addr',
												Firms.Addr2='$addr2'
												Firms.City='$city',
												Firms.State='$state',
												Firms.Zip='$zip',
												Firms.RepFirst='$repfirst',
												Firms.RepLast='$replast',
												Firms.RepEmail='$repe',
												Firms.URL='$url',
												Firms.RepPhone='$ph',
												Firms.Comments='$comments',
												Firms.Status='$status'
												 WHERE FirmID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING FIRMS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"25",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetFirmInfo {
my ($id,$dbh) = @_;

# my ($name,$addr,$addr2,$city,$state,$zip,$country,$replast,$repfirst,$repemail,$url,$phone,$regdate,$regtime,$status,$comments) = &GetFirmInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($name,$addr,$addr2,$city,$state,$zip,$country,$replast,$repfirst,$repemail,$url,$phone,$regdate,$regtime,$status,$comments) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Firms WHERE FirmID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING FIRMS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$name = $ref->{'Name'};
	  	$addr = $ref->{'Addr'};
	  	$addr2 = $ref->{'Addr2'};
	  	$city = $ref->{'City'};
	  	$state = $ref->{'State'};
	  	$zip = $ref->{'Zip'};	  	
	  	$country = $ref->{'Country'};
	  	$repfirst = $ref->{'RepFirst'};
	  	$replast = $ref->{'RepLast'};	
	  	$repemail = $ref->{'RepEmail'};
	  	$url = $ref->{'URL'};	  	
	  	$phone = $ref->{'RepPhone'};
	  	$regdate = $ref->{'RegisterDate'};
	  	$regtime = $ref->{'RegisterTime'};
	  	$comments = $ref->{'Comments'};
	  	$status = $ref->{'Status'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $name,$addr,$addr2,$city,$state,$zip,$country,$replast,$repfirst,$repemail,$url,$phone,$regdate,$regtime,$status,$comments;
}

sub GetFirmName {
my ($id,$dbh) = @_;

# my $name = &GetFirmName($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $name = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Firms WHERE FirmID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING FIRMS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$name = $ref->{'Name'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $name;
}

sub GetFirms_sel {
my ($id,$dbh) = @_;

# &GetFirms_sel($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($fid,$subtype) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Firms ORDER BY Name"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING FIRMS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$fid = $ref->{'FirmID'};
	  	$name = $ref->{'Name'};
	  	$city = $ref->{'City'};
	  	$state = $ref->{'State'};
		if ($fid eq $id) {
			print "<option value='$fid' selected>$name ($city, $state)</option>";
		} else {
			print "<option value='$fid'>$name ($city, $state)</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteFirm {
my ($id,$dbh) = @_;

# &DeleteFirm($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM Firms WHERE FirmID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING FIRMS: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"26",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# Networks Database Routines
# ==========================================================================================================================================================
sub SaveNetwork {
my ($uid,$cid,$dbh) = @_;

# my $id = &SaveNetwork($uid,$cid,$dbh);

my $nid = undef;
my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT * FROM Networks WHERE UserID='$uid' AND ColleagueID='$cid'"); 
$sth->execute() or die &ErrorMsg("DATABASE ERROR ENCOUNTERED (SaveNetworks): $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$nid = $ref->{'NetworkID'};
}
$sth->finish();
if (!$nid) {
	$sth = $dbh->prepare("INSERT INTO Networks (Networks.UserID,Networks.ColleagueID) VALUES ('$uid','$cid')");
	$sth->execute() or die &ErrorMsg("ERROR SAVING NETWORKS: $DBI::errstr");
	$sth->finish();
	$sth = $dbh->prepare("SELECT * FROM Networks WHERE UserID='$uid' AND ColleagueID='$cid'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN NETWORKS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$nid = $ref->{'NetworkID'};
  	}
  	$sth->finish();
  &LogActivity($id,"27",$dbh);
} 
if ($didmyown) {
	$dbh->disconnect();
}
return $aid;
}

sub UpdateNetwork {
my ($nid,$uid,$cid,$dbh) = @_;

# &UpdateNetwork($nid,$uid,$cid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE Networks SET Networks.UserID='$uid',Networks.ColleagueID='$cid' WHERE NetworkID='$nid'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING NETWORKS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"28",$dbh);
	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetNetworkInfo {
my ($id,$dbh) = @_;

# my ($uid,$cid) = &GetNetworkInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($uid,$cid) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Networks WHERE NetworkID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING NETWORKS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$uid = $ref->{'UserID'};
  		$cid = $ref->{'ColleagueID'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $uid,$fid;
}

sub GetNetworks_sel {
my ($id,$dbh) = @_;

# &GetNetworks_sel($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($aid,$uid,$cid) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Networks ORDER BY UserID"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING NETWORKS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$nid = $ref->{'NetworkID'};
	  	$cid = $ref->{'ColleagueID'};
  		my $sth2 = $dbh->prepare("SELECT * FROM UserMaster WHERE UserID='$cid'"); 
		$sth2->execute() or die &ErrorMsg("ERROR GETTING FIRM: $DBI::errstr");
		while (my $ref2 = $sth2->fetchrow_hashref()) {
			$last = $ref2->{'LastName'};
			$first = $ref2->{'FirstName'};
			$mi = $ref2->{'MI'};
		}
		$sth2->finish();
		if ($nid eq $id) {
			print "<option value='$nid' selected>$last, $first $mi</option>";
		} else {
			print "<option value='$nid'>$last, $first $mi</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteNetwork {
my ($id,$dbh) = @_;

# &DeleteNetwork($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM Networks WHERE NetworkID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING NETWORKS: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"29",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# Logs Database Routines
# ==========================================================================================================================================================
sub LogActivity {
my ($uid,$activity,$dbh) = @_;

# my $id = &LogActivity($uid,$amount,$term,$dbh);

	if ($uid) {
		my $lid = undef;	
		my $didmyown = undef;
		if (!$dbh) {
			$dbh = &ConnectDB();
			$didmyown++;
		}
		my $today = &GetMunge();
		my $now = &GetRawTime();
		my $sth = $dbh->prepare("INSERT INTO Logs (Logs.UserID,Logs.Activity,Logs.LogDate,Logs.LogTime) VALUES ('$uid','$activity','$today','$now')"); 
   		$sth->execute() or die &ErrorMsg("ERROR SAVING LOGS: $DBI::errstr");
  		$sth->finish();
  		$sth = $dbh->prepare("SELECT * FROM Logs WHERE UserID='$uid' AND LogDate='$today' AND LogTime='$now'"); 
  		$sth->execute() or die &ErrorMsg("ERROR IN LOGS: $DBI::errstr");
  		while (my $ref = $sth->fetchrow_hashref()) {
  			$lid = $ref->{'LogID'};
  		}
  		$sth->finish();
  		&LogActivity($stid,"28",$dbh);
		 
		if ($didmyown) {
			$dbh->disconnect();
		}
	}
	return $lid;
}

sub UpdateLog {
my ($id,$uid,$activity,$dbh) = @_;

# &UpdateLog($id,$uid,$activity,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE Logs SET Logs.UserID='$uid',Logs.Activity='$activity' WHERE LogID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING LOG: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"29",$dbh);
	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetLogInfo {
my ($id,$dbh) = @_;

# my ($uid,$activity,$logdate,$logtime) = &GetLogInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($uid,$activity,$logdate,$logtime) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Logs WHERE LogID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING LOGS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$uid = $ref->{'UserID'};
	  	$activity = $ref->{'Activity'};
	  	$date = $ref->{'LogDate'};
	  	$time = $ref->{'LogTime'};
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $uid,$activity,$logdate,$logtime;
}

sub GetLogs_sel {
my ($id,$dbh) = @_;

# &GetLogs_sel($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($uid,$activity,$logdate,$logtime) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Logs ORDER BY LogDate,LogTime"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING LOGS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$lid = $ref->{'LogID'};
	  	$uid = $ref->{'UserID'};
	  	$activity = $ref->{'Activity'};
	  	$date = $ref->{'LogDate'};
	  	$time = $ref->{'LogTime'};
		if ($lid eq $id) {
			print "<option value='$lid' selected>$act - $date $time</option>";
		} else {
			print "<option value='$lid'>$act - $date $time</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteLog {
my ($id,$dbh) = @_;

# &DeleteLog($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM Logs WHERE LogID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING LOGS: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"29",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# PayDate Database Routines
# ==========================================================================================================================================================
sub SavePaydate {
my ($fid,$cutdate,$paydate,$dbh) = @_;

# my $id = &SavePaydate($fid,$cutdate,$paydate,$dbh);

	my $pdid = undef;
	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $today = &GetMunge();
	my $now = &GetRawTime();
	my $sth = $dbh->prepare("INSERT INTO PayDate (PayDate.FirmID,PayDate.CutoffDate,PayDate.PayDate) VALUES ('$fid','$cutdate','$paydate')"); 
   	$sth->execute() or die &ErrorMsg("ERROR SAVING PayDate: $DBI::errstr");
  	$sth->finish();
  	$sth = $dbh->prepare("SELECT * FROM PayDate WHERE FirmID='$fid' AND CutoffDate='$cutdate' AND PayDate='$paydate'"); 
  	$sth->execute() or die &ErrorMsg("ERROR IN PAYDATE: $DBI::errstr");
  	while (my $ref = $sth->fetchrow_hashref()) {
  		$pdid = $ref->{'PayDateID'};
  	}
  	$sth->finish();
  	&LogActivity($stid,"30",$dbh);
	 
	if ($didmyown) {
		$dbh->disconnect();
	}
	return $pdid;
}

sub UpdatePayDate {
my ($id,$fid,$cutdate,$paydate,$dbh) = @_;

# &UpdatePayDate($id,$fid,$cutdate,$paydate,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE PayDate SET PayDate.FirmID='$fid',PayDate.CutoffDate='$cutdate',PayDate.PayDate='$paydate' WHERE PayDateID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING PAYDATE: $DBI::errstr");
  	$sth->finish();
  	&PayDateCutoffDate($id,"31",$dbh);
	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetPayDateInfo {
my ($id,$dbh) = @_;

# my ($fid,$cutdate,$paydate) = &GetPayDateInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($fid,$cutdate,$paydate) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM PayDate WHERE PayDateID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING PAYDATE: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$fid = $ref->{'FirmID'};
	  	$cutdate = $ref->{'CutoffDate'};
	  	$paydate = $ref->{'PayDate'};
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $fid,$cutdate,$paydate;
}

sub GetPayDates_sel {
my ($id,$fid,$dbh) = @_;

# &GetPayDates_sel($id,$fid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($cutdate,$paydate) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM PayDate WHERE FirmID='$fid' ORDER BY CutoffDate,PayDate"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING PAYDATE: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$pdid = $ref->{'PayDateID'};
	  	$cutdate = $ref->{'CutoffDate'};
	  	$paydate = $ref->{'PayDate'};
		if ($pdid eq $id) {
			print "<option value='$pdid' selected>Cutoff: $cutdate Payday: $paydate</option>";
		} else {
			print "<option value='$pdid'>Cutoff: $cutdate  Payday: $paydate</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeletePayDate {
my ($id,$dbh) = @_;

# &DeletePayDate($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM PayDate WHERE PayDateID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING PAYDATE: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"32",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# Subscriptions Database Routines
# ==========================================================================================================================================================
sub SaveSub {
my ($fid,$uid,$subtype,$agree,$dbh) = @_;

# my $id = &SaveSub($fid,$uid,$subtype,$agree,$dbh);

my $id = undef;
my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $today = &GetMunge();
my $now = &GetRawTime();
my $sth = $dbh->prepare("SELECT * FROM Subscriptions WHERE FirmID='$fid' AND UserID='$uid' AND SubTypeID='$subtype'"); 
$sth->execute() or die &ErrorMsg("ERROR IN UserMaster: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$id = $ref->{'SubID'};
}
$sth->finish();
if (!$id) {
	$sth = $dbh->prepare("INSERT INTO Subscriptions (Subscriptions.FirmID,
							Subscriptions.UserID,
  							Subscriptions.SubDate,
  							Subscriptions.SubTime,
  							Subscriptions.SubTypeID,
  							Subscriptions.Status,
							Subscriptions.Agree) 
						VALUES ('$fid',
  							'$uid',
  							'$today',
  							'$now',
  							'$subtype',
  							'A',
							'$agree')"); 
  $sth->execute() or die &ErrorMsg("ERROR SAVING SUBSCRIPTION: $DBI::errstr");
  $sth->finish();
} 
if ($didmyown) {
	$dbh->disconnect();
}
return;
}

sub UpdateSub {
my ($id,$subtype,$dbh) = @_;

# &UpdateSub($id,$subtype,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE Subscriptions SET Subscriptions.SubTypeID='$subtype' WHERE SubID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING SUBSCRIPTIONS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"34",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub RenewSub {
my ($id,$dbh) = @_;

# &RenewSub($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $today = &GetMunge();
	my $now = &GetRawTime();
	my $sth = $dbh->prepare("UPDATE Subscriptions SET Subscriptions.ExpDate='$today',Subscriptions.ExpTime='$now' WHERE SubID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING SUBSCRIPTIONS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"34",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub RenewSub_byFirm {
my ($id,$dbh) = @_;

# &RenewSub_byFirm($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $today = &GetMunge();
	my $now = &GetRawTime();
	my $sth = $dbh->prepare("UPDATE Subscriptions SET Subscriptions.ExpDate='$today',Subscriptions.ExpTime='$now' WHERE FirmID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING SUBSCRIPTIONS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"34",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub RenewSub_byUser {
my ($id,$dbh) = @_;

# &RenewSub_byUser($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $today = &GetMunge();
	my $now = &GetRawTime();
	my $sth = $dbh->prepare("UPDATE Subscriptions SET Subscriptions.ExpDate='$today',Subscriptions.ExpTime='$now' WHERE UserID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING SUBSCRIPTIONS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"34",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub CancelSub {
my ($id,$dbh) = @_;

# &CancelSub($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $today = &GetMunge();
	my $now = &GetRawTime();
	my $sth = $dbh->prepare("UPDATE Subscriptions SET Subscriptions.CanDate='$today',Subscriptions.CanTime='$now' WHERE SubID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING SUBSCRIPTIONS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"34",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub CancelSub_byFirm {
my ($id,$dbh) = @_;

# &CancelSub_byFirm($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $today = &GetMunge();
	my $now = &GetRawTime();
	my $sth = $dbh->prepare("UPDATE Subscriptions SET Subscriptions.CanDate='$today',Subscriptions.CanTime='$now' WHERE FirmID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING SUBSCRIPTIONS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"34",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub CancelSub_byUser {
my ($id,$dbh) = @_;

# &CancelSub_byUser($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $today = &GetMunge();
	my $now = &GetRawTime();
	my $sth = $dbh->prepare("UPDATE Subscriptions SET Subscriptions.CanDate='$today',Subscriptions.CanTime='$now' WHERE UserID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING SUBSCRIPTIONS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"34",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetSubInfo {
my ($id,$dbh) = @_;

# my ($fid,$uid,$subdate,$subtime,$subtype,$agree,$expdate,$exptime,$status,$candate,$cantime) = &GetSubInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($fid,$uid,$subdate,$subtime,$subtype,$agree,$expdate,$exptime,$status,$candate,$cantime) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Subscriptions WHERE SubID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING SUBSCRIPTIONS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$fid = $ref->{'FirmID'};
	  	$uid = $ref->{'UserID'};
	  	$subdate = $ref->{'SubDate'};
	  	$subtime = $ref->{'SubTime'};
	  	$agree = $ref->{'Agreement'};
	  	$subtype = $ref->{'SubTypeID'};
	  	$expdate = $ref->{'ExpDate'};	  	
	  	$exptime = $ref->{'ExpTime'};
	  	$status = $ref->{'Status'};
	  	$candate = $ref->{'CancelDate'};	
	  	$cantime = $ref->{'CancelTime'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $fid,$uid,$subdate,$subtime,$subtype,$agree,$expdate,$exptime,$status,$candate,$cantime;
}

sub GetSubInfo_byFirm {
my ($id,$dbh) = @_;

# my ($fid,$uid,$subdate,$subtime,$subtype,$agree,$expdate,$exptime,$status,$candate,$cantime) = &GetSubInfo_byFirm($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($fid,$uid,$subdate,$subtime,$subtype,$agree,$expdate,$exptime,$status,$candate,$cantime) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Subscriptions WHERE FirmID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING SUBSCRIPTIONS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$fid = $ref->{'FirmID'};
	  	$uid = $ref->{'UserID'};
	  	$subdate = $ref->{'SubDate'};
	  	$subtime = $ref->{'SubTime'};
	  	$agree = $ref->{'Agreement'};
	  	$subtype = $ref->{'SubTypeID'};
	  	$expdate = $ref->{'ExpDate'};	  	
	  	$exptime = $ref->{'ExpTime'};
	  	$status = $ref->{'Status'};
	  	$candate = $ref->{'CancelDate'};	
	  	$cantime = $ref->{'CancelTime'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $fid,$uid,$subdate,$subtime,$subtype,$agree,$expdate,$exptime,$status,$candate,$cantime;
}

sub GetSubInfo_byUser {
my ($id,$dbh) = @_;

# my ($fid,$uid,$subdate,$subtime,$subtype,$agree,$expdate,$exptime,$status,$candate,$cantime) = &GetSubInfo_byUser($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($fid,$uid,$subdate,$subtime,$subtype,$agree,$expdate,$exptime,$status,$candate,$cantime) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Subscriptions WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING SUBSCRIPTIONS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$fid = $ref->{'FirmID'};
	  	$uid = $ref->{'UserID'};
	  	$subdate = $ref->{'SubDate'};
	  	$subtime = $ref->{'SubTime'};
	  	$agree = $ref->{'Agreement'};
	  	$subtype = $ref->{'SubTypeID'};
	  	$expdate = $ref->{'ExpDate'};	  	
	  	$exptime = $ref->{'ExpTime'};
	  	$status = $ref->{'Status'};
	  	$candate = $ref->{'CancelDate'};	
	  	$cantime = $ref->{'CancelTime'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $fid,$uid,$subdate,$subtime,$subtype,$agree,$expdate,$exptime,$status,$candate,$cantime;
}

sub GetSubscriptions_sel {
my ($id,$dbh) = @_;

# &GetSubscriptions_sel($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($fid,$subtype) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Subscriptions ORDER BY UserID"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING SUBSCRIPTIONS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$sid = $ref->{'SubID'};
		$uid = $ref->{'UserID'};
	  	$stid = $ref->{'SubTypeID'};
  		my $sth2 = $dbh->prepare("SELECT * FROM SubTypes WHERE SubTypeID='$stid'"); 
		$sth2->execute() or die &ErrorMsg("ERROR GETTING SUBSCRIPTIONS: $DBI::errstr");
		while (my $ref2 = $sth2->fetchrow_hashref()) {
			$desc = $ref2->{'Description'};
			$amt = $ref2->{'Amount'};
			$term = $ref2->{'Term'};
			if ($term eq "1") {
				$term = "Monthly";
			} elsif ($term eq "3") {
				$term = "Quarterly";
			} elsif ($term eq "12") {
				$term = "Annual";
			} elsif ($term eq "24") {
				$term = "Two Years";
			} elsif ($term eq "36") {
				$term = "Three Years";
			}
		}
		$sth2->finish();
		if ($sid eq $id) {
			print "<option value='$sid' selected>$desc - \$$amt - $term</option>";
		} else {
			print "<option value='$sid'>$desc - \$$amt - $term</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteSub {
my ($id,$dbh) = @_;

# &DeleteSub($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM Subscriptions WHERE SubID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING SUBSCRIPTIONS: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"34",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteSub_byUser {
my ($id,$dbh) = @_;

# &DeleteSub_byUser($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM Subscriptions WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING SUBSCRIPTIONS: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"34",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteSub_byFirm {
my ($id,$dbh) = @_;

# &DeleteSub_byFirm($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM Subscriptions WHERE FirmID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING SUBSCRIPTIONS: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"34",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# Certification Type Database Routines
# ==========================================================================================================================================================
sub SaveCertType {
my ($certtype,$dbh) = @_;

# my $id = &SaveCertType($certtype,$dbh);

my $id = undef;
my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT * FROM CertTypes WHERE Certification='$certtype'"); 
$sth->execute() or die &ErrorMsg("DATABASE ERROR ENCOUNTERED (SaveCertType): $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$id = $ref->{'CTID'};
}
$sth->finish();
if (!$id) {
  $sth = $dbh->prepare("INSERT INTO CertTypes (CertTypes.Certification) VALUES ('$certtype')"); 
  $sth->execute() or die &ErrorMsg("ERROR SAVING CERTTYPES: $DBI::errstr");
  $sth->finish();
  $sth = $dbh->prepare("SELECT * FROM CertTypes WHERE Certification='$certtype'"); 
  $sth->execute() or die &ErrorMsg("ERROR IN CERTTYPES: $DBI::errstr");
  while (my $ref = $sth->fetchrow_hashref()) {
  	$aid = $ref->{'CTID'};
  }
  $sth->finish();
  &LogActivity($id,"31",$dbh);
} 
if ($didmyown) {
	$dbh->disconnect();
}
return $aid;
}

sub UpdateCertTypes {
my ($id,$certtype,$dbh) = @_;

# &UpdateCertTypes($id,$certtype,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE CertTypes SET CertTypes.Certification='$certtype' WHERE CTID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING AFFILIATIONS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"32",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetCertTypeInfo {
my ($id,$dbh) = @_;

# my $certtype = &GetCertTypeInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($certtype) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM CertTypes WHERE CTID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING CERTTYPES: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$certtype = $ref->{'Certification'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $certtype;
}

sub GetCertTypes_sel {
my ($id,$dbh) = @_;

# &GetCertTypes_sel($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($ctid,$certtype) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM CertTypes ORDER BY Certification"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING CERTTYPES: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ctid = $ref->{'CTID'};
	  	$certtype = $ref->{'Certification'};
		if ($ctid eq $id) {
			print "<option value='$ctid' selected>$certtype</option>";
		} else {
			print "<option value='$ctid'>$certtype</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteCertType {
my ($id,$dbh) = @_;

# &DeleteCertType($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM CertTypes WHERE CTID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING CERTTYPES: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"33",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# Certification Master Database Routines
# ==========================================================================================================================================================
sub SaveCertMaster {
my ($uid,$cid,$dbh) = @_;

# my $id = &SaveCertMaster($uid,$cid,$dbh);

my $id = undef;
my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT * FROM CertMaster WHERE UserID='$uid' AND CTID='$cid'"); 
$sth->execute() or die &ErrorMsg("DATABASE ERROR ENCOUNTERED: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$id = $ref->{'CertID'};
}
$sth->finish();
if (!$id) {
	$sth = $dbh->prepare("INSERT INTO CertMaster (CertMaster.UserID,CertMaster.CTID) VALUES ('$uid','$cid')"); 
	$sth->execute() or die &ErrorMsg("ERROR SAVING CERTMASTER: $DBI::errstr");
	$sth->finish();
	$sth = $dbh->prepare("SELECT * FROM CertMaster WHERE UserID='$uid' AND CTID='$cid'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN CERTMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	    $id = $ref->{'CertID'};
	}
	$sth->finish();
} 
if ($didmyown) {
	$dbh->disconnect();
}
return;
}

sub SaveNotary {
my ($uid,$cid,$dbh) = @_;

# my $id = &SaveNotary($uid,$cid,$dbh);

my $id = undef;
my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT * FROM UserNotary WHERE UserID='$uid' AND Notary='$cid'"); 
$sth->execute() or die &ErrorMsg("SaveNotary: ERROR ENCOUNTERED CHECKIGN UserNotary: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$id = $ref->{'NotaryID'};
}
$sth->finish();
if (!$id) {
	$sth = $dbh->prepare("INSERT INTO UserNotary (UserNotary.UserID,UserNotary.Notary) VALUES ('$uid','$cid')"); 
	$sth->execute() or die &ErrorMsg("ERROR SAVING USERNOTARY: $DBI::errstr");
	$sth->finish();
	$sth = $dbh->prepare("SELECT * FROM UserNotary WHERE UserID='$uid' AND Notary='$cid'"); 
	$sth->execute() or die &ErrorMsg("SaveNotary: ERROR IN USERNOTARY: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	    $id = $ref->{'NotaryID'};
	}
	$sth->finish();
} 
if ($didmyown) {
	$dbh->disconnect();
}
return $id;
}


sub UpdateCertMaster {
my ($id,$uid,$cid,$dbh) = @_;

# &UpdateCertMaster($id,$uid,$cid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE CertMaster SET CertMaster.UserID='$uid',CertMaster.CTID='$cid' WHERE CertID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING AFFILIATIONS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"35",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetCertMasterInfo {
my ($id,$dbh) = @_;

# my $certtype = &GetCertMasterInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($certtype) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM CertMaster WHERE CertID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING CERTMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$uid = $ref->{'UserID'};
	  	$cid = $ref->{'CTID'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $uid,$cid;
}

sub GetCertMaster_sel {
my ($id,$dbh) = @_;

# &GetCertMaster_sel($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($cid,$ctid) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM CertMaster ORDER BY UserID"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING CERTMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$cid = $ref->{'CertID'};
		$uid = $ref->{'UserID'};
	  	$ctid = $ref->{'CTID'};
		my ($last,$first,$mi,$addr,$addr2,$city,$state,$zip,$country,$email,$phone,$rdate,$rtime,$comments,$prereg,$ip,$type,$rid,$status) = &GetUserInfo($uid,$dbh);
 		my $certtype = &GetCertTypeInfo($ctid,$dbh);
		if ($cid eq $id) {
			print "<option value='$cid' selected>$first $last: $certtype</option>";
		} else {
			print "<option value='$cid'>$first $last: $certtype</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteCertMaster {
my ($id,$dbh) = @_;

# &DeleteCertMaster($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM CertMaster WHERE CertID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING CERTMASTER: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"36",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# Rate Database Routines
# ==========================================================================================================================================================
sub SavePreferences {
my ($fid,$uid,$ratetype,$amt,$counter,$dbh) = @_;

# my $id = &SavePreferences($fid,$uid,$ratetype,$amt,$counter,$dbh);

	my $id = undef;
	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $today = &GetMunge();
	my $now = &GetRawTime();

	my $sth = $dbh->prepare("SELECT * FROM Preferences WHERE FirmID='$fid' AND UserID='$uid' AND RateType='$ratetype' AND Amount='$amt'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN Preferences: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
  		$id = $ref->{'RateID'};
	}
	$sth->finish();
	if (!$id) {
		$sth = $dbh->prepare("INSERT INTO Preferences (Preferences.FirmID,
										Preferences.UserID,
  										Preferences.RateType,
  										Preferences.Amount,
  										Preferences.Counter) 
  						VALUES ('$fid',
  								'$uid',
  								'$ratetype',
  								'$amt',
  								'$counter')"); 
		$sth->execute() or die &ErrorMsg("ERROR SAVING Preferences: $DBI::errstr");
  		$sth->finish();
  		$sth = $dbh->prepare("SELECT * FROM Preferences WHERE FirmID='$fid' AND UserID='$uid' AND RateType='$ratetype' AND Amount='$amt'"); 
  		$sth->execute() or die &ErrorMsg("ERROR IN Subscriptions: $DBI::errstr");
  		while (my $ref = $sth->fetchrow_hashref()) {
  			$rid = $ref->{'RateID'};
  		}
  		$sth->finish();
  		&LogActivity($id,"32",$dbh);
	}	 
	if ($didmyown) {
		$dbh->disconnect();
	}
	return $rid;
}

sub UpdateRate {
my ($id,$fid,$uid,$ratetype,$amt,$counter,$dbh) = @_;

# &UpdateRate($id,$fid,$uid,$ratetype,$amt,$counter,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE Preferences SET Preferences.FirmID='$fid',Preferences.UserID='$uid',Preferences.RateType='$ratetype',Preferences.Amount='$amt',Preferences.Counter='$counter' WHERE RateID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING SUBSCRIPTIONS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"34",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetRateInfo {
my ($id,$dbh) = @_;

# my ($fid,$uid,$ratetype,$amount,$counter) = &GetRateInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($fid,$uid,$ratetype,$amount,$counter) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Preferences WHERE RateID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING Preferences: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$fid = $ref->{'FirmID'};
	  	$uid = $ref->{'UserID'};
	  	$ratetype = $ref->{'RateType'};
	  	$amt = $ref->{'Amount'};
	  	$counter = $ref->{'Counter'};
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $fid,$uid,$ratetype,$amt,$counter;
}

sub GetPreferences_sel {
my ($fid,$uid,$dbh) = @_;

# &GetPreferences_sel($fid,$uid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($sth,$subtype,$final) = undef;
	if ($fid) {
	  	$sth = $dbh->prepare("SELECT * FROM Preferences WHERE FirmID='$fid' ORDER BY RateType");
	} else {
	  	$sth = $dbh->prepare("SELECT * FROM Preferences WHERE UserID='$uid' ORDER BY RateType");
	}
	$sth->execute() or die &ErrorMsg("ERROR GETTING Preferences: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$rid = $ref->{'RateID'};
		$fid = $ref->{'FirmID'};
	  	$uid = $ref->{'UserID'};
	  	$rtid = $ref->{'RateType'};
	  	$amt = $ref->{'Amount'};
	  	$cnt = $ref->{'Counter'};
  		my $sth2 = $dbh->prepare("SELECT * FROM RateTypes WHERE RTID='$rtid'"); 
		$sth2->execute() or die &ErrorMsg("ERROR GETTING RATETYPES: $DBI::errstr");
		while (my $ref2 = $sth2->fetchrow_hashref()) {
			$desc = $ref2->{'RateType'};
		}
		$sth2->finish();
		if ($cnt) {
			if ($cnt eq "P") {
				$final = "\$" . $amt . "/page";
			} elsif ($cnt eq "H") {
				$final = "\$" . $amt . "/hour";	
			} elsif ($cnt eq "M") {
				$final = "\$" . $amt . "/mile";
			} else {
				$final = "\$$amt";	
			}
		}
		if ($fid) {
			if ($rid eq $fid) {
				print "<option value='$rid' selected>$desc - $final</option>";
			} else {
				print "<option value='$rid'>$desc - $final</option>";
			}
		} else {
			if ($rid eq $uid) {
				print "<option value='$rid' selected>$desc - $final</option>";
			} else {
				print "<option value='$rid'>$desc - $final</option>";
			}
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteRate {
my ($id,$dbh) = @_;

# &DeleteRate($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM Preferences WHERE RateID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING Preferences: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"34",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeletePreferences_byFirm {
my ($id,$dbh) = @_;

# &DeletePreferences_byFirm($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM Preferences WHERE FirmID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING Preferences: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"34",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeletePreferences_byUser {
my ($id,$dbh) = @_;

# &DeletePreferences_byUser($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM Preferences WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING Preferences: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"34",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeletePreferences_byType {
my ($id,$dbh) = @_;

# &DeletePreferences_byType($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM Preferences WHERE RateType='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING Preferences: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"34",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# Preferences Database Routines
# ==========================================================================================================================================================
sub SavePref {
my ($uid,$pref,$years,$dbh) = @_;

# my $id = &SavePref($uid,$pref,$dbh);

	my $id = undef;
	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $today = &GetMunge();
	my $now = &GetRawTime();

	my $sth = $dbh->prepare("SELECT * FROM UserRoles WHERE UserID='$uid' AND RoleID='$pref'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN UserRoles: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
  		$id = $ref->{'URID'};
	}
	$sth->finish();
	if (!$id) {
		$sth = $dbh->prepare("INSERT INTO UserRoles (UserID,RoleID,Exp) VALUES ('$uid','$pref','$years')"); 
		$sth->execute() or die &ErrorMsg("ERROR SAVING USERROLES: $DBI::errstr");
  		$sth->finish();
  		$sth = $dbh->prepare("SELECT * FROM UserRoles WHERE UserID='$uid' AND RoleID='$pref'"); 
  		$sth->execute() or die &ErrorMsg("ERROR IN USERROLES: $DBI::errstr");
  		while (my $ref = $sth->fetchrow_hashref()) {
  			$id = $ref->{'URID'};
  		}
  		$sth->finish();
#  		&LogActivity($id,"32",$dbh);
	} else {
  		$sth = $dbh->prepare("UPDATE UserRoles SET UserRoles.RoleID='$pref',Exp='$years' WHERE URID='$id'"); 
  		$sth->execute() or die &ErrorMsg("ERROR IN USERROLES: $DBI::errstr");
  		$sth->finish();		
	} 
	if ($didmyown) {
		$dbh->disconnect();
	}
	return $id;
}

sub UpdatePref {
my ($id,$pref,$years,$dbh) = @_;

# &UpdatePref($id,$pref,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE UserRoles SET UserRoles.RoleID='$pref',UserRoles.Exp='$years' WHERE URID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING USERROLES: $DBI::errstr");
  	$sth->finish();

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetPref {
my ($id,$dbh) = @_;

# my ($uid,$pref) = &GetPref($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($uid,$pref) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM UserRoles WHERE URID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING USERROLES: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$uid = $ref->{'UserID'};
	  	$pref = $ref->{'RoleID'};
	  	$years = $ref->{'Exp'};
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $uid,$pref,$years;
}

sub GetPrefbyUser_sel {
my ($uid,$dbh) = @_;

# &GetPrefbyUser_sel($uid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($sth,$subtype,$final) = undef;
  	$sth = $dbh->prepare("SELECT * FROM UserRoles WHERE UserID='$uid' ORDER BY RoleID");
	$sth->execute() or die &ErrorMsg("ERROR GETTING UserRoles: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	    $pid = $ref->{'URID'};
	    $pref = $ref->{'RoleID'};
	    $years = $ref->{'Exp'};
	    print "<option value='$pid'>$pref with $years years experience</option>";
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub GetPrefbyPref_sel {
my ($pref,$dbh) = @_;

# &GetPrefbyPref_sel($uid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($sth,$subtype,$final) = undef;
  	$sth = $dbh->prepare("SELECT * FROM UserRoles WHERE RoleID='$pref' ORDER BY UserID");
	$sth->execute() or die &ErrorMsg("ERROR GETTING USERROLES: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	    $pid = $ref->{'URID'};
	    $uid = $ref->{'UserID'};
	    $name = &GetUserName($uid,$dbh);
	    print "<option value='$pid'>$name</option>";
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteRate {
my ($id,$dbh) = @_;

# &DeleteRate($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM Preferences WHERE RateID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING Preferences: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"34",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeletePreferences_byUser {
my ($id,$dbh) = @_;

# &DeletePreferences_byUser($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM UserRoles WHERE URID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING UserRoles: $DBI::errstr");
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeletePreferences_byType {
my ($id,$dbh) = @_;

# &DeletePreferences_byType($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM UserRoles WHERE RoleID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING USERROLES: $DBI::errstr");
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# FirmOpts Database Routines
# ==========================================================================================================================================================
sub SaveFirmOpts {
my ($uid,$fid,$current,$fav,$oneclick,$restrict,$restnum,$exc1,$exc2,$dbh) = @_;

# my $id = &SaveFirmOpts($uid,$fid,$current,$fav,$oneclick,$restrict,$restnum,$exc1,$exc2,$dbh);

	my $id = undef;
	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $today = &GetMunge();
	my $now = &GetRawTime();

	my $sth = $dbh->prepare("SELECT * FROM FirmOpts WHERE UserID='$uid' AND FirmID='$fid'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN FirmOpts: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
  		$id = $ref->{'FOID'};
	}
	$sth->finish();
	if (!$id) {
		$sth = $dbh->prepare("INSERT INTO FirmOpts (FirmOpts.UserID,FirmOpts.FirmID,FirmOpts.Current,FirmOpts.Favorite,FirmOpts.OneClick,FirmOpts.Restrictions,FirmOpts.RestrictNum,FirmOpts.AllDay,FirmOpts.MultipleCopies) 
				    VALUES ('$uid','$fid','$current','$fav','$oneclick','$restrict','$restnum','$exc1','$exc2')"); 
		$sth->execute() or die &ErrorMsg("ERROR SAVING Preferences: $DBI::errstr");
  		$sth->finish();
  		$sth = $dbh->prepare("SELECT * FROM FirmOpts WHERE UserID='$uid' AND FirmID='$fid'"); 
  		$sth->execute() or die &ErrorMsg("ERROR IN FirmOpts: $DBI::errstr");
  		while (my $ref = $sth->fetchrow_hashref()) {
  			$id = $ref->{'PrefID'};
  		}
  		$sth->finish();
	}	 
	if ($didmyown) {
		$dbh->disconnect();
	}
	return $id;
}

sub UpdateFirmOpts {
my ($id,$current,$fav,$oneclick,$restrict,$restnum,$exc1,$exc2,$dbh) = @_;

# &UpdateFirmOpts($id,$current,$fav,$oneclick,$restrict,$restnum,$exc1,$exc2,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE FirmOpts SET FirmOpts.Current='$current',FirmOpts.Favorite='$fav',FirmOpts.OneClick='$oneclick',FirmOpts.Restrictions='$restrict',FirmOpts.RestrictNum='$restnum',FirmOpts.AllDay='$exc1',FirmOpts.MultipleCopies='$exc2' WHERE FOID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING PREFERENCES: $DBI::errstr");
  	$sth->finish();

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetFirmOpts {
my ($id,$dbh) = @_;

# my ($uid,$fid,$current,$fav,$oneclick,$restrict,$restnum,$exc1,$exc2) = &GetFirmOpts($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($uid,$pref) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM FirmOpts WHERE FOID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING FirmOpts: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$uid = $ref->{'UserID'};
	  	$fid = $ref->{'FirmID'};
	  	$current = $ref->{'Current'};
	  	$fav = $ref->{'Favorite'};
	  	$oneclick = $ref->{'OneClick'};
	  	$restrict = $ref->{'Restrictions'};
	  	$restnum = $ref->{'RestrictNum'};
	  	$exc1 = $ref->{'AllDay'};
	  	$exc2 = $ref->{'MultipleCopies'};
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $uid,$fid,$current,$fav,$oneclick,$restrict,$restnum,$exc1,$exc2;
}

sub GetFirmOpts_specific {
my ($uid,$fid,$dbh) = @_;

# my ($current,$fav,$oneclick,$restrict,$restnum,$exc1,$exc2) = &GetFirmOpts_specific($uid,$fid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($current,$fav,$oneclick,$restrict,$restnum,$exc1,$exc2) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM FirmOpts WHERE UserID='$uid' AND FirmID='$fid'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING FirmOpts: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$current = $ref->{'Current'};
	  	$fav = $ref->{'Favorite'};
	  	$oneclick = $ref->{'OneClick'};
	  	$restrict = $ref->{'Restrictions'};
	  	$restnum = $ref->{'RestrictNum'};
	  	$exc1 = $ref->{'AllDay'};
	  	$exc2 = $ref->{'MultipleCopies'};
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $current,$fav,$oneclick,$restrict,$restnum,$exc1,$exc2;
}

sub GetFirmOptsbyUser_sel {
my ($uid,$dbh) = @_;

# &GetFirmOptsbyUser_sel($uid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($sth,$id,$fid) = undef;
  	$sth = $dbh->prepare("SELECT * FROM FirmOpts WHERE UserID='$uid' ORDER BY FirmID");
	$sth->execute() or die &ErrorMsg("ERROR GETTING FirmOpts: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	    $id = $ref->{'FOID'};
	    $fid = $ref->{'FirmID'};
	    my $name = &GetFirmName($fid,$dbh);
	    print "<option value='$id'>$name</option>";
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub GetFirmOptsbyFirm_sel {
my ($fid,$dbh) = @_;

# &GetFirmOptsbyFirm_sel($fid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($sth,$id,$uid) = undef;
  	$sth = $dbh->prepare("SELECT * FROM FirmOpts WHERE FirmID='$uid' ORDER BY UserID");
	$sth->execute() or die &ErrorMsg("ERROR GETTING FirmOpts: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	    $id = $ref->{'FOID'};
	    $uid = $ref->{'UserID'};
	    my $name = &GetUserName($uid,$dbh);
	    print "<option value='$id'>$name</option>";
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteFirmOpts {
my ($id,$dbh) = @_;

# &DeleteFirmOpts($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM FirmOpts WHERE FOID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING FirmOpts: $DBI::errstr");
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteFirmOpts_byFirm {
my ($id,$dbh) = @_;

# &DeleteFirmOpts_byFirm($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM FirmOpts WHERE FirmID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING FirmOpts: $DBI::errstr");
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteFirmOpts_byUser {
my ($id,$dbh) = @_;

# &DeleteFirmOpts_byUser($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM FirmOpts WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING FirmOpts: $DBI::errstr");
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# SoftwareMaster Database Routines
# ==========================================================================================================================================================
sub SaveSoftwareMaster {
my ($vend,$prod,$dbh) = @_;

# my $id = &SaveSoftwareMaster($vendor,$product,$dbh);

	my $id = undef;
	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $today = &GetMunge();
	my $now = &GetRawTime();
	my $vendor = &WriteQuotes($vend);
	my $product = &WriteQuotes($prod);
	my $sth = $dbh->prepare("SELECT * FROM SoftwareMaster WHERE Vendor='$vendor' AND Product='$product'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN SoftwareMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
  		$id = $ref->{'SoftID'};
	}
	$sth->finish();
	if (!$id) {
		$sth = $dbh->prepare("INSERT INTO SoftwareMaster (SoftwareMaster.Vendor,SoftwareMaster.Product) VALUES ('$vendor','$product')"); 
		$sth->execute() or die &ErrorMsg("ERROR SAVING SOFTWAREMASTER: $DBI::errstr");
  		$sth->finish();
  		$sth = $dbh->prepare("SELECT * FROM SoftwareMaster WHERE Vendor='$vendor' AND Product='$product'"); 
  		$sth->execute() or die &ErrorMsg("ERROR IN SoftwareMaster: $DBI::errstr");
  		while (my $ref = $sth->fetchrow_hashref()) {
  			$id = $ref->{'PrefID'};
  		}
  		$sth->finish();
	}	 
	if ($didmyown) {
		$dbh->disconnect();
	}
	return $id;
}

sub SaveUserSoft {
my ($id,$sid,$dbh) = @_;

# my $id = &SaveUserSoft($id,$sid,$dbh);

	my $usid = undef;
	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("SELECT * FROM UserSoft WHERE UserID='$id' AND SoftID='$sid'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN UserSoft: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
  		$usid = $ref->{'UserSoftID'};
	}
	$sth->finish();
	if (!$usid) {
		$sth = $dbh->prepare("INSERT INTO UserSoft (UserSoft.UserID,UserSoft.SoftID) VALUES ('$id','$sid')"); 
		$sth->execute() or die &ErrorMsg("ERROR SAVING UserSOFT: $DBI::errstr");
  		$sth->finish();
  		$sth = $dbh->prepare("SELECT * FROM UserSoft WHERE UserID='$id' AND SoftID='$sid'"); 
  		$sth->execute() or die &ErrorMsg("ERROR IN UserSoft: $DBI::errstr");
  		while (my $ref = $sth->fetchrow_hashref()) {
  			$uid = $ref->{'UserSoftID'};
  		}
  		$sth->finish();
	}	 
	if ($didmyown) {
		$dbh->disconnect();
	}
	return $uid;
}


sub UpdateSoftwareMaster {
my ($id,$vend,$prod,$dbh) = @_;

# &UpdateSoftwareMaster($id,$current,$fav,$oneclick,$restrict,$restnum,$exc1,$exc2,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $vendor = &WriteQuotes($vend);
	my $product = &WriteQuotes($prod);
	my $sth = $dbh->prepare("UPDATE SoftwareMaster SET SoftwareMaster.Vendor='$vendor',SoftwareMaster.Product='$product' WHERE SoftID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING SOFTWAREMASTER: $DBI::errstr");
  	$sth->finish();

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetSoftwareMaster {
my ($id,$dbh) = @_;

# my ($vendor,$product) = &GetSoftwareMaster($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($uid,$pref) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM SoftwareMaster WHERE SoftID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING SoftwareMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$vendor = $ref->{'Vendor'};
	  	$product = $ref->{'Product'};
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $vendor,$product;
}

sub GetSoftwareMasterbyVendor_sel {
my ($vendor,$dbh) = @_;

# &GetSoftwareMasterbyVendor_sel($uid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($sth,$id,$fid) = undef;
  	$sth = $dbh->prepare("SELECT * FROM SoftwareMaster WHERE Vendor='$vendor' ORDER BY Product");
	$sth->execute() or die &ErrorMsg("ERROR GETTING SoftwareMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	    $id = $ref->{'SoftID'};
	    $product = $ref->{'Product'};
	    print "<option value='$id'>$product ($vendor)</option>";
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub GetSoftwareMaster_sel {
my $dbh = shift;

# &GetSoftwareMaster_sel($fid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($id,$vendor,$product) = undef;
  	$sth = $dbh->prepare("SELECT * FROM SoftwareMaster ORDER BY Vendor,Product");
	$sth->execute() or die &ErrorMsg("ERROR GETTING SoftwareMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	    $id = $ref->{'SoftID'};
	    $vendor = $ref->{'Vendor'};
	    $product = $ref->{'Product'};
	    print "<option value='$id'>$product ($vendor)</option>";
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteSoftwareMaster {
my ($id,$dbh) = @_;

# &DeleteSoftwareMaster($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM SoftwareMaster WHERE SoftID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING SoftwareMaster: $DBI::errstr");
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}


# ===============================================================================================================
# MISC UTILITIES
# ===============================================================================================================
sub ResetUser {
my ($id,$dbh) = @_;

# &ResetUser($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM UserMaster WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING UserMaster: $DBI::errstr");
	$sth->finish();
  	$sth = $dbh->prepare("DELETE FROM CertMaster WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING CertMaster: $DBI::errstr");
	$sth->finish();
  	$sth = $dbh->prepare("DELETE FROM Preferences WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING Preferences: $DBI::errstr");
	$sth->finish();
  	$sth = $dbh->prepare("DELETE FROM FirmOpts WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING FirmOpts: $DBI::errstr");
	$sth->finish();
  	$sth = $dbh->prepare("DELETE FROM Affiliations WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING AFFILIATIONS: $DBI::errstr");
	$sth->finish();
  	$sth = $dbh->prepare("DELETE FROM UserSoft WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING UserSoft: $DBI::errstr");
	$sth->finish();
  	$sth = $dbh->prepare("DELETE FROM UserCard WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING UserCard: $DBI::errstr");
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub Years {
	
	    print "<option value=''>Less than 1 year</option>";
	    print "<option value='1'>1 year</option>";
	    print "<option value='2'>2 years</option>";
	    print "<option value='3'>3 years</option>";
	    print "<option value='4'>4 years</option>";
	    print "<option value='5'>5 years</option>";
	    print "<option value='6'>6 years</option>";
	    print "<option value='7'>7 years</option>";
	    print "<option value='8'>8 years</option>";
	    print "<option value='9'>9 years</option>";
	    print "<option value='10'>10 years</option>";
	    print "<option value='11'>11 years</option>";
	    print "<option value='12'>12 years</option>";	    
	    print "<option value='13'>13 years</option>";
	    print "<option value='14'>14 years</option>";
	    print "<option value='15'>15 years</option>";
	    print "<option value='16'>16 years</option>";
	    print "<option value='17'>17 years</option>";
	    print "<option value='18'>18 years</option>";	    
	    print "<option value='19'>19 years</option>";
	    print "<option value='20'>20 years</option>";
	    print "<option value='21'>21 years</option>";
	    print "<option value='22'>22 years</option>";
	    print "<option value='23'>23 years</option>";
	    print "<option value='24'>24 years</option>";	    
	    print "<option value='25'>25 year</option>";
	    print "<option value='26'>26 years</option>";
	    print "<option value='27'>27 years</option>";
	    print "<option value='28'>28 years</option>";
	    print "<option value='29'>29 years</option>";
	    print "<option value='30'>30 years</option>";
	    print "<option value='*'>30+ years</option>";
}

sub Languages {
my ($id,$dbh) = @_;
	
	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("SELECT * FROM Languages");
	$sth->execute() or die &ErrorMsg("ERROR GETTING LANGUAGES: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	    $lid = $ref->{'LangID'};
	    $lang = $ref->{'Language'};
	    if ($id eq $lid) {
		   print "<option value='$lid' selected>$lang</option>";
	    } else {
		   print "<option value='$lid'>$lang</option>"; 
	    }
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# Preferences Database Routines
# ==========================================================================================================================================================
sub SaveLang {
my ($uid,$lang,$dbh) = @_;

# my $id = &SaveLang($uid,$lang,$dbh);

	my $id = undef;
	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $today = &GetMunge();
	my $now = &GetRawTime();

	my $sth = $dbh->prepare("SELECT * FROM UserLang WHERE UserID='$uid' AND LangID='$lang'"); 
	$sth->execute() or die &ErrorMsg("ERROR IN USERLANG: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
  		$id = $ref->{'ULID'};
	}
	$sth->finish();
	if (!$id) {
		$sth = $dbh->prepare("INSERT INTO UserLang (UserID,LangID) VALUES ('$uid','$lang')"); 
		$sth->execute() or die &ErrorMsg("ERROR SAVING USERLANG: $DBI::errstr");
  		$sth->finish();
  		$sth = $dbh->prepare("SELECT * FROM UserLang WHERE UserID='$uid' AND LangID='$lang'"); 
  		$sth->execute() or die &ErrorMsg("ERROR IN USERLANG: $DBI::errstr");
  		while (my $ref = $sth->fetchrow_hashref()) {
  			$id = $ref->{'ULID'};
  		}
  		$sth->finish();
#  		&LogActivity($id,"32",$dbh);
	} else {
  		$sth = $dbh->prepare("UPDATE UserLang SET LangID='$lang' WHERE ULID='$id'"); 
  		$sth->execute() or die &ErrorMsg("ERROR IN USERLANG: $DBI::errstr");
  		$sth->finish();		
	} 
	if ($didmyown) {
		$dbh->disconnect();
	}
	return $id;
}

# ==========================================================================================================================================================
# Affiliations Database Routines
# ==========================================================================================================================================================
sub SaveAffiliation {
my ($uid,$fid,$dbh) = @_;

# my $id = &SaveAffilation($uid,$fid,$dbh);

my $aid = undef;

my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT * FROM Affiliations WHERE UserID='$uid' AND FirmID='$fid'"); 
$sth->execute() or die &ErrorMsg("DATABASE ERROR ENCOUNTERED (SaveAffiliations): $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$aid = $ref->{'AffiliationID'};
}
$sth->finish();
if (!$aid) {
  $sth = $dbh->prepare("INSERT INTO Affiliations (Affiliations.UserID,Affiliations.FirmID) VALUES ('$uid','$fid')"); 
  $sth->execute() or die &ErrorMsg("ERROR SAVING AFFILIATIONS: $DBI::errstr");
  $sth->finish();
  $sth = $dbh->prepare("SELECT * FROM Affiliations WHERE UserID='$uid' AND FirmID='$fid'"); 
  $sth->execute() or die &ErrorMsg("ERROR IN AFFILIATIONS: $DBI::errstr");
  while (my $ref = $sth->fetchrow_hashref()) {
  	$aid = $ref->{'AffiliationID'};
  }
  $sth->finish();
} 
if ($didmyown) {
	$dbh->disconnect();
}
return $aid;
}

sub UpdateAffiliation {
my ($aid,$uid,$fid,$dbh) = @_;

# &UpdateAffiliation($aid,$uid,$fid,$dbh);

	my $didmyown = undef;

	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE Affiliations SET Affiliations.UserID='$uid',Affiliations.FirmID='$fid' WHERE AffiliationID='$aid'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING AFFILIATIONS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"14",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetAffiliationInfo {
my ($id,$dbh) = @_;

# my ($uid,$fid) = &GetAffiliationInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($uid,$fid) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Affiliations WHERE AffiliationID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING AFFILIATIONS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$uid = $ref->{'UserID'};
  		$fid = $ref->{'FirmID'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $uid,$fid;
}

sub GetAffiliations_sel {
my ($id,$dbh) = @_;

# &GetAffiliations_sel($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($aid,$uid,$fid) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Affiliations ORDER BY FirmID"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING AFFILIATIONS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$aid = $ref->{'AffiliationID'};
	  	$fid = $ref->{'FirmID'};
  		my $sth2 = $dbh->prepare("SELECT * FROM Firms WHERE FirmID='$fid'"); 
		$sth2->execute() or die &ErrorMsg("ERROR GETTING FIRM: $DBI::errstr");
		while (my $ref2 = $sth2->fetchrow_hashref()) {
			$firm = $ref2->{'Name'};
		}
		$sth2->finish();

		if ($aid eq $id) {
			print "<option value='$fid' selected>$firm</option>";
		} else {
			print "<option value='$fid'>$firm</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# LogActivity Database Routines
# ==========================================================================================================================================================
sub SaveLogActivity {
my ($activity,$dbh) = @_;

# my $id = &SaveLogActivity($activity,$dbh);

my $id = undef;

my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT * FROM LogActivity WHERE Activity='$activity'"); 
$sth->execute() or die &ErrorMsg("DATABASE ERROR ENCOUNTERED (SaveLogActivity): $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$id = $ref->{'LAID'};
}
$sth->finish();
if (!$id) {
  $sth = $dbh->prepare("INSERT INTO LogActivity (LogActivity.Activity) VALUES ('$activity')"); 
  $sth->execute() or die &ErrorMsg("ERROR SAVING LOGACTIVITY: $DBI::errstr");
  $sth->finish();
  $sth = $dbh->prepare("SELECT * FROM LogActivity WHERE Activity='$activity'"); 
  $sth->execute() or die &ErrorMsg("ERROR IN LOGACTIVITY: $DBI::errstr");
  while (my $ref = $sth->fetchrow_hashref()) {
  	$aid = $ref->{'LAID'};
  }
  $sth->finish();
  &LogActivity($id,"16",$dbh);
} 
if ($didmyown) {
	$dbh->disconnect();
}
return $aid;
}

sub UpdateLogActivity {
my ($id,$activity,$dbh) = @_;

# &UpdateLogActivity($id,$activity,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE LogActivity SET LogActivity.UserID='$activity',LogActivity.FirmID='$fid' WHERE LogActivityID='$aid'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING AFFILIATIONS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"14",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetLogActivityInfo {
my ($id,$dbh) = @_;

# my ($activity,$fid) = &GetLogActivityInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($activity) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM LogActivity WHERE LAID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING LOGACTIVITY: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$activity = $ref->{'Activity'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $activity;
}

sub GetLogActivity_sel {
my ($id,$dbh) = @_;

# &GetLogActivity_sel($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($aid,$activity) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM LogActivity ORDER BY Activity"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING LOGACTIVITY: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$aid = $ref->{'LAID'};
	  	$activity = $ref->{'Activity'};
		if ($aid eq $id) {
			print "<option value='$aid' selected>$activity</option>";
		} else {
			print "<option value='$aid'>$activity</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteLogActivity {
my ($id,$dbh) = @_;

# &DeleteLogActivity($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM LogActivity WHERE LAID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING LOGACTIVITY: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"15",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# Affiliations Database Routines
# ==========================================================================================================================================================
sub SaveAffiliation {
my ($uid,$fid,$dbh) = @_;

# my $id = &SaveAffilation($uid,$fid,$dbh);

my $aid = undef;

my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT * FROM Affiliations WHERE UserID='$uid' AND FirmID='$fid'"); 
$sth->execute() or die &ErrorMsg("DATABASE ERROR ENCOUNTERED (SaveAffiliations): $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$aid = $ref->{'AffiliationID'};
}
$sth->finish();
if (!$aid) {
  $sth = $dbh->prepare("INSERT INTO Affiliations (Affiliations.UserID,Affiliations.FirmID) VALUES ('$uid','$fid')"); 
  $sth->execute() or die &ErrorMsg("ERROR SAVING AFFILIATIONS: $DBI::errstr");
  $sth->finish();
  $sth = $dbh->prepare("SELECT * FROM Affiliations WHERE UserID='$uid' AND FirmID='$fid'"); 
  $sth->execute() or die &ErrorMsg("ERROR IN AFFILIATIONS: $DBI::errstr");
  while (my $ref = $sth->fetchrow_hashref()) {
  	$aid = $ref->{'AffiliationID'};
  }
  $sth->finish();
} 
if ($didmyown) {
	$dbh->disconnect();
}
return $aid;
}

sub UpdateAffiliation {
my ($aid,$uid,$fid,$dbh) = @_;

# &UpdateAffiliation($aid,$uid,$fid,$dbh);

	my $didmyown = undef;

	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE Affiliations SET Affiliations.UserID='$uid',Affiliations.FirmID='$fid' WHERE AffiliationID='$aid'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING AFFILIATIONS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"14",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetAffiliationInfo {
my ($id,$dbh) = @_;

# my ($uid,$fid) = &GetAffiliationInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($uid,$fid) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Affiliations WHERE AffiliationID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING AFFILIATIONS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$uid = $ref->{'UserID'};
  		$fid = $ref->{'FirmID'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $uid,$fid;
}

sub GetAffiliations_sel {
my ($id,$dbh) = @_;

# &GetAffiliations_sel($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($aid,$uid,$fid) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Affiliations ORDER BY FirmID"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING AFFILIATIONS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$aid = $ref->{'AffiliationID'};
	  	$fid = $ref->{'FirmID'};
  		my $sth2 = $dbh->prepare("SELECT * FROM Firms WHERE FirmID='$fid'"); 
		$sth2->execute() or die &ErrorMsg("ERROR GETTING FIRM: $DBI::errstr");
		while (my $ref2 = $sth2->fetchrow_hashref()) {
			$firm = $ref2->{'Name'};
		}
		$sth2->finish();
		if ($aid eq $id) {
			print "<option value='$fid' selected>$firm</option>";
		} else {
			print "<option value='$fid'>$firm</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub UpdateLogActivity {
my ($id,$activity,$dbh) = @_;

# &UpdateLogActivity($id,$activity,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE LogActivity SET LogActivity.UserID='$activity',LogActivity.FirmID='$fid' WHERE LogActivityID='$aid'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING AFFILIATIONS: $DBI::errstr");
  	$sth->finish();
  	&LogActivity($id,"14",$dbh);

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetLogActivityInfo {
my ($id,$dbh) = @_;

# my ($activity,$fid) = &GetLogActivityInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($activity) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM LogActivity WHERE LAID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING LOGACTIVITY: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$activity = $ref->{'Activity'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $activity;
}

sub GetLogActivity_sel {
my ($id,$dbh) = @_;

# &GetLogActivity_sel($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($aid,$activity) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM LogActivity ORDER BY Activity"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING LOGACTIVITY: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$aid = $ref->{'LAID'};
	  	$activity = $ref->{'Activity'};
		if ($aid eq $id) {
			print "<option value='$aid' selected>$activity</option>";
		} else {
			print "<option value='$aid'>$activity</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteLogActivity {
my ($id,$dbh) = @_;

# &DeleteLogActivity($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM LogActivity WHERE LAID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING LOGACTIVITY: $DBI::errstr");
	$sth->finish();
	&LogActivity($id,"15",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# WishList Database Routines
# ==========================================================================================================================================================
sub SaveWishList {
my ($uid,$fid,$dbh) = @_;

# my $id = &SaveWishList($uid,$fid,$dbh);

my $wlid = undef;
my $today = &GetMunge();
my $now = &GetRawTime();
my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT * FROM WishList WHERE UserID='$uid' AND FirmID='$fid'"); 
$sth->execute() or die &ErrorMsg("DATABASE ERROR ENCOUNTERED (SaveWishList): $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$wlid = $ref->{'WishListID'};
}
$sth->finish();
if (!$wlid) {
  $sth = $dbh->prepare("INSERT INTO WishList (WishList.UserID,WishList.FirmID,WishList.CreateDate,WishList.CreateTime) VALUES ('$uid','$fid','$today','$now')"); 
  $sth->execute() or die &ErrorMsg("ERROR SAVING WishList: $DBI::errstr");
  $sth->finish();
  $sth = $dbh->prepare("SELECT * FROM WishList WHERE UserID='$uid' AND FirmID='$fid'"); 
  $sth->execute() or die &ErrorMsg("ERROR IN WishList: $DBI::errstr");
  while (my $ref = $sth->fetchrow_hashref()) {
  	$wlid = $ref->{'WishListID'};
  }
  $sth->finish();
} 
if ($didmyown) {
	$dbh->disconnect();
}
return $wlid;
}

sub UpdateWishListStatus {
my ($id,$status,$dbh) = @_;

# &UpdateWishListStatus($id,$status,$reason,$dbh);

	my $didmyown = undef;
	my $today = &GetMunge();
	my $now = &GetRawTime();
	my $Reason = &WriteQuotes($reason);
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	if ($status eq "I") {
		my $sth = $dbh->prepare("UPDATE WishList SET WishList.Invited='Y',WishList.InviteDate='$today',WishList.InviteTime='$now' WHERE WishListID='$id'"); 
	  	$sth->execute() or die &ErrorMsg("ERROR UPDATING WishList: $DBI::errstr");
	  	$sth->finish();
#	  	&LogActivity($uid,"14",$dbh);
	} elsif ($status eq "S") {
		my $sth = $dbh->prepare("UPDATE WishList SET WishList.Seen='Y',WishList.SeenDate='$today',WishList.SeenTime='$now' WHERE WishListID='$id'"); 
	  	$sth->execute() or die &ErrorMsg("ERROR UPDATING WishList: $DBI::errstr");
	  	$sth->finish();
#	  	&LogActivity($uid,"14",$dbh);
	} elsif ($status eq "A") {
		my $sth = $dbh->prepare("UPDATE WishList SET WishList.Accepted='Y',WishList.AcceptDate='$today',WishList.AcceptTime='$now' WHERE WishListID='$id'"); 
	  	$sth->execute() or die &ErrorMsg("ERROR UPDATING WishList: $DBI::errstr");
	  	$sth->finish();
#	  	&LogActivity($uid,"14",$dbh);
	} elsif ($status eq "R") {
		my $sth = $dbh->prepare("UPDATE WishList SET WishList.Rejected='Y',WishList.RejectDate='$today',WishList.RejectTime='$now',WishList.RejectReason='$Reason' WHERE WishListID='$id'"); 
	  	$sth->execute() or die &ErrorMsg("ERROR UPDATING WishList: $DBI::errstr");
	  	$sth->finish();
#	  	&LogActivity($uid,"14",$dbh);
	}

	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetWishListInfo {
my ($id,$dbh) = @_;

# my ($uid,$fid) = &GetWishListInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($uid,$fid) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM WishList WHERE WishListID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING WishList: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$uid = $ref->{'UserID'};
  		$fid = $ref->{'FirmID'};
	  	$invited = $ref->{'Invited'};
  		$idate = $ref->{'InviteDate'};
	  	$itime = $ref->{'InviteTime'};
  		$seen = $ref->{'Seen'};
	  	$sdate = $ref->{'SeenDate'};
  		$stime = $ref->{'SeenTime'};
  		$accepted = $ref->{'Accepted'};
	  	$adate = $ref->{'AcceptDate'};
  		$atime = $ref->{'AcceptTime'};
	  	$rejected = $ref->{'Rejected'};
  		$rdate = $ref->{'RejectDate'};
	  	$rtime = $ref->{'RejectTime'};
  		$reason = $ref->{'RejectReason'};	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $uid,$fid,invited,$idate,$itime,$seen,$sdate,$stime,$accepted,$adate,$atime,$rejected,$rdate,$rtime,$reason;
}

sub GetWishList_sel {
my ($id,$dbh) = @_;

# &GetWishList_sel($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($aid,$uid,$fid) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM WishList ORDER BY FirmID"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING WishList: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$aid = $ref->{'WishListID'};
	  	$fid = $ref->{'FirmID'};
  		my $sth2 = $dbh->prepare("SELECT * FROM Firms WHERE FirmID='$fid'"); 
		$sth2->execute() or die &ErrorMsg("ERROR GETTING FIRM: $DBI::errstr");
		while (my $ref2 = $sth2->fetchrow_hashref()) {
			$firm = $ref2->{'Name'};
		}
		$sth2->finish();

		if ($aid eq $id) {
			print "<option value='$fid' selected>$firm</option>";
		} else {
			print "<option value='$fid'>$firm</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub CheckWishListStatus {
my ($id,$fid,$dbh) = @_;

# &CheckWishListStatus($id,$fid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($aid,$uid,$fid) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM WishList WHILE UserID='$id' AND FirmID='$fid'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING WishList: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$aid = $ref->{'WishListID'};
	  	$fid = $ref->{'FirmID'};
  		my $sth2 = $dbh->prepare("SELECT * FROM Firms WHERE FirmID='$fid'"); 
		$sth2->execute() or die &ErrorMsg("ERROR GETTING FIRM: $DBI::errstr");
		while (my $ref2 = $sth2->fetchrow_hashref()) {
			$firm = $ref2->{'Name'};
		}
		$sth2->finish();
		$invite = $ref->{'Invite'};
		$idate = $ref->{'InviteDate'};
		$itime = $ref->{'InviteTime'};
		$seen = $ref->{'Seen'};
		$sdate = $ref->{'SeenDate'};
		$stime = $ref->{'SeenTime'};
		$accepted = $ref->{'Accepted'};
		$adate = $ref->{'AcceptDate'};
		$atime = $ref->{'AcceptTime'};
		$rejected = $ref->{'Rejected'};
		$rdate = $ref->{'RejectDate'};
		$rtime = $ref->{'RejectTime'};
		$reason = $ref->{'RejectReason'};
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $invite,$idate,$itime,$seen,$sdate,$stime,$accepted,$adate,$atime,$rejected,$rdate,$rtime,$reason;
}

sub DeleteWishList {
my ($id,$dbh) = @_;

# &DeleteWishList($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM WishList WHERE WishListID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING WishList: $DBI::errstr");
	$sth->finish();
#	&LogActivity($id,"15",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteWishList_byFirm {
my ($id,$dbh) = @_;

# &DeleteWishList_byFirm($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM WishList WHERE FirmID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING WishList: $DBI::errstr");
	$sth->finish();
#	&LogActivity($id,"15",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteWishList_byUser {
my ($id,$dbh) = @_;

# &DeleteWishList_byUser($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM WishList WHERE UserID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING WishList: $DBI::errstr");
	$sth->finish();
#	&LogActivity($id,"15",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# Experience Type Database Routines
# ==========================================================================================================================================================
sub SaveExpType {
my ($exptype,$dbh) = @_;

# my $id = &SaveExpType($exptype,$dbh);

my $id = undef;
my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT * FROM ExperienceType WHERE Skills='$exptype'"); 
$sth->execute() or die &ErrorMsg("DATABASE ERROR ENCOUNTERED (SaveExpType): $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$id = $ref->{'ETID'};
}
$sth->finish();
if (!$id) {
  $sth = $dbh->prepare("INSERT INTO ExperienceType (ExperienceType.Skills) VALUES ('$exptype')"); 
  $sth->execute() or die &ErrorMsg("ERROR SAVING EXPERIENCETYPES: $DBI::errstr");
  $sth->finish();
  $sth = $dbh->prepare("SELECT * FROM ExperienceType WHERE Skills='$exptype'"); 
  $sth->execute() or die &ErrorMsg("ERROR IN EXPERIENCETYPE: $DBI::errstr");
  while (my $ref = $sth->fetchrow_hashref()) {
  	$eid = $ref->{'ETID'};
  }
  $sth->finish();
#  &LogActivity($id,"31",$dbh);
} 
if ($didmyown) {
	$dbh->disconnect();
}
return $eid;
}

sub UpdateExpTypes {
my ($id,$type,$dbh) = @_;

# &UpdateExpTypes($id,$type,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE ExperienceType SET ExperienceType.Skills='$type' WHERE EID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING EXPERIENCETYPE: $DBI::errstr");
  	$sth->finish();
#  	&LogActivity($id,"32",$dbh);
	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetExpTypeInfo {
my ($id,$dbh) = @_;

# my $type = &GetExpTypeInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($type) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM ExperienceType WHERE ETID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING EXPERIENCETYPE: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$type = $ref->{'Skills'};
	}	
	$sth->finish();

	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $type;
}

sub GetExpTypes_sel {
my ($id,$dbh) = @_;

# &GetExpTypes_sel($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($etid,$type) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM ExperienceType ORDER BY Skills"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING EXPERIENCETYPE: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$etid = $ref->{'ETID'};
	  	$type = $ref->{'Skills'};
		if ($etid eq $id) {
			print "<option value='$etid' selected>$type</option>";
		} else {
			print "<option value='$etid'>$type</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteExpType {
my ($id,$dbh) = @_;

# &DeleteExpType($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM ExperienceType WHERE ETID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING EXPERIENCETYPE: $DBI::errstr");
	$sth->finish();
#	&LogActivity($id,"33",$dbh);
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# User Experience Database Routines
# ==========================================================================================================================================================
sub SaveUserExp {
my ($id,$etid,$years,$dbh) = @_;

# my $id = &SaveUserExp($id,$etid,$years,$dbh);

my $id = undef;
my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT * FROM UserExp WHERE UserID='$id' AND ExpType='$etid'"); 
$sth->execute() or die &ErrorMsg("DATABASE ERROR ENCOUNTERED (SaveUserExpType): $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$id = $ref->{'UEID'};
}
$sth->finish();
if (!$id) {
  $sth = $dbh->prepare("INSERT INTO UserExp (UserExp.UserID,UserExp.ExpType,UserExp.ExpYears) VALUES ('$id','$etid','$years')"); 
  $sth->execute() or die &ErrorMsg("ERROR SAVING USEREXP: $DBI::errstr");
  $sth->finish();
  $sth = $dbh->prepare("SELECT * FROM UserExp WHERE UserID='$id' AND ExpType='$etid'"); 
  $sth->execute() or die &ErrorMsg("ERROR IN USEREXP: $DBI::errstr");
  while (my $ref = $sth->fetchrow_hashref()) {
  	$eid = $ref->{'UEID'};
  }
  $sth->finish();
#  &LogActivity($id,"31",$dbh);
} 
if ($didmyown) {
	$dbh->disconnect();
}
return $eid;
}

sub UpdateUserExpTypes {
my ($id,$uid,$etid,$years,$dbh) = @_;

# &UpdateUserExpTypes($id,$uid,$etid,$users,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE UserExp SET UserExp.UserID='$uid',UserExp.ExpType='$etid',UserExp.ExpYears='$years' WHERE UEID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING USEREXP: $DBI::errstr");
  	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetUserExpInfo {
my ($id,$dbh) = @_;

# my $type = &GetUserExpInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($type) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM UserExp WHERE UEID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING USEREXP: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$uid = $ref->{'UserID'};
	  	$etid = $ref->{'ExpType'};
	  	$yrs = $ref->{'Years'};
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $uid,$etid,$yrs;
}

sub GetUserExp_sel {
my ($id,$uid,$dbh) = @_;

# &GetUserExp_sel($id,$uid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($etid,$type) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM UserExp WHERE UserID='$uid' ORDER BY ExpType"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING USEREXP: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ueid = $ref->{'UEID'};
	  	$etid = $ref->{'ExpType'};
	  	$yrs = $ref->{'Years'};
	  	$type = &GetExpTypeInfo($etid,$dbh);
		if ($ueid eq $id) {
			print "<option value='$ueid' selected>$type ($yrs years)</option>";
		} else {
			print "<option value='$etid'>$type</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteUserExp {
my ($id,$dbh) = @_;

# &DeleteUserExp($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM UserExp WHERE UEID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING USEREXP: $DBI::errstr");
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# User Language Database Routines
# ==========================================================================================================================================================
sub SaveUserLang {
my ($id,$lid,$dbh) = @_;

# my $id = &SaveUserLang($id,$lid,$dbh);

my $id = undef;
my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT * FROM UserLang WHERE UserID='$id' AND LangID='$lid'"); 
$sth->execute() or die &ErrorMsg("DATABASE ERROR ENCOUNTERED (SaveUserLang): $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$id = $ref->{'ULID'};
}
$sth->finish();
if (!$id) {
  $sth = $dbh->prepare("INSERT INTO UserLang (UserLang.UserID,UserLang.LangID) VALUES ('$id','$lid')"); 
  $sth->execute() or die &ErrorMsg("ERROR SAVING USERLANG: $DBI::errstr");
  $sth->finish();
  $sth = $dbh->prepare("SELECT * FROM UserLang WHERE UserID='$id' AND LangID='$lid'"); 
  $sth->execute() or die &ErrorMsg("ERROR IN USERLANG: $DBI::errstr");
  while (my $ref = $sth->fetchrow_hashref()) {
  	$ulid = $ref->{'ULID'};
  }
  $sth->finish();
} 
if ($didmyown) {
	$dbh->disconnect();
}
return $ulid;
}

sub UpdateUserLang {
my ($id,$uid,$lid,$dbh) = @_;

# &UpdateUserLang($id,$uid,$lid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE UserLang SET UserLang.UserID='$uid',UserLang.LangID='$lid' WHERE ULID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING USEREXP: $DBI::errstr");
  	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetUserLangInfo {
my ($id,$dbh) = @_;

# my $type = &GetUserLangInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($type) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM UserLang WHERE ULID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING USERLANG: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$uid = $ref->{'UserID'};
	  	$lid = $ref->{'LangID'};
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $uid,$lid;
}

sub GetUserLang_sel {
my ($id,$uid,$dbh) = @_;

# &GetUserLang_sel($id,$uid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($lid,$type) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM UserLang WHERE UserID='$uid' ORDER BY LangID"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING USERLANG: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$ueid = $ref->{'ULID'};
	  	$lid = $ref->{'LangID'};
	  	$type = &GetLangInfo($lid,$dbh);
		if ($ueid eq $id) {
			print "<option value='$ueid' selected>$type</option>";
		} else {
			print "<option value='$ueid'>$type</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteUserLang {
my ($id,$dbh) = @_;

# &DeleteUserLang($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM UserLang WHERE ULID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING USERLANG: $DBI::errstr");
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# Role Master Database Routines
# ==========================================================================================================================================================
sub SaveRole {
my ($role,$dbh) = @_;

# my $id = &SaveRole($role,$dbh);

my $id = undef;
my $Role = &WriteQuotes($role);
my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT * FROM RoleMaster WHERE Role='$Role'"); 
$sth->execute() or die &ErrorMsg("DATABASE ERROR ENCOUNTERED (SaveRoleMaster): $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$id = $ref->{'RoleID'};
}
$sth->finish();
if (!$id) {
  $sth = $dbh->prepare("INSERT INTO RoleMaster (RoleMaster.Role) VALUES ('$id')"); 
  $sth->execute() or die &ErrorMsg("ERROR SAVING ROLEMASTER: $DBI::errstr");
  $sth->finish();
  $sth = $dbh->prepare("SELECT * FROM RoleMaster WHERE Role='$Role'"); 
  $sth->execute() or die &ErrorMsg("ERROR IN ROLEMASTER: $DBI::errstr");
  while (my $ref = $sth->fetchrow_hashref()) {
  	$rid = $ref->{'RoleID'};
  }
  $sth->finish();
} 
if ($didmyown) {
	$dbh->disconnect();
}
return $rid;
}

sub UpdateRole {
my ($id,$role,$dbh) = @_;

# &UpdateRole($id,$role,$dbh);

	my $Role = &WriteQuotes($role);
	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE RoleMaster SET RoleMaster.Role='$Role' WHERE RoleID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING ROLEMASTER: $DBI::errstr");
  	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetRole {
my ($id,$dbh) = @_;

# my $type = &GetRoleInfo($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $role = undef;
  	my $sth = $dbh->prepare("SELECT * FROM RoleMaster WHERE RoleID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING RoleMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$role = $ref->{'Role'};
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $role;
}

sub GetRoleNum {
my $dbh = shift;

# my $type = &GetRoleNum($dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $cnt = undef;
  	my $sth = $dbh->prepare("SELECT * FROM RoleMaster"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING RoleMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$cnt++;
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $cnt;
}

sub GetRoles_sel {
my ($id,$dbh) = @_;

# &GetRoles_sel($id,$uid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($lid,$type) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM RoleMaster ORDER BY Role"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING RoleMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$rid = $ref->{'RoleID'};
	  	$role = $ref->{'Role'};
		if ($rid eq $id) {
			print "<option value='$rid' selected>$role</option>";
		} else {
			print "<option value='$rid'>$role</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteRole {
my ($id,$dbh) = @_;

# &DeleteRole($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("DELETE FROM RoleMaster WHERE RoleID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING ROLEMASTER: $DBI::errstr");
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

# ==========================================================================================================================================================
# Uploads Database Routines
# ==========================================================================================================================================================
sub SaveUpload {
my ($id,$jid,$name,$size,$dbh) = @_;

my $didmyown = undef;
if (!$dbh) {
	$didmyown++;
	$dbh = &ConnectDB();
}
my $date = &GetMunge();
my $time = &GetRawTime();
my $Name = &WriteQuotes($name);
my $fid = undef;
my $sth = $dbh->prepare("SELECT * FROM Uploads WHERE UserID='$id' AND FileName='$Name'"); 
$sth->execute() or die &ErrorMsg("ERROR GETTING RoleMaster: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
	$fid = $ref->{'FileID'};
}	
$sth->finish();
if (!$fid) {
	$sth = $dbh->prepare("INSERT INTO Uploads (UserID,FileName,FileSize,UploadDate,UploadTime) VALUES ('$id','$Name','$size','$date','$time')"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING UPLOADS: $DBI::errstr");
	$sth->finish();
	$sth = $dbh->prepare("SELECT * FROM Uploads WHERE UserID='$id' AND FileName='$Name'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING RoleMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	    $fid = $ref->{'FileID'};
	    $size = $ref->{'FileSize'};
	    $date = $ref->{'UploadDate'};
	    $time = $ref->{'UploadTime'};
	}
	$sth->finish();
	$sth = $dbh->prepare("UPDATE JobMaster SET TriptName='$Name' WHERE JobID='$jid'"); 
	$sth->execute() or die &ErrorMsg("ERROR UPDATING JOBMASTER: $DBI::errstr");
	$sth->finish();
}	

if ($didmyown) {
	$dbh->disconnect();
}
return $id,$fid,$Name,$size,$date,$time;
}

sub SaveCCUpload {
my ($id,$jid,$name,$size,$dbh) = @_;

my $didmyown = undef;
if (!$dbh) {
	$didmyown++;
	$dbh = &ConnectDB();
}
my $date = &GetMunge();
my $time = &GetRawTime();
my $Name = &WriteQuotes($name);
my $fid = undef;
my $sth = $dbh->prepare("SELECT * FROM Uploads WHERE UserID='$id' AND FileName='$Name'"); 
$sth->execute() or die &ErrorMsg("ERROR GETTING RoleMaster: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
	$fid = $ref->{'FileID'};
}	
$sth->finish();
if (!$fid) {
	$sth = $dbh->prepare("INSERT INTO Uploads (UserID,FileName,FileSize,UploadDate,UploadTime) VALUES ('$id','$Name','$size','$date','$time')"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING UPLOADS: $DBI::errstr");
	$sth->finish();
	$sth = $dbh->prepare("SELECT * FROM Uploads WHERE UserID='$id' AND FileName='$Name'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING RoleMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	    $fid = $ref->{'FileID'};
	    $size = $ref->{'FileSize'};
	    $date = $ref->{'UploadDate'};
	    $time = $ref->{'UploadTime'};
	}
	$sth->finish();
	$sth = $dbh->prepare("UPDATE JobMaster SET CaseCaption='$Name' WHERE JobID='$jid'"); 
	$sth->execute() or die &ErrorMsg("ERROR UPDATING JOBMASTER: $DBI::errstr");
	$sth->finish();
}	

if ($didmyown) {
	$dbh->disconnect();
}
return $id,$fid,$Name,$size,$date,$time;
}

sub SaveWordUpload {
my ($id,$fid,$lid,$jid,$name,$size,$dbh) = @_;

my $didmyown = undef;
if (!$dbh) {
	$didmyown++;
	$dbh = &ConnectDB();
}
my $date = &GetMunge();
my $time = &GetRawTime();
my $Name = &WriteQuotes($name);
my $fid = undef;
my $sth = $dbh->prepare("SELECT * FROM Uploads WHERE UserID='$id' AND FileName='$Name'"); 
$sth->execute() or die &ErrorMsg("ERROR GETTING RoleMaster: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
	$fid = $ref->{'FileID'};
}	
$sth->finish();
if (!$fid) {
	$sth = $dbh->prepare("INSERT INTO Uploads (UserID,FileName,FileSize,UploadDate,UploadTime) VALUES ('$id','$Name','$size','$date','$time')"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING UPLOADS: $DBI::errstr");
	$sth->finish();
	$sth = $dbh->prepare("SELECT * FROM Uploads WHERE UserID='$id' AND FileName='$Name'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING RoleMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	    $fid = $ref->{'FileID'};
	    $size = $ref->{'FileSize'};
	    $date = $ref->{'UploadDate'};
	    $time = $ref->{'UploadTime'};
	}
	$sth->finish();
	$sth = $dbh->prepare("UPDATE JobMaster SET WordList='$Name' WHERE JobID='$jid'"); 
	$sth->execute() or die &ErrorMsg("ERROR UPDATING JOBMASTER: $DBI::errstr");
	$sth->finish();
}	

if ($didmyown) {
	$dbh->disconnect();
}
return $id,$fid,$Name,$size,$date,$time;
}

sub UpdateUpload {
my ($id,$size,$dbh) = @_;

# &UpdateUpload($id,$size,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $sth = $dbh->prepare("UPDATE Uploads SET Uploads.FileSize='$size' WHERE FileID='$id'"); 
  	$sth->execute() or die &ErrorMsg("ERROR UPDATING UPLOADS: $DBI::errstr");
  	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
	return;
}

sub GetUpload {
my ($id,$dbh) = @_;

# my ($name,$size,$date,$time) = &GetUpload($id,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $role = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Uploads WHERE FileID='$id'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING RoleMaster: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
	  	$name = $ref->{'FileName'};
	  	$size = $ref->{'FileSize'};
	  	$date = $ref->{'UploadDate'};
	  	$time = $ref->{'UploadTime'};
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return $name,$size,$date,$time;
}

sub GetUploads_sel {
my ($id,$dbh) = @_;

# &GetUploards_sel($id,$uid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($lid,$type) = undef;
  	my $sth = $dbh->prepare("SELECT * FROM Uploads ORDER BY FileName"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING UPLOADS: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$fid = $ref->{'FileID'};
	  	$name = $ref->{'FileName'};
		if ($fid eq $id) {
			print "<option value='$fid' selected>$name</option>";
		} else {
			print "<option value='$fid'>$name</option>";
		}
	}	
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub DeleteUpload {
my ($id,$fid,$dbh) = @_;

# &DeleteUpload($id,$fid,$dbh);

	my $didmyown = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my ($name,$size,$date,$time) = &GetUpload($fid,$dbh);
	unlink("/home/mydepo/public_html/cloud/$id/$name");
  	my $sth = $dbh->prepare("DELETE FROM Uploads WHERE FileID='$fid'"); 
	$sth->execute() or die &ErrorMsg("ERROR DELETING UPLOAD: $DBI::errstr");
	$sth->finish();
	if ($didmyown) {	
		$dbh->disconnect();
	}
	return;
}

sub GetTitles {
my ($id,$dbh) = @_;

	my ($temp,$didmyown) = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
	my $title = "";
  	my $sth = $dbh->prepare("SELECT m.Title FROM CertTypes m,CertMaster c 
					WHERE c.UserID='$id'
					AND c.CTID=m.CTID
					AND m.Title<>''"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING CERTMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$temp = $ref->{'Title'};
		$title .= " $temp";
	}	
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
	return $title;
}

sub GetLanguage {
my ($id,$dbh) = @_;

	my ($temp,$didmyown) = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("SELECT Language FROM Languages WHERE LangID='$id'"); 
	$sth->execute() or die &ErrorMsg("GetLanguage: ERROR GETTING LANGUAGES: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$temp = $ref->{'Language'};
	}	
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
	return $temp;
}

sub DidNotify {
my ($uid,$jid,$dbh) = @_;	

my $didmyown = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $today = &GetMunge();
my $now = &GetRawTime();
my $sth = $dbh->prepare("INSERT INTO NotifyMaster (NotifyMaster.UserID,NotifyMaster.JobID,NotifyMaster.NotifyDate,NotifyMaster.NotifyTime)
						VALUES ('$uid','$jid','$today','$now')"); 
$sth->execute() or die &ErrorMsg("GetLanguage: ERROR CREATING NOTIFYMASTER: $DBI::errstr");
$sth->finish();
if ($didmyown) {
	$dbh->disconnect();
}
}

sub CheckNotify {
my ($uid,$jid,$dbh) = @_;

	my ($didmyown,$date,$time) = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("SELECT * FROM NotifyMaster WHERE UserID='$uid' AND JobID='$jid'"); 
	$sth->execute() or die &ErrorMsg("GetLanguage: ERROR CHECKING NOTIFYMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$date = $ref->{'NotifyDate'};
		$time = $ref->{'NotifyTime'};
	}	
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
	return $date,$time;
}

sub GoFindJob {
my ($id,$date,$rid,$dbh) = @_;

	my ($temp,$didmyown) = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("SELECT * FROM JobMaster WHERE FirmID='$id' AND JobDate='$date' AND UserID=''"); 
	$sth->execute() or die &ErrorMsg("GetLanguage: ERROR GETTING JOBMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$temp++;
		$jid = $ref->{'JobID'};
	}	
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
	if ($temp > 1) {
		$jid = undef;
	}
	return $jid,$temp;
}

sub GoCheckAvail {
my ($id,$date,$time,$dbh) = @_;

	my ($status,$am,$noon,$pm,$ad,$ev,$ed,$didmyown) = undef;
	if (!$dbh) {
		$dbh = &ConnectDB();
		$didmyown++;
	}
  	my $sth = $dbh->prepare("SELECT * FROM ScheduleMaster WHERE UserID='$id' AND SchedDate='$date'"); 
	$sth->execute() or die &ErrorMsg("GetLanguage: ERROR GETTING SCHEDULEMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$am = $ref->{'AM'};
		$noon = $ref->{'NOON'};
		$pm = $ref->{'PM'};
		$ad = $ref->{'AD'};
		$ev = $ref->{'EV'};
		$ed = $ref->{'ED'};
	}	
	$sth->finish();
	if ($didmyown) {
		$dbh->disconnect();
	}
	if ($am) {
		if (($time eq "AM") || ($time eq "NOON") || ($time eq "AD") || ($time eq "ED")) {
			$status = "N";
		} elsif (($time eq "PM") || ($time eq "EV")) {
			$status = "Y";
		}
	} elsif ($noon) {
		if (($time eq "AM") || ($time eq "NOON") || ($time eq "AD") || ($time eq "ED") || ($time eq "PM")) {
			$status = "N";
		} elsif ($time eq "EV") {
			$status = "Y";
		}		
	} elsif ($pm) {
		if (($time eq "PM") || ($time eq "NOON") || ($time eq "AD") || ($time eq "ED")) {
			$status = "N";
		} elsif (($time eq "AM") || ($time eq "EV")) {
			$status = "Y";
		}			
	} elsif ($ad) {
		if ($time eq "EV") {
			$status = "Y";
		} else {
			$status = "N";
		}
	} elsif ($ev) {
		if (($time eq "AM") || ($time eq "NOON") || ($time eq "PM")) {
			$status = "Y";
		} else {
			$status = "N";
		}
	} elsif ($ed) {
		$status = "N";
	} else {
		$status = "Y";
	}
	return $status;
}

sub GeneratePassword {

my $pwd = ('A'...'Z')[26 * rand];
$pwd .= int rand (100) + 1;
$pwd .= ('a'...'z')[26 * rand];
$pwd .= ('A'...'Z')[26 * rand];
$pwd .= int rand (1000) + 1;
return $pwd;
}


1;