#!/usr/bin/perl

require "dbutil.pl";
require "errormaster.pl";
require "fairlayout.pl";

use CGI qw(param);
use CGI::Carp qw(fatalsToBrowser);
use DBI;

$cgi = new CGI;
print $cgi->header;


my ($id,$fac,$loc,$map,$cap,$mb,$me,$pid,$mid,$sid,$lid,$status,$power,$plumb,$rest,$kitchen,$air,$heat,$net,$wifi,$notes) = undef;
if (param()) {
	$id = param('id');
	$fac = param('fac');
	$loc = param('loc');
	$map = param('map');
	$cap = param('cap');
	$mb = param('mb');
	$me = param('me');
	$pid = param('pid');
	$mid = param('mid');
	$sid = param('sid');
	$lid = param('lid');
	$status = param('status');
	$power = param('power');
	$plumb = param('plumb');
	$rest = param('rest');
	$kitchen = param('kitchen');
	$air = param('air');
	$heat = param('heat');
	$net = param('net');
	$wifi = param('wifi');
	$notes = param('notes');
}

if($fac && $pid && $cap && $status){
my $dbh = &ConnectDB();
my $Fac = &WriteQuotes($fac);
my $Loc = &WriteQuotes($loc);
my $Notes = &WriteQuotes($notes);
if (!$id) {				# if no ID is passed, 
# ==================================================================
# See if it already exists.
# ==================================================================
	my $sth = $dbh->prepare("SELECT * FROM PropertyMaster WHERE Facility='$Fac' AND PurposeID='$pid'"); 
	$sth->execute() or return NULL;
	while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'PMID'};
	}
	$sth->finish();
	if (!$id) {				# Create a new one.
		$sth = $dbh->prepare("INSERT INTO PropertyMaster 
									(PropertyMaster.Facility,
									PropertyMaster.Location,
  									PropertyMaster.Map,
  									PropertyMaster.Capacity,
  									PropertyMaster.MaxBooths,
  									PropertyMaster.MaxExhibits,
  									PropertyMaster.PurposeID,
  									PropertyMaster.ManagerID,
  									PropertyMaster.SponsorID,
  									PropertyMaster.LastInspected,
  									PropertyMaster.Status,
  									PropertyMaster.Power,
									PropertyMaster.Plumbing,
									PropertyMaster.Restrooms,
									PropertyMaster.Kitchen,
									PropertyMaster.Air,
									PropertyMaster.Heat,
									PropertyMaster.Network,
									PropertyMaster.WiFi,
									PropertyMaster.Notes) 
  					VALUES ('$Fac',
							'$Loc',
							'$map',
  							'$cap',
  							'$mb',
  							'$me',
  							'$pid',
							'$mid',
							'$sid',
							'$lid',
							'$status',
							'$power',
							'$plumb',
							'$rest',
							'$kitchen',
							'$air',
							'$heat',
							'$net',
							'$wifi',
							'$Notes')"); 
	    $sth->execute() or return NULL;
	    $sth->finish();
	    $sth = $dbh->prepare("SELECT * FROM PropertyMaster WHERE Facility='$Fac' AND PurposeID='$pid'"); 
	    $sth->execute() or return NULL;
	    while (my $ref = $sth->fetchrow_hashref()) {
		$id = $ref->{'PMID'};
	    }
	    $sth->finish();	
	} else {
		$sth = $dbh->prepare("Update PropertyMaster SET PropertyMaster.Facility='$Fac',
													PropertyMaster.Location='$Loc',
													PropertyMaster.Map='$map',
													PropertyMaster.Capacity='$cap',
													PropertyMaster.MaxBooths='$mb',
													PropertyMaster.MaxExhibits='$me',
													PropertyMaster.PurposeID='$pid',
													PropertyMaster.ManagerID='$mid',
													PropertyMaster.SponsorID='$sid',
													PropertyMaster.LastInspected='$lid',
													PropertyMaster.Status='$status',
													PropertyMaster.Power='$power',
													PropertyMaster.Plumbing='$plumb',
													PropertyMaster.Restrooms='$rest',
													PropertyMaster.Kitchen='$kitchen',
													PropertyMaster.Air='$air',
													PropertyMaster.Heat='$heat',
													PropertyMaster.Network='$net',
													PropertyMaster.WiFi='$wifi',
													PropertyMaster.Notes='$Notes'
												WHERE PMID='$id'"); 
		$sth->execute() or $id = undef;
		$sth->finish();
	}
} else {
	$sth = $dbh->prepare("Update PropertyMaster SET PropertyMaster.Facility='$Fac',
												PropertyMaster.Location='$Loc',
												PropertyMaster.Map='$map',
												PropertyMaster.Capacity='$cap',
												PropertyMaster.MaxBooths='$mb',
												PropertyMaster.MaxExhibits='$me',
												PropertyMaster.PurposeID='$pid',
												PropertyMaster.ManagerID='$mid',
												PropertyMaster.SponsorID='$sid',
												PropertyMaster.LastInspected='$lid',
												PropertyMaster.Status='$status',
												PropertyMaster.Power='$power',
												PropertyMaster.Plumbing='$plumb',
												PropertyMaster.Restrooms='$rest',
												PropertyMaster.Kitchen='$kitchen',
												PropertyMaster.Air='$air',
												PropertyMaster.Heat='$heat',
												PropertyMaster.Network='$net',
												PropertyMaster.WiFi='$wifi',
												PropertyMaster.Notes='$Notes'
											WHERE PMID='$id'"); 
	$sth->execute() or $id = "";
	$sth->finish();
}
$dbh->disconnect();
print "$id";
} else{
	print "nada";
}
exit;