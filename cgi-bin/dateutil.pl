sub MungeDate {
my $NDate=shift;

	my @NewDate = split(/-/,$NDate);
	foreach $i (0..$#NewDate) {
		$NewDate[$i] =~ s/\+/ /g;
		}
	my $Y = $NewDate[0];
	if ($Y >= "2000") {
		$Y -= "2000";
	}
	if ($Y eq "0") {
		$Y = "00";
	} elsif ($Y eq "1") {
		$Y = "01";
	} elsif ($Y eq "2") {
		$Y = "02";
	}
	my $Month = $NewDate[1];
	my $Day = $NewDate[2];
	my $month = sprintf "%0.2u",$Month;
	my $day = sprintf "%0.2u",$Day;
	my $year = sprintf "%0.2u",$Y;
	my $ret = "$month\/$day\/$year";
	return $ret;
}

sub GetDayOfWeek {
my ($date,$dbh) = @_;

my ($didmyown,$weekday) = undef;
if (!$dbh) {
	$dbh = &ConnectDB();
	$didmyown++;
}
my $sth = $dbh->prepare("SELECT DayOfWeek FROM DayMaster WHERE Day='$date'"); 
$sth->execute() or die &ErrorMsg("ERROR IN DAYMASTER: $DBI::errstr");
while (my $ref = $sth->fetchrow_hashref()) {
  	$weekday = $ref->{'DayOfWeek'};
}
$sth->finish();
if ($didmyown) {
	$dbh->disconnect();
}
return $weekday;
}

sub CompareDates {
my ($date1,$date2) = @_;

	my ($greater,$lesser,$M1,$M2,$D1,$D2,$Y1,$Y2) = undef;
	($M1,$D1,$Y1) = &SplitDate($date1);
	($M2,$D2,$Y2) = &SplitDate($date2);
	if ($Y1 > $Y2) {				# If First year is greater, end here.
		$greater = 1;
	} elsif ($Y2 > $Y1) {			# If Second year is greater, end here.
		$lesser = 1;
	} else {						# If Same Year, look at months.
		if ($M1 > $M2) {			# If First Month is greater, end here.
			$greater = 1;
		} elsif ($M2 > $M1) {		# If Second Month is greater, end here.
			$lesser = 1;
		} else {					# If the months are the same, look at days.
			if ($D1 > $D2) {		# If First Day is greater, end here.
				$greater = 1;
			} elsif ($D2 > $D1) {	# If Second Day is greater, end here.
				$lesser = 1;
			} else {				# Dates are the same.
				$lesser = undef;
				$greater= undef;
			}
		}
	}
	if ((!$greater) && (!$lesser)) {
		$final = undef;
	} elsif ($greater) {
		$final = "G";
	} else {
		$final = "L";
	}
	return $final;	
}

sub FormatDateElements {
my $NDate = shift;

	my @NewDate = split(/-/,$NDate);
	foreach $i (0..$#NewDate) {
		$NewDate[$i] =~ s/\+/ /g;
		}
	my $Year = $NewDate[0];
	my $Month = $NewDate[1];
	my $Day = $NewDate[2];
	my $m = sprintf "%0.2u",$Month;
	my $d = sprintf "%0.2u",$Day;
	my $ret = "$Year-$m-$d";
	return $ret;
}

sub MungeDate2 {
my $NDate = shift;

	@NewDate=split(/-/,$NDate);
	foreach $i (0..$#NewDate) {
		$NewDate[$i] =~ s/\+/ /g;
		}
	$Year = $NewDate[0];
	$Month = $NewDate[1];
	$Day = $NewDate[2];
	$ret = "$Month\/$Day\/$Year";
	return $ret;
}

sub GetMonthDay {
my $NDate=shift;

	@NewDate=split(/-/,$NDate);
	foreach $i (0..$#NewDate) {
		$NewDate[$i] =~ s/\+/ /g;
		}
	$Month = $NewDate[1];
	$Day = $NewDate[2];
	$ret = "$Day";
	return $ret;
}

sub Get_Month {
my $NDate=shift;

	@NewDate=split(/-/,$NDate);
	foreach $i (0..$#NewDate) {
		$NewDate[$i] =~ s/\+/ /g;
		}
	$Month = $NewDate[1];
	return $Month;
}

sub CheckCalDate {
my ($SDate,$EDate,$today) =@_;

	my $GoodDate = 0;
	@StartDate = split(/ /,$SDate);			# process Start Date... parse string
	$FinalS = $StartDate[0];
	@NewSDate=split(/-/,$FinalS);
	foreach $i (0..$#NewSDate) {
		$NewSDate[$i] =~ s/\+/ /g;
		}
	$SYear = $NewSDate[0];
	$SMonth = $NewSDate[1];
	$SDay = $NewSDate[2];
	@EndDate = split(/ /,$EDate);			# process End Date... parse string
	$FinalE = $EndDate[0];
	@NewEDate=split(/-/,$FinalE);
	foreach $i (0..$#NewEDate) {
		$NewEDate[$i] =~ s/\+/ /g;
		}
	$EYear = $NewEDate[0];
	$EMonth = $NewEDate[1];
	$EDay = $NewEDate[2];
#	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
#	$newyear = ($year-100)+2000;
#	$month += 1;
	$newyear = $calyear;
	$newmonth = sprintf "%0.2u",$calmonth;
	$newsmonth = sprintf "%0.2u",$SMonth;
	$newday = sprintf "%0.2u",$SDay;
	$newtoday = sprintf "%0.2u",$today;	
	if ($newyear eq $SYear) {
		if ($newmonth eq $newsmonth) {
			if ($newtoday eq $newday) {
				$GoodDate++;
			} 
		} 
	} 
	return $GoodDate;
}

sub ReturnDate {
	$datetime=shift;
	@Date = split(/ /,$datetime);			# process Start Date... parse string
	$Final = $Date[0];
	@NewDate=split(/-/,$Final);
	foreach $i (0..$#NewDate) {
		$NewDate[$i] =~ s/\+/ /g;
		}
	$Year = $NewDate[0];
	$Month = $NewDate[1];
	$Day = $NewDate[2];
	$monthd = &GetMonthName($Month);
	$datetext = "$monthd $Day, $Year";
	return $datetext;
}

sub ReturnDayMonth {
	$datetime=shift;
	@Date = split(/ /,$datetime);			# process Start Date... parse string
	$Final = $Date[0];
	@NewDate=split(/-/,$Final);
	foreach $i (0..$#NewDate) {
		$NewDate[$i] =~ s/\+/ /g;
		}
	$Year = $NewDate[0];
	$Month = $NewDate[1];
	$Day = $NewDate[2];
	return $Month, $Day;
}

sub FindDateDiff {
my ($higher,$lower) = @_;

	my ($month1,$day1,$year1) = &SplitDate($higher);
	my ($month2,$day2,$year2) = &SplitDate($lower);
	$ydiff = $year1-$year2;
	$mdiff = $month1-$month2;
	$ddiff = $day1-$day2;
	
	if ($mdiff < 1) {
		$ydiff--;
		$mdiff += 12;
	}
	
	if ($ddiff < 1) {
		$mdiff--;
		$ddiff += 30;
	}
	
	return $mdiff,$ddiff,$ydiff;
}

sub SplitDate {
my $date = shift;

	my $i = undef;
	my @NewDate=split(/-/,$date);
	foreach $i (0..$#NewDate) {
		$NewDate[$i] =~ s/\+/ /g;
		}
	my $Y = $NewDate[0];
	my $M = $NewDate[1];
	my $D = $NewDate[2];

	return $M,$D,$Y;	
}

sub ReturnTime {
	$datetime=shift;
	@Time = split(/ /,$datetime);			# process Start Date... parse string
	$Final = $Time[1];
	@NewTime=split(/:/,$Final);
	foreach $i (0..$#NewTime) {
		$NewTime[$i] =~ s/\+/ /g;
		}
	$Hour = $NewTime[0];
	$Hour = sprintf "%0.2u",$Hour;
	$Min = $NewTime[1];
	$Min = sprintf "%0.2u",$Min;
	$Sec = $NewTime[2];
	if ($Hour eq "00") {
		$Hour = "12";
	} elsif ($Hour < 12) {
		$Hour = sprintf "%0.1u",$Hour;
		$am = "am";
	} else {
		$am = "pm";
		$Hour -= 10;
	}
	$timetext = "$Hour:$Min $am";
	return $timetext;
}

sub BreakDate {
	$NDate=shift;
	@NewDate=split(/-/,$NDate);
	foreach $i (0..$#NewDate) {
		$NewDate[$i] =~ s/\+/ /g;
		}
	return $NewDate[0],$NewDate[1],$NewDate[2];
}

sub DateMunge {
	my $Date = shift;
	my @Date=split(/\//,$Date);
	foreach my $i (0..$#Date) {
		$Date[$i] =~ s/\+/ /g;
		}
	my $Y = $Date[2];
	$Y += 2000;
	my $Month = sprintf "%0.2u",$Date[0];
	my $Day = sprintf "%0.2u",$Date[1];
	my $ret = "$Y-$Month-$Day";
	return $ret;
}

sub GetTime {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$Min = sprintf "%0.2u",$minutes;
	$Hour = sprintf "%0.2u",$hour;
	if ($Hour gt "14") {
		$pm = "pm";
		$Hour -= 14;
	} elsif (($Hour eq "14") && ($Min)) {
		$pm = "pm";
		$Hour -= 2;
	} else {
		$pm = "am";
		$Hour -= 2;
	}
	$time = "$Hour\:$Min $pm";
	return $time;
}

sub GetTimeSpan {
my ($time,$dur) = @_;

	my $nh = shift;
	if ($dur eq "1") {
		$nh = 2;
	} elsif ($dur eq "2") {
		$nh = 4;
	} elsif ($dur eq "3") {
		$nh = 6;
	} elsif ($dur eq "4") {
		$nh = 8;
	}
	my @Time=split(/:/,$time);
	foreach my $i (0..$#Time) {
		$Time[$i] =~ s/\+/ /g;
	}
	my $h = $Time[0];
	my $m = $Time[1];
	my $s = $Time[2];
	my $diff = $h+$nh;
	if ($diff > 24) {
		$diff = 24-$diff;
	}
	my $new = "$diff:$m:$s";
	
return $time,$new;
}

sub GetRawTime {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$Min = sprintf "%0.2u",$minutes;
	$Hour = sprintf "%0.2u",$hour;
	$Secs = sprintf "%0.2u",$seconds;
	$time = "$Hour:$Min:$Secs";
	return $time;
}

sub GetRawWeekday {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	return $weekday;
}

sub GetESTTime {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$Min = sprintf "%0.2u",$minutes;
	$Hour = sprintf "%0.2u",$hour;
	$Hour += 4;
	$Secs = sprintf "%0.2u",$seconds;
	$time = "$Hour:$Min:$Secs";
	return $time;
}

sub DateMunge2 {
my $Date = shift;

	my @Date=split(/\//,$Date);
	foreach my $i (0..$#Date) {
		$Date[$i] =~ s/\+/ /g;
		}
	my $Y = $Date[2];
	$Y =~ s/\s+$//;
	my $Month = sprintf "%0.2u",$Date[0];
	my $Day = sprintf "%0.2u",$Date[1];
	my $ret = "$Y-$Month-$Day";
	return $ret;
}

sub GetDateTime {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$month += 1;
	if ($hour eq "0") {
		$hour = "22";
	} elsif ($hour eq "1") {
		$hour = "23";
	}
	$year -= 100;
	$year += 2000;
	$mnth = sprintf "%0.2u",$month;
	$day = sprintf "%0.2u",$monthday;
	$datetime = "$year-$mnth-$day $hour:$minutes:$seconds";
	return $datetime;
}

sub GetDate {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$month += 1;
	$Yr = $year;
	$Yr -= 100;
	$Year = sprintf "%0.2u",$Yr;
	$day = sprintf "%0.2u",$monthday;
	$mnth = sprintf "%0.2u",$month;
	$date = "$mnth\/$day\/$Year";
	return $date;
}

sub GetDate_plus_week {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$month += 1;
	$Yr = $year;
	$Yr -= 100;
	$Year = sprintf "%0.2u",$Yr;
	my $newday = $monthday+7;
	my $dim = &GetDaysInMonth($month);
	if ($newday > $dim) {
		$month += 1;
		$day = ($newday-$dim);
		if ($month eq "13") {
			$month = "1";
			$Year++;
		}
	}
	my $nday = sprintf "%0.2u",$day;
	my $nmonth = sprintf "%0.2u",$month;
	$date = "$nmonth\/$nday\/$Year";
	return $date;
}

sub Add_Seven_Days {
my $olddate = shift;

	@Date=split(/\//,$olddate);
	foreach $i (0..$#Date) {
		$Date[$i] =~ s/\+/ /g;
		}
	$Y = $Date[2];
	$Y += 2000;
	$Month = sprintf "%0.2u",$Date[0];
	$Day = sprintf "%0.2u",$Date[1];
	my $newday = $Day+7;
	my $dim = &GetDaysInMonth($Month);
	if ($newday > $dim) {
		$Month += 1;
		$day = ($newday-$dim);
		if ($Month eq "13") {
			$Month = "1";
			$Y++;
		}
	} else {
		$day = $newday;
	}
	$year = $Y-2000;
	my $nday = sprintf "%0.2u",$day;
	my $nmonth = sprintf "%0.2u",$Month;
	my $nyear = sprintf "%0.2u",$year;
	$date = "$nmonth\/$nday\/$nyear";
	return $date;
}

sub Add_X_Days {
my ($olddate,$daysnum) = @_;

	my @Date=split(/\//,$olddate);
	foreach $i (0..$#Date) {
		$Date[$i] =~ s/\+/ /g;
		}
	my $Y = $Date[2];
	$Y += 2000;
	my $Month = sprintf "%0.2u",$Date[0];
	my $Day = sprintf "%0.2u",$Date[1];
	my $newday = $Day+$daysnum;
	my $dim = &GetDaysInMonth($Month);
	if ($newday > $dim) {
		$Month += 1;
		$day = ($newday-$dim);
		if ($Month eq "13") {
			$Month = "1";
			$Y++;
		}
	} else {
		$day = $newday;
	}
	my $nday = sprintf "%0.2u",$day;
	my $nmonth = sprintf "%0.2u",$Month;
	my $date = "$Y-$nmonth-$nday";
	return $date;
}

sub Add_x_Days {
my ($olddate,$daysnum) = @_;

	my @Date=split(/-/,$olddate);
	foreach $i (0..$#Date) {
		$Date[$i] =~ s/\+/ /g;
		}
	my $Y = $Date[0];
	my $Month = sprintf "%0.2u",$Date[1];
	my $Day = sprintf "%0.2u",$Date[2];
	my $newday = $Day+$daysnum;
	my $dim = &GetDaysInMonth($Month);
	if ($newday > $dim) {
		$Month += 1;
		$day = ($newday-$dim);
		if ($Month eq "13") {
			$Month = "1";
			$Y++;
		}
	} else {
		$day = $newday;
	}
	my $nday = sprintf "%0.2u",$day;
	my $nmonth = sprintf "%0.2u",$Month;
	my $date = "$Y-$nmonth-$nday";
	return $date;
}

sub Add_Sunday {
my ($olddate,$daysnum) = @_;

	my ($first,$gc) = undef;
	my @Date=split(/-/,$olddate);
	foreach $i (0..$#Date) {
		$Date[$i] =~ s/\+/ /g;
		}
	my $Y = $Date[0];
	my $Month = sprintf "%0.2u",$Date[1];
	my $Day = sprintf "%0.2u",$Date[2];
	my $newday = $Day+$daysnum;
	my $dim = &GetDaysInMonth($Month);
	if ($newday > $dim) {
		$Month += 1;
		$first++;
		if (($Month eq "4") || ($Month eq "10")) {
			$gc++;
			$first = undef;
		} else {
			$gc = undef;
		}
		$day = ($newday-$dim);
		if ($Month eq "13") {
			$Month = "1";
			$Y++;
		}
	} else {
		$day = $newday;
	}
	my $nday = sprintf "%0.2u",$day;
	my $nmonth = sprintf "%0.2u",$Month;
	my $date = "$Y-$nmonth-$nday";
	return $date,$first,$gc;
}

sub Add_Three_Years {
my $olddate = shift;

	@Date=split(/\//,$olddate);
	foreach $i (0..$#Date) {
		$Date[$i] =~ s/\+/ /g;
		}
	$Y = $Date[2];
	$Y += 2000;
	$Month = sprintf "%0.2u",$Date[0];
	$Day = sprintf "%0.2u",$Date[1];
	$year = $Y-2000;
	$year += 3;
	my $nday = sprintf "%0.2u",$day;
	my $nmonth = sprintf "%0.2u",$Month;
	my $nyear = sprintf "%0.2u",$year;
	$date = "$nmonth\/$nday\/$nyear";
	return $date;
}

sub GetThisWeek {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$month += 1;
	$Yr = $year;
	$Yr -= 100;
	$Yr += 2000;
	print "Content-type:text/html\n\n";
	if (($monthday-weekday) >= 0) {
		$weekday -= 1;						#  Adjust for Monday being start of week, not Sunday.
		$day = $monthday-$weekday;			#  Day is the date of the Monday of the current week
		$dim = &GetDaysInMonth($month);		#  Days in month.
		if (($day+6) > $dim) {
			if (($diff = ($dim - $day)) < 7) {	  # check and see if last week of the month.
				$month++;						#  If so, bump end date to next month.
				$eday = (6 - $diff);			#  End date is into the next month.
			} else {
				$eday = $diff;	
			}
		} else {
			$eday = $day+6;	
		}
		$xyear = sprintf "%0.2u",$Yr;
		$xmonth = sprintf "%0.2u",$month;
		$xday = sprintf "%0.2u",$day;
		$xeday = sprintf "%0.2u",$eday;
		$bdate = "$xyear-$xmonth-$xday";
		$edate = "$xyear-$xmonth-$xeday";
	} else {
		$nmonth = $month-1;
		$tdays = $weekday-$monthday;
		$dim = &GetDaysInMonth($nmonth);
		$day = $dim-$tdays;
		$xyear = sprintf "%0.2u",$Yr;
		$xmonth = sprintf "%0.2u",$month;
		$xnmonth = sprintf "%0.2u",$nmonth;
		$xday = sprintf "%0.2u",$day;
		$xeday = sprintf "%0.2u",$monthday;
		$bdate = "$xyear-$xnmonth-$xday";
		$edate = "$xyear-$xmonth-$xeday";
	}
	return $bdate,$edate;
}

sub GetNextSunday {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$month += 1;
	my $Yr += $year+1900;
	my $mn = sprintf "%0.2u",$month;
	my $addnum = 7-$weekday;
	my $date = "$Yr-$mn-$monthday";
	my $newdate = &Add_x_Days($date,$addnum);
	return $newdate;
}

sub IsTodaySunday {
	my $answer = undef;
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	if ($weekday eq "6") {
		$answer = "Y";
	} else {
		$answer = "N";
	}
	return $answer;	
}

sub GetLastWeek {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$month += 1;
	$Yr = $year+1900;
	if (($monthday-$weekday)-7 >= 0) {
		$day = ($monthday-$weekday)-7;
		$eday = sprintf "%0.2u",$day;
		$mnth = sprintf "%0.2u",$month;
		$bdate = "$Yr-$mnth-$eday";
		$eday = $day+5;
		$day = sprintf "%0.2u",$eday;
		$edate = "$Yr-$mnth-$day";
	} else {
		$nmonth = $month-1;
		$dim = &GetDaysInMonth($nmonth);
		$nttl = $dim+$monthday;
		$day = ($nttl-$weekday)-7;
		$eday = $day+5;
		$bday = sprintf "%0.2u",$day;
		$mnth = sprintf "%0.2u",$nmonth;
		$bdate = "$Yr-$mnth-$bday";
		if ($eday > $dim) {
			$mnth++;
			$eday = $eday-$dim;
		}
		$day = sprintf "%0.2u",$eday;
		$edate = "$Yr-$mnth-$day";
	}
	return $bdate,$edate;
}

sub GetThisMonth {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$month += 1;
	$Yr = $year+2000;
	$mnth = sprintf "%0.2u",$month;
	$Year = sprintf "%0.2u",$Yr;
	$bdate = "$Year-$mnth-01";
	$edate = "$Year-$mnth-$monthday";
	return $bdate,$edate;
}

sub CheckBDay {
my $dob = shift;

	my @DOB = split(/-/,$dob);
	foreach $i (0..$#DOB) {
		$DOB[$i] =~ s/\+/ /g;
		}
	$Month = $DOB[1];
	$Day = $DOB[2];
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$month += 1;
	$f_mnth = sprintf "%0.2u",$Month;
	$f_mnth2 = sprintf "%0.2u",$month;
	if ($f_mnth eq $f_mnth2) {
		$ret = "$Month $Day";
	} else {
		$ret = undef;
	}
	return $ret;
}

sub GetThisTerm {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$month += 1;
	$Yr = $year+1900;
	$mnth = sprintf "%0.2u",$month;
	$Year = sprintf "%0.2u",$Yr;
	my ($term,$year) = &GetCurrentTerm_Year();
	$bdate = &GetTermStart($term,$year);
	$edate = "$Year-$mnth-$monthday";
	return $bdate,$edate;
}

sub GetLastTerm {
	my ($term,$year) = &GetCurrentTerm_Year();
	if ($term eq "1") {
		$year -= 1;
		$term = "4"; 
	} else {
		$term -= 1;
	}
	($bdate,$edate) = &GetTermStart_End($term,$year);
	return $bdate,$edate;
}

sub GetLastMonth {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$Yr = $year+1900;
	$mnth = sprintf "%0.2u",$month;
	$bdate = "$Yr-$mnth-01";
	$endday = &GetDaysInMonth($month);
	$day = sprintf "%0.2u",$endday;
	$edate = "$Yr-$mnth-$endday";
	return $bdate,$edate;
}

sub GetPriorSixMonths {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	my $Yr = $year+1900;
	$month++;
	my $mnth = sprintf "%0.2u",$month;
	my $day = sprintf "%0.2u",$monthday;
	my $bdate = "$Yr-$mnth-$day";
	if ($mnth ge "07") {
		$newmnth = $mnth-6;
		my $nm = sprintf "%0.2u",$newmnth;
		$edate = "$Yr-$nm-01";
	} else {
		--$Yr;
		my $temp1=6-$mnth;
		my $temp2=12-$temp1;
		my $nm = sprintf "%0.2u",$temp2;
		$edate="$Yr-$nm-01";
	}
	return $edate;
}

sub GetYesterday {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$month += 1;
	$Yr = $year;
	$Yr -= 100;
	$Yr += 2000;
	$Year = sprintf "%0.2u",$Yr;
	if ($monthday eq 1) {
		$nmonth = $month-1;
		$day = &GetDaysInMonth($nmonth);
		if ($weekday eq 2) {
			$day -= 1;
		}
	} else {
		$nmonth = $month;
		if ($weekday eq 2) {
			$day -= 1;
		} else {
			$day = $monthday-1;
		}
	}
	$bdate = "$year-$nmonth-$day";
	return $bdate;
}

sub GetThisYear {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$month += 1;
	$Yr = $year;
	$Yr -= 100;
	$Year = sprintf "%0.2u",$Yr;
	if ($month < 8) {
		$nyear = $year-1;
	}
	my ($term,$year) = &GetCurrentTerm_Year();
	$term = "1";
	$bdate = &GetTermStart($term,$year);
	$edate = "$year-$month-$monthday";
	return $bdate,$edate;
}

sub CheckTime {
my $code = shift;

	my $date = &GetMunge();
	if ($code eq "T") {					#  Checking for Todays records
		$bdate = $date;
		$edate = $date;
	} elsif ($code eq "WTD") {			#  Week To Date
		($bdate,$edate) = &GetThisWeek($date);
	} elsif ($code eq "MTD") {			#  Month To Date
		($bdate,$edate) = &GetThisMonth($date);
	} elsif ($code eq "LW") {			#  Last Week
		($bdate,$edate) = &GetLastWeek($date);
	} elsif ($code eq "LM") {			#  Last Month
		($bdate,$edate) = &GetLastMonth($date);
	} elsif ($code eq "Y") {			#  Last Week
		($bdate,$edate) = &GetYesterday($date);
	} elsif ($code eq "TTD") {			#  This Term
		($bdate,$edate) = &GetThisTerm();
	} elsif ($code eq "LT") {			#  Last Term
		($bdate,$edate) = &GetLastTerm();
	} elsif ($code eq "YTD") {			#  Year To Date
		($bdate,$edate) = &GetThisYear($date);
	}
	return $bdate,$edate;
}

sub GetDBDate {
	my ($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$month += 1;
	$Yr = $year;
	$Yr -= 100;
	$Yr += 2000;
	$mnth = sprintf "%0.2u",$month;
	$day = sprintf "%0.2u", $monthday;
	$date = "$year-$mnth-$day";
	return $date;
}

sub GetDay {
	my ($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	return $monthday;
}

sub GetCalendarStart {
my ($month,$year) = @_;

	my $weekday = undef;
	if (!$month) {
		my ($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
		return $monthday,$weekday;
	} else {
		if (!$year) {
			$year = &GetYear();
		}
		my $mnth = sprintf "%0.2u",$month;
		my $day = "01";
		my $date = "$year-$mnth-$day";
#		print "<tr><td>Date: $date</td></tr>";
		$temp = &GetDayOfWeek($date,undef);
#		print "<tr><td>Day of the Week: $temp</td></tr>";
		if ($temp eq "Sunday") {
			$weekday = 0;
		} elsif ($temp eq "Monday") {
			$weekday = 1;
		} elsif ($temp eq "Tuesday") {
			$weekday = 2;
		} elsif ($temp eq "Wednesday") {
			$weekday = 3;
		} elsif ($temp eq "Thursday") {
			$weekday = 4;
		} elsif ($temp eq "Friday") {
			$weekday = 5;
		} elsif ($temp eq "Saturday") {
			$weekday = 6;
		}
#		print "<p><h3>GetCalendarStart: [ $month | $year | $date | $temp | $weekday ]</h3></p>";
		return $day,$weekday;
	}
}

sub GetDay_Of_Week {
my $date = shift;

	my ($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$year += 1900;
	@NewDate=split(/-/,$date);
	foreach $i (0..$#NewDate) {
		$NewDate[$i] =~ s/\+/ /g;
		}
	$Year = $NewDate[0];
	$Month = $NewDate[1];
	$Day = $NewDate[2];
	if (($Year eq $year) && ($Month eq $month) && ($Day eq $monthday)) {
		return $weekday;
	} else {
		if ($Year eq $year) {
			if ($Month eq $month) {
				if (($monthday-$Day) <= 7) {
					$newwd = ($weekday-$Day);			
				} else {			
					$newwd = ($temp%7);			
				} 
			} else {
				if (($month - $Month) eq 1) {
					$newwd = ($monthday+(&GetDaysInMonth($Month)-$Day)%7);
				} else {
					$temp = $month-$Month;
					$tempmonth = $month;
					while ($temp) {
						$days += &GetDaysInMonth(--$tempmonth);
						$temp--;
					}
					$newwd = ((($monthday+$days)-$Day)%7);
				}
			}
		} 
		return $newwd;	
	}
}


sub GetMonth {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$month += 1;
	return $month;
}

sub GetMonthName {
	$month = shift;
	if ($month eq "1") {
		$monthdesc = "January";
	} elsif ($month eq "2") {
		$monthdesc = "February";
	} elsif ($month eq "3") {
		$monthdesc = "March";
	} elsif ($month eq "4") {
		$monthdesc = "April";
	} elsif ($month eq "5") {
		$monthdesc = "May";
	} elsif ($month eq "6") {
		$monthdesc = "June";
	} elsif ($month eq "7") {
		$monthdesc = "July";
	} elsif ($month eq "8") {
		$monthdesc = "August";
	} elsif ($month eq "9") {
		$monthdesc = "September";
	} elsif ($month eq "10") {
		$monthdesc = "October";
	} elsif ($month eq "11") {
		$monthdesc = "November";
	} elsif ($month eq "12") {
		$monthdesc = "December";
	} elsif ($month eq "01") {
		$monthdesc = "January";
	} elsif ($month eq "02") {
		$monthdesc = "February";
	} elsif ($month eq "03") {
		$monthdesc = "March";
	} elsif ($month eq "04") {
		$monthdesc = "April";
	} elsif ($month eq "05") {
		$monthdesc = "May";
	} elsif ($month eq "06") {
		$monthdesc = "June";
	} elsif ($month eq "07") {
		$monthdesc = "July";
	} elsif ($month eq "08") {
		$monthdesc = "August";
	} elsif ($month eq "09") {
		$monthdesc = "September";
	} 
	return $monthdesc;
}

sub GetMonthAbbr {
	$month = shift;
	if ($month eq 1) {
		$monthdesc = "Jan";
	} elsif ($month eq 2) {
		$monthdesc = "Feb";
	} elsif ($month eq 3) {
		$monthdesc = "Mar";
	} elsif ($month eq 4) {
		$monthdesc = "Apr";
	} elsif ($month eq 5) {
		$monthdesc = "May";
	} elsif ($month eq 6) {
		$monthdesc = "Jun";
	} elsif ($month eq 7) {
		$monthdesc = "Jul";
	} elsif ($month eq 8) {
		$monthdesc = "Aug";
	} elsif ($month eq 9) {
		$monthdesc = "Sep";
	} elsif ($month eq 10) {
		$monthdesc = "Oct";
	} elsif ($month eq 11) {
		$monthdesc = "Nov";
	} elsif ($month eq 12) {
		$monthdesc = "Dec";
	}
	return $monthdesc;
} 

sub GetYear {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$year += 1900;
	return $year;
}

sub GetSchoolYear {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$year += 1900;
	if ($month < 7) {
		$year -= 1;	
	}
	return $year;
}

sub GetWeekday {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	return $weekday;
}

sub GetDMWeekday {
my $date = shift;

	my $dbh = &ConnectDB();
  	my $sth = $dbh->prepare("SELECT * FROM DayMaster WHERE Day='$date'"); 
	$sth->execute() or die &ErrorMsg("ERROR GETTING DAYMASTER: $DBI::errstr");
	while (my $ref = $sth->fetchrow_hashref()) {
		$dayofweek = $ref->{'DayOfWeek'};
	}
	$sth->finish();
	$dbh->disconnect();
	return $dayofweek;
}

sub GetMunge {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$month += 1;
	$Yr = $year;
	$Yr -= 100;
	$Year = sprintf "%0.2u",$Yr;
	$date = "$month\/$monthday\/$Year";
	$date = &DateMunge($date);
	return $date;
}

sub FigureOutCalendarStart {
my ($month,$year) = @_;


my ($fday,$weekday) = &GetCalendarStart($month,$year);
# print "<tr><td>Get Calendar Start: [ $month | $year | $fday | $weekday ]</td></tr>";
my $day = sprintf "%0.0u",$fday;
if ($day gt $weekday) {
	if (!$weekday) {
		$weekstart = 1;
	} 
} elsif ($day lt $weekday) {
	if ($weekday eq 6) {
		$weekstart = 1;
		$day = 3;
	} else {
		$weekstart = $weekday;
	}
} elsif ($day eq $weekday) {
	$weekstart = $weekday;
}
# print "<tr><td>Leaving Calendar Start: [ $day | $weekstart ]</td></tr>";
return $day,$weekstart;	
}

sub GetLongDate_parm {
my $date = shift;
	
	my ($year,$mnth,$day) = &BreakDate($date);
	my $month = sprintf "%0.2u",$mnth;
	if ($month eq "01") {
		$month = "Jan";
	} elsif ($month eq "02") {
		$month = "Feb";
	} elsif ($month eq "03") {
		$month = "Mar";
	} elsif ($month eq "04") {
		$month = "Apr";
	} elsif ($month eq "05") {
		$month = "May";
	} elsif ($month eq "06") {
		$month = "Jun";
	} elsif ($month eq "07") {
		$month = "Jul";
	} elsif ($month eq "08") {
		$month = "Aug";
	} elsif ($month eq "09") {
		$month = "Sep";
	} elsif ($month eq "10") {
		$month = "Oct";
	} elsif ($month eq "11") {
		$month = "Nov";
	} elsif ($month eq "12") {
		$month = "Dec";
	}
	$date = "$month $day, $year";	my $month = sprintf "%0.2u",$mnth;
	if ($month eq "01") {
		$month = "Jan";
	} elsif ($month eq "02") {
		$month = "Feb";
	} elsif ($month eq "03") {
		$month = "Mar";
	} elsif ($month eq "04") {
		$month = "Apr";
	} elsif ($month eq "05") {
		$month = "May";
	} elsif ($month eq "06") {
		$month = "Jun";
	} elsif ($month eq "07") {
		$month = "Jul";
	} elsif ($month eq "08") {
		$month = "Aug";
	} elsif ($month eq "09") {
		$month = "Sep";
	} elsif ($month eq "10") {
		$month = "Oct";
	} elsif ($month eq "11") {
		$month = "Nov";
	} elsif ($month eq "12") {
		$month = "Dec";
	}
	$date = "$month $day, $year";
	return $date;
}

sub GetLongDate_parm_noyear {
my $date = shift;

	$ndate = &BreakDate($date);
	my $month = @$ndate[1];
	my $day = @$ndate[2];
	if ($month eq "01") {
		$month = "Jan";
	} elsif ($month eq "02") {
		$month = "Feb";
	} elsif ($month eq "03") {
		$month = "Mar";
	} elsif ($month eq "04") {
		$month = "Apr";
	} elsif ($month eq "05") {
		$month = "May";
	} elsif ($month eq "06") {
		$month = "Jun";
	} elsif ($month eq "07") {
		$month = "Jul";
	} elsif ($month eq "08") {
		$month = "Aug";
	} elsif ($month eq "09") {
		$month = "Sep";
	} elsif ($month eq "10") {
		$month = "Oct";
	} elsif ($month eq "11") {
		$month = "Nov";
	} elsif ($month eq "12") {
		$month = "Dec";
	}
	$date = "$month $day";
	return $date;
}

sub GetLongDate_normal {
my $date = shift;

	$weekday = &Get_DayMaster_Weekday($date);
	$ndate = &BreakDate($date);
	my $month = @$ndate[1];
	my $day = @$ndate[2];
	my $year = @$ndate[0];
	if ($month eq "01") {
		$month = "Jan";
	} elsif ($month eq "02") {
		$month = "Feb";
	} elsif ($month eq "03") {
		$month = "Mar";
	} elsif ($month eq "04") {
		$month = "Apr";
	} elsif ($month eq "05") {
		$month = "May";
	} elsif ($month eq "06") {
		$month = "Jun";
	} elsif ($month eq "07") {
		$month = "Jul";
	} elsif ($month eq "08") {
		$month = "Aug";
	} elsif ($month eq "09") {
		$month = "Sep";
	} elsif ($month eq "10") {
		$month = "Oct";
	} elsif ($month eq "11") {
		$month = "Nov";
	} elsif ($month eq "12") {
		$month = "Dec";
	}
	$date = "$weekday, $month $day, $year";
	return $date;
}

sub GetLongDate {
	($seconds, $minutes, $hour, $monthday, $month, $year, $weekday, $yearday, $isdist) = localtime(time);
	$month += 1;
	$weekday += 1;
	$Yr = $year;
	$Yr += 1900;
	if ($month eq "1") {
		$month = "Jan";
	} elsif ($month eq "2") {
		$month = "Feb";
	} elsif ($month eq "3") {
		$month = "Mar";
	} elsif ($month eq "4") {
		$month = "Apr";
	} elsif ($month eq "5") {
		$month = "May";
	} elsif ($month eq "6") {
		$month = "Jun";
	} elsif ($month eq "7") {
		$month = "Jul";
	} elsif ($month eq "8") {
		$month = "Aug";
	} elsif ($month eq "9") {
		$month = "Sep";
	} elsif ($month eq "10") {
		$month = "Oct";
	} elsif ($month eq "11") {
		$month = "Nov";
	} elsif ($month eq "12") {
		$month = "Dec";
	}
	if ($weekday eq "1") {
		$weekday = "Sunday";
	} elsif ($weekday eq "2") {
		$weekday = "Monday";
	} elsif ($weekday eq "3") {
		$weekday = "Tuesday";
	} elsif ($weekday eq "4") {
		$weekday = "Wednesday";
	} elsif ($weekday eq "5") {
		$weekday = "Thursday";
	} elsif ($weekday eq "6") {
		$weekday = "Friday";
	} elsif ($weekday eq "7") {
		$weekday = "Saturday";
	}
	$date = "$weekday, $month $monthday, $Yr";
	return $date;
}

sub GetDaysInMonth {
my $mnth = shift;

	$month = sprintf "%0.2u",$mnth;
	if (($month eq "01") || ($month eq "1")) {
		$days = 31;
	} elsif (($month eq "02") || ($month eq "2")) {
		$days = 28;
	} elsif (($month eq "03") || ($month eq "3")) {
		$days = 31;
	} elsif (($month eq "04") || ($month eq "4")) {
		$days = 30;
	} elsif (($month eq "05") || ($month eq "5")) {
		$days = 31;
	} elsif (($month eq "06") || ($month eq "6")) {
		$days = 30;
	} elsif (($month eq "07") || ($month eq "7")) {
		$days = 31;
	} elsif (($month eq "08") || ($month eq "8")) {
		$days = 31;
	} elsif (($month eq "09") || ($month eq "9")) {
		$days = 30;
	} elsif ($month eq "10") {
		$days = 31;
	} elsif ($month eq "11") {
		$days = 30;
	} elsif ($month eq "12") {
		$days = 31;
	}
	return $days;
}

sub GetSchoolDaysInMonth {
my $mnth = shift;
my $term = shift;
my $year = shift;

  my $oldmonth = sprintf "%0.2u",$mnth;
  my $dbh = &ConnectDB();
  my $cnt = undef;
  my $sth = $dbh->prepare("SELECT * FROM DayMaster WHERE DayMaster.Term='$term' 
  												AND DayMaster.Year='$year'
  												AND DayMaster.InSession='Y'
  												ORDER BY DayMaster.Day"); 
  $sth->execute() or print "<p><h3>ERROR IN DAYMASTER: $DBI::errstr</h3></p>";
  while (my $ref = $sth->fetchrow_hashref()) {
	$day = $ref->{'Day'};
	$the_month = &Get_Month($day);
	my $month = sprintf "%0.2u",$the_month;
	if ($month eq $oldmonth) {
		$cnt++;
		}
	}
  $sth->finish();
  $dbh->disconnect();
  return $cnt;
}

sub GetSchoolDaysThisTerm {
my $term = shift;
my $year = shift;

  my $dbh = &ConnectDB();
  my $cnt = undef;
  my $sth = $dbh->prepare("SELECT * FROM DayMaster WHERE DayMaster.Term='$term' 
  												AND DayMaster.Year='$year'
  												AND DayMaster.InSession='Y'
  												ORDER BY DayMaster.Day"); 
  $sth->execute() or print "<p><h3>ERROR IN DAYMASTER: $DBI::errstr</h3></p>";
  while (my $ref = $sth->fetchrow_hashref()) {
	$cnt++;
	}
  $sth->finish();
  $dbh->disconnect();
  return $cnt;
}

sub Get_DayMaster_Weekday {
my $date = shift;

  my $dbh = &ConnectDB();
  my $sth = $dbh->prepare("SELECT * FROM DayMaster WHERE Day='$date'"); 
  $sth->execute() or print "<p><h3>ERROR IN DAYMASTER: $DBI::errstr</h3></p>";
  while (my $ref = $sth->fetchrow_hashref()) {
	$weekday = $ref->{'DayOfWeek'};
	}
  $sth->finish();
  $dbh->disconnect();
  return $weekday;
}

sub GetLateDays {
	$datedue = $_[0];
	$datein = $_[1];

	@Date=split(/-/,$datedue);
	foreach $i (0..$#datedue) {
		$Date[$i] =~ s/\+/ /g;
		}
	$DDM = $Date[1];
	$DDM = sprintf "%0.1u",$DDM;
	$DDY = $Date[0] -= 2000;
	$DDD = $Date[2];

	@Date2=split(/-/,$datein);
	foreach $i (0..$#datein) {
		$Date2[$i] =~ s/\+/ /g;
		}
	$DIM = $Date2[1];
	$DIM = sprintf "%0.1u",$DIM;	
	$DIY = $Date2[0] -= 2000;
	$DID = $Date2[2];
	
	if ($DIM lt $DDM) {
		$AdjustedDIM = $DIM+$DDM;
	} else {
		$AdjustedDIM = $DIM;
	}
#  If the month due and the month turned in are the same, simply subtract the days.
	if ($DIM eq $DDM) {
		$final = $DID-$DDD;
	} elsif (($AdjustedDIM-$DDM) eq 1) {
		$ddue = &GetDaysInMonth($DDM);
		$due = $ddue-$DDD;
		$final = $DID+$due;
	} elsif (($AdjustedDIM-$DDM) gt 1) {
		$MD = ($DIM-$DDM)+1;
		$ddue = &GetDaysInMonth($DDM);
		$due = $ddue-$DDD;
		$final = $DID+$due;
		$MD -= 2;
		while ($MD) {
			$final += &GetDaysInMonth(++$DDM);
			$MD--;
		}
	}
	return $final;
}

sub CheckDate { 
my $date = shift;
my $exp = shift;
my $check_date = shift;

	$ndate = &BreakDate($date);
	$DisplayMonth = @$ndate[1];
	$DisplayDay = @$ndate[2];
	$DisplayYear = @$ndate[0];
	$edate = &BreakDate($exp);
	$ExpMonth = @$edate[1];
	$ExpDay = @$edate[2];
	$ExpYear = @$edate[0];
	if (!$check_date) {
		$currdate = &GetDate();
		$mdate = &DateMunge($currdate);
	} else {
		$mdate = $check_date;
	}
	$bdate = &BreakDate($mdate);
	$CurrM = @$bdate[1];
	$CurrD = @$bdate[2];
	$CurrY = @$bdate[0];
	$flag = 0;
	if ($CurrY >= $DisplayYear) {						# If the current year is greater than or equal to the display year,
		if ($CurrM >= $DisplayMonth) {					# And if the current month is greater than or equal to the display month,
			if ($CurrM eq $DisplayMonth) {				# Check to see if the month is the same.. if not, approve.
				if ($CurrD >= $DisplayDay) {			# Otherwise, check days.  If day is greater than or equal to the display day, then approve.
					$flag = 1;
				}
			} else {
				$flag = 1;
			}
		}
	}
	if ($flag eq 1) {							# If current date is after display date, check expiration
		$flag = 0;								# reset flag
		if ($CurrY == $ExpYear) {					# If the current year is greater than or equal to the display year,
			if ($CurrM <= $ExpMonth) {			# And if the current month is greater than or equal to the display month,
				if ($CurrM eq $ExpMonth) {			# Check to see if the month is the same.. if not, approve.
					if ($CurrD <= $ExpDay) {		# Otherwise, check days.  If day is greater than or equal to the display day, then approve.
						$flag = 1;
					}
				} else {
					$flag = 1;
				}
			} 
		}  elsif ($CurrY < $ExpYear) {
			$flag = 1;
		} 
	}
	return $flag;		
}

sub DatePast { 
my $date = shift;

	$adate = &BreakDate($date);
	$AssignMonth = @$adate[1];
	$AssignDay = @$adate[2];
	$AssignYear = @$adate[0];

	$currdate = &GetDate();
	$mdate = &DateMunge($currdate);
	$bdate = &BreakDate($mdate);
	$CurrM = @$bdate[1];
	$CurrD = @$bdate[2];
	$CurrY = @$bdate[0];

	$flag = 0;
	if ($CurrY >= $AssignYear) {						# If the current year is greater than or equal to the display year,
		if ($CurrM >= $AssignMonth) {					# And if the current month is greater than or equal to the display month,
			if ($CurrM eq $AssignMonth) {				# Check to see if the month is the same.. if not, approve.
				if ($CurrD > $AssignDay) {				# Otherwise, check days.  If day is greater than or equal to the display day, then approve.
					$flag = 1;
				} 
			} else {
				$flag = 1;
			}
		} 
	} 	
	return $flag;		
}  

sub GetTime_Minute_sel {
	
my ($hour,$min,$sec) = 0;	
my $time = "00:00:00";
for ($hour = 0;$hour < 25;$hour++) {
	for ($min = 0;$min < 60;$min++) {
		$h = sprintf "%0.2u",$hour;
		$m = sprintf "%0.2u",$min;
		$s = sprintf "%0.2u",$sec;
		if ($h < 13) {
			$am = "am";
		} else {
			$h -= 12;
			$am = "pm";	
		}
		$time = "$h:$m:$s";
		$desc = "$h:$m $am";
		print "<option value='$time'>$desc</option>";
	}
}

}

sub GetFormattedDate {
my $date = shift;

	my $wkdy = &GetDMWeekday($date);
	my $long = &GetLongDate_parm($date);
	my $date = "$wkdy, $long";
	return $date;	
}

sub GetSundayFormattedDate {
my $date = shift;

	my @NewDate = split(/-/,$date);
	foreach $i (0..$#NewDate) {
		$NewDate[$i] =~ s/\+/ /g;
		}
	my $Y = $NewDate[0];
	my $Month = $NewDate[1];
	my $Day = $NewDate[2];
	my $month = sprintf "%0.2u",$Month;
	if ($month eq "01") {
		$month = "Jan";
	} elsif ($month eq "02") {
		$month = "Feb";
	} elsif ($month eq "03") {
		$month = "Mar";
	} elsif ($month eq "04") {
		$month = "Apr";
	} elsif ($month eq "05") {
		$month = "May";
	} elsif ($month eq "06") {
		$month = "Jun";
	} elsif ($month eq "07") {
		$month = "Jul";
	} elsif ($month eq "08") {
		$month = "Aug";
	} elsif ($month eq "09") {
		$month = "Sep";
	} elsif ($month eq "10") {
		$month = "Oct";
	} elsif ($month eq "11") {
		$month = "Nov";
	} elsif ($month eq "12") {
		$month = "Dec";
	}
	my $date = "Sunday, $month $Day, $Y";
	return $date;	
}

sub GetFormattedDate_short {
my $date = shift;

	my $date = &GetLongDate_parm($date);
	return $date;	
}

sub GetFormattedDate_evenshorter {
my $date = shift;

	my $date = &GetLongDate_parm_noyear($date);
	return $date;	
}

sub GetNEWFormattedDate {
my $date = shift;

	my $wkdy = &GetWeekday($date);
	my $long = &GetLongDate_parm($date);
	return $wkdy,$long;	
}

sub GetFormattedTime {
my $etime = shift;

	@NewTime=split(/:/,$etime);
	foreach $i (0..$#NewTime) {
		$NewTime[$i] =~ s/\+/ /g;
		}
	$Hour = $NewTime[0];
	$Min = $NewTime[1];
	
	if ($Hour eq "00") {
		$pdt = "9";
		$pdt_ampm = "pm";
		$mdt = "10";
		$mdt_ampm = "pm";
		$cdt = "11";
		$cdt_ampm = "pm";
		$edt = "12";
		$edt_ampm = "Midnight";
	} elsif ($Hour eq "01") {
		$pdt = "10";
		$pdt_ampm = "pm";
		$mdt = "11";
		$mdt_ampm = "pm";
		$cdt = "12";
		$cdt_ampm = "Midnight";
		$edt = "1";
		$edt_ampm = "am";
	} elsif ($Hour eq "02") {
		$pdt = "11";
		$pdt_ampm = "pm";
		$mdt = "12";
		$mdt_ampm = "Midnight";
		$cdt = "1";
		$cdt_ampm = "am";
		$edt = "2";
		$edt_ampm = "am";
	} elsif ($Hour eq "03") {
		$pdt = "12";
		$pdt_ampm = "Midnight";
		$mdt = "1";
		$mdt_ampm = "am";
		$cdt = "2";
		$cdt_ampm = "am";
		$edt = "3";
		$edt_ampm = "am";		
	} else {
		$pdt = $Hour-3;
		if ($pdt < 12) {
			$pdt_ampm = "am";
		} elsif ($pdt eq 12) {
			$pdt_ampm = "Noon";	
		} elsif ($pdt > 12) {
			$pdt -= 12;
			$pdt_ampm = "pm";
		}
		$mdt = $Hour-2;
		if ($mdt < 12) {
			$mdt_ampm = "am";
		} elsif ($mdt eq 12) {
			$mdt_ampm = "Noon";	
		} elsif ($mdt > 12) {
			$mdt -= 12;
			$mdt_ampm = "pm";
		}
		$cdt = $Hour-1;
		if ($cdt < 12) {
			$cdt_ampm = "am";
		} elsif ($cdt eq 12) {
			$cdt_ampm = "Noon";	
		} elsif ($cdt > 12) {
			$cdt -= 12;
			$cdt_ampm = "pm";
		}
		$edt = $Hour;
		if ($edt < 12) {
			$edt_ampm = "am";
		} elsif ($edt eq 12) {
			$edt_ampm = "Noon";	
		} elsif ($edt > 12) {
			$edt -= 12;
			$edt_ampm = "pm";
		}			
	}
	
	my $EDT = "$edt:$Min $edt_ampm";
	my $CDT = "$cdt:$Min $cdt_ampm";
	my $MDT = "$mdt:$Min $mdt_ampm";
	my $PDT = "$pdt:$Min $pdt_ampm";
	my $newtime = "$EDT";	
	return $newtime;
}

sub GetFormattedTime_short {
my $etime = shift;

	@NewTime=split(/:/,$etime);
	foreach $i (0..$#NewTime) {
		$NewTime[$i] =~ s/\+/ /g;
		}
	$Hour = $NewTime[0];
	$Min = $NewTime[1];
	
	if ($Hour eq "00") {
		$h = "12";
		$ampm = "Midnight";
	} elsif ($Hour eq "01") {
		$h = "1";
		$ampm = "am";
	} elsif ($Hour eq "02") {
		$h = "2";
		$ampm = "am";
	} elsif ($Hour eq "03") {
		$h = "3";
		$ampm = "am";		
	} else {
		if ($Hour < 12) {
			$h = $Hour;
			$ampm = "am";
		} elsif ($Hour eq 12) {
			$h = "12";
			$ampm = "Noon";	
		} elsif ($Hour > 12) {
			$Hour -= 12;
			$h = $Hour;
			$ampm = "pm";
		}			
	}
	my $newtime = "$h:00 $ampm";	
	return $newtime;
}

sub GetFormattedTime_mtn {
my $etime = shift;

	@NewTime=split(/:/,$etime);
	foreach $i (0..$#NewTime) {
		$NewTime[$i] =~ s/\+/ /g;
		}
	$Hour = $NewTime[0];
	$Min = $NewTime[1];
	
	if ($Hour < 12) {
		$ampm = "am";
	} elsif ($Hour eq 12) {
		$ampm = "Noon";	
	} elsif ($Hour > 12) {
		$Hour -= 12;
		$ampm = "pm";
	}			
	my $newtime = "$Hour:00 $ampm";	
	return $newtime;
}

sub GetTime_sel {
my $time = shift;

	if (!$time) {
		print "<option value='' selected>(select time)</option>";
	} 
#	if ($time eq "00:00:00") {
#		print "<option value='00:00:00' selected>12:00 Midnight</option>";
#	} else {
#		print "<option value='00:00:00'>12:00 Midnight</option>";
#	}
#	if ($time eq "00:30:00") {
#		print "<option value='00:30:00' selected>12:30 am</option>";
#	} else {
#		print "<option value='00:30:00'>12:30 am</option>";
#	}
#	if ($time eq "01:00:00") {
#		print "<option value='01:00:00' selected>1:00 am</option>";
#	} else {
#		print "<option value='01:00:00'>1:00 am</option>";
#
#
#		print "<option value='01:30:00' selected>1:30 am</option>";
#	} else {
#		print "<option value='01:30:00'>1:30 am</option>";
#	}
#	if ($time eq "02:00:00") {
#		print "<option value='02:00:00' selected>2:00 am</option>";
#	} else {
#		print "<option value='02:00:00'>2:00 am</option>";
#	}
#	if ($time eq "02:30:00") {
#		print "<option value='02:30:00' selected>2:30 am</option>";
#	} else {
#		print "<option value='02:30:00'>2:30 am</option>";
#	}
#	if ($time eq "03:00:00") {
#		print "<option value='03:00:00' selected>3:00 am</option>";
#	} else {
#		print "<option value='03:00:00'>3:00 am</option>";
#	}
#	if ($time eq "03:30:00") {
#		print "<option value='03:30:00' selected>3:30 am</option>";
#	} else {
#		print "<option value='03:30:00'>3:30 am</option>";
#	}
#	if ($time eq "04:00:00") {
#		print "<option value='04:00:00' selected>4:00 am</option>";
#	} else {
#		print "<option value='04:00:00'>4:00 am</option>";
#	}
#	if ($time eq "04:30:00") {
#		print "<option value='04:30:00' selected>4:30 am</option>";
#	} else {
#		print "<option value='04:30:00'>4:30 am</option>";
#	}
#	if ($time eq "05:00:00") {
#		print "<option value='05:00:00' selected>5:00 am</option>";
#	} else {
#		print "<option value='05:00:00'>5:00 am</option>";
#	}
#	if ($time eq "05:30:00") {
#		print "<option value='05:30:00' selected>5:30 am</option>";
#	} else {
#		print "<option value='05:30:00'>5:30 am</option>";
#	}
	if ($time eq "06:00:00") {
		print "<option value='06:00:00' selected>6:00 am</option>";
	} else {
		print "<option value='06:00:00'>6:00 am</option>";
	}
	if ($time eq "06:30:00") {
		print "<option value='06:30:00' selected>6:30 am</option>";
	} else {
		print "<option value='06:30:00'>6:30 am</option>";
	}
	if ($time eq "07:00:00") {
		print "<option value='07:00:00' selected>7:00 am</option>";
	} else {
		print "<option value='07:00:00'>7:00 am</option>";
	}
	if ($time eq "07:30:00") {
		print "<option value='07:30:00' selected>7:30 am</option>";
	} else {
		print "<option value='07:30:00'>7:30 am</option>";
	}
	if ($time eq "08:00:00") {
		print "<option value='08:00:00' selected>8:00 am</option>";
	} else {
		print "<option value='08:00:00'>8:00 am</option>";
	}
	if ($time eq "08:30:00") {
		print "<option value='08:30:00' selected>8:30 am</option>";
	} else {
		print "<option value='08:30:00'>8:30 am</option>";
	}
	if ($time eq "09:00:00") {
		print "<option value='09:00:00' selected>9:00 am</option>";
	} else {
		print "<option value='09:00:00'>9:00 am</option>";
	}
	if ($time eq "09:30:00") {
		print "<option value='09:30:00' selected>9:30 am</option>";
	} else {
		print "<option value='09:30:00'>9:30 am</option>";
	}
	if ($time eq "10:00:00") {
		print "<option value='10:00:00' selected>10:00 am</option>";
	} else {
		print "<option value='10:00:00'>10:00 am</option>";
	}
	if ($time eq "10:30:00") {
		print "<option value='10:30:00' selected>10:30 am</option>";
	} else {
		print "<option value='10:30:00'>10:30 am</option>";
	}
	if ($time eq "11:00:00") {
		print "<option value='11:00:00' selected>11:00 am</option>";
	} else {
		print "<option value='11:00:00'>11:00 am</option>";
	}
	if ($time eq "11:30:00") {
		print "<option value='11:30:00' selected>11:30 am</option>";
	} else {
		print "<option value='11:30:00'>11:30 am</option>";
	}
	if ($time eq "12:00:00") {
		print "<option value='12:00:00' selected>12:00 noon</option>";
	} else {
		print "<option value='12:00:00'>12:00 noon</option>";
	}
	if ($time eq "12:30:00") {
		print "<option value='12:30:00' selected>12:30 pm</option>";
	} else {
		print "<option value='12:30:00'>12:30 pm</option>";
	}
	if ($time eq "13:00:00") {
		print "<option value='13:00:00' selected>1:00 pm</option>";
	} else {
		print "<option value='13:00:00'>1:00 pm</option>";
	}
	if ($time eq "13:30:00") {
		print "<option value='13:30:00' selected>1:30 pm</option>";
	} else {
		print "<option value='13:30:00'>1:30 pm</option>";
	}
	if ($time eq "14:00:00") {
		print "<option value='14:00:00' selected>2:00 pm</option>";
	} else {
		print "<option value='14:00:00'>2:00 pm</option>";
	}
	if ($time eq "14:30:00") {
		print "<option value='14:30:00' selected>2:30 pm</option>";
	} else {
		print "<option value='14:30:00'>2:30 pm</option>";
	}
	if ($time eq "15:00:00") {
		print "<option value='15:00:00' selected>3:00 pm</option>";
	} else {
		print "<option value='15:00:00'>3:00 pm</option>";
	}
	if ($time eq "15:30:00") {
		print "<option value='15:30:00' selected>3:30 pm</option>";
	} else {
		print "<option value='15:30:00'>3:30 pm</option>";
	}
	if ($time eq "16:00:00") {
		print "<option value='16:00:00' selected>4:00 pm</option>";
	} else {
		print "<option value='16:00:00'>4:00 pm</option>";
	}
	if ($time eq "16:30:00") {
		print "<option value='16:30:00' selected>4:30 pm</option>";
	} else {
		print "<option value='16:30:00'>4:30 pm</option>";
	}
	if ($time eq "17:00:00") {
		print "<option value='17:00:00' selected>5:00 pm</option>";
	} else {
		print "<option value='17:00:00'>5:00 pm</option>";
	}
	if ($time eq "17:30:00") {
		print "<option value='17:30:00' selected>5:30 pm</option>";
	} else {
		print "<option value='17:30:00'>5:30 pm</option>";
	}
	if ($time eq "18:00:00") {
		print "<option value='18:00:00' selected>6:00 pm</option>";
	} else {
		print "<option value='18:00:00'>6:00 pm</option>";
	}
	if ($time eq "18:30:00") {
		print "<option value='18:30:00' selected>6:30 pm</option>";
	} else {
		print "<option value='18:30:00'>6:30 pm</option>";
	}
	if ($time eq "19:00:00") {
		print "<option value='19:00:00' selected>7:00 pm</option>";
	} else {
		print "<option value='19:00:00'>7:00 pm</option>";
	}
	if ($time eq "19:30:00") {
	print "<option value='19:30:00' selected>7:30 pm</option>";
	} else {
		print "<option value='19:30:00'>7:30 pm</option>";
	}
	if ($time eq "20:00:00") {
		print "<option value='20:00:00' selected>8:00 pm</option>";
	} else {
		print "<option value='20:00:00'>8:00 pm</option>";
	}
	if ($time eq "20:30:00") {
	print "<option value='20:30:00' selected>8:30 pm</option>";
	} else {
		print "<option value='20:30:00'>8:30 pm</option>";
	}
	if ($time eq "21:00:00") {
		print "<option value='21:00:00' selected>9:00 pm</option>";
	} else {
		print "<option value='21:00:00'>9:00 pm</option>";
	}
	if ($time eq "21:30:00") {
	print "<option value='21:30:00' selected>9:30 pm</option>";
	} else {
		print "<option value='21:30:00'>9:30 pm</option>";
	}
	if ($time eq "22:00:00") {
		print "<option value='22:00:00' selected>10:00 pm</option>";
	} else {
		print "<option value='22:00:00'>10:00 pm</option>";
	}
	if ($time eq "22:30:00") {
	print "<option value='22:30:00' selected>10:30 pm</option>";
	} else {
		print "<option value='22:30:00'>10:30 pm</option>";
	}
	if ($time eq "23:00:00") {
		print "<option value='23:00:00' selected>11:00 pm</option>";
	} else {
		print "<option value='23:00:00'>11:00 pm</option>";
	}
	if ($time eq "23:30:00") {
	print "<option value='23:30:00' selected>11:30 pm</option>";
	} else {
		print "<option value='23:30:00'>11:30 pm</option>";
	}
}

sub WhatQuarter {
my $mydate = shift;

#	print "Content-type:text/html\n\n";
#	print "<p><h3><font color='#FFFFFF' size='4'><b>[ $mydate ]</b></font></h3></p>";
	my $qtr = undef;
	my @Date = split(/-/,$mydate);
	foreach my $i (0..$#Date) {
		$Date[$i] =~ s/\+/ /g;
	}
	my $year = $Date[0];
	my $month = sprintf "%0.2u",$Date[1];
	my $day = $Date[2];
#	print "<p><h3><font color='#FFFFFF' size='4'><b>[ Year: $year | Month: $month | Day: $day ]</b></font></h3></p>";
	if (($month ge "01") && ($month le "03")) {
		$qtr = 1;
	} elsif (($month ge "04") && ($month le "06")) {
		$qtr = 2;
	} elsif (($month ge "07") && ($month le "09")) {
		$qtr = 3;
	} elsif ($month ge "10") {
		$qtr = 4;
	} else {
		&ErrorMsg("Invalid Month: $month");
		exit;
	}
return $qtr;
}

sub IsThisQuarter {
my ($q,$d) = @_;

	my $flag = undef;
	my $tqtr = &WhatQuarter($d);
	if ($tqtr eq $q) {
		$flag++;
	} else {
		$flag = undef;
	}
return $flag;
}

1;