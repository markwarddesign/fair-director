var $modal = $("#ajax-modal");

$(".popup").on("click", function(){
      $modal.modal();
});

$("button.dismiss").on("click", function(){
      $("#responsive").get(0).reset();
	  $(".the-alert").html('');
});

function processForm(){
	
  	$("#responsive").modal('loading');
  
  	setTimeout(function(){
	  	$(".modal .loading-mask").hide();
	  	$(".the-alert").show();
  	}, 1000);

        $.ajax({
            type: "POST",
            url: "/cgi-bin/saveEntertain.cgi",
            data: {
				id: $('input[name="id"]').val(),
				fid: $('input[name="fid"]').val(),
				type: $('select[name="type"]').val(),
				group: $('input[name="group"]').val(),
				contact: $('input[name="contact"]').val(),
				addr: $('input[name="addr"]').val(),
				addr2: $('input[name="addr2"]').val(),
				city: $('input[name="city"]').val(),
				state: $('input[name="state"]').val(),
				zip: $('input[name="zip"]').val(),
				email: $('input[name="email"]').val(),
				phone: $('input[name="phone"]').val(),
				cell: $('input[name="cell"]').val(),
				status: $('input[name="status"]').val(),
				fee: $('input[name="fee"]').val(),
				notes: $('input[name="notes"]').val(),
				
			},
				
            error: function(result) {

				var msg = '<div class="alert alert-success fade in">' +result+'<button type="button" class="close" data-dismiss="alert">&times;</button>' +'</div>';
               $("#responsive").find('.the-alert').html(msg);
            },
            success: function(result) {
					var status = '';
					
					var res = result.split("|");
					if(res[0] == 0){
					status = 'danger';	
					}else{
					status = 'success';	
					}
					var msg = '<div class="alert alert-'+status+' fade in">'+res[1]+'<button type="button" class="close" data-dismiss="alert">&times;</button>' +'</div>';
					$("#responsive").find('.the-alert').html(msg);
					$(".the-alert .close").on("click",function(){
						$("#responsive").find('.the-alert').hide();		
					});
            }
			
			
        });

  
};


$(document).ready(function() {
 
    $('form[name="AE"]').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
	/*	addOns: {
            reCaptcha2: {
                element: 'captchaContainer',
                theme: 'light',
                siteKey: '6LdYWAITAAAAAFd7UPA1e3pHTs4uxfb13qLLPGOt',
                timeout: 120,
                message: 'The captcha is not valid'
            }
        }, */
        fields: {
         group: {
                validators: {
                    notEmpty: {
                        message: 'The group name is required'
                    },
                    stringLength: {
                        min: 3,
                        max: 30,
                        message: 'The name must be more than 3 and less than 30 characters long'
                    }
                }
            },
            type: {
                validators: {
                    notEmpty: {
                        message: 'The type is required'
                    },
                    numeric: {
                        message: 'The type must be a number'
                    }
                }
            },
            'status[]': {
                validators: {
                    notEmpty: {
                        message: 'The status is required'
                    }
                }
            },
            contact: {
                validators: {
                    notEmpty: {
                        message: 'The contact name is required'
                    }
                }
            },
			addr: {
                validators: {
                    notEmpty: {
                        message: 'The address is required'
                    }
                }
            },
			city: {
                validators: {
                    notEmpty: {
                        message: 'The city is required'
                    }
                }
            },
			state: {
                validators: {
                    notEmpty: {
                        message: 'The state is required'
                    }
                }
            },
			zip: {
                validators: {
                    regexp: {
                        regexp: /^\d{5}$/,
                        message: 'The US zipcode must contain 5 digits'
                    }
                }
            },
			phone:{
				digits: {
                 message: 'The phone number can contain digits only'
				},
				notEmpty: {
					message: 'The phone number is required'
				}	
			},
		 cell:{
				digits: {
                 message: 'The phone number can contain digits only'
				},
				notEmpty: {
					message: 'The phone number is required'
				}	
			}
        },
		onError: function(e) {
                var msg = '<div class="alert alert-danger fade in">Danger Will Robinson! Danger! <button type="button" class="close" data-dismiss="alert">&times;</button>' +
        '</div>';
                $("#responsive").find('.the-alert').html(msg);
            },
       onSuccess: function(e) {
				processForm();
            }
    });
});